import os,os.path
import shutil
import sys
import tarfile
import subprocess
import re
try:
  # Python 2.7
  import sysconfig
except:
  # older Python versions
  from distutils import sysconfig
  
def get_libdir():
  machine=os.uname()[4]
  if machine.find('x86_64') !=-1:
    return 'lib64'
  else: 
    return 'lib'
    
def get_arch():
  machine=os.uname()[4]
  if machine.find('x86_64') !=-1:
    return '64bit'
  else: 
    return '32bit'

def get_python_version():
    return '%d.%d' % tuple(sys.version_info)[0:2]

def copy_stage(stagedir,standalonedir):
  libdir=get_libdir()
  copytree_ex(os.path.join(stagedir,'bin'),os.path.join(standalonedir,'bin'))
  copytree_ex(os.path.join(stagedir,libdir),os.path.join(standalonedir,libdir))
  if os.path.exists(os.path.join(stagedir,'share')):
    copytree_ex(os.path.join(stagedir,'share'),os.path.join(standalonedir,'share'))

def collect_deps(binaries,exclude_list):
  dependencies=set()
  dep_tree={}
  for binary in binaries:
    dep_tree[binary]=[]
    collect_deps_recursive_(binary,dependencies,exclude_list,dep_tree)
  return (dependencies,dep_tree)

def clean(standalonedir):
  if os.path.exists(standalonedir):
    shutil.rmtree(standalonedir)
  
def collect_deps_recursive_(binary,collected,exclude_list,dep_tree):
  ldd_stdout=subprocess.Popen(['ldd',binary],stdout=subprocess.PIPE).stdout
  output=ldd_stdout.readlines()
  new_dependencies=set()
  for entry in output:
    if entry.find("statically linked")!=-1:
      continue
    sp=entry.split('(0x')
    if len(sp)>1: # only continue if line is dependency
     sp2=sp[0].split('=>',1)[-1].split()
     if sp2: #ensure libname exists (fix for virtual linux-gate)
       text=sp2[0]
       if not text in collected:
         if match_list(os.path.basename(text).split('.')[0],exclude_list):
           continue
         collected.add(text) 
         dep_tree[text]=[]
         dep_tree[binary].append(text)
         collect_deps_recursive_(text,collected,exclude_list,dep_tree)
    elif entry.find('not found')!=-1:
      raise Exception(entry)
def match_list(text,l):
  for entry in l:
    if re.search("^"+entry+"$",text):
     return True
  return False  

def write_tree(fp,tree,key,prefix):
  for child in tree[key]:
    fp.write(prefix+"|---"+child+'\n')
    if child == tree[key][-1]:
      write_tree(fp,tree,child,prefix+"     ")
    else:
      write_tree(fp,tree,child,prefix+"|    ")

def copy_deps(binaries,standalonedir,exclude_list):
  dependencies=set()
  walk_list=os.walk(standalonedir)
  binaries_list=[]
  for dir_entry in walk_list:
    for file_entry in dir_entry[2]:
      if file_entry.endswith('.so') or file_entry in binaries:
        filepath=os.path.join(dir_entry[0],file_entry)
        binaries_list.append(filepath)  
  (dependencies,dep_tree)=collect_deps(binaries_list,exclude_list)
  for dep in dependencies:
    shutil.copy(dep,os.path.join(standalonedir,get_libdir()))
  fp=open('dependency_tree.txt','w')
  for binary in binaries_list:
    fp.write(binary+'\n')
    write_tree(fp,dep_tree,binary,"")
  fp.close()

def copy_examples(stagedir):
  #todo
  pass

def process_scripts(scripts,standalonedir,root_var):
  for script in scripts:
    fp=open(os.path.join(standalonedir,'bin',script),'r')
    text=fp.read()
    fp.close()
    libdir=root_var+'/'+get_libdir()
    text=re.sub('#?.*export PYTHONPATH=.*\n','export PYTHONPATH='+libdir+":"+libdir+'/python%s:' % (get_python_version())+libdir+'/iplt:'+libdir+"/openstructure\n",text)
    text=re.sub('#?.*export QT_PLUGIN_PATH=','export QT_PLUGIN_PATH=',text)
    fp=open(os.path.join(standalonedir,'bin',script),'w')
    fp.write(text)
    fp.close()

def create_package(name,standalonedir):
  try:
    import pysvn
    try:
      client=pysvn.Client()
      svn_info=client.info('.')
      revstring='-rev'+str(svn_info['revision'].number)
    except pysvn.ClientError:
      print 'No .svn directory found. Adding no revision number.'
      revstring=''
  except ImportError:
    print 'Pysvn not installed. Adding no revision number.'
    revstring=''
  tar_name=name+'-linux-'+get_arch()+revstring+'.tgz'
  if os.path.exists(tar_name):
    os.remove(tar_name)
  tar = tarfile.open(tar_name, "w:gz")
  tar.add(standalonedir,name+'-linux-'+get_arch()+revstring)
  tar.close()

def ignorer(ignore_list,site_package_include_list):
  def ignore_func(dirpath,files):
    ignores=[]
    for f in files:
      if dirpath.endswith('site-packages'):
        if not match_list(f,site_package_include_list):
          ignores.append(f)
      base=os.path.splitext(f)[0]
      if match_list(base,ignore_list):
        ignores.append(f)
#      elif f.endswith('.py'):
#        if base+'.pyc' in files or base+'.pyo' in files:
#          ignores.append(f)
#      elif f.endswith('.pyc'):
#        if base+'.pyo' in files:
#          ignores.append(f)
      elif f.endswith('.pyc') or f.endswith('.pyo'):
        if base+'.py' in files:
          ignores.append(f)
    return ignores
  return ignore_func

def copy_python_dirs(standalonedir,python_exclude_list,site_package_include_list):
  dirs=sys.path[1:]
  dirs.sort() #get toplevel dir to the beginning
  toplevel=dirs[0]
  shutil.copytree(toplevel,os.path.join(standalonedir,get_libdir(),'python%s' % (get_python_version())),ignore=ignorer(python_exclude_list,site_package_include_list))
  for d in dirs[1:]:
    if not d.startswith(toplevel) and os.path.isdir(d):
      toplevel=d
      copytree_ex(toplevel,os.path.join(standalonedir,get_libdir(),'python%s' % (get_python_version())),ignore=ignorer(python_exclude_list,site_package_include_list))
  includepath=os.path.join(standalonedir,'include','python%s' % (get_python_version()))
  os.mkdir(os.path.join(standalonedir,'include'))
  os.mkdir(includepath)
  shutil.copy(sysconfig.get_config_h_filename(),os.path.join(includepath,'pyconfig.h'))


def copytree_ex(source,destination, symlinks=False,ignore=None):
  if not os.path.exists(destination):
   shutil.copytree(source,destination,symlinks,ignore)
  elif os.path.isdir(destination):
    files=os.listdir(source)
    if ignore:
      ignores=ignore(source,files)
    else:
      ignores=[]
    for f in files:
      srcname=os.path.join(source,f)
      dstname=os.path.join(destination,f)
      if os.path.isdir(os.path.join(source,f)):
        copytree_ex(srcname,dstname,symlinks,ignore=ignore)
      elif f not in ignores:
        if symlinks and os.path.islink(srcname):
          linkto = os.readlink(srcname)
          os.symlink(linkto, dstname)
        else:
          shutil.copy2(srcname, dstname)
  else:
    raise IOError("destination is not a directory")
       
############   main script ####################

stagedir='../../stage'
ost_stagedir='../../../openstructure/stage'
standalonedir=os.path.join(os.getcwd(),'standalone')
qt_plugin_dir='/usr/lib/qt4/plugins'
binaries=['gosty','iplt_bin']
exclude_list=['libpthread','libgthread','librt','libm','libdl','libGLU','libc','ld-linux','libexpat','libgcc_s','libglib','libice','libSM','libX.*','libg','libGL','libfontconfig','libfreetype','libdrm','libxcb','libICE']
python_exclude_list=['gtk.*','wx.*','libsvn','rpm','planner.*','cups','gtksourceview','scanext']
site_package_include_list=['numpy','Numeric','PyQt4','matplotlib','dateutil','sip.*']
ost_scripts=['dng','ost']
scripts=['iplt','giplt','giplt_diff_manager','iplt_diff','iplt_diff_merge']

#compile_stage()

clean(standalonedir)
print 'copying stage'
copy_stage(stagedir,standalonedir)
print 'copying ost stage'
copy_stage(ost_stagedir,standalonedir)
print 'copying python'
shutil.copy(sys.executable,os.path.join(stagedir,'bin'))
copy_python_dirs(standalonedir,python_exclude_list,site_package_include_list)
print "copying qt plugins"
copytree_ex(qt_plugin_dir,os.path.join(standalonedir,'bin','plugins'))
print 'copying dependencies'
binaries.append(sys.executable)
copy_deps(binaries,standalonedir,exclude_list)
print 'copying examples'
copy_examples(stagedir)
print 'processing ost scripts'
process_scripts(ost_scripts,standalonedir,'$DNG_ROOT')
print 'processing iplt scripts'
process_scripts(scripts,standalonedir,'$IPLT_ROOT')
print 'creating package'
create_package('iplt','standalone')
