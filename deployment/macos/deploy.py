import bundle
import deps
import sys
bundle.create_bundle('DNG','dng','../../../openstructure_fot_iplt/graphics')
bundle.create_bundle('IPLT','giplt')
bundle.create_bundle('IPLT Diffraction Manager','giplt_diff_manager')
bundle.create_bundle('IPLT Correlation Averaging','giplt_corr_avg')
deps.make_standalone('../../../openstructure_for_iplt/stage','../../stage', 'standalone', True, 
                     '--no-rpath' in sys.argv, 
                     macports_workaround='--macports-workaround' in sys.argv)
