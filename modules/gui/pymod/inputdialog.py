#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Andreas Schenk
#
import sys
import atexit

try:
    from PyQt4.QtCore import *
    from PyQt4.QtGui import *
except ImportError:
    print "----------------------------------------------------------------------"
    print "PyQt4 is not installed. Please install PyQt4 to use this script."
    print "----------------------------------------------------------------------"
    exit()
    
class InputDialog(QDialog):
    def __init__(self,title=''):
        QDialog.__init__(self)
        self.setWindowTitle(title)
        vb = QVBoxLayout()
        self.grid_ = QGridLayout()
        vb.addLayout(self.grid_)
        self.controls_={}
        buttonbox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        QObject.connect(buttonbox,SIGNAL("accepted()"),self.accept)
        QObject.connect(buttonbox,SIGNAL("rejected()"),self.reject)
        vb.addWidget(buttonbox)
        self.setLayout(vb)

    def AddFloat(self,name,minimum=0.0,maximum=100.0,decimals=4,startv=0.0):
        idx=self.grid_.rowCount()
        self.grid_.addWidget(QLabel(name),idx,0)
        widget=QDoubleSpinBox()
        widget.setRange(minimum,maximum)
        widget.setValue(startv)
        widget.setDecimals(decimals)
        self.grid_.addWidget(widget,idx,1)
        self.controls_[idx]=(widget,"float")
        
    def AddInt(self,name,minimum=0,maximum=100,startv=0):
        idx=self.grid_.rowCount()
        self.grid_.addWidget(QLabel(name),idx,0)
        widget=QSpinBox()
        widget.setRange(minimum,maximum)
        widget.setValue(startv)
        self.grid_.addWidget(widget,idx,1)
        self.controls_[idx]=(widget,"int")
        
    def AddList(self,name,stringlist,selected=0):
        idx=self.grid_.rowCount()
        self.grid_.addWidget(QLabel(name),idx,0)
        widget=QComboBox()
        widget.addItems(stringlist)
        widget.setCurrentIndex(selected)
        self.grid_.addWidget(widget,idx,1)
        self.controls_[idx]=(widget,"list")
        
    def AddCheckBox(self,name,flag=False):
        idx=self.grid_.rowCount()
        self.grid_.addWidget(QLabel(name),idx,0)
        widget=QCheckBox()
        if flag:
          widget.setCheckState(Qt.Checked)
        else:
          widget.setCheckState(Qt.Unchecked)
        self.grid_.addWidget(widget,idx,1)
        self.controls_[idx]=(widget,"bool")

    def AddString(self,name,startv=""):
        idx=self.grid_.rowCount()
        self.grid_.addWidget(QLabel(name),idx,0)
        widget=QLineEdit()
        widget.setText(startv)
        self.grid_.addWidget(widget,idx,1)
        self.controls_[idx]=(widget,"string")
        
    def AddPath(self,name,dtype="open",startv=""):
        idx=self.grid_.rowCount()
        self.grid_.addWidget(QLabel(name),idx,0)
        hb=QHBoxLayout()
        widget=QLineEdit()
        widget.setText(startv)
        hb.addWidget(widget)
        self.controls_[idx]=(widget,"path")
        button=QPushButton("Browse...")
        hb.addWidget(button)
        
        if dtype=="save":
            #using lambda to circumvent broken QSignalMapper
            receiver = lambda who=idx: self.open_dialog_(who)
        else:
            #using lambda to circumvent broken QSignalMapper
            receiver = lambda who=idx: self.open_dialog_(who)
        QObject.connect(button,SIGNAL("clicked()"),receiver)

        self.grid_.addLayout(hb,idx,1)
        
    def save_dialog_(self, num):
      widget=self.controls_[num][0]
      filename = QFileDialog.getSaveFileName(self,"Save File",widget.text())
      if filename!="":
        widget.setText(filename)     

        
    def open_dialog_(self, num):    
        widget=self.controls_[num][0]
        filename = QFileDialog.getOpenFileName(self,"Open File",widget.text())
        if filename!="":
           widget.setText(filename)     

    def GetData(self):
        result=[]
        for key in self.controls_:
            result.append(self.GetItemData(self.controls_[key]))
        return result
        
    
    def GetItemData(self,control):
        if control[1]=="float":
            return control[0].value()
        elif control[1]=="int":
            return control[0].value()
        elif control[1]=="string" or control[1]=="path":
            return str(control[0].text())
        elif control[1]=="bool":
            return control[0].checkState()==Qt.Checked
        elif control[1]=="list":
            return str(control[0].currentText())
        else:
            return ''

