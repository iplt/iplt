#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

# 
# Authors: Ansgar Philippsen, Andreas Schenk
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from iplt import *
from ost.img import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose
import iplt.alg

class RlistGrid(QTableWidget):
    def __init__(self,parent,rlist):
        LogVerbose("creating RlistGrid")
        QTableWidget.__init__(self,parent)
        self.setWindowTitle("HK Grid")

        stat=iplt.alg.RListIndexStat()
        rlist.Apply(stat)
        index_list=stat.GetIndexList()
        minh=stat.GetMinimumH()
        maxh=stat.GetMaximumH()
        mink=stat.GetMinimumK()
        maxk=stat.GetMaximumK()
        self.setDragDropMode(QAbstractItemView.NoDragDrop)
        self.setDragEnabled(False)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)

        self.setRowCount(maxk-mink+1)
        qsl=QStringList()
        for r in range(mink,maxk+1):
            qsl.append(str("%+03d"%r))
        self.setVerticalHeaderLabels(qsl)
        qsl.clear()
        self.setColumnCount(maxh-minh+1)
        for c in range(minh,maxh+1):
            qsl.append(str("%+03d"%c))
        self.setHorizontalHeaderLabels(qsl)
        
        for i in index_list:
            qti = QTableWidgetItem("X")
            qti.setTextAlignment(Qt.AlignCenter)
            self.setItem(i[1]-mink,i[0]-minh,qti)
            
        self.resizeRowsToContents()
        self.resizeColumnsToContents()

        self.minh_=minh
        self.mink_=mink

        QObject.connect(self, SIGNAL("itemSelectionChanged()"), self.on_selection_changed)
        
        self.selection_=[]
        self.setCurrentCell(0,0,QItemSelectionModel.SelectCurrent)

    def SetMultipleSelection(self,flag):
      if flag:
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
      else:
        self.setSelectionMode(QAbstractItemView.SingleSelection)

    def on_selection_changed(self):
      LogVerbose("on selection changed")
      selected=[]
      for item in self.selectedItems():
        selected.append(Point(self.column(item)+self.minh_,self.row(item)+self.mink_))
      if selected!=self.selection_:
        LogVerbose("new selection")
        self.selection_=selected
        self.emit(SIGNAL("SelectionChanged"), selected)        
      else:
        LogVerbose("no new selection")

    def GetSelection(self):
        return self.selection_
