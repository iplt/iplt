//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
#include <boost/python/implicit.hpp>
#include <iplt/gui/lattice_overlay.hh>
#include <iplt/gui/lp_overlay.hh>
#include <iplt/gui/uc_overlay.hh>
#include <iplt/gui/rlist_overlay.hh>
#include <iplt/gui/ctf_overlay.hh>
#include <iplt/gui/spotlist_overlay.hh>
#include <iplt/gui/indexed_list.hh>
/*
#include <iplt/gui/sinc_sum_plot_function.hh>
*/

using namespace boost::python;
using namespace iplt;
using namespace ost::img::gui;
using namespace iplt::gui;

void export_overlays()
{
  class_<LatticeOverlayBase,bases<PointlistOverlayBase>,boost::noncopyable>("LatticeOverlayBase",init<String,Lattice>())
    .def("SetLattice",&LatticeOverlayBase::SetLattice)
    .def("GetLattice",&LatticeOverlayBase::GetLattice, return_value_policy<copy_const_reference>())
    .def("SetReferenceCell",&LatticeOverlayBase::SetReferenceCell)
    .def("GetReferenceCell",&LatticeOverlayBase::GetReferenceCell)
	;

  class_<LatticeOverlay,bases<LatticeOverlayBase>,boost::noncopyable>("LatticeOverlay",init<const Lattice&,optional<const String&> >())
    .def(init<optional<const String&> >())
    .def("SetMinAngle",&LatticeOverlay::SetMinAngle)
    .def("SetLattice",&LatticeOverlay::SetLattice)
    ;

  class_<LPOverlay,bases<Overlay>,boost::noncopyable>("LPOverlay",init<>())
    ;

  class_<UnitCellOverlay,bases<Overlay>,boost::noncopyable>("UnitCellOverlay",init<const SpatialUnitCell&>())
    .def("SetPhaseShift",&UnitCellOverlay::SetPhaseShift)
    .def("GetPhaseShift",&UnitCellOverlay::GetPhaseShift)
    ;

  class_<RListOverlay,bases<LatticeOverlayBase>,boost::noncopyable>("RListOverlay",init<const ReflectionList&, optional<const String&> >())
    .def("AddResolutionRing",&RListOverlay::AddResolutionRing)
    .def("RemoveResolutionRing",&RListOverlay::RemoveResolutionRing)
    .def("DisplayResolutionRings",&RListOverlay::DisplayResolutionRings)
    .def("GetReflectionList",&RListOverlay::GetReflectionList)
    .def("HighlightSymmetryRelatedPoints",&RListOverlay::HighlightSymmetryRelatedPoints)
    ;

  class_<CTFOverlay,bases<Overlay>,boost::noncopyable>("CTFOverlay",init<optional<TCIFData> >())
    .def("SetTCIFData",&CTFOverlay::SetTCIFData)
    .def("GetTCIFData",&CTFOverlay::GetTCIFData)
    ;

  class_<IndexedList >("IndexedList",init<Lattice>())
    .def("AddIndex",&IndexedList::AddIndex)
    .def("DelIndex",&IndexedList::DelIndex)
    .def("EmptyList",&IndexedList::EmptyList)
    .def("SetLattice",&IndexedList::SetLattice)
    .def("GetLattice",&IndexedList::GetLattice)
    .def("Append",&IndexedList::Append)
    ;
	
  class_<SpotlistOverlay,bases<LatticeOverlayBase>,boost::noncopyable>("SpotlistOverlay",init<Lattice>())
    .def(init<IndexedList>())
    .def("SetIndexedList",&SpotlistOverlay::SetIndexedList)
    .def("GetIndexedList",&SpotlistOverlay::GetIndexedList)
    ;
		
  def("ImportSpotlist",ImportSpotlist);
  def("ExportSpotlist",ExportSpotlist);  

  /*
  class_<SincSumAmplitudePlotFunction,boost::shared_ptr<SincSumAmplitudePlotFunction> >("SincSumAmplitudePlotFunction",init<double >())
    .def(init<const ImageHandle&>())
    .def("AddSinc",&SincSumAmplitudePlotFunction::AddSinc)
    ;
  class_<SincSumPhasePlotFunction,boost::shared_ptr<SincSumPhasePlotFunction> >("SincSumPhasePlotFunction",init<double >())
    .def(init<const ImageHandle&>())
    .def("AddSinc",&SincSumPhasePlotFunction::AddSinc)
    ;
    
  implicitly_convertible<boost::shared_ptr<SincSumAmplitudePlotFunction>,boost::shared_ptr<PlotFunctionBase> >();
  implicitly_convertible<boost::shared_ptr<SincSumPhasePlotFunction>,boost::shared_ptr<PlotFunctionBase> >();
  */
}
