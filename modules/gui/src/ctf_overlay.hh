//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#ifndef IPLT_EX_GUI_CTF_OVERLAY_HH
#define IPLT_EX_GUI_CTF_OVERLAY_HH

#include <ost/base.hh>
#include <ost/img/point.hh>
#include <ost/geom/vec2.hh>
#include <ost/gui/data_viewer/overlay_base.hh>
#include <iplt/gui/module_config.hh>
#include <iplt/tcif_data.hh>
#include <iplt/ctf_func_base.hh>

namespace iplt  {  namespace gui {


class DLLEXPORT_IPLT_GUI CTFOverlay: public ost::img::gui::Overlay
{
public:
  CTFOverlay(const TCIFData& data=TCIFData());
  
  TCIFData GetTCIFData() const;
  void SetTCIFData(const TCIFData& data);

  virtual void OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active);
  virtual void OnMenuEvent(QAction* e);
  virtual bool OnMouseEvent(QMouseEvent* e, ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse);
  virtual bool OnKeyEvent(QKeyEvent* e, ost::img::gui::DataViewerPanel* dvp);
  virtual QMenu* GetMenu();

protected:
  void update_rings_(geom::Vec2 half_size);
  QPainterPath draw_path_(QVector<QPointF>& pos_nodes,QVector<QPointF>& neg_nodes,bool close);
  CTFFuncBase ctf_;
  QColor color_;
  QMenu* menu_;
  unsigned int symbolsize_;
  QAction* a_lock_astigmatism_;
  QAction* a_change_defocus_sign_;
  QAction* a_change_defocus_x_sign_;
  QAction* a_change_defocus_y_sign_;
  QAction* a_set_color_;
  unsigned int maxrings_;
  QList <QPainterPath> paths_;
  bool redraw_rings_;
};

}} // ns


#endif
