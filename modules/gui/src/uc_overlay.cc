//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#include <ost/gui/data_viewer/data_viewer_panel.hh>

#include "uc_overlay.hh"

namespace iplt  {  namespace gui {

typedef std::vector<geom::Vec3> Vec3List;

UnitCellOverlay::UnitCellOverlay(const SpatialUnitCell& uc):
  Overlay("Unitcell"),
  uc_(uc),
  color_(QColor(QColor(255,0,0))),
  phase_shift_(geom::Vec2(0,0)),
  draw_list_()
{
  menu_=new QMenu;
  menu_->addAction("Color");
  menu_->addAction("Clear");
}

void UnitCellOverlay::OnMenuEvent(QAction* e)
{
}

void UnitCellOverlay::OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active)
{
  double zoomscale=dvp->GetZoomScale();
  geom::Vec3 sampling=dvp->GetPixelSampling(); 
  QColor color1;
  if(is_active){
    color1= color_;
  } else {
    color1= QColor(100,100,100);
  }
  pnt.setPen(QPen(color1,1));
  pnt.setBrush(Qt::NoBrush);
  double axh=uc_.GetVecA()[0]/sampling[0]*zoomscale/2.0;
  double ayh=uc_.GetVecA()[1]/sampling[0]*zoomscale/2.0;
  double bxh=uc_.GetVecB()[0]/sampling[0]*zoomscale/2.0;
  double byh=uc_.GetVecB()[1]/sampling[0]*zoomscale/2.0;
  QPoint center=dvp->PointToWin(dvp->GetExtent().GetCenter());
  double cenx = static_cast<double>(center.x());
  double ceny = static_cast<double>(center.y());
  cenx+=phase_shift_[0]/(2*M_PI)*2.0*axh+phase_shift_[1]/(2*M_PI)*2.0*bxh;
  ceny+=phase_shift_[0]/(2*M_PI)*2.0*ayh+phase_shift_[1]/(2*M_PI)*2.0*byh;
  QPolygon qpoly;
  qpoly << QPoint(static_cast<int>(cenx-axh-bxh),static_cast<int>(ceny-ayh-byh));
  qpoly << QPoint(static_cast<int>(cenx-axh+bxh),static_cast<int>(ceny-ayh+byh));
  qpoly << QPoint(static_cast<int>(cenx+axh+bxh),static_cast<int>(ceny+ayh+byh));
  qpoly << QPoint(static_cast<int>(cenx+axh-bxh),static_cast<int>(ceny+ayh-byh));
  pnt.drawPolygon(qpoly);

  double crosssize=3.0;
  pnt.drawLine(static_cast<int>(cenx-0.5*crosssize*zoomscale), static_cast<int>(ceny),
               static_cast<int>(cenx+0.5*crosssize*zoomscale+1.0),static_cast<int>(ceny));
  pnt.drawLine(static_cast<int>(cenx),static_cast<int>(ceny-0.5*crosssize*zoomscale),
               static_cast<int>(cenx),static_cast<int>(ceny+0.5*crosssize*zoomscale+1.0));

  //freehand drawing
  Symmetry sym=uc_.GetSymmetry();
  std::vector<ReflectionIndex> allpoints=sym.GenerateSymmetryRelatedPoints(ReflectionIndex(1,1,1));
  std::vector<ReflectionIndex> points;
  for(std::vector<ReflectionIndex>::iterator it=allpoints.begin();it!=allpoints.end();++it){
    if(it->GetZStar()>0){
      points.push_back(*it);
    }
  }
  int nrsym=sym.GenerateSymmetryRelatedPositions(geom::Vec3(0.1,0.2,0.3)).size();
  for(std::vector<std::vector<geom::Vec2> >::const_iterator it=draw_list_.begin();it!=draw_list_.end();++it){
    std::vector<QPoint> pl(nrsym*it->size());
    std::vector<QPolygon> qpoly(nrsym);
    Vec3List oldlist;
    for(unsigned int i=0; i<it->size();++i ){
      Vec3List relposlist=sym.GenerateSymmetryRelatedPositions(geom::Vec3((*it)[i][0],(*it)[i][1],0.0));
      for(int j=0;j<nrsym;++j){
        qpoly[j] << QPoint(static_cast<int>(cenx-axh-bxh+relposlist[j][0]*2.0*axh+relposlist[j][1]*2.0*bxh),static_cast<int>(ceny-ayh-byh+relposlist[j][0]*2.0*ayh+relposlist[j][1]*2.0*byh));
      }
    }
    for(int j=0;j<nrsym;++j){
      QPolygon qp = qpoly[j];
      pnt.drawPolyline(qp);
      qp.translate(static_cast<int>(2.0*axh),static_cast<int>(2.0*ayh));
      pnt.drawPolyline(qp);
      qp=qpoly[j];
      qp.translate(static_cast<int>(-2.0*axh),static_cast<int>(-2.0*ayh));
      pnt.drawPolyline(qp);
      qp=qpoly[j];
      qp.translate(static_cast<int>(2.0*bxh),static_cast<int>(2.0*byh));
      pnt.drawPolyline(qp);
      qp=qpoly[j];
      qp.translate(static_cast<int>(-2.0*bxh),static_cast<int>(-2.0*byh));
      pnt.drawPolyline(qp);
      qp=qpoly[j];
      qp.translate(static_cast<int>(2.0*(axh+bxh)),static_cast<int>(2.0*(ayh+byh)));
      pnt.drawPolyline(qp);
      qp=qpoly[j];
      qp.translate(static_cast<int>(2.0*(-axh+bxh)),static_cast<int>(2.0*(-ayh+byh)));
      pnt.drawPolyline(qp);
      qp=qpoly[j];
      qp.translate(static_cast<int>(2.0*(axh-bxh)),static_cast<int>(2.0*(ayh-byh)));
      pnt.drawPolyline(qp);
      qp=qpoly[j];
      qp.translate(static_cast<int>(2.0*(-axh-bxh)),static_cast<int>(2.0*(-ayh-byh)));
      pnt.drawPolyline(qp);
      qp=qpoly[j];
    }
  }
}

void UnitCellOverlay::DrawArrow(QPainter& pnt, const QPoint& p0, const QPoint& p1, double l,double w)
{
  pnt.drawLine(p0,p1);

  QPolygon qpoly;
  qpoly << p1;
  geom::Vec2 v0= geom::Vec2(p0.x(), p0.y());
  geom::Vec2 v1= geom::Vec2(p1.x(), p1.y());
  if(Length(v1-v0)>0){
    geom::Vec2 ve=geom::Vec2(v1-v0)/Length(v1-v0);
    geom::Vec2 vn=geom::Vec2(ve[1],-ve[0]);
    geom::Vec2 v2=v1-l*ve+w/2*vn;
    geom::Vec2 v3=v1-l*ve-w/2*vn;
    qpoly << QPoint(static_cast<int>(v2[0]),static_cast<int>(v2[1]));
    qpoly << QPoint(static_cast<int>(v3[0]),static_cast<int>(v3[1]));
    pnt.drawPolygon(qpoly);
  }
}

bool UnitCellOverlay::OnMouseEvent(QMouseEvent* e,  ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse)
{
  // Translation
  if(e->buttons()==Qt::RightButton) {
    //dvp->SetCursor(wxCursor(wxCURSOR_HAND));
    double zoomscale=dvp->GetZoomScale();
    geom::Vec3 sampling=dvp->GetPixelSampling(); 
    geom::Vec2 asc=uc_.GetVecA()/sampling[0]*zoomscale;
    geom::Vec2 bsc=uc_.GetVecB()/sampling[0]*zoomscale;
    geom::Vec2 sh(e->x()-lastmouse.x(),e->y()-lastmouse.y());
    phase_shift_[0]+=Det(Mat2(sh[0],bsc[0],sh[1],bsc[1]))/Det(Mat2(asc[0],bsc[0],asc[1],bsc[1]))*2*M_PI;
    phase_shift_[1]+=Det(Mat2(sh[0],asc[0],sh[1],asc[1]))/Det(Mat2(bsc[0],asc[0],bsc[1],asc[1]))*2*M_PI;
    return true;
  }

  if(e->type()==QEvent::MouseButtonRelease){
    //dvp->SetCursor(wxNullCursor);
    return true;
  }

  if(e->buttons()==Qt::LeftButton) {
    double zoomscale=dvp->GetZoomScale();
    geom::Vec3 sampling=dvp->GetPixelSampling(); 
    geom::Vec2 asc=uc_.GetVecA()/sampling[0]*zoomscale;
    geom::Vec2 bsc=uc_.GetVecB()/sampling[0]*zoomscale;
    QPoint center=dvp->PointToWin(dvp->GetExtent().GetCenter());
    geom::Vec2 tl=geom::Vec2(center.x(),center.y())+phase_shift_[0]/(2*M_PI)*2.0*asc+phase_shift_[1]/(2*M_PI)*bsc-asc/2.0-bsc/2.0;
    geom::Vec2 pos(e->x(),e->y());
    geom::Vec2 dpos=pos-tl;
    double relposx=Det(Mat2(dpos[0],bsc[0],dpos[1],bsc[1]))/Det(Mat2(asc[0],bsc[0],asc[1],bsc[1]));
    double relposy=Det(Mat2(dpos[0],asc[0],dpos[1],asc[1]))/Det(Mat2(bsc[0],asc[0],bsc[1],asc[1]));
    if(relposx==floor(relposx) || relposy==floor(relposy)){
      return true;
    }
    relposx-=floor(relposx);
    relposy-=floor(relposy);
    if(std::fabs(relposx-draw_list_.back().back()[0])>0.5 || std::fabs(relposy-draw_list_.back().back()[1])>0.5){
      draw_list_.push_back(std::vector<geom::Vec2>());
    }
    draw_list_.back().push_back(geom::Vec2(relposx,relposy));
    return true;
  }
  return false;
}


bool UnitCellOverlay::OnKeyEvent(QKeyEvent* e, ost::img::gui::DataViewerPanel* dvp)
{
  return false;
}

QMenu* UnitCellOverlay::GetMenu()
{
  return menu_;
}

}}//ns
