//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  Authors: Ansgar Philippsen
*/

#include <ost/message.hh>

#include <ost/gui/data_viewer/data_viewer_panel.hh>
#include "gauss2d_overlay.hh"

namespace iplt {  namespace gui {

Gauss2DOverlay::Gauss2DOverlay(const alg::ParamsGauss2D& p):
  Overlay("Gauss2D"),
  params_(p),
  active_color1_(235,127,255),
  active_color2_(185,127,205),
  active_color3_(135,127,155),
  passive_color_(139,127,139)
{
}

void Gauss2DOverlay::OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active)
{
  pnt.setBrush(Qt::NoBrush);
  Real cosw=cos(-params_.GetW());
  Real sinw=sin(-params_.GetW());

  Real sdv[]={0.5,1.0,2.0};
  QPen pp[]={QPen(active_color1_,1),
             QPen(active_color2_,1),
             QPen(active_color3_,1)};
  if(!is_active) {
    for(int i=0;i<3;++i) pp[i]=QPen(passive_color_,1);
  }

  for(int isdv=0;isdv<3;++isdv) {
    QPolygon qpoly;
    for(int iang=0;iang<360;++iang) {
      Real ang=static_cast<Real>(iang)*M_PI/180.0;
      Real x=sdv[isdv]*params_.GetBx()*cos(ang);
      Real y=sdv[isdv]*params_.GetBy()*sin(ang);
      qpoly<<dvp->FracPointToWinCenter(geom::Vec2(x*cosw-y*sinw+params_.GetUx(),
                                                  x*sinw+y*cosw+params_.GetUy()));
    }
    
    pnt.setPen(pp[isdv]);
    pnt.drawPolyline(qpoly);
  }

  if(is_active) {
    pnt.setPen(QPen(active_color1_,1));
  } else {
    pnt.setPen(QPen(passive_color_,1));
  }
  QPoint cen=dvp->FracPointToWinCenter(params_.GetU());
  pnt.drawLine(cen.x()-3,cen.y()  ,cen.x()+3,cen.y()  );
  pnt.drawLine(cen.x()  ,cen.y()-3,cen.x()  ,cen.y()+3);
}

}}  //ns
