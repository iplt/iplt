//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk, Johan Hebert
*/

#ifndef IPLT_LATTICE_OVERLAY_HH
#define IPLT_LATTICE_OVERLAY_HH

#include "lattice_overlay_base.hh"
#include <ost/img/data.hh>
#include <iplt/gui/module_config.hh>

namespace iplt  {  namespace gui {

class LatticeOverlaySettings: public ost::img::gui::PointlistOverlayBaseSettings
{
public:
  LatticeOverlaySettings(const QColor& ac, const QColor& pc, int ssize, int sstr, 
                         const QColor& c1,
                         QWidget* p);

public slots:
  void OnColor1();

public:
  QColor color1;

private:
  QPushButton* c1_b_;
};


class DLLEXPORT_IPLT_GUI LatticeOverlay: public LatticeOverlayBase
{
public:
  LatticeOverlay(const String& name="Lattice");
  LatticeOverlay(const Lattice& lat, const String& name="Lattice");

  virtual void OnMenuEvent(QAction* e);
  virtual void OnDraw(QPainter& dc,  ost::img::gui::DataViewerPanel* dvp, bool is_active);
  virtual bool OnMouseEvent(QMouseEvent* e, ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse);
  virtual bool OnKeyEvent(QKeyEvent* e, ost::img::gui::DataViewerPanel* dvp);
  void SetMinAngle(const double& a);
  double GetMinAngle() const;
  bool OnEditLattice(const QPoint& p,  ost::img::gui::DataViewerPanel* dvp);

protected:
  void set_info(ost::img::gui::DataViewerPanel*, bool update_index);
  void adjust_lattice_(const geom::Vec2& position, const ost::img::Point& index);
  geom::Vec2 fit_peak_position_(const ost::img::Point& center, const Data& data);
  double minangle_;
  QColor anchor_color_;
  ost::img::Point anchor1_;
  ost::img::Point anchor2_;
  bool anchor1flag_;
  bool anchor2flag_;
  ost::img::Point distortion_index_;
  ost::img::Point current_index_;
};

}} // ns


#endif
