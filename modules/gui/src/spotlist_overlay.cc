//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Gian A. Signorell, Andreas Schenk, Ansgar Philippsen
*/

#include <iomanip>
#include <set>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <ost/gui/data_viewer/data_viewer_panel.hh>
#include <ost/gui/data_viewer/strategies.hh>

#include <ost/geom/geom.hh>
#include <ost/geom/geom.hh>

#include "spotlist_overlay.hh"

namespace iplt  {  namespace gui {

SpotlistOverlay::SpotlistOverlay(const Lattice& lat):
  LatticeOverlayBase("Spotlist",lat),
  ilist_(IndexedList(lat)),
  selected_color_(QColor(0,255,0))
{
}

SpotlistOverlay::SpotlistOverlay(const IndexedList& ilist):
  LatticeOverlayBase("Spotlist",Lattice()),
  ilist_(ilist),
  selected_color_(QColor(0,255,0))
{
}

void SpotlistOverlay::OnMenuEvent(QAction* e)
{
}

void SpotlistOverlay::SetIndexedList(const IndexedList& ili)
{
  ilist_ = ili;
}

IndexedList SpotlistOverlay::GetIndexedList() const
{
  return ilist_;
}

void SpotlistOverlay::OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active)
{
  LatticeOverlayBase::OnDraw(pnt,dvp,is_active);
  // draw al selected points
  // loop over all points in the the spot-list an draw them
  if(is_active){
    std::set<Point>::iterator pos;
    strategy_->SetPenColor(selected_color_);
    for(pos = ilist_.Begin(); pos != ilist_.End(); ++pos) {
      QPoint index = dvp->FracPointToWinCenter(GetLattice().CalcPosition(*pos));
      strategy_->Draw(pnt,index);
    }
  }
}
      
bool SpotlistOverlay::OnMouseEvent(QMouseEvent* e,  ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse)
{
  static int h=0;
  static int k=0;
  
  geom::Vec2 comp = GetLattice().CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(e->pos())));
  h=static_cast<int>(round(comp[0]));
  k=static_cast<int>(round(comp[1]));
  emit InfoTextChanged(QString("h: %1, k: %2").arg(h,k));

  if(e->buttons()==Qt::LeftButton && OnLattice(e->pos(),dvp)){
    geom::Vec2 comp = GetLattice().CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(e->pos())));
    h=static_cast<int>(round(comp[0]));
    k=static_cast<int>(round(comp[1]));
    if(ilist_.AddIndex(ost::img::Point(h,k)) && ilist_.AddIndex(ost::img::Point(-h,-k))) {
    } else {
      ilist_.DelIndex(ost::img::Point(h,k));
      ilist_.DelIndex(ost::img::Point(-h,-k));
    }
  }

  return false;
}

bool SpotlistOverlay::OnKeyEvent(QKeyEvent* e,  ost::img::gui::DataViewerPanel* dvp)
{
  return false;
}

QMenu* SpotlistOverlay::GetMenu()
{
  return 0;
}

}}//ns
