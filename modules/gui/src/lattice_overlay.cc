//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk, Johan Hebert, Giani Signorell
*/


#include <cmath>

#include <boost/format.hpp>

#include <QGroupBox>

#include <ost/gui/data_viewer/data_viewer_panel.hh>
#include <ost/gui/data_viewer/strategies.hh>

#include <iplt/alg/lattice_point.hh>

#include "lattice_overlay.hh"

namespace iplt  {  namespace gui {

using namespace ::iplt::alg;

LatticeOverlaySettings::LatticeOverlaySettings(const QColor& ac, const QColor& pc, int ssize, int sstr, 
                                               const QColor& c1, 
                                               QWidget* p):
  ost::img::gui::PointlistOverlayBaseSettings(ac,pc,ssize,sstr,p),
  color1(c1)
{
  QPixmap pm(32,32);
  pm.fill(color1);
  c1_b_=new QPushButton(QIcon(pm),"anchor");
  connect(c1_b_,SIGNAL(pressed()), this, SLOT(OnColor1()));

  QVBoxLayout* vb = new QVBoxLayout;
  vb->addWidget(c1_b_);
  QGroupBox* gb = new QGroupBox("Lattice Overlay",this);
  gb->setLayout(vb);
  main_layout_->addWidget(gb);
}
      
void LatticeOverlaySettings::OnColor1()
{
  QColor col=QColorDialog::getColor(color1); 
  if(col.isValid()) {
    color1=col;
    QPixmap pm(32,32);
    pm.fill(col);
    c1_b_->setIcon(QIcon(pm));
  }
}

//////////////////////////

LatticeOverlay::LatticeOverlay(const String& name):
  LatticeOverlayBase(name,Lattice(Vec2(100.0,0.0),Vec2(0.0,100.0))),
  minangle_(0.1),
  anchor_color_(QColor(0,255,0)),
  anchor1_(ost::img::Point(0,0)),
  anchor2_(ost::img::Point(0,0)),
  anchor1flag_(true),
  anchor2flag_(false),
  distortion_index_(0,0),
  current_index_()
{
}

LatticeOverlay::LatticeOverlay(const Lattice& lat, const String& name):
  LatticeOverlayBase(name,lat),
  minangle_(0.1),
  anchor_color_(QColor(0,255,0)),
  anchor1_(ost::img::Point(0,0)),
  anchor2_(ost::img::Point(0,0)),
  anchor1flag_(true),
  anchor2flag_(false),
  distortion_index_(0,0),
  current_index_()
{
}

void LatticeOverlay::OnMenuEvent(QAction* e)
{
  if(e==a_settings_) {
    LatticeOverlaySettings set(active_color_,passive_color_,symbolsize_,symbolstrength_,
                               anchor_color_,
                               e->parentWidget());
    if(set.exec()==QDialog::Accepted) {
      SetProps(&set);
      anchor_color_=set.color1;
    }
  } else {
    LatticeOverlayBase::OnMenuEvent(e);
  }
}

void LatticeOverlay::SetMinAngle(const double& a)
{
  minangle_=a;
}

double LatticeOverlay::GetMinAngle() const
{
  return minangle_;
}

void LatticeOverlay::set_info(ost::img::gui::DataViewerPanel* dvp, bool update_index)
{
  std::ostringstream itxt;
  if(update_index) {
    
    if(HasTilt()) {
      geom::Vec2 tc = CalcTiltComponents(GetEditTiltGeometry(),GetEditLattice(),current_index_);
      std::pair<Vec2,Vec2> tcv = CalcTiltComponentVectors(GetEditTiltGeometry(),
                                                          GetEditLattice(),
                                                          current_index_);
      itxt << boost::format("Index (%d,%d)  (%.1f, %.1f)\n") % current_index_[0] % current_index_[1] % tc[0] % tc[1];
    } else {
      itxt << boost::format("Index (%d,%d)\n") % current_index_[0] % current_index_[1];
    }
    itxt << std::endl;
    itxt << boost::format("Reciprocal Sampling: %g\n") % GetReciprocalSampling();
  }
  itxt << boost::format(" A: (%7.2f,%7.2f) %7.2f\n B: (%7.2f,%7.2f) %7.2f\n G: %3.2f\n O: (%7.2f,%7.2f)\n K: %g %g") %
    GetEditLattice().GetFirst()[0] %
    GetEditLattice().GetFirst()[1] %
    Length(GetEditLattice().GetFirst()) %
    GetEditLattice().GetSecond()[0] %
    GetEditLattice().GetSecond()[1] %
    Length(GetEditLattice().GetSecond()) %
    (Angle(GetEditLattice().GetFirst(),GetEditLattice().GetSecond())*180.0/M_PI) %
    GetEditLattice().GetOffset()[0] %
    GetEditLattice().GetOffset()[1] %
    GetEditLattice().GetBarrelDistortion() %
    GetEditLattice().GetSpiralDistortion();
  if(HasTilt()) {
    LatticeTiltGeometry tg=GetEditTiltGeometry();
    itxt << boost::format("\n\nTilt Geometry:\n  Tilt Angle: %5.2f\n  X Axis Angle: %5.2f\n  A* Angle: %5.2f") % 
      (tg.GetTiltAngle()*180.0/M_PI) %
      (tg.GetXAxisAngle()*180.0/M_PI) %
      (tg.GetAStarVectorAngle()*180.0/M_PI);

  }

  emit InfoTextChanged(QString(itxt.str().c_str()));
}

void LatticeOverlay::OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active)
{
  LatticeOverlayBase::OnDraw(pnt,dvp,is_active);

  if(is_active){
    set_info(dvp,true);
    strategy_->SetPenColor(anchor_color_);

    if(GetEditFlag()) {
      if(distortion_index_!=Point(0,0)){
        QColor edit_color = active_color_.toHsv();
        edit_color.setHsv(edit_color.hue()+45,edit_color.saturation(),edit_color.value());
        pnt.setPen(QPen(edit_color,1,Qt::DashLine));
        geom::Vec2 undistorted_position=distortion_index_[0]*GetEditLattice().GetFirst()+distortion_index_[1]*GetEditLattice().GetSecond();
        geom::Vec2 barrel_position=undistorted_position*(1+GetEditLattice().GetBarrelDistortion()*Length(undistorted_position)*Length(undistorted_position));
        geom::Vec2 distorted_position=GetEditLattice().CalcPosition(distortion_index_);
        QPoint up=dvp->FracPointToWinCenter(undistorted_position+GetEditLattice().GetOffset());
        QPoint bp=dvp->FracPointToWinCenter(barrel_position+GetEditLattice().GetOffset());
        QPoint dp=dvp->FracPointToWinCenter(distorted_position);
        pnt.drawLine(up,bp);
        pnt.drawLine(bp,dp);
        pnt.drawLine(dp,up);
      }
    }
    QColor anchor_brush_color=anchor_color_;
    anchor_brush_color.setAlphaF(0.3);
    strategy_->SetBrush(QBrush(anchor_brush_color));
    strategy_->SetPenColor(anchor_color_);
    if(anchor1flag_){
      QPoint index = dvp->FracPointToWinCenter(GetEditLattice().CalcPosition(anchor1_));
      strategy_->Draw(pnt,index);
    }
    if(anchor2flag_){
      QPoint index = dvp->FracPointToWinCenter(GetEditLattice().CalcPosition(anchor2_));
      strategy_->Draw(pnt,index);
    }
    strategy_->SetBrush(QBrush(Qt::NoBrush));
  }
}

bool LatticeOverlay::OnEditLattice(const QPoint& mousepos,  ost::img::gui::DataViewerPanel* dvp)
{
  geom::Vec2 comp = GetEditLattice().CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(mousepos)));
  QPoint index = dvp->FracPointToWinCenter(GetEditLattice().CalcPosition(ost::img::Point(static_cast<int>(round(comp[0])),static_cast<int>(round(comp[1])))));
  int i = static_cast<int>((GetSymbolSize()-0.5)* dvp->GetZoomScale()) ;
  int dmx=mousepos.x()-index.x();
  int dmy=mousepos.y()-index.y();
#if 1
  if(dmx*dmx+dmy*dmy<=i*i){
    return true;
  }
#else
  if(menu_->IsChecked(ID_Square)){
    if(std::abs(dmx)<=i  && std::abs(dmy)<=i) {
      return true;
    }
  } else {
    if(dmx*dmx+dmy*dmy<=i*i){
      return true;
    }
  }
#endif
  return false;
}

bool LatticeOverlay::OnMouseEvent(QMouseEvent* e,  ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse)
{
  static int h=0;
  static int k=0;
  static bool hkset=false;

  // display index of editable lattice
  geom::Vec2 comp = GetEditLattice().CalcComponents(ost::img::Point(dvp->WinToFracPoint(e->pos())));
  current_index_=Point(static_cast<int>(round(comp[0])),
		       static_cast<int>(round(comp[1])));


  if(e->buttons() == Qt::MidButton){
    SetEditFlag(true);
    //dvp->SetCursor(ipltCURSOR_ROTATE);
    geom::Vec2 v1 = dvp->WinToFracPoint(QPoint(e->x(), e->y()))-GetLattice().GetOffset();
    geom::Vec2 v2 = dvp->WinToFracPoint(lastmouse)-GetLattice().GetOffset();
    double angle=SignedAngle(v1,v2);
    Mat2 rotmat=Mat2(cos(angle),sin(angle),-sin(angle),cos(angle));
    Lattice el=GetEditLattice();
    SetEditLattice(Lattice(rotmat*el.GetFirst(),
			   rotmat*el.GetSecond(),
			   GetLattice().GetOffset()));
    return true;
  }
  // Center Translation
  if((e->buttons()==Qt::RightButton && !(e->modifiers()&Qt::ShiftModifier)) ||
     (e->buttons()==Qt::LeftButton && e->type()==QEvent::MouseMove && hkset && h==0 && k==0 && !(anchor1flag_ && anchor2flag_))) {
    SetEditFlag(true);
    //dvp->SetCursor(wxCursor(wxCURSOR_HAND));
    Lattice el=GetEditLattice();
    el.SetOffset(el.GetOffset()+dvp->WinToFracPoint(QPoint(e->x(), e->y()))-dvp->WinToFracPoint(lastmouse));
    SetEditLattice(el);
  }

  // drag lattice point
  if(e->button()==Qt::LeftButton && OnEditLattice(e->pos(),dvp)){
    SetEditFlag(true);
    geom::Vec2 comp = GetEditLattice().CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(e->pos())));
    h=static_cast<int>(round(comp[0]));
    k=static_cast<int>(round(comp[1]));
    hkset=true;
  }
  if(e->button()==Qt::RightButton && OnEditLattice(e->pos(),dvp)){
    SetEditFlag(true);
    geom::Vec2 comp = GetEditLattice().CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(e->pos())));
    h=static_cast<int>(round(comp[0]));
    k=static_cast<int>(round(comp[1]));
    distortion_index_=Point(h,k);
    hkset=true;
  }

  if(e->type()==QEvent::MouseButtonRelease) {
    hkset=false;
    //dvp->SetCursor(wxNullCursor);
    distortion_index_=Point(0,0);
  }

  //adjust distortion
  if(e->buttons()==Qt::RightButton && e->modifiers()&Qt::ShiftModifier && e->type()==QEvent::MouseMove && hkset) {
    SetEditFlag(true);
    Lattice el=GetEditLattice();
    geom::Vec2 position=dvp->WinToFracPointCenter(e->pos())-el.GetOffset();
    geom::Vec2 undistorted_position=h*el.GetFirst()+k*el.GetSecond();
    geom::Vec2 dv=position-undistorted_position;
    geom::Vec2 dvrot=Rotate(dv,SignedAngle(undistorted_position,Vec2(1,0)));
    double r3=Length(undistorted_position)*Length(undistorted_position)*Length(undistorted_position);
    el.SetBarrelDistortion(dvrot[0]/r3);
    el.SetSpiralDistortion(dvrot[1]/r3);
    SetEditLattice(el);
  }

  if(e->buttons()==Qt::LeftButton && e->type()==QEvent::MouseMove && hkset) {
    SetEditFlag(true);
    //dvp->SetCursor(ipltCURSOR_ARROWMOVE);
    geom::Vec2 fractional_position=dvp->WinToFracPointCenter(e->pos());
    ost::img::Point index(h,k);
    if(!(e->modifiers() & Qt::ShiftModifier)){
      adjust_lattice_(fractional_position,index);
    }else{    
      ost::img::Point center = ost::img::Point(static_cast<int>(floor((fractional_position[0]))),
                           static_cast<int>(floor((fractional_position[1]))));
      geom::Vec2 u=fit_peak_position_(center,dvp->GetObservedData());
      if( (u[0] >= (center.ToVec2()[0] - GetSymbolSize()) ) &&
          (u[1] >= (center.ToVec2()[1] - GetSymbolSize()) ) &&
          (u[0] <= (center.ToVec2()[0] + GetSymbolSize()) ) &&
          (u[1] <= (center.ToVec2()[1] + GetSymbolSize()) ) ){
        adjust_lattice_(u,index);      
      }
    }
    
  }
  return true;
}

void LatticeOverlay::adjust_lattice_(const geom::Vec2& position, const ost::img::Point& index)
{
  Lattice el=GetEditLattice();
  try{
    if(anchor1flag_ && anchor2flag_){ // restricted mode
      geom::Vec2 v0= el.CalcPosition(anchor1_);
      geom::Vec2 v1= el.CalcPosition(anchor2_);
      geom::Vec2 v2= el.CalcPosition(index);
      if(index!= anchor1_ && index!= anchor2_ && 
         !IsOnLine(Line2(v0,v1),v2,1) && 
         !IsOnLine(Line2(v0,v1),position,1) ) { // not colinear
        Lattice lat;
        if(el.GetBarrelDistortion()==0.0 && el.GetSpiralDistortion()==0.0){
          lat= Lattice(v0,anchor1_,v1,anchor2_,position,index);//use simple fit
        }else{
          lat= Lattice(v0,anchor1_,v1,anchor2_,position,index,el.GetBarrelDistortion(),el.GetSpiralDistortion());
        }
        if(Angle(lat.GetFirst(),lat.GetSecond())> minangle_ && 
           Angle(lat.GetFirst(),lat.GetSecond())< M_PI-minangle_ && 
          Length(lat.GetFirst()) > 5.0 &&
          Length(lat.GetSecond()) > 5.0 ) {
          SetEditLattice(lat);
        }
      }
    } else { // not restricted, 'classic' mode
      if(index[0]!=0 && index[1]!=0){
        geom::Vec2 comp = el.CalcComponents(position);
        if(comp[0] !=0.0 && comp[1] !=0 &&Length(el.GetFirst()/index[0]*comp[0])>5 && Length(el.GetSecond()/index[1]*comp[1])>5){
          el.SetFirst(el.GetFirst()/index[0]*comp[0]);
          el.SetSecond(el.GetSecond()/index[1]*comp[1]);
          SetEditLattice(el);
        }
      }
      if(index[0]!=0 && index[1]==0){
        geom::Vec2 v =  (position- el.GetOffset())/index[0];
        if(Angle(v,el.GetSecond())>minangle_ && Angle(v,el.GetSecond())<M_PI-minangle_ && Length(v)>5 ){
          Lattice l(el.GetOffset(),ost::img::Point(0,0),el.GetOffset()+el.GetSecond(),ost::img::Point(0,1),position,index,el.GetBarrelDistortion(),el.GetSpiralDistortion());
          el.SetFirst(l.GetFirst());
          SetEditLattice(el);
        }
      }
      if(index[0]==0 && index[1]!=0){
        geom::Vec2 v =  (position-el.GetOffset())/index[1];
        if(Angle(v,el.GetFirst())>minangle_ && Angle(v,el.GetFirst())<M_PI-minangle_ && Length(v)>5 ){
          Lattice l(el.GetOffset(),ost::img::Point(0,0),el.GetOffset()+el.GetFirst(),ost::img::Point(1,0),position,index,el.GetBarrelDistortion(),el.GetSpiralDistortion());
          el.SetSecond(l.GetSecond());
          SetEditLattice(el);
        }
      }
    }
  }catch(ost::Error e){
   //ignore errors due to colinear lattice 
  }
}

geom::Vec2 LatticeOverlay::fit_peak_position_(const ost::img::Point& center, const Data& data)
{
  //calculate subimage
  Extent ext = Extent(center - ost::img::Point(GetSymbolSize(),GetSymbolSize()),
                      center + ost::img::Point(GetSymbolSize(),GetSymbolSize()));
  ImageHandle sub_img = CreateImage(ext);
  sub_img.Paste(data);
  //calculate gaussfit
  FitGauss2D gauss_fit = FitGauss2D();
  gauss_fit.SetU(center.ToVec2());
  gauss_fit.SetA(sub_img.GetReal(center));
  sub_img.Apply(gauss_fit);
  //calculate shift
  return gauss_fit.GetU();
}

bool LatticeOverlay::OnKeyEvent(QKeyEvent* e, ost::img::gui::DataViewerPanel* dvp)
{
  /*
    R and F (reset and fix edit lattice) as well as the re-indexing commands
    are handled by lattice overlay base
  */
  QPoint mousepos = dvp->mapFromGlobal(QCursor::pos());
  geom::Vec2 edit_comp = GetEditLattice().CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(mousepos)));
  ost::img::Point edit_p=Point(static_cast<int>(round(edit_comp[0])),
		     static_cast<int>(round(edit_comp[1])));

  if (e->key()=='1') {
    if(OnEditLattice(mousepos,dvp)){
      if(!anchor2flag_ ||  edit_p!=anchor2_){
        anchor1_=edit_p;
        anchor1flag_=true;
      }
    } else {
      anchor1flag_=false;
    }
    return true;
  } else if (e->key()=='2') {
    if(OnEditLattice(mousepos,dvp)){
      if(!anchor1flag_ ||  edit_p!=anchor1_){
        anchor2_=edit_p;
        anchor2flag_=true;
      }
    } else {
      anchor2flag_=false;
    }
    return true;
  } else if (e->key()=='G' &&
              OnEditLattice(mousepos,dvp)){ //move this spot to the gauss fitted center
    //find spot: edit_p
    SetEditFlag(true);
    ost::img::Point center = ost::img::Point(edit_p[0]*GetEditLattice().GetFirst()+
                         edit_p[1]*GetEditLattice().GetSecond()+
			 GetEditLattice().GetOffset());
    geom::Vec2 u = fit_peak_position_(center,dvp->GetObservedData());
    if( (u[0] >= (center.ToVec2()[0] - GetSymbolSize()) ) &&
        (u[1] >= (center.ToVec2()[1] - GetSymbolSize()) ) &&
        (u[0] <= (center.ToVec2()[0] + GetSymbolSize()) ) &&
        (u[1] <= (center.ToVec2()[1] + GetSymbolSize()) ) ){
      adjust_lattice_(u,edit_p);      
      }
  }
  return LatticeOverlayBase::OnKeyEvent(e,dvp);
}

}}//ns
