//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#include <QColorDialog>
#include <ost/log.hh>
#include <ost/units.hh>
#include <ost/gui/data_viewer/data_viewer_panel.hh>
#include <ost/gui/data_viewer/drawing_functions.hh>

#include "ctf_overlay.hh"

namespace iplt  {  namespace gui {

CTFOverlay::CTFOverlay(const TCIFData& data):
  Overlay("CTF"),
  ctf_(data),
  color_(QColor(255,0,0)),
  menu_(new QMenu()),
  symbolsize_(4),
  a_lock_astigmatism_(new QAction("Lock astigmatism",this)),
  a_change_defocus_sign_(new QAction("Change sign of defocus",this)),
  a_change_defocus_x_sign_(new QAction("Change sign of X defocus",this)),
  a_change_defocus_y_sign_(new QAction("Change sign of Y defocus",this)),
  a_set_color_(new QAction("Color",this)),
  maxrings_(40),
  paths_(),
  redraw_rings_(true)
{
  a_lock_astigmatism_->setCheckable(true);
  a_lock_astigmatism_->setChecked(false);
  menu_->addAction(a_lock_astigmatism_);
  menu_->addAction(a_change_defocus_sign_);
  menu_->addAction(a_change_defocus_x_sign_);
  menu_->addAction(a_change_defocus_y_sign_);
  menu_->addAction(a_set_color_);
}

void CTFOverlay::OnMenuEvent(QAction* e)
{
  if(e==a_set_color_){
    QColor newcolor=QColorDialog::getColor(color_);
    if(newcolor.isValid()){
      color_=newcolor;
    }
  }else if(e==a_change_defocus_sign_){
    ctf_.SetDefocusX(-ctf_.GetDefocusX());
    ctf_.SetDefocusY(-ctf_.GetDefocusY());
    redraw_rings_=true;
  }else if(e==a_change_defocus_x_sign_){
    ctf_.SetDefocusX(-ctf_.GetDefocusX());
    redraw_rings_=true;
  }else if(e==a_change_defocus_y_sign_){
    ctf_.SetDefocusY(-ctf_.GetDefocusY());
    redraw_rings_=true;
  }
}
TCIFData CTFOverlay::GetTCIFData() const
{
  return ctf_.GetTCIFData();
}

void CTFOverlay::SetTCIFData(const TCIFData& data)
{
  ctf_.SetTCIFData(data);
  redraw_rings_=true;
}

void CTFOverlay::OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active)
{
  QColor color;
  if(is_active){
    color=color_;
    emit InfoTextChanged(QString("defocus X (A): %1\ndefocus Y (A): %2\nangle (deg): %3").arg(ctf_.GetDefocusX()).arg(ctf_.GetDefocusY()).arg(ctf_.GetAstigmatismAngle()*180.0/M_PI));
  } else{
    color=QColor(100,100,100);
  }
  Size size=dvp->GetExtent().GetSize();
  geom::Vec2 sampling=Vec2(dvp->GetPixelSampling());
  if(redraw_rings_){
    update_rings_(CompMultiply(geom::Vec2(size[0]/2.0,size[1]/2.0),sampling));
    redraw_rings_=false;
  }
  pnt.setPen(QPen(color,0));
  double zoomscale=dvp->GetZoomScale();
  QPoint center= dvp->FracPointToWin(geom::Vec2(0,0));
  pnt.translate(center);
  pnt.scale(zoomscale/sampling[0],zoomscale/sampling[1]);
  pnt.setClipRect(QRectF(QPointF(-static_cast<int>(size[0])/2.0*sampling[0],-static_cast<int>(size[1])/2.0*sampling[0]),QSizeF(size[0]*sampling[0],size[1]*sampling[0])));
  for (int  i=0;i<paths_.size();++i){
     pnt.drawPath(paths_.at(i));
   }
  geom::Vec2 adjusted_symbolsize(symbolsize_/zoomscale*sampling[0],symbolsize_/zoomscale*sampling[1]);
  pnt.setBrush(color);
  unsigned int j=ctf_.GetDefocusX() >=0 ? 1 :0 ;
  for(unsigned int i=1;i<=maxrings_;++i){
    geom::Vec2List nodes=ctf_.CalcThonRingPosition(ctf_.GetAstigmatismAngle(),i);
    if(nodes.size()==0){
      break;
    }
    pnt.drawEllipse(QPointF(nodes[j][0],nodes[j][1]),adjusted_symbolsize[0],adjusted_symbolsize[1]);
  }
  j=ctf_.GetDefocusY() >=0 ? 1 :0 ;
  for(unsigned int i=1;i<=maxrings_;++i){
    geom::Vec2List nodes=ctf_.CalcThonRingPosition(ctf_.GetAstigmatismAngle()-M_PI/2.0,i);
    if(nodes.size()==0){
      break;
    }
    pnt.drawEllipse(QPointF(nodes[j][0],nodes[j][1]),adjusted_symbolsize[0],adjusted_symbolsize[1]);
  }
}

bool CTFOverlay::OnMouseEvent(QMouseEvent* e, ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse)
{
  static int axis=0,ring=0;

  Real zoomscale=dvp->GetZoomScale();
  Size size=dvp->GetExtent().GetSize();
  QPoint center= dvp->FracPointToWin(geom::Vec2(0,0));
  geom::Vec2 sampling(dvp->GetPixelSampling());
  geom::Vec2 event_pos(CompMultiply(geom::Vec2(e->x()-center.x(),e->y()-center.y())/zoomscale,sampling));
  geom::Vec2 adjusted_symbolsize(symbolsize_/zoomscale*sampling[0],symbolsize_/zoomscale*sampling[1]);
  if(e->button()==Qt::LeftButton && e->type()==QEvent::MouseButtonPress) {
    unsigned int j=ctf_.GetDefocusX() >=0 ? 1 :0 ;
    for(unsigned int i=1;i<=maxrings_;++i){
      geom::Vec2List nodes=ctf_.CalcThonRingPosition(ctf_.GetAstigmatismAngle(),i);
      if(nodes.size()==0){
        break;
      }
      if(geom::Length2(nodes[j]-event_pos)<geom::Length2(adjusted_symbolsize)){
        axis=3-2*static_cast<int>(j);
        ring=i;
        return true;
      }
    }
    j=ctf_.GetDefocusY() >=0 ? 1 :0 ;
    for(unsigned int i=1;i<=maxrings_;++i){
      geom::Vec2List nodes=ctf_.CalcThonRingPosition(ctf_.GetAstigmatismAngle()-M_PI/2.0,i);
      if(nodes.size()==0){
        break;
      }
      if(geom::Length2(nodes[j]-event_pos)<geom::Length2(adjusted_symbolsize)){
        axis=4-2*static_cast<int>(j);;
        ring=i;
        return true;
      }
    }
  }

  if(e->type()==QEvent::MouseButtonRelease) {
    axis=0;
    ring=0;
  }

  if(e->type()==QEvent::MouseMove && e->buttons() & Qt::LeftButton && axis>0) {
    int sign= axis>2 ? -1:1;
    Real znew=ctf_.CalcDefocusFromThonRing(event_pos,sign*ring);
    switch(axis){
    case 1:
      ctf_.SetAstigmatismAngle(atan2(event_pos[1],event_pos[0]));
      if(a_lock_astigmatism_->isChecked()){
        ctf_.SetDefocusY(ctf_.GetDefocusY()*znew/ctf_.GetDefocusX());
      }
      ctf_.SetDefocusX(znew);
      redraw_rings_=true;
      break;
    case 2:
      ctf_.SetAstigmatismAngle(atan2(event_pos[1],event_pos[0])+M_PI/2.0);
      if(a_lock_astigmatism_->isChecked()){
        ctf_.SetDefocusX(ctf_.GetDefocusX()*znew/ctf_.GetDefocusY());
      }
      ctf_.SetDefocusY(znew);
      redraw_rings_=true;
      break;
    case 3:
      ctf_.SetAstigmatismAngle(atan2(event_pos[1],event_pos[0]));
      if(a_lock_astigmatism_->isChecked()){
        ctf_.SetDefocusY(ctf_.GetDefocusY()*znew/ctf_.GetDefocusX());
      }
      ctf_.SetDefocusX(znew);
      redraw_rings_=true;
      break;
    case 4:
      ctf_.SetAstigmatismAngle(atan2(event_pos[1],event_pos[0])+M_PI/2.0);
      if(a_lock_astigmatism_->isChecked()){
        ctf_.SetDefocusX(ctf_.GetDefocusX()*znew/ctf_.GetDefocusY());
      }
      ctf_.SetDefocusY(znew);
      redraw_rings_=true;
      break;
    }
  }
  return true;
}

bool CTFOverlay::OnKeyEvent(QKeyEvent* e,  ost::img::gui::DataViewerPanel* dvp)
{
  return false;
}

QMenu* CTFOverlay::GetMenu()
{
  return menu_;
}



QPainterPath CTFOverlay::draw_path_(QVector<QPointF>& pos_nodes,QVector<QPointF>& neg_nodes,bool close){
  QPainterPath path;
  if(pos_nodes.empty()){
    if(! neg_nodes.empty()){
      path.addPolygon(QPolygonF(neg_nodes));
    }
    return path;
  }else if(neg_nodes.empty()){
    if(! pos_nodes.empty()){
      path.addPolygon(QPolygonF(pos_nodes));
    }
    return path;
  }
  QPointF pos_diff_p=pos_nodes.back()-pos_nodes.front();
  QPointF neg_diff_p=neg_nodes.back()-neg_nodes.front();
  QPointF pos_neg_front_diff_p=pos_nodes.front()-neg_nodes.front();
  QPointF pos_neg_back_diff_p=pos_nodes.back()-neg_nodes.back();
  Real pos_diff2=pos_diff_p.x()*pos_diff_p.x()+pos_diff_p.y()*pos_diff_p.y();
  Real neg_diff2=neg_diff_p.x()*neg_diff_p.x()+neg_diff_p.y()*neg_diff_p.y();
  Real pos_neg_front_diff2=pos_neg_front_diff_p.x()*pos_neg_front_diff_p.x()+pos_neg_front_diff_p.y()*pos_neg_front_diff_p.y();
  Real pos_neg_back_diff2=pos_neg_back_diff_p.x()*pos_neg_back_diff_p.x()+pos_neg_back_diff_p.y()*pos_neg_back_diff_p.y();
  if( std::min(pos_diff2,neg_diff2)<1e-8){
    path.addPolygon(QPolygonF(pos_nodes));
    path.addPolygon(QPolygonF(neg_nodes));
  } else if(pos_neg_back_diff2>pos_neg_front_diff2){
      while(neg_nodes.size()>0){
        pos_nodes.push_front(neg_nodes.front());
        neg_nodes.pop_front();
      }
      path.addPolygon(QPolygonF(pos_nodes));
      if(close && pos_neg_back_diff2<0.1){
        path.closeSubpath();
      }
  } else{
      while(neg_nodes.size()>0){
        pos_nodes.push_back(neg_nodes.back());
        neg_nodes.pop_back();
      }
      path.addPolygon(QPolygonF(pos_nodes));
      if(close && pos_neg_front_diff2<0.1){
        path.closeSubpath();
      }
  }
  return path;
}

void CTFOverlay::update_rings_(geom::Vec2 half_size)
{
  Real max_distance2=geom::Length2(half_size);
  paths_.clear();
  for(unsigned int ring=1;ring<=maxrings_ ;++ring){
    geom::Vec2List xaxislist=ctf_.CalcThonRingPosition(ctf_.GetAstigmatismAngle(),ring);
    geom::Vec2List yaxislist=ctf_.CalcThonRingPosition(ctf_.GetAstigmatismAngle()-M_PI/2.0,ring);
    bool outside=true;
    for(unsigned int i=0;i< xaxislist.size();++i){
      if(geom::Length2(xaxislist[i])<=max_distance2){
        outside=false;
      }
    }
    for(unsigned int i=0;i< yaxislist.size();++i){
      if(geom::Length2(yaxislist[i])<=max_distance2){
        outside=false;
      }
    }
    if(outside){
      break;
    }
    QVector<QPointF> pos_nodes;
    QVector<QPointF> neg_nodes;
    double step=2*M_PI/360.0;
    bool start_flag=true;
    for(double angle=-M_PI;angle<=M_PI;angle+=step){
      geom::Vec2List current_nodes=ctf_.CalcThonRingPosition(angle,ring);
      int numnodes=current_nodes.size();
      if(numnodes>0){
        pos_nodes.push_back(QPointF(current_nodes[0][0],current_nodes[0][1]));
        if(numnodes>1){
          neg_nodes.push_back(QPointF(current_nodes[1][0],current_nodes[1][1]));
        }
      }else {
        if(pos_nodes.size()>0){
          paths_<<draw_path_(pos_nodes,neg_nodes,!start_flag);
          pos_nodes.clear();
          neg_nodes.clear();
        }
        start_flag=false;
      }
    }
    if(pos_nodes.size()>0){
      paths_<<draw_path_(pos_nodes,neg_nodes,false);
      pos_nodes.clear();
      neg_nodes.clear();
    }
  }
}


}}//ns
