//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
	Author: Andreas Schenk
	
*/

#ifndef Z_STAR_LIST_HH_
#define Z_STAR_LIST_HH_

#include <vector>
#include <wx/wx.h>
#include <wx/grid.h>

#include <iplt/reflection_list.hh>
#include <iplt/dllexport.hh>

namespace iplt{namespace ex{namespace gui{

class DLLEXPORT_IPLT_GUI ZStarList: public wxGrid
{
  typedef std::vector<ReflectionProxyter> ProxyterList;
	
public:
  ZStarList(ReflectionList& rlist,wxWindow* parent, wxWindowID id=-1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxWANTS_CHARS, const wxString& name = wxT(""));
  virtual ~ZStarList();
  void SetRange(ost::img::Point from, ost::img::Point to);
  void SetRange(ost::img::Point fromto);
  void OnDataChange(wxGridEvent& e);
  void OnLabelRightClick(wxGridEvent& e);
  void OnPopupMenu(wxCommandEvent& e);
protected:
  void build();
  void build_grid_();
  ReflectionList& rlist_;
  ProxyterList proxyters_;
  wxMenu* popupmenu_;
  int popupmenu_rownr_;
	
DECLARE_EVENT_TABLE()
};

}}//ns

#endif /*Z_STAR_LIST_HH_*/
