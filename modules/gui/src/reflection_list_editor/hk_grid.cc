//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
	Author: Andreas Schenk
	
*/

#include <algorithm>
#include "hk_grid.hh"

namespace iplt{namespace ex{namespace gui{


HKGrid::HKGrid(const ReflectionList& rlist,wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
  wxGrid(parent,id,pos,size,style,name),
  minh_(0),
  maxh_(0),
  mink_(0),
  maxk_(0)
{
	build(rlist);
  
}
void HKGrid::build(const ReflectionList& rlist)
{
  wxFont font=GetLabelFont();
  font.SetPointSize(font.GetPointSize()-2);
  SetLabelFont(font);
  ost::img::PointList pl;
	// find min/max h,k
  for(ReflectionProxyter rp=rlist.Begin();rp.IsValid();++rp){
    ReflectionIndex idx=rp.GetIndex();
    if(pl.end()==std::find(pl.begin(),pl.end(),ost::img::Point(idx.GetH(),idx.GetK()))){
      pl.push_back(ost::img::Point(idx.GetH(),idx.GetK()));
    }
		minh_=std::min<int>( idx.GetH() , minh_);
		maxh_=std::max<int>( idx.GetH() , maxh_);
		mink_=std::min<int>( idx.GetK() , mink_);
		maxk_=std::max<int>( idx.GetK() , maxk_);
	}
  DisableDragColSize();
  DisableDragGridSize();
  DisableDragRowSize();
        
  SetDefaultRowSize(20,true);
  SetDefaultColSize(20,true);
	CreateGrid( maxk_-mink_+1, maxh_-minh_+1 );
	
	//set labels
	for(int i=0;i<maxk_-mink_+1;++i){
		SetRowLabelValue(i,wxString::Format(wxT("%d"),i+mink_));
	}
	for(int i=0;i<maxh_-minh_+1;++i){
		SetColLabelValue(i,wxString::Format(wxT("%d"),i+minh_));
	}

	for(PointList::const_iterator it=pl.begin();it!=pl.end();++it){
    	SetCellBackgroundColour((*it)[1]-mink_, (*it)[0]-minh_, wxColour(150,150,250));
	}
}

HKGrid::~HKGrid()
{
}
Point HKGrid::GetIndex(int row, int col)
{
  return ost::img::Point(col+minh_,row+mink_);
}


}}//ns
