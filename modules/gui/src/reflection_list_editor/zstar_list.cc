//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
	Author: Andreas Schenk
	
*/

#include <ost/gui/conv.hh>
#include "zstar_list.hh"

enum PopupMenuIds{
  ID_DELETE
};

namespace iplt{namespace ex{namespace gui{

BEGIN_EVENT_TABLE(ZStarList,wxGrid)
  EVT_GRID_CELL_CHANGE(ZStarList::OnDataChange)
  EVT_GRID_LABEL_RIGHT_CLICK(ZStarList::OnLabelRightClick)
  EVT_MENU(ID_DELETE,ZStarList::OnPopupMenu)
END_EVENT_TABLE()


ZStarList::ZStarList(ReflectionList& rlist,wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
  wxGrid(parent,id,pos,size,style,name),
  rlist_(rlist),
  proxyters_(),
  popupmenu_(new wxMenu()),
  popupmenu_rownr_(-1)
{
	build();
}

ZStarList::~ZStarList()
{
}

void ZStarList::SetRange(ost::img::Point fromto)
{
  SetRange(fromto,fromto);
}
void ZStarList::SetRange(ost::img::Point from, ost::img::Point to)
{
	proxyters_.clear();
	for(ReflectionProxyter rp=rlist_.Begin();!rp.AtEnd();++rp){
		ReflectionIndex idx=rp.GetIndex();
		if(idx.GetH()>=from[0] && idx.GetH()<=to[0] && idx.GetK()>=from[1] && idx.GetK()<=to[1]){
			proxyters_.push_back(rp);
		}
	}
  build_grid_();
}
void ZStarList::build_grid_()
{
  BeginBatch();
  DeleteRows(0,GetNumberRows());
	AppendRows(proxyters_.size());
	unsigned int row=0;
	for(ProxyterList::const_iterator it=proxyters_.begin();it!=proxyters_.end();++it,++row){
		ReflectionIndex idx=it->GetIndex();
    SetCellValue(row,0,wxString::Format(wxT("%d"),idx.GetH()));
    SetCellValue(row,1,wxString::Format(wxT("%d"),idx.GetK()));
    SetCellValue(row,2,wxString::Format(wxT("%f"),idx.GetZStar()));
		for(int i=0;i<rlist_.GetPropertyCount();++i){
      SetCellValue(row,i+3,wxString::Format(wxT("%f"),it->Get(rlist_.GetPropertyName(i))));
		}	
	}
	EndBatch();
}
void ZStarList::build()
{
	CreateGrid(0,rlist_.GetPropertyCount()+3);
	
	//set labels
	SetColLabelValue(0,wxT("h"));
	SetColLabelValue(1,wxT("k"));
	SetColLabelValue(2,wxT("zstar"));
	SetColFormatNumber(0);
	SetColFormatNumber(1);
	SetColFormatFloat(2);
	for(int i=0;i<rlist_.GetPropertyCount();++i){
		SetColLabelValue(i+3,iplt::gui::ToWxString(rlist_.GetPropertyName(i)));
		SetColFormatFloat(i+3);
	}
  popupmenu_->Append(ID_DELETE,wxT("Delete"));
}

void ZStarList::OnDataChange(wxGridEvent& e)
{
  int row=e.GetRow();
  int col=e.GetCol();
  if(col>=3){
    double val;
    GetCellValue(row,col).ToDouble(&val);
    proxyters_[row].Set(rlist_.GetPropertyName(col-3),val);
  }
  e.Skip();
}
void ZStarList::OnLabelRightClick(wxGridEvent& e)
{
  int row=e.GetRow();
  int col=e.GetCol();
  if(col==-1){
    popupmenu_rownr_=row;
    PopupMenu(popupmenu_);
  }
  e.Skip();
}
void ZStarList::OnPopupMenu(wxCommandEvent& e)
{
  switch(e.GetId()){
  case ID_DELETE:
    proxyters_[popupmenu_rownr_].Delete();
    proxyters_.erase(proxyters_.begin()+popupmenu_rownr_);
    build_grid_();
    break;
  default:
    break;
  }
}

}}//ns
