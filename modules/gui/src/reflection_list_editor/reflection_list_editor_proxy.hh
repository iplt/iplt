//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/
#ifndef REFLECTION_LIST_EDITOR_PROXY_HH_
#define REFLECTION_LIST_EDITOR_PROXY_HH_

#include <boost/shared_ptr.hpp>

#include "reflection_list_editor.hh"

namespace iplt{namespace ex{namespace gui{

class DLLEXPORT_IPLT_GUI ReflectionListEditorProxy
{
public:
  ReflectionListEditorProxy();
  ReflectionListEditorProxy(ReflectionListEditor* editor);
	virtual ~ReflectionListEditorProxy();
private:
  ReflectionListEditor* editor_;
};

typedef boost::shared_ptr<ReflectionListEditorProxy> ReflectionListEditorProxyPtr;

}}//ns


#endif /*REFLECTION_LIST_EDITOR_PROXY_HH_*/
