//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
	Author: Andreas Schenk
	
*/

#include "reflection_list_editor.hh"
#include "hk_grid.hh"
#include "zstar_list.hh"

namespace iplt{namespace ex{namespace gui{

enum window_id{
  ID_HKGRID=1,
  ID_ZSTAR_LIST
};

BEGIN_EVENT_TABLE(ReflectionListEditor,wxFrame)
  EVT_GRID_CMD_RANGE_SELECT(ID_HKGRID,ReflectionListEditor::OnRangeSelect)
  EVT_GRID_CMD_SELECT_CELL(ID_HKGRID,ReflectionListEditor::OnCellSelect)
END_EVENT_TABLE()


ReflectionListEditor::~ReflectionListEditor()
{
  frame_manager_.UnInit();
}
ReflectionListEditor::ReflectionListEditor(ReflectionList& rlist):
  wxFrame(NULL,-1,wxT("ReflectionListEditor"),wxDefaultPosition,wxDefaultSize,wxDEFAULT_FRAME_STYLE,wxT("ReflectionListEditor")),
  zstar_list_(new ZStarList(rlist,this,ID_ZSTAR_LIST)),
  hkgrid_(new HKGrid(rlist,this,ID_HKGRID))
{
  // EnableEditing must not be called within grid ctor because it sends a SelectCell event before the grid is initialized
  hkgrid_->EnableEditing(false);
  frame_manager_.SetFrame(this);
  frame_manager_.AddPane(hkgrid_, wxPaneInfo().Name(wxT("HKGrid")).CenterPane());
  frame_manager_.AddPane(zstar_list_,  wxPaneInfo().Name(wxT("ZStarList")).
                                                        Caption(wxT("ZStarList")).
                                                        Right());
  frame_manager_.Update();
  Show();
  
}

void ReflectionListEditor::OnCellSelect(wxGridEvent& e)
{
  if(e.Selecting()){
    ost::img::Point idx=hkgrid_->GetIndex(e.GetRow(),e.GetCol());
    zstar_list_->SetRange(idx);
  }
  e.Skip();
}
void ReflectionListEditor::OnRangeSelect(wxGridRangeSelectEvent& e)
{
  if(e.Selecting()){
    ost::img::Point idx1=hkgrid_->GetIndex(e.GetTopRow(),e.GetLeftCol());
    ost::img::Point idx2=hkgrid_->GetIndex(e.GetBottomRow(),e.GetRightCol());
    zstar_list_->SetRange(idx1,idx2);
  }
  e.Skip();
}

}}//ns
