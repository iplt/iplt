//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Gian A. Signorell
*/

#ifndef IPLT_EX_GUI_INDEXED_LIST_HH
#define IPLT_EX_GUI_INDEXED_LIST_HH

#include <set>

#include <ost/gui/data_viewer/overlay_base.hh>
#include <iplt/lattice.hh>
#include <iplt/reflection_list.hh>
#include <iplt/gui/module_config.hh>

namespace iplt  {  namespace gui {

class DLLEXPORT_IPLT_GUI IndexedList {
  typedef std::set<Point> HKL_List;

public:
 
  IndexedList(const Lattice& lat=Lattice());  
     
  bool AddIndex(const ost::img::Point& p);
  bool DelIndex(const ost::img::Point& p);
  void EmptyList();
  void SetLattice(const Lattice& lat);
  Lattice GetLattice();
  void Append(const IndexedList& lst);
  std::set<Point>::iterator Begin();
  std::set<Point>::const_iterator Begin() const;
  std::set<Point>::iterator End();
  std::set<Point>::const_iterator End() const;
  
private:  
  Lattice lattice_;
  HKL_List hkl_list_;
};

DLLEXPORT_IPLT_GUI IndexedList ImportSpotlist(const String& file);
DLLEXPORT_IPLT_GUI void ExportSpotlist(const IndexedList& l, const String& file);

}} // ns

#endif
