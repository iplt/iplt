//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#ifndef IPLT_EX_GUI_UC_OVERLAY_HH
#define IPLT_EX_GUI_UC_OVERLAY_HH

#include <vector>
#include <ost/gui/data_viewer/overlay_base.hh>
#include <iplt/unit_cell.hh>
#include <iplt/gui/module_config.hh>

namespace iplt  {  namespace gui {

// this is a safe using directive
using namespace iplt::gui;

class DLLEXPORT_IPLT_GUI UnitCellOverlay: public ost::img::gui::Overlay
{
public:
  UnitCellOverlay(const SpatialUnitCell& uc);

  virtual void OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active);
  virtual void OnMenuEvent(QAction* e);
  virtual bool OnMouseEvent(QMouseEvent* e, ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse);
  virtual bool OnKeyEvent(QKeyEvent* e, ost::img::gui::DataViewerPanel* dvp);
  virtual QMenu* GetMenu();

  void SetUnitCell(const SpatialUnitCell& s) {uc_=s;}
  void SetPhaseShift(const geom::Vec2& po) {phase_shift_=po;};
  geom::Vec2 GetPhaseShift() const {return phase_shift_;};

protected:
  QMenu* menu_;
  SpatialUnitCell uc_;
  QColor color_;
  geom::Vec2 phase_shift_;
  std::vector<std::vector<Vec2> > draw_list_;;

  void DrawArrow(QPainter& dc, const QPoint& p0, const QPoint& p1, double l, double w);
};

}} // ns


#endif
