//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Gian A. Signorell
*/

#include <iomanip>
#include <fstream>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <ost/log.hh>

#include "indexed_list.hh"

namespace iplt  {  namespace gui {

IndexedList::IndexedList(const Lattice& lat)
{
  lattice_ = lat;
}

bool IndexedList::AddIndex(const ost::img::Point& p)
{
  return hkl_list_.insert(p).second;	
}

bool IndexedList::DelIndex(const ost::img::Point& p)
{
  //check if it exists
	int el = hkl_list_.erase(p);
	if (el == 1){
    return true;
	} else {
    return false;
	}
}

void IndexedList::EmptyList()
{
  hkl_list_.clear();
}

void IndexedList::SetLattice(const Lattice& lat)
{
  lattice_=lat;
}	

Lattice IndexedList::GetLattice()
{
  return lattice_;
}

void IndexedList::Append(const IndexedList& lst)
{
  std::set<Point>::const_iterator pos;
  for(pos = lst.Begin(); pos != lst.End(); ++pos){
    hkl_list_.insert(*pos);
  }	
}

std::set<Point>::iterator IndexedList::Begin()
{
  return hkl_list_.begin();
}

std::set<Point>::const_iterator IndexedList::Begin() const
{
  return hkl_list_.begin();
}


std::set<Point>::iterator IndexedList::End()
{
  return hkl_list_.end();
}

std::set<Point>::const_iterator IndexedList::End() const
{
  return hkl_list_.end();
}

IndexedList ImportSpotlist(const String& file)
{
  IndexedList ili = IndexedList();
  std::ifstream infile(file.c_str());
  if (!infile) throw Error(file+" not found");

  String line;
  while (std::getline(infile,line)) {
                String s_h = line.substr(0,6);
                String s_k = line.substr(7,6);
    int i_h = boost::lexical_cast<int>(boost::trim_copy(s_h));
    int i_k = boost::lexical_cast<int>(boost::trim_copy(s_k));
    ili.AddIndex(ost::img::Point(i_h,i_k));
  }
	return ili;
}


void ExportSpotlist(const IndexedList& l, const String& file)
{
  std::ofstream outfile(file.c_str());
  if (!outfile) throw Error(file+" cannot be opened");
	
  std::set<Point>::const_iterator pos;
  for(pos = l.Begin(); pos != l.End(); ++pos){
		Point hk(*pos);
		outfile << std::setw(6) << std::setfill(' ') << hk[0];
    outfile << ',';
		outfile << std::setw(6) << std::setfill(' ') << hk[1];
		outfile << std::endl;
  }
}

}}//ns



