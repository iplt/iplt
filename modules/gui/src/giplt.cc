//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <iostream>
#include <QDir>
#include <QTimer>
#include <QSettings>
#include <ost/log.hh>
#include <ost/platform.hh>
#include <ost/gui/python_shell/python_interpreter.hh>
#include <ost/gui/python_shell/text_logger.hh>
#include "giplt.hh"
#include <QApplication>
#include <QResource>
#include <QFileInfo>
#include <QIcon>
#include <QTextStream>
#include <stdlib.h>




DelayedScriptExecutor::DelayedScriptExecutor()
{
  // the trick here is, that timers won't get executed until we call app.exec()
  QTimer::singleShot(0, this, SLOT(Exec()));
}
void DelayedScriptExecutor::Exec()
{
  ost::gui::PythonInterpreter& interp=ost::gui::PythonInterpreter::Instance();
  interp.Start();
}

namespace {

static void sigint_handler(int) 
{ 
  std::cerr << "shutting down..." << std::endl; 
  QApplication::exit(-1);
} 

static void reclaim_signals() 
{ 
#ifndef _MSC_VER
  struct sigaction sa; 
  sa.sa_sigaction=0; // first assign this in case sigaction is union 
  sa.sa_handler = sigint_handler; 
  sa.sa_mask = sigset_t(); 
  sa.sa_flags = 0; 
  sigaction(SIGINT, &sa, 0);
#endif
}

String get_ost_root()
{
  QDir dir(QApplication::applicationDirPath());

  #ifdef _MSC_VER
    dir.cdUp();
    dir.cdUp();
  #else
    dir.cdUp();
  #endif

  return dir.path().toStdString();
}



void setup_python_search_path(const String& ost_root, ost::gui::PythonInterpreter& pi)
{
#ifdef _MSC_VER
    pi.AppendModulePath(QString::fromStdString(ost_root+"\\lib\\openstructure"));
    pi.AppendModulePath(QString::fromStdString(ost_root+"\\lib"));
#else
#  if (defined(__ppc64__) || defined(__x86_64__)) && !defined(__APPLE__)
    pi.AppendModulePath(QString::fromStdString(ost_root+"/lib64/openstructure"));
    pi.AppendModulePath(QString::fromStdString(ost_root+"/lib64"));
#  else
    pi.AppendModulePath(QString::fromStdString(ost_root+"/lib/openstructure"));
    pi.AppendModulePath(QString::fromStdString(ost_root+"/lib"));
#  endif
#endif
  pi.AppendModulePath(".");  
}
  
int setup_resources(QApplication& app) 
{
  QResource qr(":/images");
   if(!qr.isValid()) {
     LOG_ERROR("no valid /image Qt resource found");
     return -1;
   }
   int sizes[]={512,256,128, 0};
   int i=0;
   QIcon icon;
   while (sizes[i]>0) {
    icon.addFile(QString(":images/cube_")+QString::number(sizes[i]),
                 QSize(sizes[i], sizes[i]));
    ++i; 
   }
   app.setWindowIcon(icon);
   return 0;


}
void read_logger_settings(const QString& group_name, ost::gui::TextLogger* logger)
{
  QSettings settings;
  settings.beginGroup("logging");
  settings.beginGroup(group_name);   
  logger->SetCodeLogging(settings.value("log_code",QVariant(false)).toBool());
  logger->SetOutputLogging(settings.value("log_output",QVariant(true)).toBool());
  logger->SetErrorLogging(settings.value("log_error",QVariant(true)).toBool());
  settings.endGroup();
  settings.endGroup();
}

int init_python_interpreter()
{
  // the order of these two calls is important!
  ost::gui::PythonInterpreter::Instance();
  reclaim_signals();
  //
  ost::gui::PythonInterpreter& py=ost::gui::PythonInterpreter::Instance();
  String ost_root =get_ost_root();
  if(ost_root == "") {
    return -1;
  }
  ost::SetPrefixPath(ost_root);
    // setup python shell logging
  ost::gui::TextLogger* console_logger=new ost::gui::TextLogger(stdout);
  read_logger_settings("console", console_logger);
  if (console_logger->GetErrorLogging()) {
    QObject::connect(&py,  
                     SIGNAL(ErrorOutput(unsigned int, const QString &)),
                     console_logger,
                     SLOT(AppendOutput(unsigned int, const QString &)));
  }
  if (console_logger->GetOutputLogging()) {
    QObject::connect(&py, 
                     SIGNAL(Output(unsigned int, const QString &)),
                     console_logger,
                     SLOT(AppendOutput(unsigned int, const QString &)));
  }  

  setup_python_search_path(ost_root, py);
  //todo move import somewhere else
  py.RunCommand("from ost import *");
  return 0;
}

void prepare_scripts(int argc, char** argv, ost::gui::PythonInterpreter& py)
{
  for (int param_iter=2; param_iter<argc; ++param_iter) {
    py.AppendCommandlineArgument(QString(argv[param_iter]));
  }
  py.RunScript(argv[1]);
}


} //ns

// initialise gosty - the graphical open structure interpreter
int main(int argc, char** argv)
{
  int dummy_argc=1;
  QApplication app(dummy_argc,argv);
  QCoreApplication::setOrganizationName("IPLT");
  QCoreApplication::setOrganizationDomain("iplt.org");
  QCoreApplication::setApplicationName("IPLT");
  app.setLibraryPaths(QStringList());
  if (int rv=setup_resources(app)<0) {
    return rv;
  }
  if (int r=init_python_interpreter()<0) {
    return r;
  }
  ost::gui::PythonInterpreter& py_int=ost::gui::PythonInterpreter::Instance();
  //py_int.RunInitRC();
  //  delay all execution of python scripts after app.exec() has been called.
  DelayedScriptExecutor delayed_executor;

  prepare_scripts(argc,argv,py_int);
  return app.exec();
}
