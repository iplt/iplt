set(IPLT_GUI_HEADERS
ctf_overlay.hh
gauss2d_overlay.hh
indexed_list.hh
lattice_overlay.hh
lattice_overlay_base.hh
lp_overlay.hh
rlist_overlay.hh
spotlist_overlay.hh
uc_overlay.hh
module_config.hh
)

set(IPLT_GUI_SOURCES
ctf_overlay.cc
gauss2d_overlay.cc
indexed_list.cc
lattice_overlay.cc
lattice_overlay_base.cc
lp_overlay.cc
rlist_overlay.cc
spotlist_overlay.cc
uc_overlay.cc
)



include(${QT_USE_FILE})
module(NAME gui 
       SOURCES  ${IPLT_GUI_SOURCES}
       HEADERS ${IPLT_GUI_HEADERS}
       HEADER_OUTPUT_DIR iplt/gui
       DEPENDS_ON iplt_base iplt_alg ost_gui
       LINK ${QT_LIBRARIES} ${PYTHON_LIBRARIES} ${BOOST_PYTHON_LIBRARIES}
       PREFIX iplt
       )

qt4_add_resources(IPLT_QT_RESOURCE giplt.qrc)
qt4_wrap_cpp(IPLT_IPLT_MOC "giplt.hh")


if (NOT WIN32)
  set(LINK LINK ${BOOST_PROGRAM_OPTIONS_LIBRARIES})
endif()

include_directories(${PYTHON_INCLUDE_PATH})
executable(NAME iplt_bin SOURCES giplt.cc ${IPLT_IPLT_MOC} ${IPLT_QT_RESOURCE} 
           DEPENDS_ON iplt_gui ${LINK}
           )
