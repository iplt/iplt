//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#include "lp_overlay.hh"
#include <ost/log.hh>
#include <ost/gui/data_viewer/data_viewer_panel.hh>

namespace iplt  {  namespace gui {

LPOverlay::LPOverlay():
  Overlay("LowPass"),
  cutoff_(ost::img::Point(120,120)),
  color_(QColor(255,0,0))
{
  /*
  menu_=new wxMenu(wxT("LowPass Overlay"),wxMENU_TEAROFF);
  menu_->Append(ID_Color,wxT("Color"));
  */
}

void LPOverlay::OnMenuEvent(QAction* e)
{
}

void LPOverlay::OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active)
{
  QColor color;
  if(is_active){
    color=color_;
  } else{
    color=QColor(100,100,100);
  }
  pnt.setPen(QPen(color,1));
  pnt.setBrush(QBrush(color,Qt::DiagCrossPattern));

  double zoomscale=dvp->GetZoomScale();
  QPoint center= dvp->FracPointToWin(geom::Vec2(0,0));
  pnt.drawEllipse(-cutoff_[0]*zoomscale/2+center.x(),
                  -cutoff_[1]*zoomscale/2+center.y(),
                  cutoff_[0]*zoomscale+1,
                  cutoff_[1]*zoomscale+1);
}

bool LPOverlay::OnMouseEvent(QMouseEvent* e,  ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse)
{
  QPoint cutoff= e->pos() - dvp->FracPointToWin(geom::Vec2(0,0));
  double zoomscale=dvp->GetZoomScale();
  SetCutOff(ost::img::Point(static_cast<int>(2*cutoff.x()/zoomscale),
		  static_cast<int>(2*cutoff.y()/zoomscale)));
  return true;
}

bool LPOverlay::OnKeyEvent(QKeyEvent* e,  ost::img::gui::DataViewerPanel* dvp)
{
  return false;
}

QMenu* LPOverlay::GetMenu()
{
  return 0;
}

}}//ns
