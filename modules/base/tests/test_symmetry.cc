//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <sstream> 
#include <complex> 
#include <iplt/symmetry.hh>
#include <iplt/symmetry_exception.hh>
#include <iplt/reflection.hh>

using namespace iplt;

extern const Real tolerance;


BOOST_AUTO_TEST_SUITE( iplt_base )


BOOST_AUTO_TEST_CASE(symmetry_reflection)
{
  // check constructors and getters
  SymmetryReflection sr1(1,2,0.8,12.0,0.5);
  BOOST_CHECK(sr1.GetReflectionIndex()==ReflectionIndex(1,2,0.8));
  Complex v1=sr1.GetValue();
  Complex v2=std::polar<Real>(12.0,0.5);
  BOOST_CHECK_CLOSE(real(v1),real(v2),tolerance);
  BOOST_CHECK_CLOSE(imag(v1),imag(v2),tolerance);
  BOOST_CHECK_CLOSE(sr1.GetAmplitude(),12.0,tolerance);
  BOOST_CHECK_CLOSE(sr1.GetPhase(),0.5,tolerance);

  SymmetryReflection sr2(ReflectionIndex(3,4,0.1),11.0,0.3);
  BOOST_CHECK(sr2.GetReflectionIndex()==ReflectionIndex(3,4,0.1));
  v1=sr2.GetValue();
  v2=std::polar<Real>(11.0,0.3);
  BOOST_CHECK_CLOSE(real(v1),real(v2),tolerance);
  BOOST_CHECK_CLOSE(imag(v1),imag(v2),tolerance);
  BOOST_CHECK_CLOSE(sr2.GetAmplitude(),11.0,tolerance);
  BOOST_CHECK_CLOSE(sr2.GetPhase(),0.3,tolerance);

  SymmetryReflection sr3(5,6,0.7,std::polar<Real>(2.3,0.6));
  BOOST_CHECK(sr3.GetReflectionIndex()==ReflectionIndex(5,6,0.7));
  v1=sr3.GetValue();
  v2=std::polar<Real>(2.3,0.6);
  BOOST_CHECK_CLOSE(real(v1),real(v2),tolerance);
  BOOST_CHECK_CLOSE(imag(v1),imag(v2),tolerance);
  BOOST_CHECK_CLOSE(sr3.GetAmplitude(),2.3,tolerance);
  BOOST_CHECK_CLOSE(sr3.GetPhase(),0.6,tolerance);

  SymmetryReflection sr4(ReflectionIndex(7,8,0.17),std::polar<Real>(9.4,0.25));
  BOOST_CHECK(sr4.GetReflectionIndex()==ReflectionIndex(7,8,0.17));
  v1=sr4.GetValue();
  v2=std::polar<Real>(9.4,0.25);
  BOOST_CHECK_CLOSE(real(v1),real(v2),tolerance);
  BOOST_CHECK_CLOSE(imag(v1),imag(v2),tolerance);
  BOOST_CHECK_CLOSE(sr4.GetAmplitude(),9.4,tolerance);
  BOOST_CHECK_CLOSE(sr4.GetPhase(),0.25,tolerance);

  //check setters
  sr1.SetReflectionIndex(ReflectionIndex(9,10,0.05));
  sr1.SetValue(std::polar<Real>(26.4,0.43));
  BOOST_CHECK(sr1.GetReflectionIndex()==ReflectionIndex(9,10,0.05));
  v1=sr1.GetValue();
  v2=std::polar<Real>(26.4,0.43);
  BOOST_CHECK_CLOSE(real(v1),real(v2),tolerance);
  BOOST_CHECK_CLOSE(imag(v1),imag(v2),tolerance);
  BOOST_CHECK_CLOSE(sr1.GetAmplitude(),26.4,tolerance);
  BOOST_CHECK_CLOSE(sr1.GetPhase(),0.43,tolerance);

  sr1.SetAmplitude(77.5);
  BOOST_CHECK_CLOSE(sr1.GetAmplitude(),77.5,tolerance);
  BOOST_CHECK_CLOSE(sr1.GetPhase(),0.43,tolerance);

  sr1.SetPhase(0.7);
  BOOST_CHECK_CLOSE(sr1.GetAmplitude(),77.5,tolerance);
  BOOST_CHECK_CLOSE(sr1.GetPhase(),0.7,tolerance);
}

BOOST_AUTO_TEST_CASE(symmetry_restrict)
{
	std::vector<SymmetryReflection> reflist;
	reflist.push_back(SymmetryReflection(2,1,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,1,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,-1,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(2,-1,0.0,2.1,1.3));

	reflist.push_back(SymmetryReflection(2,1,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,1,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,-1,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(2,-1,0.3,2.1,1.3));

	reflist.push_back(SymmetryReflection(2,1,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,1,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,-1,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(2,-1,-0.3,2.1,1.3));

	reflist.push_back(SymmetryReflection(1,2,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,2,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,-2,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(1,-2,0.0,2.1,1.3));

	reflist.push_back(SymmetryReflection(1,2,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,2,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,-2,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(1,-2,0.3,2.1,1.3));

	reflist.push_back(SymmetryReflection(1,2,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,2,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,-2,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(1,-2,-0.3,2.1,1.3));
	int symnums[]={1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1017,1018, 1020,1021,1022,1023,1059,1094,1146,1148,1155,1160,1161,1166,1167,1197,2005,2017,2018,3004,3005,3018,4005};


	for(unsigned int i=1;i<231;++i)
	{
		try{
			Symmetry s(i);
			for(std::vector<SymmetryReflection>::const_iterator it=reflist.begin();it!=reflist.end();++it){
				try{
					s.Restrict(*it);
				}catch(SymmetryException e){
					ReflectionIndex idx=it->GetReflectionIndex();
					std::stringstream s; 
					s << "Symmetry " << i << ": unable to reduce index " << idx ; 
					BOOST_ERROR( s.str());      
				}
			}
		}catch(...){
			std::stringstream s; 
			s << "Symmetry " << i << " missing "; 
			BOOST_ERROR( s.str());      
		}
	}
	for(unsigned int i=0;i<36;++i)
	{
		try{
			Symmetry s(symnums[i]);
			for(std::vector<SymmetryReflection>::const_iterator it=reflist.begin();it!=reflist.end();++it){
				try{
					s.Restrict(*it);
				}catch(...){
					ReflectionIndex idx=it->GetReflectionIndex();
					std::stringstream s; 
					s << "Symmetry " << symnums[i] << ": unable to reduce index " << idx ; 
					BOOST_ERROR( s.str());      
				}
			}
		}catch(...){
			std::stringstream s; 
			s << "Symmetry " << symnums[i] << " missing "; 
			BOOST_ERROR( s.str());      
		}
	}
}

BOOST_AUTO_TEST_CASE(symmetry_asu)
{
	std::vector<SymmetryReflection> reflist;
	reflist.push_back(SymmetryReflection(2,1,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,1,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,-1,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(2,-1,0.0,2.1,1.3));

	reflist.push_back(SymmetryReflection(2,1,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,1,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,-1,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(2,-1,0.3,2.1,1.3));

	reflist.push_back(SymmetryReflection(2,1,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,1,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-2,-1,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(2,-1,-0.3,2.1,1.3));

	reflist.push_back(SymmetryReflection(1,2,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,2,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,-2,0.0,2.1,1.3));
	reflist.push_back(SymmetryReflection(1,-2,0.0,2.1,1.3));

	reflist.push_back(SymmetryReflection(1,2,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,2,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,-2,0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(1,-2,0.3,2.1,1.3));

	reflist.push_back(SymmetryReflection(1,2,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,2,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(-1,-2,-0.3,2.1,1.3));
	reflist.push_back(SymmetryReflection(1,-2,-0.3,2.1,1.3));
	int symnums[]={1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1017,1018, 1020,1021,1022,1023,1059,1094,1146,1148,1155,1160,1161,1166,1167,1197,2005,2017,2018,3004,3005,3018,4005};


	//for(unsigned int i=1;i<231;++i)
	//TODO fix: 221-230
	for(unsigned int i=1;i<221;++i)
	{
		try{
			Symmetry s(i);
			for(std::vector<SymmetryReflection>::const_iterator it=reflist.begin();it!=reflist.end();++it){
				try{
					s.Reduce(*it);
				}catch(SymmetryException e){
					ReflectionIndex idx=it->GetReflectionIndex();
					std::stringstream s; 
					s << "Symmetry " << i << ": unable to reduce index " << idx ; 
					BOOST_ERROR( s.str());      
				}
			}
		}catch(...){
			std::stringstream s; 
			s << "Symmetry " << i << " missing "; 
			BOOST_ERROR( s.str());      
		}
	}
	for(unsigned int i=0;i<36;++i)
	{
		try{
			Symmetry s(symnums[i]);
			for(std::vector<SymmetryReflection>::const_iterator it=reflist.begin();it!=reflist.end();++it){
				try{
					s.Reduce(*it);
				}catch(...){
					ReflectionIndex idx=it->GetReflectionIndex();
					std::stringstream s; 
					s << "Symmetry " << symnums[i] << ": unable to reduce index " << idx ; 
					BOOST_ERROR( s.str());      
				}
			}
		}catch(...){
			std::stringstream s; 
			s << "Symmetry " << symnums[i] << " missing "; 
			BOOST_ERROR( s.str());      
		}
	}
}

BOOST_AUTO_TEST_SUITE_END()
