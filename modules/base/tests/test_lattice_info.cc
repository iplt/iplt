//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <iostream>

#include <iplt/info/info.hh>
#include <iplt/lattice.hh>
#include <iplt/lattice_info.hh>


#include "test_lattice_info.hh"

using namespace iplt;
using namespace iplt::ex;

namespace test_lattice_info {


void test_lattice()
{
  ost::info::InfoHandle ih = ost::info::LoadInfo("test_info.xml");
  Lattice lat = InfoToLattice(ih.Root().GetGroup("test"));
  
  BOOST_CHECK(compare_Vec2(lat.GetFirst(),Vec2(15.0,-1.9)));
  BOOST_CHECK(compare_Vec2(lat.GetSecond(),Vec2(-12.4,33.1)));

  ost::info::InfoGroup g2=ih.Root().CreateGroup("test2");
  LatticeToInfo(lat,g2);
  Lattice lat2 = InfoToLattice(g2);

  BOOST_CHECK(lat==lat2);
}

}

test_suite* CreateLatticeInfoTest()
{
  using namespace test_lattice_info;

  test_suite* ts=BOOST_TEST_SUITE("LatticeInfo Test");

  ts->add(BOOST_TEST_CASE(&test_lattice));

  return ts;
}

