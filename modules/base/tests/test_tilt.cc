//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <ost/info/info.hh>
#include <ost/units.hh>
#include <iplt/tilt.hh>
#include <iplt/unit_cell.hh>

extern const Real tolerance;


using namespace iplt;

BOOST_AUTO_TEST_SUITE( iplt_base )


BOOST_AUTO_TEST_CASE(tilt_init)
{
  TiltGeometry t1(46.0*Units::deg,33.0*Units::deg);
  BOOST_CHECK_CLOSE(t1.GetTiltAngle(),46.0*Units::deg,tolerance);
  BOOST_CHECK_CLOSE(t1.GetXAxisAngle(),33.0*Units::deg,tolerance);

  //for negative tilt angles, the tilt axis will be rotated 180 deg to give a positive tilt angle
  TiltGeometry t2(-34.0*Units::deg,67.0*Units::deg);
  BOOST_CHECK_CLOSE(t2.GetTiltAngle(),34.0*Units::deg,tolerance);
  BOOST_CHECK_CLOSE(t2.GetXAxisAngle(),(67.0-180.0)*Units::deg,tolerance);
}
BOOST_AUTO_TEST_CASE(tilt_spatial)
{
  std::ostringstream msg;
  
  SpatialUnitCell sc1(200,100,108*Units::deg);
  Real ly=100.0*sin(54*Units::deg);
  Real lx=100.0*cos(54*Units::deg);
  Lattice l1(Rotate(geom::Vec2(2*lx*cos(55*Units::deg),-2*ly),-10.0*Units::deg),Rotate(geom::Vec2(lx*cos(55*Units::deg),ly),-10.0*Units::deg));
  TiltGeometry tg1 = TiltGeometry(55*Units::deg,80*Units::deg);
  
  for(int i=0;i<=1;++i){
    for(int j=0;j<=1;++j){
      Lattice ltmp(pow(-1,j)*l1.GetFirst(),pow(-1,i)*l1.GetSecond());
      LatticeTiltGeometry tg2=CalcTiltFromSpatialLattice(ltmp,sc1);
      BOOST_CHECK_CLOSE(tg1.GetXAxisAngle(),tg2.GetXAxisAngle(),tolerance);
      BOOST_CHECK_CLOSE(tg1.GetTiltAngle(),tg2.GetTiltAngle(),tolerance);
    }
  }
}
BOOST_AUTO_TEST_CASE(tilt_reciprocal)
{
  std::ostringstream msg;
  
  ReciprocalUnitCell rc1(200,100,82*Units::deg);
  Real ly=100.0*sin(41*Units::deg);
  Real lx=100.0*cos(41*Units::deg);
  Lattice l1(Rotate(geom::Vec2(2*lx/cos(55*Units::deg),-2*ly),-10.0*Units::deg),Rotate(geom::Vec2(lx/cos(55*Units::deg),ly),-10.0*Units::deg));
  TiltGeometry tg1 = TiltGeometry(55*Units::deg,80*Units::deg);
  
  for(int i=0;i<=1;++i){
    for(int j=0;j<=1;++j){
      Lattice ltmp(pow(-1,j)*l1.GetFirst(),pow(-1,i)*l1.GetSecond());
      LatticeTiltGeometry tg2=CalcTiltFromReciprocalLattice(ltmp,rc1);
      BOOST_CHECK_CLOSE(tg1.GetXAxisAngle(),tg2.GetXAxisAngle(),tolerance);
      BOOST_CHECK_CLOSE(tg1.GetTiltAngle(),tg2.GetTiltAngle(),tolerance);
    }
  }
  ReciprocalUnitCell ruc(100.0,160.0,90*Units::deg);
  Lattice lat1(geom::Vec2(10.0,0),Vec2(0.0,16.0/cos(30.0*Units::deg)));
  LatticeTiltGeometry tg3=CalcTiltFromReciprocalLattice(lat1,ruc);
  // TODO fix
  BOOST_CHECK_CLOSE(tg3.GetTiltAngle(),30.0*Units::deg,tolerance);    
  BOOST_CHECK_SMALL(tg3.GetXAxisAngle(),tolerance);    

  Lattice lat2(geom::Vec2(10.0/cos(15.0*Units::deg),0),Vec2(0.0,16.0));
  LatticeTiltGeometry tg4=CalcTiltFromReciprocalLattice(lat2,ruc);
  BOOST_CHECK_CLOSE(tg4.GetTiltAngle(),15.0*Units::deg,tolerance);    
  BOOST_CHECK_CLOSE(90.0*Units::deg,-tg4.GetXAxisAngle(),tolerance);    

  SpatialUnitCell suc2(100.0,100.0,120*Units::deg);
  Lattice lat3(geom::Vec2(10.0,-10.0),Vec2(10.0,10.0));
  LatticeTiltGeometry tg5=CalcTiltFromReciprocalLattice(lat3,suc2);
  Real ta=acos(tan(30.0*Units::deg));
  BOOST_CHECK_CLOSE(tg5.GetTiltAngle(),ta,tolerance);    
  BOOST_CHECK_SMALL(tg5.GetXAxisAngle(),tolerance);    
}

BOOST_AUTO_TEST_CASE(tilt_info)
{
  ost::info::InfoHandle ih = ost::info::LoadInfo("test_info.xml");

  TiltGeometry tg = InfoToTiltGeometry(ih.Root().GetGroup("test"));
  
  BOOST_CHECK_CLOSE(tg.GetTiltAngle(),33.45*Units::deg,tolerance);
  BOOST_CHECK_CLOSE(tg.GetXAxisAngle(),-112.98*Units::deg,tolerance);

  ost::info::InfoGroup g2=ih.Root().CreateGroup("test2");
  TiltGeometryToInfo(tg,g2);

  TiltGeometry tg2 = InfoToTiltGeometry(g2);

  BOOST_CHECK(tg==tg2);
}

BOOST_AUTO_TEST_CASE(lattice_tilt_info)
{
  ost::info::InfoHandle ih = ost::info::LoadInfo("test_info.xml");

  LatticeTiltGeometry tg = InfoToLatticeTiltGeometry(ih.Root().GetGroup("test"));

  Lattice lat1(geom::Vec2(15.0,-1.9),Vec2(-12.4,33.1));
  Lattice lat2=tg.GetReciprocalUntiltedLattice();
  
  BOOST_CHECK_CLOSE(tg.GetTiltAngle(),33.45*Units::deg,tolerance);
  BOOST_CHECK_CLOSE(tg.GetXAxisAngle(),-112.98*Units::deg,tolerance);
  BOOST_CHECK_CLOSE(lat1.GetFirst()[0],lat2.GetFirst()[0],tolerance);
  BOOST_CHECK_CLOSE(lat1.GetFirst()[1],lat2.GetFirst()[1],tolerance);
  BOOST_CHECK_CLOSE(lat1.GetSecond()[0],lat2.GetSecond()[0],tolerance);
  BOOST_CHECK_CLOSE(lat1.GetSecond()[1],lat2.GetSecond()[1],tolerance);

  ost::info::InfoGroup g2=ih.Root().CreateGroup("test2");
  LatticeTiltGeometryToInfo(tg,g2);

  LatticeTiltGeometry tg2 = InfoToLatticeTiltGeometry(g2);

  BOOST_CHECK(tg==tg2);
}

BOOST_AUTO_TEST_SUITE_END()

