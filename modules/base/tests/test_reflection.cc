//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <sstream>
#include <iplt/reflection.hh>
#include <iplt/reflection_io.hh>
#include <iplt/reflection_exception.hh>

using namespace iplt;

extern const Real tolerance;

BOOST_AUTO_TEST_SUITE( iplt_base )


BOOST_AUTO_TEST_CASE(reflection_index)
{
  ReflectionIndex i1(10,20);
  BOOST_CHECK(i1.GetH()==10);
  BOOST_CHECK(i1.GetK()==20);
  BOOST_CHECK_SMALL(i1.GetZStar(),tolerance);
  BOOST_CHECK(Equal(i1.ToVec3(),Vec3(10.0,20.0,0.0)));
  BOOST_CHECK(i1.AsDuplet()==Point(10,20,0));
  BOOST_CHECK(-i1==ReflectionIndex(-10,-20));

  ReflectionIndex i2(11,21,-3.5);
  BOOST_CHECK(i2.GetH()==11);
  BOOST_CHECK(i2.GetK()==21);
  BOOST_CHECK_CLOSE(i2.GetZStar(),-3.5,tolerance);
  BOOST_CHECK(Equal(i2.ToVec3(),Vec3(11.0,21.0,-3.5)));
  BOOST_CHECK(i2.AsDuplet()==Point(11,21,0));

  ReflectionIndex i3(i1,7.3);
  BOOST_CHECK(i3.GetH()==10);
  BOOST_CHECK(i3.GetK()==20);
  BOOST_CHECK_CLOSE(i3.GetZStar(),7.3,tolerance);
  BOOST_CHECK(Equal(i3.ToVec3(),Vec3(10.0,20.0,7.3)));

  BOOST_CHECK(i1<i2);
  BOOST_CHECK(i1<i3);

  ReflectionIndex i4(i2);
  BOOST_CHECK(i4.GetH()==11);
  BOOST_CHECK(i4.GetK()==21);
  BOOST_CHECK_CLOSE(i4.GetZStar(),-3.5,tolerance);
  BOOST_CHECK(Equal(i4.ToVec3(),Vec3(11.0,21.0,-3.5)));
  BOOST_CHECK(i4.AsDuplet()==Point(11,21,0));

  std::ostringstream output;
  output << i3;
  BOOST_CHECK(output.str()=="( 10, 20,     7.3)");
}

BOOST_AUTO_TEST_CASE(reflection_list)
{
  ReflectionList rl;

  BOOST_CHECK(rl.GetPropertyCount()==0);

  BOOST_CHECK(!rl.HasProperty("amp"));

  rl.AddProperty("amp");

  BOOST_CHECK(rl.HasProperty("amp"));
  BOOST_CHECK(rl.GetPropertyCount()==1);

  ReflectionIndex i1(1,0,0.4);
  ReflectionIndex i2(2,1,1.2);
  ReflectionIndex i3(2,1,6.2);

  rl.AddReflection(i1);
  rl.AddReflection(i2);
  rl.AddReflection(i3);

  ReflectionProxyter rp = rl.Begin();

  BOOST_CHECK(rp.GetIndex()==i1);
  BOOST_CHECK_CLOSE(rp.GetIndex().GetZStar(),0.4,tolerance);
  BOOST_CHECK_SMALL(rp.Get("amp"),tolerance);
  rp.Set("amp",2.3);
  BOOST_CHECK_CLOSE(rp.Get("amp"),2.3,tolerance);

  ++rp;

  BOOST_CHECK(rp.GetIndex()==i2);
  BOOST_CHECK_CLOSE(rp.GetIndex().GetZStar(),1.2,tolerance);
  BOOST_CHECK_SMALL(rp.Get("amp"),tolerance);
  rp.Set("amp",4.1);
  BOOST_CHECK_CLOSE(rp.Get("amp"),4.1,tolerance);

  BOOST_CHECK(!rp.AtEnd());

  ++rp;
  ++rp;

  BOOST_CHECK(rp.AtEnd());

  BOOST_CHECK(rl.Find(i1).IsValid());
  BOOST_CHECK(rl.Find(ReflectionIndex(1,0,0.0)).IsValid()==false);

  ReflectionIndex i4(2,1,-3.3);
  rl.AddReflection(i4);

  rp = rl.FindFirst(ost::img::Point(2,1));
  BOOST_CHECK(rp.IsValid());
  BOOST_CHECK(rp.GetIndex()==i4);
  ++rp;
  BOOST_CHECK(rp.GetIndex()==i2);
  ++rp;
  BOOST_CHECK(rp.GetIndex()==i3);
}

BOOST_AUTO_TEST_CASE(refletion_io)
{
  class TestAlg1: public ReflectionNonModAlgorithm {
  public:
    TestAlg1(): n_(0) {}

    void Visit(const ReflectionList& rl) {
      for(ReflectionProxyter rp=rl.Begin();!rp.AtEnd();++rp) {
        n_++;
      }
    }

    int GetN() const {return n_;}
  private:
    int n_;
  };

  ReflectionList rl;
  for(int i=0;i<10;++i) {
    rl.AddReflection(ReflectionIndex(i,i));
  }
  TestAlg1 test_alg_1;
  rl.Apply(test_alg_1);
  BOOST_CHECK(test_alg_1.GetN()==10);
}

BOOST_AUTO_TEST_CASE(reflection_io)
{
  Lattice lat(geom::Vec2(3.0,4.0),Vec2(-2.0,1.0),Vec2(5.0,6.0));
  SpatialUnitCell cell(110.0,90.0,M_PI_2,200.0);
  ReflectionList rl1(cell,lat);
  
  rl1.AddProperty("AAA");
  rl1.AddProperty("BBB");
  rl1.SetPropertyType("BBB",PT_AMPLITUDE);
  
  BOOST_CHECK(rl1.GetPropertyType("AAA")==PT_REAL);
  BOOST_CHECK(rl1.GetPropertyType("BBB")==PT_AMPLITUDE);

  BOOST_REQUIRE(rl1.GetPropertyCount()==2);

  for(int h=0;h<4;++h) {
    for(int k=0;k<4;++k) {
      Real zstar=static_cast<Real>(h*4+k)/cell.GetC();
      ReflectionProxyter rp = rl1.AddReflection(ReflectionIndex(h,k,zstar));
      rp.Set("AAA",static_cast<Real>(-h));
      rp.Set("BBB",static_cast<Real>(-k));
    }
  }
  
  ExportMtz(rl1,"test.mtz");

  ReflectionList rl2=ImportMtz("test.mtz");

  BOOST_CHECK(rl2.GetLattice()==lat);

  BOOST_REQUIRE(rl2.HasProperty("AAA"));
  BOOST_REQUIRE(rl2.HasProperty("BBB"));
  BOOST_REQUIRE(rl2.HasProperty("aaa"));
  BOOST_REQUIRE(rl2.HasProperty("bBb"));

  for(ReflectionProxyter rp=rl2.Begin(); !rp.AtEnd(); ++rp) {
    int h = rp.GetIndex().GetH();
    int k = rp.GetIndex().GetK();
    BOOST_REQUIRE(rp.Get("AAA")==static_cast<Real>(-h));
    BOOST_REQUIRE(rp.Get("BBB")==static_cast<Real>(-k));
    BOOST_REQUIRE(rp.Get("aAa")==static_cast<Real>(-h));
    BOOST_REQUIRE(rp.Get("bbb")==static_cast<Real>(-k));
    Real zstar1=static_cast<Real>(h*4+k)/cell.GetC();
    Real zstar2=rp.GetIndex().GetZStar();
    BOOST_REQUIRE_CLOSE(zstar1,zstar2,tolerance);
  }

  try {
    ImportMtz("nonexistent.mtz");
  } catch (ReflectionException& e) {
    std::cerr << "CCP4 message is supposed to be printed!" << std::endl;
    // ok
  } catch(...) {
    BOOST_FAIL("Unknown exception encountered while trying to import nonexistent mtz file");
  }
}

BOOST_AUTO_TEST_CASE(reflection_reindex)
{
  // a wrong lattice where (0,1) is really (1,1)
  ReflectionList rl1(Lattice(geom::Vec2(2.0,0.0),Vec2(2.0,3.0)));

  rl1.AddProperty("X");
  rl1.AddProperty("Y");

  for(int ih=-2;ih<=2;++ih) {
    for(int ik=-2;ik<=2;++ik) {
      ReflectionProxyter rp = rl1.AddReflection(ReflectionIndex(ih,ik));
      rp.Set("X",static_cast<Real>(ih));
      rp.Set("Y",static_cast<Real>(ik));
    }
  }
  
  ReflectionList rl2 = ReIndex(rl1,Mat2(1,1, 0,1));

  BOOST_CHECK(rl2.GetLattice()==Lattice(geom::Vec2(2.0,0.0),Vec2(0.0,3.0)));

  for(ReflectionProxyter rp=rl2.Begin(); !rp.AtEnd(); ++rp) {
    int h = rp.GetIndex().GetH();
    int k = rp.GetIndex().GetK();
    int x = static_cast<int>(rp.Get("X"));
    int y = static_cast<int>(rp.Get("Y"));
    BOOST_REQUIRE(h==x+y);
    BOOST_REQUIRE(k==y);
  }
}

BOOST_AUTO_TEST_SUITE_END()

