//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <iplt/lattice.hh>
#include "vec_mat_predicates.hh"

using namespace iplt;

extern const Real tolerance;
extern boost::test_tools::predicate_result compare_Vec2(const Vec2&,const Vec2&);


BOOST_AUTO_TEST_SUITE( iplt_base )


BOOST_AUTO_TEST_CASE(lattice_calc_position)
{
  geom::Vec2 a(10.0,5.0);
  geom::Vec2 b(-15.0,9.0);
  Lattice lat(a,b,Vec2(0.0,0.0));

  ost::img::Point indx1(2,3);
  geom::Vec2 pos1(geom::Vec2(static_cast<Real>(indx1[0])*a+static_cast<Real>(indx1[1])*b));
  //BOOST_CHECK(compare_Vec2(lat.CalcPosition(indx1),pos1));
  BOOST_CHECK(vec2_is_close(lat.CalcPosition(indx1),pos1,tolerance));
}

BOOST_AUTO_TEST_CASE(lattice_calc_components)
{
  geom::Vec2 a(10.0,5.0);
  geom::Vec2 b(-15.0,9.0);
  geom::Vec2 o(-40.0,17.0);
  Lattice lat(a,b,o);

  geom::Vec2 r1=lat.CalcComponents(4.7*a+2.3*b+o);

  BOOST_CHECK_CLOSE(r1[0],4.7,tolerance);
  BOOST_CHECK_CLOSE(r1[1],2.3,tolerance);
}

BOOST_AUTO_TEST_SUITE_END()

