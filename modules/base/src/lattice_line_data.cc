//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/
#include <cmath>
#include <iostream>

#include "lattice_line_data.hh"

namespace iplt { 

LatticeLineData::LatticeLineData(const ReflectionList& rlist,String f_label, String phi_label):
  rlist_(rlist),
  f_label_(f_label),
  phi_label_(phi_label)
{
  if(f_label_==""){
    if(!rlist_.HasPropertyType(PT_AMPLITUDE)){
      throw("no amplitude data found");
    }
    f_label_=rlist_.GetPropertyNameByType(PT_AMPLITUDE);
  }
  if(phi_label_==""){
    if(rlist_.HasPropertyType(PT_PHASE)){
      phi_label_=rlist_.GetPropertyNameByType(PT_PHASE);
    }
  }
}

Real LatticeLineData::sinc(Real x) const
{
  return x==0.0 ? 1.0 : sin(x)/x;
}
Real LatticeLineData::GetAmplitude(ReflectionIndex ri) const
{
  return std::abs<Real>(Get(ri));
}
Real LatticeLineData::GetPhase(ReflectionIndex ri) const
{
  return std::arg<Real>(Get(ri));
}
Complex LatticeLineData::Get(ReflectionIndex ri) const
{
  Real c=rlist_.GetUnitCell().GetC();
  ost::img::Point hkindex=ri.AsDuplet();
  ReflectionProxyter rp=rlist_.FindFirst(hkindex);
  Complex result(0.0,0.0);
  if(phi_label_==""){
    while(!rp.AtEnd() && rp.GetIndex().AsDuplet()==hkindex){
      result+=Complex(rp.Get(f_label_),0.0)*sinc(c*M_PI*(ri.GetZStar()-rp.GetIndex().GetZStar()));
      ++rp;
    }
  }else{
    while(!rp.AtEnd() && rp.GetIndex().AsDuplet()==hkindex){
      result+=Complex(rp.Get(f_label_),rp.Get(phi_label_))*sinc(c*M_PI*(ri.GetZStar()-rp.GetIndex().GetZStar()));
      ++rp;
    }
  }
  return result;
}

LatticeLineData::~LatticeLineData()
{
}

} //ns
