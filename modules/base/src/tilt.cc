//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Tilt Geometry

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <ost/message.hh>
#include <ost/info/info.hh>
#include <ost/log.hh>
#include <ost/units.hh>

#include "tilt.hh"

namespace iplt { 

namespace {
Real force_angle_range(Real angle, Real min, Real max)
{
  Real diff=max-min;
  while(angle>max){
    angle-=diff;
  }
  while(angle<min){
    angle+=diff;
  }
  return angle;
}

}//ns

/////////////////////////////////////
// TiltGeometry member functions

TiltGeometry::TiltGeometry():
  tilt_angle_(0.0),
  x_axis_angle_(0.0)
{
}
TiltGeometry::TiltGeometry(Real tilt_angle, Real x_axis_angle):
  tilt_angle_(0.0),
  x_axis_angle_(0.0)
{
  SetXAxisAngle(x_axis_angle);
  SetTiltAngle(tilt_angle);
}
Real TiltGeometry::GetTiltAngle(void) const
{
  return tilt_angle_;
}
void TiltGeometry::SetTiltAngle(Real ta)
{
  if(ta>M_PI/2.0 || ta<-M_PI/2.0){
    throw("invalid tilt angle");
  }else{
    if(ta<0.0){
      SetXAxisAngle(GetXAxisAngle()+M_PI);
      ta=-ta;
    }
    tilt_angle_=ta;
  }
}
Real TiltGeometry::GetXAxisAngle(void) const
{
  return x_axis_angle_;
}
void TiltGeometry::SetXAxisAngle(Real xaa)
{
  x_axis_angle_=force_angle_range(xaa,-M_PI,M_PI);
}
Lattice TiltGeometry::ApplyReciprocal(const Lattice& untilted_lattice) const
{
  return  Lattice(ApplyReciprocal(untilted_lattice.GetFirst()),ApplyReciprocal(untilted_lattice.GetSecond()));
}
Lattice TiltGeometry::ApplySpatial(const Lattice& untilted_lattice) const
{
  return  Lattice(ApplySpatial(untilted_lattice.GetFirst()),ApplySpatial(untilted_lattice.GetSecond()));
}

geom::Vec2 TiltGeometry::ApplyReciprocal(const geom::Vec2& untilted_vec) const
{
  return geom::Vec2(GetReciprocalTransformation()*Vec3(untilted_vec));
}

geom::Vec2 TiltGeometry::ApplySpatial(const geom::Vec2& untilted_vec) const
{
  return geom::Vec2(GetSpatialTransformation()*Vec3(untilted_vec));
}

Mat3 TiltGeometry::GetSpatialTransformation() const
{
  Real cosa = cos(tilt_angle_);
  Real sina = sin(tilt_angle_);
  Real cosb = cos(x_axis_angle_);
  Real sinb = sin(x_axis_angle_);

  Mat3 nrvo=Mat3(cosb,-sinb,0, sinb,cosb,0.0, 0.0,0.0,1.0)*
    Mat3(1.0,0.0,0.0, 0.0,cosa,-sina, 0.0,sina,cosa)*
    Mat3(cosb,sinb,0, -sinb,cosb,0.0, 0.0,0.0,1.0);
  return nrvo;
}

Mat3 TiltGeometry::GetReciprocalTransformation() const
{
  Real cosa = cos(tilt_angle_);
  Real tana = tan(tilt_angle_);
  Real cosb = cos(x_axis_angle_);
  Real sinb = sin(x_axis_angle_);

  Mat3 nrvo=Mat3(cosb,-sinb,0.0, sinb,cosb,0.0, 0.0,0.0,1.0)*
    Mat3(1.0,0.0,0.0, 0.0,1.0/cosa,0.0, 0.0,tana,1.0)*
    Mat3(cosb,sinb,0.0, -sinb,cosb,0.0, 0.0,0.0,1.0);
  return nrvo;
}

bool TiltGeometry::operator==(const TiltGeometry& tg)
{
  return std::fabs(tilt_angle_-tg.tilt_angle_)<1e-5 && 
    std::fabs(x_axis_angle_-tg.x_axis_angle_)<1e-5;
}


/////////////////////////////////////
// LatticeTiltGeometry member functions

LatticeTiltGeometry::LatticeTiltGeometry(Real tilt_angle, Real x_axis_angle, const Lattice& untilted_lattice, bool is_reciprocal):
  TiltGeometry(tilt_angle,x_axis_angle),
  x_astar_angle_(0.0),
  x_bstar_angle_(0.0),
  astar_length_(0.0),
  bstar_length_(0.0)
{
  if(is_reciprocal)
  {
    x_astar_angle_=atan2(untilted_lattice.GetFirst()[1],untilted_lattice.GetFirst()[0]);
    x_bstar_angle_=atan2(untilted_lattice.GetSecond()[1],untilted_lattice.GetSecond()[0]);
    astar_length_=Length(untilted_lattice.GetFirst());
    bstar_length_=Length(untilted_lattice.GetSecond());
  }else{
    Lattice ilat=InvertLattice(untilted_lattice);
    x_astar_angle_=atan2(ilat.GetFirst()[1],ilat.GetFirst()[0]);
    x_bstar_angle_=atan2(ilat.GetSecond()[1],ilat.GetSecond()[0]);
    astar_length_=Length(ilat.GetFirst());
    bstar_length_=Length(ilat.GetSecond());
  }
}

LatticeTiltGeometry::LatticeTiltGeometry(const Lattice& tilted_reciprocal_lattice, const ReciprocalUnitCell& uc):
  x_astar_angle_(0.0),
  x_bstar_angle_(0.0),
  astar_length_(0.0),
  bstar_length_(0.0)
{
  calc_tilt_shaw(tilted_reciprocal_lattice,uc);
}
LatticeTiltGeometry::LatticeTiltGeometry(const Lattice& tilted_spatial_lattice, const SpatialUnitCell& uc):
  x_astar_angle_(0.0),
  x_bstar_angle_(0.0),
  astar_length_(0.0),
  bstar_length_(0.0)
{
  calc_tilt_shaw(InvertLattice(tilted_spatial_lattice),ReciprocalUnitCell(uc));
}

Real LatticeTiltGeometry::GetAStarVectorAngle() const
{
  return GetXAxisAngle()-x_astar_angle_;
}

void LatticeTiltGeometry::SetAStarVectorAngle(Real ava)
{
  Real diffangle=ava-GetAStarVectorAngle();
  x_astar_angle_+=diffangle;
  x_bstar_angle_+=diffangle;
}

Real LatticeTiltGeometry::GetTiltedAStarVectorAngle() const
{
  return atan(tan(GetAStarVectorAngle())/cos(GetTiltAngle()));
}

void LatticeTiltGeometry::SetTiltedAStarVectorAngle(Real tava)
{
  SetAStarVectorAngle(atan(tan(tava)*cos(GetTiltAngle())));
}
  
Real LatticeTiltGeometry::GetAVectorAngle() const
{
  return GetXAxisAngle()-(x_bstar_angle_+M_PI);
}

void LatticeTiltGeometry::SetAVectorAngle(Real ava)
{
  Real diffangle=ava-GetAVectorAngle();
  x_astar_angle_+=diffangle;
  x_bstar_angle_+=diffangle;
}

Real LatticeTiltGeometry::GetTiltedAVectorAngle() const
{
  return atan(tan(GetAVectorAngle())*cos(GetTiltAngle()));
}

void LatticeTiltGeometry::SetTiltedAVectorAngle(Real tava)
{
  SetAVectorAngle(atan(tan(tava)/cos(GetTiltAngle())));
}

Lattice LatticeTiltGeometry::GetReciprocalUntiltedLattice() const
{
  return Lattice(geom::Vec2(cos(x_astar_angle_)*astar_length_,sin(x_astar_angle_)*astar_length_),
                 geom::Vec2(cos(x_bstar_angle_)*bstar_length_,sin(x_bstar_angle_)*bstar_length_));
}

Lattice LatticeTiltGeometry::GetReciprocalTiltedLattice() const
{
  return ApplyReciprocal(GetReciprocalUntiltedLattice());
}

Lattice LatticeTiltGeometry::GetSpatialUntiltedLattice() const
{
  return InvertLattice(GetReciprocalUntiltedLattice());
}

Lattice LatticeTiltGeometry::GetSpatialTiltedLattice() const
{
  return ApplySpatial(GetSpatialUntiltedLattice());
}

Real LatticeTiltGeometry::GetZStar(const ost::img::Point& p) const
{
  return static_cast<Real>(p[0])*astar_length_*sin(GetAStarVectorAngle())*tan(GetTiltAngle())
        +static_cast<Real>(p[1])*bstar_length_*sin(GetXAxisAngle()-x_bstar_angle_)*tan(GetTiltAngle());
}

Real LatticeTiltGeometry::CalcReciprocalSampling(const Lattice& lat)
{
  Real sinxi = sin(GetAStarVectorAngle());
  Real tana = tan(GetTiltAngle());
  return astar_length_*sqrt(1.0+sinxi*sinxi*tana*tana)/Length(lat.GetFirst());
}

void LatticeTiltGeometry::calc_tilt_shaw(const Lattice& lat, const ReciprocalUnitCell& ref)
{
  static Real epsilon=1e-5, phi=0.0,theta=0.0, phitilt=0.0;
  
  //sign of phi is dependent on handedness of the two lattice vectors
  Real handedness_sign= Cross(Vec3(lat.GetFirst()),Vec3(lat.GetSecond()))[2]>=0?-1.0:1.0;
  Real angle_convention_sign=Dot(lat.GetFirst(),lat.GetSecond())<0.0?1.0:-1.0;
  Real gammasign=angle_convention_sign*handedness_sign*ref.GetGamma();
  
  // taking the absolute of the dot product ensures that the angle between the two vectors is smaller than 90 deg
  Real dottilt=std::fabs(Dot(lat.GetFirst(),lat.GetSecond()));
  Real dot=ref.GetA()*ref.GetB()*cos(ref.GetGamma());

  Real c1=ref.GetA()*ref.GetA()/Length2(lat.GetFirst());
  Real c2=ref.GetB()*ref.GetB()/Length2(lat.GetSecond());

  if(dottilt/Length(lat.GetFirst())/Length(lat.GetSecond())<epsilon){ // gammatilt==90
    if(cos(ref.GetGamma())<epsilon){ // gamma==90
      if(c1>c2) {// tilt axis along a
        phi=0.0;
        phitilt=0.0;
        Real k=ref.GetA()/Length(lat.GetFirst());
        theta=acos(ref.GetB()/(k*Length(lat.GetSecond()))); 
      }else{ //tilt axis along b
        phi=M_PI/2.0;
        phitilt=M_PI/2.0;
        Real k=ref.GetB()/Length(lat.GetSecond());
        theta=acos(ref.GetA()/(k*Length(lat.GetFirst()))); 
      }
    }else{
      Real A=c1;
      Real B=c1-c2;
      Real C=-c2*cos(gammasign)*cos(gammasign);
      Real d=B*B-4*A*C;
      assert(d>=0);
      Real psq1=(-B+sqrt(d))/(2.0*A);
      Real psq2=(-B-sqrt(d))/(2.0*A);
      Real p=sqrt(std::max<Real>(psq1,psq2)); 
      Real q = -cos(ref.GetGamma())/p;
      phi = atan2(sin(gammasign),q/p-cos(gammasign));
      theta = atan(p/sin(phi));
      phitilt=atan2(sin(gammasign),cos(theta)*(q/p-cos(gammasign)));
    }
  }else{
    Real c3=ref.GetA()*ref.GetB()/dottilt;
    Real c4=dot/dottilt;
    Real A=c2*c1/c3*c1/c3-c1;
    Real B=c2+2*c2*c1/c3*(c1-c4)/c3-c1;
    Real C=c2*(c1-c4)/c3*(c1-c4)/c3;
    Real d=B*B-4*A*C;
    assert(d>=0);
    Real psq1=(-B+sqrt(d))/(2.0*A);
    Real psq2=(-B-sqrt(d))/(2.0*A);
    Real p=sqrt(std::max<Real>(psq1,psq2)); 
    Real q = (c1*(1+p*p)-c4)/(c3*p);
    phi = atan2(sin(gammasign),q/p-cos(gammasign));
    theta = atan(p/sin(phi));
    phitilt=atan2(sin(gammasign),cos(theta)*(q/p-cos(gammasign)));
  }
  LOG_TRACE( "phitilt:" << phitilt/Units::deg <<  " phi:" << phi/Units::deg << " theta:" << theta/Units::deg );
  // This version of Shaw's algorithm always returns a positive tilt angle and a X-axisangle between -pi/2 and pi/2
  SetTiltAngle(std::fabs(theta));
  Real axisangle=atan2(lat.GetFirst()[1],lat.GetFirst()[0])-phitilt;
  axisangle=force_angle_range(axisangle,-M_PI/2.0,M_PI/2.0);
  SetXAxisAngle(axisangle);
  x_astar_angle_=GetXAxisAngle()-phi;
  x_bstar_angle_=x_astar_angle_+handedness_sign*ref.GetGamma();
  astar_length_=ref.GetA();
  bstar_length_=ref.GetB();
}

/////////////////////////////////////
// stand-alone functions for TG


LatticeTiltGeometry CalcTiltFromSpatialLattice(const Lattice& lat, const SpatialUnitCell& ref)
{
  return LatticeTiltGeometry(lat,ref);
}

LatticeTiltGeometry CalcTiltFromReciprocalLattice(const Lattice& lat, const ReciprocalUnitCell& ref)
{
  return LatticeTiltGeometry(lat,ref);
}

TiltGeometry InfoToTiltGeometry(const ost::info::InfoGroup& g)
{
  if(!g.HasGroup("TiltGeometry")) {
    throw ost::Error("no TiltGeometry found in given info group");
  }

  ost::info::InfoGroup g2=g.GetGroup("TiltGeometry");
  return TiltGeometry(g2.GetItem("TiltAngle").AsFloat()*M_PI/180.0,
                      g2.GetItem("XAxisAngle").AsFloat()*M_PI/180.0);

}

void TiltGeometryToInfo(const TiltGeometry& tg, ost::info::InfoGroup g)
{
  ost::info::InfoGroup g2=g.RetrieveGroup("TiltGeometry");
  ost::info::InfoItem itm = g2.RetrieveItem("TiltAngle");
  itm.SetFloat(tg.GetTiltAngle()*180.0/M_PI);
  itm = g2.RetrieveItem("XAxisAngle");
  itm.SetFloat(tg.GetXAxisAngle()*180.0/M_PI);
}

LatticeTiltGeometry InfoToLatticeTiltGeometry(const ost::info::InfoGroup& g)
{
  if(!g.HasGroup("LatticeTiltGeometry")) {
    throw ost::Error("no LatticeTiltGeometry found in given info group");
  }

  ost::info::InfoGroup g2=g.GetGroup("LatticeTiltGeometry");
  Lattice lat;
  if(g2.HasGroup("Lattice")) {
    lat = InfoToLattice(g2);
  }
  return LatticeTiltGeometry(g2.GetItem("TiltAngle").AsFloat()*M_PI/180.0,
			     g2.GetItem("XAxisAngle").AsFloat()*M_PI/180.0,
			     lat);
}

void LatticeTiltGeometryToInfo(const LatticeTiltGeometry& tg, ost::info::InfoGroup g)
{
  ost::info::InfoGroup g2=g.RetrieveGroup("LatticeTiltGeometry");
  ost::info::InfoItem itm = g2.RetrieveItem("TiltAngle");
  itm.SetFloat(tg.GetTiltAngle()*180.0/M_PI);
  itm = g2.RetrieveItem("XAxisAngle");
  itm.SetFloat(tg.GetXAxisAngle()*180.0/M_PI);
  LatticeToInfo(tg.GetReciprocalUntiltedLattice(),g2);
}

geom::Vec2 CalcTiltComponents(const TiltGeometry& tg, const Lattice& lat, const ost::img::Point& index)
{
  Real beta=tg.GetXAxisAngle();
  Real cosb=cos(beta);
  Real sinb=sin(beta);
  geom::Vec2 vindex=lat.CalcPosition(index)-lat.GetOffset();
  geom::Vec2 nrvo(cosb*vindex[0]+sinb*vindex[1],    // parallel component
      -sinb*vindex[0]+cosb*vindex[1]);  // perpendicular component
  return nrvo;
}

DLLEXPORT_IPLT_BASE std::pair<geom::Vec2,geom::Vec2> CalcTiltComponentVectors(const TiltGeometry& tg, const Lattice& lat, const ost::img::Point& index)
{
  Real beta=tg.GetXAxisAngle();
  Real cosb=cos(beta);
  Real sinb=sin(beta);
  geom::Vec2 vindex=lat.CalcPosition(index)-lat.GetOffset();
  // nrvo in std::make_pair
  return std::make_pair((cosb*vindex[0]+sinb*vindex[1])*geom::Vec2(cosb,sinb),
      (-sinb*vindex[0]+cosb*vindex[1])*geom::Vec2(-sinb,cosb));
}

/*
  derivation:

     /|
    / | z* = x tan(a)
   /a |
   ---
    x = A* sin(xi)  
    (xi is angle between tilt axis and a* vector, corresponding tri not drawn)

     /|
 A*'/ | z*
   /  |
   ---
    A*

    A'*^2 = A*^2 + z*^2
          = A*^2 (1+sin^2(xi) tan^2(a)
*/

std::ostream& operator<<(std::ostream& o, const TiltGeometry& g)
{
  o <<"tilt_angle: " << g.GetTiltAngle()/Units::deg << " x_axis_angle: " << g.GetXAxisAngle()/Units::deg;
  return o;
}

std::ostream& operator<<(std::ostream& o, const LatticeTiltGeometry& g)
{
  o << TiltGeometry(g) << " x_astar_angle: " << g.x_astar_angle_/Units::deg << " x_bstar_angle: " << g.x_bstar_angle_/Units::deg << " astar_length: " << g.astar_length_ << " bstar_length: " << g.bstar_length_;
  return o;
}


} //ns
