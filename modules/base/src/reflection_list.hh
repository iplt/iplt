//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  main interface for reflection lists

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#ifndef IPLT_EX_REFLECTION_LIST_H
#define IPLT_EX_REFLECTION_LIST_H

#include <ost/base.hh>

#include "reflection_index.hh"
#include "reflection_proxyter.hh"
#include "reflection_list_impl.hh"
#include "reflection_common.hh"
#include "lattice.hh"

namespace iplt { 

// forward declarations
class ReflectionNonModAlgorithm;
class ReflectionModIPAlgorithm;
class ReflectionModOPAlgorithm;
class ReflectionConstModIPAlgorithm;
class ReflectionConstModOPAlgorithm;

//! Container for reflections
/*!
  The ReflectionList contains, as the name suggests, a list
  of reflections, each identified by a unique ReflectionIndex. 
  The list maintains a strict ordering by using 
  ReflectionIndoperator<(), reflected in the order of
  the ReflectionProxyter increment.

  Properties of the ReflectionList allow a value per reflection
  to be stored via the ReflectionProxyter interface.

  A ReflectionList contains a lattice and a unit cell. The former 
  is usually in (fractional) pixel dimensions, referring to the
  image the reflections were originally extracted from. The latter
  represents the unit cell of the dataset. Neither of these two
  is mandatory, however, the conversion from zstar values stored
  in the reflection indices (see iplt::ReflectionIndex) to
  fractional l stored in the mtz file during Export uses the
  thickness (3rd dimension) of the spatial unit cell!
*/
/*
  The reflection list should contain some sort of index2frequency
  conversion factor that allows the pysical frequency to be obtained
  from the indices.
*/
class DLLEXPORT_IPLT_BASE ReflectionList {
public:

  //! Create empty reflection list from scratch
  ReflectionList();
  //! Create reflection list with lattice
  ReflectionList(const Lattice& lat);
  //! Create reflection list with unit cell
  ReflectionList(const SpatialUnitCell& cell);
  //! Create reflection list with unit cell and lattice
  ReflectionList(const Lattice& lat, const SpatialUnitCell& cell);
  //! Create reflection list with unit cell and lattice
  ReflectionList(const SpatialUnitCell& cell, const Lattice& lat);

  
  //! copy ctor
  ReflectionList(const ReflectionList& l);

  //! pseudo copy ctor
  /*!
    the boolean copy parameter determines wether the
    actual reflection list entries are copied, or
    only the skeleton, ie the lattice, the unit-cell
    and the properties
   */
  ReflectionList(const ReflectionList& l, bool copy);

  //! assignement op
  ReflectionList& operator=(const ReflectionList& l);

  //! reset this list and copy properties from given one
  /*!
    This does not modify the lattice
  */
  void CopyProperties(const ReflectionList& l);

  //! Add property to all reflections in list, with default type Real
  unsigned int AddProperty(const String& prop);

  //! Deletes property from all reflections in list
  void DeleteProperty(const String& prop);

  //! Add property to all reflections in list, with given type
  unsigned int AddProperty(const String& prop, PropertyType type);

  //! Check if specific property type is present
  bool HasPropertyType(PropertyType ptype) const;
  
  //! Retrieve Property name by type
  String GetPropertyNameByType(PropertyType ptype) const;

  //! Check if specific property is present
  bool HasProperty(const String& prop) const;

  //! Rename property of all reflections in list
  void RenameProperty(const String& from, const String& to);

  //! Set Property type
  void SetPropertyType(const String& prop, PropertyType type);

  //! Retrieve Property type by name
  PropertyType GetPropertyType(const String& prop) const;

  //! Retrieve Property index by name
  unsigned int GetPropertyIndex(const String& name) const;

  //! Number of properties
  unsigned int GetPropertyCount() const;
  
  //! Retrieve Property by index
  String GetPropertyName(int n) const;

  //! Retrieve Property type by index
  PropertyType GetPropertyType(int n) const;

  std::vector<String> GetPropertyList() const;

  //! Add new reflection based on given index
  /*!
    There can be multiple reflections with the same index
  */
  ReflectionProxyter AddReflection(const ReflectionIndex& indx);

  //! Add new reflection based on given index, take property values from other proxyter
  /*!
  */
  ReflectionProxyter AddReflection(const ReflectionIndex& indx, const ReflectionProxyter& rp);

  //! Add proxyter from another reflection list
  /*!
    Properties that are defined in this list and the
    other one are taken, properties only defined in
    this list default to zero, and properties only 
    present in the other are ignored.
  */
  ReflectionProxyter AddProxyter(const ReflectionProxyter& prox);

  //! like add, but implicit assumption that properties are identical
  ReflectionProxyter CopyProxyter(const ReflectionProxyter& prox);

  //! Add reflections from another reflection list
  /*!
    Properties that are defined in this list and the
    other one are taken, properties only defined in
    this list default to zero, and properties only 
    present in the other are ignored.
  */
  void Merge(const ReflectionList& list);
  
  //! Return first reflection
  /*!
    This is the main method to initialize an iteration
    over all reflections in the list.
  */
  ReflectionProxyter Begin() const;

  //! Find reflection with specified index
  /*!
    If a reflection with this index is not present, an
    invalid proxyter is returned. This match is _exact_,
    ie the floating point value of zstar must match as well
  */
  ReflectionProxyter Find(const ReflectionIndex& i) const;

  //! Find the first reflection with the given hk duplet
  ReflectionProxyter FindFirst(const ost::img::Point& hk) const;
  

  //! Return number of reflections
  unsigned int NumReflections() const;

  //! Remove all reflection entries
  void ClearReflections();

  //! dump to stdout
  String DumpFormatted() const;
  String Dump() const;
  String DumpHeader() const;

  //! Set lattice
  void SetLattice(const Lattice& l);
  //! Retrieve lattice
  Lattice GetLattice() const;

  //! Set unit cell
  void SetUnitCell(const SpatialUnitCell& cell);

  //! Retrieve unit cell
  SpatialUnitCell GetUnitCell() const;

  //! Set symmetry
  void SetSymmetry(const String& sym);

  //! Set symmetry
  void SetSymmetry(const Symmetry& sym);

  //! Retrieve symmetry
  Symmetry GetSymmetry() const;

  // algorithm interface
  void Apply(ReflectionNonModAlgorithm& a) const;
  void ApplyIP(ReflectionNonModAlgorithm& a) const {Apply(a);};

  ReflectionList Apply(ReflectionModIPAlgorithm& a) const;
  void ApplyIP(ReflectionModIPAlgorithm& a);

  ReflectionList Apply(const ReflectionConstModIPAlgorithm& a) const;
  void ApplyIP(const ReflectionConstModIPAlgorithm& a);

  ReflectionList Apply(ReflectionModOPAlgorithm& a) const;
  void ApplyIP(ReflectionModOPAlgorithm& a);

  ReflectionList Apply(const ReflectionConstModOPAlgorithm& a) const;
  void ApplyIP(const ReflectionConstModOPAlgorithm& a);

  //! swap internals
  void Swap(ReflectionList&);

  //! get resolution of corresponding ReflectionIndex
  Real GetResolution(const ReflectionIndex& idx) const;

private:
  reflection_detail::ListPtr lptr_;
};

//! Re-index the given reflection list based on a 2D matrix
/*!
  The lattice vectors will be corrected accordingly
*/
DLLEXPORT_IPLT_BASE ReflectionList ReIndex(const ReflectionList& r, const Mat2& m);

//! Re-index the given reflection list based on a 3D matrix
/*!
  The lattice vectors will not be corrected!!
*/
DLLEXPORT_IPLT_BASE ReflectionList ReIndex(const ReflectionList& r, const Mat3& m);

//! Shift the given reflection list based on a 2D vector
/*!
  The lattice offset will be corrected accordingly
*/
DLLEXPORT_IPLT_BASE ReflectionList ReIndex(const ReflectionList& r, const geom::Vec2& v);

//! Shift the given reflection list based on a 3D vector
/*!
  The lattice offset will not be corrected!!
*/
DLLEXPORT_IPLT_BASE ReflectionList ReIndex(const ReflectionList& r, const geom::Vec3& v);


} //ns

#endif
