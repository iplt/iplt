//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include "microscope_data.hh"

namespace iplt { 

MicroscopeData::MicroscopeData(Real cs,Real cc, Real ca,Real de,Real alpha,Real kv):
  cs_(cs),
  kv_(kv),
  cc_(cc),
  ca_(ca),
  alpha_(alpha),
  de_(de),
  lambda_(0.0)
{
  calcwavelength();
}

void MicroscopeData::calcwavelength() {
  lambda_=h_/sqrt(2*m_*e_*kv_+ e_*kv_/c_*e_*kv_/c_);
}

const Real MicroscopeData::h_=6.626e-34*ost::Units::J*ost::Units::s;
const Real MicroscopeData::m_=9.110e-31*ost::Units::kg;
const Real MicroscopeData::e_=1.602e-19*ost::Units::C;
const Real MicroscopeData::c_=3.0e8*ost::Units::m/ost::Units::s;


void MicroscopeDataToInfo(const MicroscopeData& md, ost::info::InfoGroup& ig)
{
  using namespace ost::info;
  InfoGroup mdgroup=ig.CreateGroup("MicroscopeData");
  InfoItem cs=mdgroup.CreateItem("cs","");
  cs.SetFloat(md.GetSphericalAberration());
  InfoItem kv=mdgroup.CreateItem("kv","");
  kv.SetFloat(md.GetAccelerationVoltage());
  InfoItem cc=mdgroup.CreateItem("cc","");
  cc.SetFloat(md.GetChromaticAberration());
  InfoItem ca=mdgroup.CreateItem("ca","");
  ca.SetFloat(md.GetAmplitudeContrast());
  InfoItem alpha=mdgroup.CreateItem("alpha","");
  alpha.SetFloat(md.GetConvergenceAngle());
  InfoItem de=mdgroup.CreateItem("de","");
  de.SetFloat(md.GetEnergySpread());
}

MicroscopeData InfoToMicroscopeData(const ost::info::InfoGroup& ig)
{
  using namespace ost::info;
  MicroscopeData result;
  InfoGroup mdgroup=ig.GetGroup("MicroscopeData");
  result.SetSphericalAberration(mdgroup.GetItem("cs").AsFloat());
  result.SetAccelerationVoltage(mdgroup.GetItem("kv").AsFloat());
  result.SetChromaticAberration(mdgroup.GetItem("cc").AsFloat());
  result.SetAmplitudeContrast(mdgroup.GetItem("ca").AsFloat());
  result.SetConvergenceAngle(mdgroup.GetItem("alpha").AsFloat());
  result.SetEnergySpread(mdgroup.GetItem("de").AsFloat());
  return result;
}

} //ns
    
