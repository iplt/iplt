//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  ReflectionList IO implementation

  Author: Ansgar Philippsen, Andreas Schenk
*/

#include "reflection_io.hh"

#include <cstring>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>

#include<boost/tokenizer.hpp>
#include<boost/lexical_cast.hpp>
#include<boost/variant.hpp>

#include <ost/message.hh>
#include <ost/units.hh>
#include <ost/log.hh>

#include "reflection.hh"
#include "mtzlib/mtzlib.hh"
#include "symmetry_exception.hh"

// define this to get the old multi xtal entry export behavior
// the new behavior is to write a second dataset with the base xtal
//#define IPLT_MTZ_MULTIXTAL_EXPORT

namespace iplt { 

////////////////////////////////
// MTZ import export code

using namespace CCP4;
using namespace CMtz;
using namespace CSym;

namespace {
class MTZPtr {
public:
  MTZPtr(MTZ* p):
    p_(p) {
    if(!p) throw ReflectionException("NULL mtz pointer");
  }
  ~MTZPtr() {
    MtzFree(p_);
  }

  operator MTZ*() {
    return p_;
  }

  MTZ* operator->() {
    return p_;
  }

private:
  MTZ* p_;
};

class MTZBATPtr {
public:
  MTZBATPtr(MTZBAT* p):
    p_(p) {}
  ~MTZBATPtr() {
    //MtzFreeBatch(p_);
  }

  operator MTZBAT*() {
    return p_;
  }

  MTZBAT* operator->() {
    return p_;
  }

private:
  MTZBAT* p_;
};

void init_batch(MTZBATPtr& bat, const Lattice& lat)
{
  MTZBAT* p=bat;
  memset(p,0,sizeof(MTZBAT));
  bat->num=1;
  bat->title[0]='\0';
  bat->next=0;
  bat->nbsetid=0;

  // the batch is abused to store the lattice!
  bat->e1[0]=static_cast<float>(lat.GetFirst()[0]);
  bat->e1[1]=static_cast<float>(lat.GetFirst()[1]);
  bat->e2[0]=static_cast<float>(lat.GetSecond()[0]);
  bat->e2[1]=static_cast<float>(lat.GetSecond()[1]);
  bat->e3[0]=static_cast<float>(lat.GetOffset()[0]);
  bat->e3[1]=static_cast<float>(lat.GetOffset()[1]);
  bat->e1[2]=static_cast<float>(lat.GetBarrelDistortion());
  bat->e2[2]=static_cast<float>(lat.GetSpiralDistortion());
}

} // ns


ReflectionList ImportMtz(const String& file)
{
  //  open mtz file for reading, and load it into into memory
  LOG_VERBOSE( "ImportMtz: reading " << file );
  MTZ * mtzptr =MtzGet(file.c_str(), 1);
  if(!mtzptr){
    throw ReflectionException("MTZ file could not be read: "+file);
  }
  MTZPtr mtzin(mtzptr); 

  // retrieve lattice from abused batch header
  MTZBAT* mtzbatch = mtzin->batch;
  Lattice lattice;
  if(mtzbatch) {
    lattice=Lattice(geom::Vec2(static_cast<Real>(mtzbatch->e1[0]),
			 static_cast<Real>(mtzbatch->e1[1])),
		    geom::Vec2(static_cast<Real>(mtzbatch->e2[0]),
			 static_cast<Real>(mtzbatch->e2[1])),
		    geom::Vec2(static_cast<Real>(mtzbatch->e3[0]),
			 static_cast<Real>(mtzbatch->e3[1])));
    lattice.SetBarrelDistortion(static_cast<Real>(mtzbatch->e1[2]));
    lattice.SetSpiralDistortion(static_cast<Real>(mtzbatch->e2[2]));
    LOG_DEBUG( "ImportMtz: retrieving lattice from batch entry: " << lattice );
  } else {
    LOG_DEBUG( "ImportMtz: batch is null, using default lattice" );
  }
  LOG_DEBUG( "ImportMtz: initializing ReflectionList" );
  ReflectionList rlist(lattice);

  MTZSET* mtz_base_set;
  std::vector<std::pair<MTZSET*,int> > col_entry;
  if(mtzin->nxtal<1) {
    throw ReflectionException("ImportMtz: Expected one or more xtal entries");
  }

  mtz_base_set = mtzin->xtal[0]->set[0];

  if(mtz_base_set->ncol<3) {
    throw ReflectionException("ImportMtz: expected base set to have at least 3 columns");
  }

  // sanity check for hkl columns  
  if(String(mtz_base_set->col[0]->label)!=String("H")) {
    throw ReflectionException("ImportMtz: expected base set to have first column 'H'");
  }
  if(String(mtz_base_set->col[1]->label)!=String("K")) {
    throw ReflectionException("ImportMtz: expected base set to have first column 'K'");
  }
  if(String(mtz_base_set->col[2]->label)!=String("L")) {
    throw ReflectionException("ImportMtz: expected base set to have first column 'L'");
  }

  // additional base set entries
  if(mtz_base_set->ncol>3) {
    LOG_DEBUG( "ImportMtz: reading in " << mtz_base_set->ncol-3 << " additional base columns" );
    for(int n=3;n<mtz_base_set->ncol;++n) {
      col_entry.push_back(std::make_pair(mtz_base_set,n));
    }
  }

  // additional entries
  for(int nxtal=0;nxtal<mtzin->nxtal;++nxtal) {
    for(int k= (nxtal==0) ? 1 : 0;
	k<mtzin->xtal[nxtal]->nset;++k) { 
    
      MTZSET* mtz_data_set=mtzin->xtal[nxtal]->set[k];
      LOG_DEBUG( "ImportMtz: reading in " << mtz_data_set->ncol << " columns from xtal " << nxtal << " and dataset " << k );
      for(int n=0;n<mtz_data_set->ncol;++n) {
	col_entry.push_back(std::make_pair(mtz_data_set,n));
      }
    }
  }
  
  std::vector<String> cname(col_entry.size());
  std::vector<int> cid(col_entry.size());
  
  #define REF_IO_SET_PROP_TYPE(S,T) \
  } else if(prop_type== S ) { rlist.SetPropertyType(cname[c],T);

  Real conversion_factors[col_entry.size()];
  for(unsigned int c=0;c<col_entry.size();++c) {
    MTZSET* mtz_data_set = col_entry[c].first;
    int coln = col_entry[c].second;
    cname[c]=String(mtz_data_set->col[coln]->label);
    String prop_type(mtz_data_set->col[coln]->type);
    if(prop_type=="P"){
      conversion_factors[c]=Units::deg;
    }else{
      conversion_factors[c]=1.0;
    }
    LOG_DEBUG( " column " << c << " name: " << cname[c] << " type: " << prop_type );
    cid[c] = rlist.AddProperty(cname[c]);
    if (false) { // dummy entry for #define to work
    REF_IO_SET_PROP_TYPE("F",PT_AMPLITUDE)
    REF_IO_SET_PROP_TYPE("H",PT_INDEX)
    REF_IO_SET_PROP_TYPE("J",PT_INTENSITY)
    REF_IO_SET_PROP_TYPE("D",PT_ANOMALOUS_DIFFERENCE)
    REF_IO_SET_PROP_TYPE("Q",PT_STDDEV)
    REF_IO_SET_PROP_TYPE("G",PT_AMPLITUDE2)
    REF_IO_SET_PROP_TYPE("L",PT_STDDEVA2)
    REF_IO_SET_PROP_TYPE("K",PT_INTENSITY2)
    REF_IO_SET_PROP_TYPE("M",PT_STDDEVI2)
    REF_IO_SET_PROP_TYPE("E",PT_NORM_AMPLITUDE)
    REF_IO_SET_PROP_TYPE("P",PT_PHASE)
    REF_IO_SET_PROP_TYPE("W",PT_WEIGHT)
    REF_IO_SET_PROP_TYPE("A",PT_PHASE_PROB)
    REF_IO_SET_PROP_TYPE("B",PT_BATCH)
    REF_IO_SET_PROP_TYPE("Y",PT_MISYM)
    REF_IO_SET_PROP_TYPE("I",PT_INTEGER)
    REF_IO_SET_PROP_TYPE("R",PT_REAL)
    }
  }
  #undef REF_IO_SET_PROP_TYPE

  LOG_VERBOSE( "ImportMtz: number of reflections: " << mtzin->nref );

  int idummy;
  char cdummy[256];
  char spcg[256];

  ccp4_lrsymi(mtzin, &idummy, cdummy, &idummy,spcg,cdummy);
  Symmetry sym("P1");
  try {
    sym=Symmetry(spcg);
  } catch (SymmetryException& e) {
    LOG_INFO( "could not interpret spacegroup name " << spcg << ", using P1" );
  } 
  // retrieve unit cell
  SpatialUnitCell cell(mtzin->xtal[0]->cell[0]*Units::A,
           mtzin->xtal[0]->cell[1]*Units::A,
           mtzin->xtal[0]->cell[5]*Units::deg,
           mtzin->xtal[0]->cell[2]*Units::A);

  cell.SetSymmetry(sym);

  rlist.SetUnitCell(cell);

  LOG_VERBOSE( "ImportMtz: retrieved unit cell: " << cell );

  Real l2zstar=1.0/mtzin->xtal[0]->cell[2];

  LOG_DEBUG( "ImportMtz: importing reflections" );
  for(int r=0;r<mtzin->nref;++r) {
    // retrieve hkl, convert l to zstar using thickness from unit cell
    ReflectionIndex indx(static_cast<int>(mtz_base_set->col[0]->ref[r]),
       static_cast<int>(mtz_base_set->col[1]->ref[r]),
       l2zstar*static_cast<Real>(mtz_base_set->col[2]->ref[r]));

    ReflectionProxyter rp = rlist.AddReflection(indx);
    for(unsigned int c=0;c<col_entry.size();++c) {
      MTZSET* mtz_data_set = col_entry[c].first;
      int coln = col_entry[c].second;
      rp.Set(cid[c],static_cast<Real>(mtz_data_set->col[coln]->ref[r])*conversion_factors[c]);
    }
  }
  return rlist;
}

void ExportMtz(const ReflectionList& rl, const String& filename)
{
  MTZPtr mtzout(MtzMalloc( 0, NULL ));

  MTZBATPtr mtzbatch(MtzMallocBatch());
  init_batch(mtzbatch, rl.GetLattice());
  mtzout->batch=mtzbatch;
  //mtzout->batch=0;

  ccp4_lwtitl( mtzout, "exported from iplt", 0 );
  mtzout->refs_in_memory = 1;

  // setup dummy spacegroup
  float rsym[192][4][4];
  for(int i=0;i<4;++i) 
    for(int k=0;k<4;++k)
      rsym[0][i][k]= (i==k) ? 1 : 0;
  char symb1[]={'P',0};
  char symb2[]={'P','G','1',0};

  String spgn=rl.GetSymmetry().GetSpacegroupName();
  String pgn=rl.GetSymmetry().GetPointgroupName();
  // convert to char* as required by ccp4_lwsymm
  std::vector<char> spgn_c(spgn.size()+1); std::copy(spgn.begin(),spgn.end(),spgn_c.begin()); spgn_c[spgn.size()]=0;
  std::vector<char> pgn_c(pgn.size()+1); std::copy(pgn.begin(),pgn.end(),pgn_c.begin());pgn_c[pgn.size()]=0;
  LOG_VERBOSE( "ExportMtz: calling ccp4_lwsymm with " << rl.GetSymmetry().GetSpacegroupNumber() << " " << spgn << " " << pgn );
  ccp4_lwsymm(mtzout, 1, 1, rsym, symb1, 
	      rl.GetSymmetry().GetSpacegroupNumber(),
	      &spgn_c[0],&pgn_c[0]);

  // the base set (containing the miller indices)
  float ucell[]={1.0,1.0,1.0,90.0,90.0,90.0};
  SpatialUnitCell sp_ucell=rl.GetUnitCell();

  // CCP4 saves unit cell information in Angstrom and degrees
  // iplt in Angstrom and radians
  LOG_VERBOSE( "ExportMtz: using spatial unit cell " << sp_ucell );
  ucell[0]=sp_ucell.GetA();
  ucell[1]=sp_ucell.GetB();
  ucell[2]=sp_ucell.GetC();
  ucell[5]=sp_ucell.GetGamma()/Units::deg;
  
  MTZXTAL* mtz_base_xtal = MtzAddXtal(mtzout,"HKL_base","HKL_base",ucell);
  MTZSET* mtz_base_set = MtzAddDataset(mtzout,mtz_base_xtal,"HKL_base",1.0);
  MTZCOL* mtz_base_col[3];
  mtz_base_col[0] = MtzAddColumn(mtzout, mtz_base_set, "H", "H");
  mtz_base_col[1] = MtzAddColumn(mtzout, mtz_base_set, "K", "H");
  mtz_base_col[2] = MtzAddColumn(mtzout, mtz_base_set, "L", "H");

#ifdef IPLT_MTZ_MULTIXTAL_EXPORT
  // the data set
  LOG_VERBOSE( "ExportMtz: using multi xtal output, ie data will be in a separate xtal entry" );
  MTZXTAL* mtz_data_xtal = MtzAddXtal(mtzout,"Data","Data",ucell);
  MTZSET* mtz_data_set = MtzAddDataset(mtzout,mtz_data_xtal,"Data",1.0);
#else
  MTZSET* mtz_data_set = MtzAddDataset(mtzout,mtz_base_xtal,"Data",1.0);
#endif
  int col_count = rl.GetPropertyCount();

  std::vector<MTZCOL*> mtz_data_col(col_count);
  std::vector<String> col_name(col_count);

  LOG_DEBUG( "MTZEXport: column list:" );
  Real conversion_factors[col_count];
  for(int c=0;c<col_count;++c) {
    col_name[c] = rl.GetPropertyName(c);
    String prop_type_string("R");
    switch(rl.GetPropertyType(c)) {
    case PT_AMPLITUDE: 
      prop_type_string=String("F");
      conversion_factors[c]=1.0;
      break;
    case PT_INDEX:
      prop_type_string=String("H");
      conversion_factors[c]=1.0;
      break;
    case PT_INTENSITY:
      prop_type_string=String("J");
      conversion_factors[c]=1.0;
      break;
    case PT_ANOMALOUS_DIFFERENCE:
      prop_type_string=String("D");
      conversion_factors[c]=1.0;
      break;
    case PT_STDDEV:
      prop_type_string=String("Q");
      conversion_factors[c]=1.0;
      break;
    case PT_AMPLITUDE2:
      prop_type_string=String("G");
      conversion_factors[c]=1.0;
      break;
    case PT_STDDEVA2:
      prop_type_string=String("L");
      conversion_factors[c]=1.0;
      break;
    case PT_INTENSITY2:
      prop_type_string=String("K");
      conversion_factors[c]=1.0;
      break;
    case PT_STDDEVI2:
      prop_type_string=String("M");
      conversion_factors[c]=1.0;
      break;
    case PT_NORM_AMPLITUDE:
      prop_type_string=String("E");
      conversion_factors[c]=1.0;
      break;
    case PT_PHASE:
      prop_type_string=String("P");
      conversion_factors[c]=1.0/Units::deg;
      break;
    case PT_WEIGHT:
      prop_type_string=String("W");
      conversion_factors[c]=1.0;
      break;
    case PT_PHASE_PROB:
      prop_type_string=String("A");
      conversion_factors[c]=1.0;
      break;
    case PT_BATCH:
      prop_type_string=String("B");
      conversion_factors[c]=1.0;
    break;
    case PT_MISYM:
      prop_type_string=String("Y");
      conversion_factors[c]=1.0;
      break;
    case PT_INTEGER:
      prop_type_string=String("I");
      conversion_factors[c]=1.0;
    break;
    case PT_REAL:
      prop_type_string=String("R");
      conversion_factors[c]=1.0;
      break;
    }
    LOG_DEBUG( " column " << c << " name: " << col_name[c] << " type: " << prop_type_string );
    mtz_data_col[c] = MtzAddColumn(mtzout, mtz_data_set, 
				   col_name[c].c_str(),
				   prop_type_string.c_str());
  }

  // add reflections
  std::vector<float> fdata(col_count+3);
  int ref=1;
  Real zstar2l = sp_ucell.GetC();
  for(ReflectionProxyter rp=rl.Begin();!rp.AtEnd(); ++rp, ++ref) {
    // set fdata
    fdata[0] = static_cast<float>(rp.GetIndex().GetH());
    fdata[1] = static_cast<float>(rp.GetIndex().GetK());
    // zstar to l conversion
    fdata[2] = static_cast<float>(rp.GetIndex().GetZStar()*zstar2l);

    for(int c=0;c<col_count;++c) {
      fdata[c+3] = static_cast<float>(rp.Get(col_name[c])*conversion_factors[c]);
    }

    ccp4_lwrefl( mtzout, &fdata[0], mtz_base_col, 3, ref);
    for(int n=0;n<col_count;n++) {
      ccp4_lwrefl( mtzout, &fdata[n+3], &mtz_data_col[n], 1, ref);
    }
  }

  LOG_DEBUG( "ExportMtz: calling MtzPut with " << filename );
  MtzPut(mtzout, filename.c_str());
}

////////////////////////////////
// APH import export code

namespace {

using boost::lexical_cast;
using boost::bad_lexical_cast;
using namespace std;

class IsIntVisitor: public boost::static_visitor<bool>
{
public:
  bool operator()(const Real& ) const
  {
    return false;
  }
  bool operator()(const int& ) const
  {
    return true;
  }
};

class GetIntVisitor: public boost::static_visitor<int>
{
public:
  template <typename T>
  int operator()( T & operand ) const
  {
    return static_cast<int>(operand);
  }
};

class GetDoubleVisitor : public boost::static_visitor<Real> {
public:
  template <typename T> 
  Real operator()( T & operand ) const
  {
    return static_cast<Real>(operand);
  }
};

class LessVisitor : public boost::static_visitor<bool> {
public:
  template <typename T, typename U>
  bool operator()( const T & lhs, const U & rhs) const
  {
    return static_cast<Real>(lhs) < static_cast<Real>(rhs); 
  }
  template <typename T>
  bool operator()( const T & lhs, const T & rhs ) const
  {
    return lhs < rhs;
  }
};

class AphImportExport
{
  typedef boost::variant< int, Real > VariantValue;
	
public:
  ReflectionList Import(const String& filename) {  
    using namespace std;
    vector<vector<VariantValue> >raw_data;
    String s,title;
    ReflectionList rlist;
    ifstream file(filename.c_str());
    //read int title
    getline(file,title);
    // read in raw data
    while(getline(file,s)){
      raw_data.push_back(ConvertLine(s));
    }
    //check if title is also data
    try {
      vector<VariantValue> tmp_vec=ConvertLine(title);
      if(tmp_vec.size()==raw_data[0].size()){
	raw_data.push_back(tmp_vec);
      }
    } catch (bad_lexical_cast &e){
    }
    
    // calculate min and max values for column identification
    vector<VariantValue> mins=minvec(raw_data);
    vector<VariantValue> maxs=maxvec(raw_data);
    //fill Reflectionlist
    if(boost::apply_visitor(GetDoubleVisitor(),maxs[2])>1.0 || boost::apply_visitor(GetDoubleVisitor(),mins[2])<-1.0){ // third column only zstar if max<=0.5 and min>=-0.5
      LOG_DEBUG( "zstar column absent" );
      for(unsigned int i=2;i<raw_data[0].size();++i){
        rlist.AddProperty(lexical_cast<String>(i-2),PT_REAL);
      }
      for(vector<vector<VariantValue> >::const_iterator it=raw_data.begin();it!=raw_data.end();++it){
        ReflectionProxyter rp=rlist.AddReflection(ReflectionIndex(boost::apply_visitor(GetIntVisitor(),(*it)[0]),boost::apply_visitor(GetIntVisitor(),(*it)[1])));
        for(unsigned int i=2;i<raw_data[0].size();++i){
          rp.Set(lexical_cast<String>(i-2),boost::apply_visitor(GetDoubleVisitor(),(*it)[i]));
        }
      }
    }else{
      LOG_DEBUG( "zstar column present" );
      for(unsigned int i=3;i<raw_data[0].size();++i){
        rlist.AddProperty(lexical_cast<String>(i-3),PT_REAL);
      }
      for(vector<vector<VariantValue> >::const_iterator it=raw_data.begin();it!=raw_data.end();++it){
        ReflectionProxyter rp=rlist.AddReflection(ReflectionIndex(boost::apply_visitor(GetIntVisitor(),(*it)[0]),boost::apply_visitor(GetIntVisitor(),(*it)[1]),boost::apply_visitor(GetDoubleVisitor(),(*it)[2])));
        for(unsigned int i=3;i<raw_data[0].size();++i){
          rp.Set(lexical_cast<String>(i-3),boost::apply_visitor(GetDoubleVisitor(),(*it)[i]));
        }
      }
    }
    SetColumnNameAndTypes(rlist,raw_data,mins,maxs);
    return rlist;
  }

  void Export(const ReflectionList& rlist,const String& filename,
              const String& title,const String& order) {
    std::vector<String> order_vec;
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep_comma(",");
    tokenizer tokens(order, sep_comma);
    for(tokenizer::iterator beg=tokens.begin(); beg!=tokens.end();++beg){
      order_vec.push_back((*beg));
    }
    ofstream file(filename.c_str());
    if(title.size()>0){
      file << title << std::endl;
    }
    if(order_vec.size()>0){
      for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp){
        ReflectionIndex idx=rp.GetIndex();
        file  << std::resetiosflags(std::ios::scientific )<< idx.GetH() << " " << idx.GetK() << " " << idx.GetZStar() ;
        for(std::vector<String>::const_iterator it=order_vec.begin();it!=order_vec.end();++it){
          if(rlist.GetPropertyType(*it)==PT_PHASE || rlist.GetPropertyType(*it)==PT_INTEGER){
            file << std::resetiosflags(std::ios::scientific ) <<" "<<rp.Get(*it);
          }else{
            file << scientific <<" "<<rp.Get(*it);
          }
        }
        file <<std::endl;
      }
    }else{
      for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp){
        ReflectionIndex idx=rp.GetIndex();
        file  << std::resetiosflags(std::ios::scientific ) << idx.GetH() << " " << idx.GetK() << " " << idx.GetZStar() ;
        for(unsigned int i=0;i<rlist.GetPropertyCount();++i){
          if(rlist.GetPropertyType(rlist.GetPropertyName(i))==PT_PHASE|| rlist.GetPropertyType(rlist.GetPropertyName(i))==PT_INTEGER){
            file << std::resetiosflags(std::ios::scientific )<<" "<<rp.Get(rlist.GetPropertyName(i));
          }else{
            file << scientific <<" "<<rp.Get(rlist.GetPropertyName(i));
          }
        }
        file <<std::endl;
      }
    }
  }

private:
  const static unsigned NROFDIGITS=4;

  vector<VariantValue> ConvertLine(const String& line) {
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    boost::char_separator<char> sep_space(" ");
    tokenizer tokens(line, sep_space);
    vector<VariantValue> tmp_vec;
    for(tokenizer::iterator beg=tokens.begin(); beg!=tokens.end();++beg){
      try {
	VariantValue v=lexical_cast<int>(*beg);
	if((*beg).size()>NROFDIGITS){ // split image nr. into tilt angle and neg nr (workaround for rounding error in mtz)
	  String ta((*beg),0,2);
	  String nr((*beg),2,(*beg).size());
	  tmp_vec.push_back(lexical_cast<int>(ta));
	  tmp_vec.push_back(lexical_cast<int>(nr));
	}
	tmp_vec.push_back(v);
      } catch(bad_lexical_cast &e){
	try {
	  tmp_vec.push_back(lexical_cast<Real>(*beg));
	} catch(bad_lexical_cast &e){
	  //LOG_ERROR( "Could not convert "<<  (*beg) << " to Real." );
	  throw(e);
	}
      }
    }
    return tmp_vec;
  }

  void SetColumnNameAndTypes(ReflectionList&r,
			     const vector< vector<VariantValue> >& data,
			     const vector<VariantValue>& mins,
			     const vector<VariantValue>& maxs) {
    bool id=false,ta=false,iq=false,bck=false,ctf=false,fom=false,w=false,phi=false;
    int idxlength=data[0].size()-r.GetPropertyCount();
    r.RenameProperty("0","fobs");
    r.SetPropertyType("fobs",PT_AMPLITUDE);
    for(int i=data[0].size()-1;i>idxlength;--i){
      if(!iq && boost::apply_visitor(IsIntVisitor(),data[0][i]) && boost::apply_visitor(GetIntVisitor(),mins[i])>=-9 && boost::apply_visitor(GetIntVisitor(),maxs[i])<=9){
	    LOG_DEBUG( "column Nr. " << i-idxlength << " identified as iq" );
        iq=true;
	    r.RenameProperty(lexical_cast<String>(i-idxlength),"IQ");
	    r.SetPropertyType("IQ",PT_INTEGER);
	    continue;
      } else if(!id && boost::apply_visitor(IsIntVisitor(),data[0][i]) ) {
        LOG_DEBUG( "column Nr. " << i-idxlength << " identified as id" );
	    id=true;
	    r.RenameProperty(lexical_cast<String>(i-idxlength),"ID");
	    r.SetPropertyType("ID",PT_INTEGER);
	    continue;
      } else if(!ta && boost::apply_visitor(IsIntVisitor(),data[0][i]) ) {
        LOG_DEBUG( "column Nr. " << i-idxlength << " identified as tilt angle" );
	    ta=true;
	    r.RenameProperty(lexical_cast<String>(i-idxlength),"tiltangle");
	    r.SetPropertyType("tiltangle",PT_INTEGER);
	    continue;
      } else if(!ctf && boost::apply_visitor(GetDoubleVisitor(),mins[i])>=-1.0 && boost::apply_visitor(GetDoubleVisitor(),maxs[i])<=1.0) {
        LOG_DEBUG( "column Nr. " << i-idxlength << " identified as ctf" );
	    ctf=true;
	    r.RenameProperty(lexical_cast<String>(i-idxlength),"CTF");
	    r.SetPropertyType("CTF",PT_REAL);
	    continue;
      } else if(!fom && boost::apply_visitor(GetDoubleVisitor(),mins[i])>=0.0 && boost::apply_visitor(GetDoubleVisitor(),maxs[i])<=100.0) {
        LOG_DEBUG( "column Nr. " << i-idxlength << " identified as FOM" );
	    w=true;
	    r.RenameProperty(lexical_cast<String>(i-idxlength),"FOM");
	    r.SetPropertyType("FOM",PT_REAL);
	    continue;
      } else if(!w && boost::apply_visitor(GetDoubleVisitor(),mins[i])>=0.0 && boost::apply_visitor(GetDoubleVisitor(),maxs[i])<=1.0) {
        LOG_DEBUG( "column Nr. " << i-idxlength << " identified as FOM" );
	    w=true;
	    r.RenameProperty(lexical_cast<String>(i-idxlength),"FOM");
	    r.SetPropertyType("FOM",PT_REAL);
	    continue;
      } else if(!bck && boost::apply_visitor(GetDoubleVisitor(),mins[i])>=0.0 ) {
        LOG_DEBUG( "column Nr. " << i-idxlength << " identified as BCK" );
	    bck=true;
	    r.RenameProperty(lexical_cast<String>(i-idxlength),"bck");
	    r.SetPropertyType("bck",PT_REAL);
	    continue;
      } else if(!phi && boost::apply_visitor(GetDoubleVisitor(),mins[i])>=-360.0 && boost::apply_visitor(GetDoubleVisitor(),maxs[i])<=360.0) {
        LOG_DEBUG( "column Nr. " << i-idxlength << " identified as phi" );
	    phi=true;
	    r.RenameProperty(lexical_cast<String>(i-idxlength),"phi");
	    r.SetPropertyType("phi",PT_PHASE);
	    continue;
      }else{
        LOG_INFO( "column Nr. " << i-idxlength << " not identified" );
      }
    }
  }

  VariantValue Min(VariantValue a,VariantValue b) {
    if(boost::apply_visitor(LessVisitor(),a,b)){
      return a;
    }
    return b;
  }

  VariantValue Max(VariantValue a,VariantValue b) {
    if(boost::apply_visitor(LessVisitor(),a,b)){
      return b;
    }
    return a;
  }

  vector<VariantValue> vecmin(const vector<VariantValue>& a,
			      const vector<VariantValue>& b) {
    vector<VariantValue> r;
    for(vector<VariantValue>::const_iterator ita=a.begin(), itb=b.begin();
	ita!=a.end()|| itb!=b.end();++ita,++itb) {
      r.push_back(Min((*ita),(*itb)));
    }
    return r;
  }

  vector<VariantValue> vecmax(const vector<VariantValue>& a,
			      const vector<VariantValue>& b) {
    vector<VariantValue> r;
    for(vector<VariantValue>::const_iterator ita=a.begin(), itb=b.begin();
	ita!=a.end()|| itb!=b.end(); ++ita,++itb) {
      r.push_back(Max((*ita),(*itb)));
    }
    return r;
  }
  
  vector<VariantValue> minvec(const vector< vector<VariantValue> >& data) {
    vector<VariantValue> r=data[0];
    for(vector< vector<VariantValue> >::const_iterator it=data.begin();
	it!=data.end();++it){
      r=vecmin((*it),r);
    }
    return r;
  }

  vector<VariantValue> maxvec(const vector< vector<VariantValue> >& data) {
    vector<VariantValue> r=data[0];
    for(vector< vector<VariantValue> >::const_iterator it=data.begin();
	it!=data.end();++it){
      r=vecmax((*it),r);
    }
    return r;
  }

};


} //ns

void ExportAph(const ReflectionList& rl,const String& file,const String& title,const String& order)
{
  AphImportExport().Export(rl,file,title,order);
}

ReflectionList ImportAph(const String& filename)
{
  return AphImportExport().Import(filename);
}
} //ns
