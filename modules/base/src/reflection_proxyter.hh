//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Reflection proxyter

  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_REFLECTION_PROXYTER_HH
#define IPLT_EX_REFLECTION_PROXYTER_HH

#include "reflection_index.hh"
#include "reflection_list_impl.hh"

namespace iplt { 

class ReflectionList;

//! Combined Proxy and Iterator for a Reflection
/*!
  The concept of a proxter combines the features of
  a proxy (indirect access to underlying/internal
  object) and iterator.

  The proxy interface offers the Get and Set methods.

  The iterator interface offers the increment operator
  as well as the AtEnd() method.
*/
class DLLEXPORT_IPLT_BASE ReflectionProxyter
{
  friend class ReflectionList;
public:
  ReflectionProxyter(const ReflectionProxyter& p);
  ReflectionProxyter& operator=(const ReflectionProxyter& p);

  //! Retrieve value of the given property
  /*!
    If a property by this name does not exist in the originating
    ReflectionList, a ReflectionException is thrown.
  */
  Real Get(const String& prop) const;
  //! Retrieve a value by property index
  /*!
    This shortcut allows the value of a property to be 
    looked up by the property index, as returned by
    ReflectionList::AddProperty or ReflectionList::GetPropertyIndex.
    Out of bounds access will throw a ReflectionException.
  */
  Real Get(unsigned int n) const;

  //! Set value of the given property
  /*!
    If a property by this name does not exist in the originating
    ReflectionList, a ReflectionException is thrown.
  */
  void Set(const String& prop, Real value);

  //! Set value by property index
  /*!
    This shortcut allows the value of a property to be 
    looked up by the property index, as returned by
    ReflectionList::AddProperty or ReflectionList::GetPropertyIndex.
    Out of bounds access will throw a ReflectionException.
  */
  void Set(unsigned int n, Real value);

  //! Set all values from other proxyter
  void Set(const ReflectionProxyter& rp);

  //! Advance to next reflection
  /*!
    If already at last reflection, do nothing. The python method is called Inc()
  */
  ReflectionProxyter& operator++();

  //! Advance to next reflection
  /*!
    If already at last reflection, do nothing
  */
  ReflectionProxyter operator++(int);

  //! Check wether end of reflections has been reached
  bool AtEnd() const;

  //! Allows checking of validity
  bool IsValid() const;

  //! Retrieve index of this particular reflection DEPRECATED
  ReflectionIndex Index() const;

  //! Retrieve index of this particular reflection 
  ReflectionIndex GetIndex() const;
  
  //! Get the resolution of this particular reflection
  Real GetResolution() const;

  //! deletes coresponding reflection and invalidates ReflectionProxyter
  void Delete();

private:
  ReflectionProxyter(reflection_detail::ListPtr lptr, 
		     const reflection_detail::IndexMapIterator& indx);

  reflection_detail::ListPtr lptr_;
  reflection_detail::IndexMapIterator index_;

  void inc();
};

} //ns


#endif
