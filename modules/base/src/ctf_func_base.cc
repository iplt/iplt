//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/
#include <ost/geom/vecmat2_op.hh>
#include "ctf_func_base.hh"

namespace iplt {

CTFFuncBase::CTFFuncBase(const TCIFData& data):
  data_(data),
  Q_(atan(data.GetAmplitudeContrast()/sqrt(1.0-data.GetAmplitudeContrast()*data.GetAmplitudeContrast()))/M_PI)
{
}

CTFFuncBase::~CTFFuncBase()
{
}

Real CTFFuncBase::GetDefocusX() const
{
  return data_.GetDefocusX();
}

Real CTFFuncBase::GetDefocusY() const
{
  return data_.GetDefocusY();
}

void CTFFuncBase::SetDefocusX(Real defocus)
{
  data_.SetDefocusX(defocus);
}

void CTFFuncBase::SetDefocusY(Real defocus)
{
  data_.SetDefocusY(defocus);
}

Real CTFFuncBase::GetAstigmatismAngle() const
{
  return data_.GetAstigmatismAngle();
}

void CTFFuncBase::SetAstigmatismAngle(Real angle)
{
  data_.SetAstigmatismAngle(angle);
}

TCIFData CTFFuncBase::GetTCIFData() const
{
  return data_;
}

void CTFFuncBase::SetTCIFData(const TCIFData& data)
{
  data_=data;
}


Real CTFFuncBase::CTF(geom::Vec2 xy) const{
  Real xy2=geom::Length2(xy);
  Real lambda=data_.GetWavelength();
  Real Ca=data_.GetAmplitudeContrast();
  Real Cs=data_.GetSphericalAberration();

  Real W0=M_PI/2.0*(Cs*lambda*lambda*lambda*xy2*xy2-2.0*defocus_(atan2(xy[1],xy[0]))*lambda*xy2);
  return -2.0*(sqrt(1.0-Ca*Ca)*sin(W0)-Ca*cos(W0));
}

Real CTFFuncBase::DampedCTF(geom::Vec2 xy) const{
  return CTF(xy)*SpatialEnvelope(xy)*TemporalEnvelope(xy);
}

Real CTFFuncBase::SpatialEnvelope(geom::Vec2 xy) const{
  Real absxy=geom::Length(xy);
  Real lambda=data_.GetWavelength();
  Real Cs=data_.GetSphericalAberration();

  Real f=M_PI*data_.GetConvergenceAngle()*(absxy*absxy*absxy*Cs*lambda*lambda-defocus_(atan2(xy[1],xy[0]))*absxy);
  return exp(-f*f/log(2.0));
}

Real CTFFuncBase::TemporalEnvelope(geom::Vec2 xy) const{
  Real xy2=geom::Length2(xy);
  Real lambda=data_.GetWavelength();
  Real Cc=data_.GetChromaticAberration();

  Real f=M_PI*Cc*lambda*xy2*data_.GetEnergySpread()/data_.GetAccelerationVoltage();
  return exp(-f*f/(4.0*log(2.0)));
}

Real CTFFuncBase::defocus_(Real arg) const{
  Real ast_angle=data_.GetAstigmatismAngle();
  Real cosval=cos(arg-ast_angle);
  Real sinval=sin(arg-ast_angle);

  return data_.GetDefocusX()*cosval*cosval+data_.GetDefocusY()*sinval*sinval;
}

geom::Vec2List CTFFuncBase::CalcThonRingPosition(Real angle, unsigned int x) const
{
  Real df= defocus_(angle);
  Real lambda=data_.GetWavelength();
  Real Cs=data_.GetSphericalAberration();
  Real sign=df>=0 ? 1.0 : -1.0;
  Real D=df*df-2*Cs*lambda*(sign*x-Q_);
 if(D<0){
   return geom::Vec2List();
  }
  geom::Vec2List result;
  if((df+sqrt(D))>0)
  {
    result.push_back(sqrt((df+sqrt(D))/Cs)/lambda*geom::Vec2(cos(angle),sin(angle)));
  }
  if((df-sqrt(D))>0){
    result.push_back(sqrt((df-sqrt(D))/Cs)/lambda*geom::Vec2(cos(angle),sin(angle)));
  }
  return result;
}

Real CTFFuncBase::CalcDefocusFromThonRing(geom::Vec2 k, int x) const
{
  Real sign= x>=0 ? 1.0 : -1.0;
  Real lambda=data_.GetWavelength();
  Real k2=geom::Length2(k);
  Real Cs=data_.GetSphericalAberration();
  if(0.5*Cs*lambda*lambda*lambda*k2*k2>sign*(x-Q_)){
    // defocus between 0 and  sqrt(2)*scherzer defocus is not supported, because starting at that point the first oscillation doesn't reach 0 anymore
    //therefore return sqrt(2)*scherzer defocus or -0.01
    if(sign>0){
      return sqrt(2.0*Cs*lambda*sign*(x-Q_));
    }else{
      return -0.01;
    }
  }
  return 0.5*Cs*lambda*lambda*k2+(x-Q_)/(lambda*k2);
}



}//ns
