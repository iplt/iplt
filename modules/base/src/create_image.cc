//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <ost/img/image.hh>
#include <ost/message.hh>

#include "create_image.hh"

namespace iplt { 
	
ImageHandle CreateImage(ost::img::Extent e,ReflectionList in)
{
  String ampid="",phaid="",sigampid="";
  unsigned columnnum=in.GetPropertyCount();
  for(unsigned i=0;i<columnnum;i++){
    switch(in.GetPropertyType(i)){
    case iplt::PT_AMPLITUDE:
      ampid=in.GetPropertyName(i);
      break;
    case iplt::PT_PHASE:
      phaid=in.GetPropertyName(i);
      break;
    default:
      break;
    }
  }
  if(ampid==""){
    throw(ReflectionException("no amplitude column found"));
  }
  if(phaid==""){
    throw(ReflectionException("no phase column found"));
  }
  ost::img::ImageHandle i=CreateImage(e,ost::img::COMPLEX,HALF_FREQUENCY);
  ost::img::Point start=e.GetStart();
  ost::img::Point end=e.GetEnd();
  if(e.GetDim()==2){
    for(ReflectionProxyter rp=in.Begin(); !rp.AtEnd();++rp){
      ReflectionIndex index=rp.GetIndex();
      if(index.GetH()>=start[0] && index.GetH()<=end[0] && index.GetK()>=start[1] && index.GetK()<=end[1]){
	i.SetComplex(ost::img::Point(index.GetH(),index.GetK()),std::polar(rp.Get(ampid),rp.Get(phaid)));	
      }
    }
  }
  else{
    throw("only implemented for 2 dimensional image.");
  }
  return i;
}
  
} //ns
