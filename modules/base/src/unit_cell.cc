//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Unit Cell

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <sstream>

#include <ost/message.hh>
#include <ost/log.hh>
#include <ost/units.hh>

#include "unit_cell.hh"

namespace iplt { 

namespace cell_detail {

CellImpl::CellImpl():
  a_(100.0*Units::A),
  b_(100.0*Units::A),
  c_(100.0*Units::A),
  gamma_(90.0*Units::deg),
  A_(geom::Vec2(100.0,0.0)*Units::A),
  B_(geom::Vec2(0.0,100.0)*Units::A),
  symmetry_(Symmetry("P 1"))
{
}

CellImpl::CellImpl(Real a, Real b, Real c, Real gamma,String symmetry):
  a_(a), b_(b), c_(c), gamma_(gamma),
  A_(a_,0.0),
  B_(b_*cos(gamma_),b_*sin(gamma_)),
  symmetry_(Symmetry(symmetry))
{}

CellImpl::CellImpl(Real a, Real b, Real c, Real gamma,const Symmetry& s):
  a_(a), b_(b), c_(c), gamma_(gamma),
  A_(a_,0.0),
  B_(b_*cos(gamma_),b_*sin(gamma_)),
  symmetry_(s)
{}

CellImpl::CellImpl(const geom::Vec2& va, const geom::Vec2& vb, Real c,String symmetry):
  a_(1.0),b_(1.0),c_(c),gamma_(0.0),
  A_(va),
  B_(vb),
  symmetry_(Symmetry(symmetry))
{
  a_=sqrt(A_[0]*A_[0]+A_[1]*A_[1]);
  b_=sqrt(B_[0]*B_[0]+B_[1]*B_[1]);
  gamma_ = acos(Dot(A_,B_)/(a_*b_));
}

CellImpl::CellImpl(const geom::Vec2& va, const geom::Vec2& vb, Real c,const Symmetry& s):
  a_(1.0),b_(1.0),c_(c),gamma_(0.0),
  A_(va),
  B_(vb),
  symmetry_(s)
{
  a_=sqrt(A_[0]*A_[0]+A_[1]*A_[1]);
  b_=sqrt(B_[0]*B_[0]+B_[1]*B_[1]);
  gamma_ = acos(Dot(A_,B_)/(a_*b_));
}

CellImpl CellImpl::Invert() const
{
  Real D = A_[1]*B_[0]-A_[0]*B_[1];
  geom::Vec2 iA=geom::Vec2(-B_[1]/D,B_[0]/D);
  geom::Vec2 iB=geom::Vec2(A_[1]/D,-A_[0]/D);
  Real ic=1.0/c_;
  CellImpl nrvo(iA,iB,ic,symmetry_);
  return nrvo;
}

geom::Vec2 CellImpl::GetPosition(const ost::img::Point& hk)
{
  geom::Vec2 nrvo = static_cast<Real>(hk[0])*A_ + static_cast<Real>(hk[1])*B_;
  return nrvo;
}

void CellImpl::Print(std::ostream& o) const
{
  o << "{{";
  o << A_[0] << "," << A_[1];
  o << "},{";
  o << B_[0] << "," << B_[1];
  o << "}}";
  o << " | a=" << a_ << " b=" << b_;
  o << " g=" << gamma_/Units::deg << " c=" << c_;
  o << " sym=" << symmetry_.GetSpacegroupName();
}

} // ns

SpatialUnitCell::SpatialUnitCell():
  cell_detail::CellImpl()
{
}

SpatialUnitCell::SpatialUnitCell(Real a, Real b, Real g, Real c, const String& s):
  cell_detail::CellImpl(a,b,c,g,s)
{
  check();
}

SpatialUnitCell::SpatialUnitCell(const geom::Vec2& a, const geom::Vec2& b, Real c, const String& s):
      cell_detail::CellImpl(a,b,c,s)
{
  check();
}

SpatialUnitCell::SpatialUnitCell(const Lattice& l, const geom::Vec3& sampling, Real c, const String& s):
  cell_detail::CellImpl(geom::Vec2(l.GetFirst()[0]*sampling[0],l.GetFirst()[1]*sampling[1]),
			geom::Vec2(l.GetSecond()[0]*sampling[0],l.GetSecond()[1]*sampling[1]),
			c,s)
{
  check();
}

SpatialUnitCell::SpatialUnitCell(const SpatialUnitCell& c):
  cell_detail::CellImpl(c)
{}

SpatialUnitCell::SpatialUnitCell(const ReciprocalUnitCell& c):
  cell_detail::CellImpl(c.Invert())
{}

SpatialUnitCell::SpatialUnitCell(const cell_detail::CellImpl& impl):
  cell_detail::CellImpl(impl)
{}

void SpatialUnitCell::check()
{
  if(Length(A_)<1.0e-6) {
    LOG_VERBOSE( "SpatialUnitCell: cell dimensions too small, resetting to unit Angstrom scale" );
    A_*=1.0e10;
    B_*=1.0e10;
    a_*=1.0e10;
    b_*=1.0e10;
  }
  if(geom::Angle(A_,B_)<M_PI_2) {
    LOG_INFO( "SpatialUnitCell: flipping b vector" );
    B_ = -B_;
    gamma_ = M_PI-gamma_;
  }

}

//////

ReciprocalUnitCell::ReciprocalUnitCell():
  cell_detail::CellImpl()
{}

ReciprocalUnitCell::ReciprocalUnitCell(const geom::Vec2& a, const geom::Vec2& b, Real c, const String& s):
  cell_detail::CellImpl(a,b,c,s)
{
  check();
}

ReciprocalUnitCell::ReciprocalUnitCell(Real a, Real b, Real g, Real c, const String& s):
  cell_detail::CellImpl(a,b,c,g,s)
{
  check();
}

ReciprocalUnitCell::ReciprocalUnitCell(const Lattice& l, const geom::Vec3& sampling, Real c, const String& s):
  cell_detail::CellImpl(geom::Vec2(l.GetFirst()[0]*sampling[0],l.GetFirst()[1]*sampling[1]),
			geom::Vec2(l.GetSecond()[0]*sampling[0],l.GetSecond()[1]*sampling[1]),
			c,s)
{
  check();
}

ReciprocalUnitCell::ReciprocalUnitCell(const ReciprocalUnitCell& c):
  cell_detail::CellImpl(c)
{}

ReciprocalUnitCell::ReciprocalUnitCell(const SpatialUnitCell& c):
  cell_detail::CellImpl(c.Invert())
{}

ReciprocalUnitCell::ReciprocalUnitCell(const cell_detail::CellImpl& impl):
  cell_detail::CellImpl(impl)
{}


void ReciprocalUnitCell::check()
{
  if(Length(A_)>1.0e6) {
    LOG_VERBOSE( "ReciprocalunitCell: cell dimensions too large, resetting to unit Angstrom scale" );
    A_*=1.0e-10;
    B_*=1.0e-10;
    a_*=1.0e-10;
    b_*=1.0e-10;
  }
  if(geom::Angle(A_,B_)>M_PI_2) {
    LOG_INFO( "ReciprocalUnitCell: flipping b vector" );
    B_ = -B_;
    gamma_ = M_PI-gamma_;
  }

}

// ops

std::ostream& operator<<(std::ostream& o, const SpatialUnitCell& c)
{
  c.Print(o);
  return o;
}

std::ostream& operator<<(std::ostream& o, const ReciprocalUnitCell& c)
{
  c.Print(o);
  return o;
}

} //ns
