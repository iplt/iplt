//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include "defocus.hh"

namespace iplt { 

Defocus::Defocus(Real dx,Real dy,Real angle):
    dx_(dx),
    dy_(dy),
    angle_(angle)
{
}

Defocus::Defocus(Real d):
    dx_(d),
    dy_(d),
    angle_(0)
{
}

void Defocus::SetMeanDefocus(Real d)
{
    Real  diff=(dx_-dy_)/2;
    dx_=d+diff;
    dy_=d-diff;
}

void DefocusToInfo(const Defocus& df, ost::info::InfoGroup& ig)
{
  using namespace ost::info;
  InfoGroup dfgroup=ig.RetrieveGroup("Defocus");
  InfoItem x=dfgroup.RetrieveItem("x");
  x.SetFloat(df.GetDefocusX());
  InfoItem y=dfgroup.RetrieveItem("y");
  y.SetFloat(df.GetDefocusY());
  InfoItem ang=dfgroup.RetrieveItem("angle");
  ang.SetFloat(df.GetAstigmatismAngle());
}
Defocus InfoToDefocus(const ost::info::InfoGroup& ig)
{
    using namespace ost::info;
  Defocus result;
  InfoGroup dfgroup=ig.GetGroup("Defocus"); 
  result.SetDefocusX(dfgroup.GetItem("x").AsFloat());
  result.SetDefocusY(dfgroup.GetItem("y").AsFloat());
  result.SetAstigmatismAngle(dfgroup.GetItem("angle").AsFloat());
  return result;
}


} //ns

