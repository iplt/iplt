//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#ifndef TCIF_PARAMETERS_HH_
#define TCIF_PARAMETERS_HH_

#include "tilt.hh"
#include "defocus.hh"
#include "microscope_data.hh"

namespace iplt { 

/*
  convenience aggregate
*/
class DLLEXPORT_IPLT_BASE TCIFData: public MicroscopeData, 
			  public Defocus,
			  public TiltGeometry
{
public:
  TCIFData(const MicroscopeData& mic_data=MicroscopeData(),
	   const Defocus& defocus=Defocus(),
	   const TiltGeometry& tilt=TiltGeometry());
  void SetDefocus(const Defocus& d);
  void SetMicroscopeData(const MicroscopeData& m);
  void SetTiltGeometry(const TiltGeometry& t);
};
DLLEXPORT_IPLT_BASE void TCIFDataToInfo(const TCIFData& md, ost::info::InfoGroup& ig);
DLLEXPORT_IPLT_BASE TCIFData InfoToTCIFData(const ost::info::InfoGroup& ig);

} //ns


#endif /*TCIF_PARAMETERS_HH_*/
