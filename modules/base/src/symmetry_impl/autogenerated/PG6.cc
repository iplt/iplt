
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/////////////////////////////////////////////////////////
// 
// This file was autogenerated from a slightly modified 
// version of the ccp4 syminfo.lib.
// 
//            http://www.ccp4.ac.uk/main.html
// 
/////////////////////////////////////////////////////////

/*
  author: Andreas Schenk
*/


#include "PG6.hh"

namespace iplt {  namespace symmetry {

P_6::P_6():SymmetryImplementation(168,"P 6","PG6")
{
	opvec_.push_back(SymmetryOperator(Mat3(1,-1,0,1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,1,0,-1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,1,0,0,0,1)));
}

bool P_6::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

P_61::P_61():SymmetryImplementation(169,"P 61","PG6")
{
	opvec_.push_back(SymmetryOperator(Mat3(1,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/6.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,2.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,5.0/6.0)));
}

bool P_61::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

P_65::P_65():SymmetryImplementation(170,"P 65","PG6")
{
	opvec_.push_back(SymmetryOperator(Mat3(1,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,5.0/6.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,2.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/6.0)));
}

bool P_65::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

P_62::P_62():SymmetryImplementation(171,"P 62","PG6")
{
	opvec_.push_back(SymmetryOperator(Mat3(1,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,2.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,2.0/3.0)));
}

bool P_62::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

P_64::P_64():SymmetryImplementation(172,"P 64","PG6")
{
	opvec_.push_back(SymmetryOperator(Mat3(1,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,2.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,2.0/3.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/3.0)));
}

bool P_64::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

P_63::P_63():SymmetryImplementation(173,"P 63","PG6")
{
	opvec_.push_back(SymmetryOperator(Mat3(1,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,1,0,-1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
}

bool P_63::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

}} //ns
