
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/////////////////////////////////////////////////////////
// 
// This file was autogenerated from a slightly modified 
// version of the ccp4 syminfo.lib.
// 
//            http://www.ccp4.ac.uk/main.html
// 
/////////////////////////////////////////////////////////

/*
  author: Andreas Schenk
*/

#include <iplt/symmetry_impl/symmetry_impl_holder.hh>
#include <iplt/symmetry_impl/symmetry_impl.hh>
#include "PGmm2.hh"

namespace iplt {  namespace symmetry {

	
void SymmetryImplementationHolder::create_PGmm2()
{
	addsymmetry_(SymmetryImplementationPtr(new P_m_m_2),25,"P m m 2,P 2 -2");
	addsymmetry_(SymmetryImplementationPtr(new P_2_m_m),0,"P 2 m m,P 2 -2 (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_m_2_m),0,"P m 2 m,P 2 -2 (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_m_c_21),26,"P m c 21,P 2c -2");
	addsymmetry_(SymmetryImplementationPtr(new P_c_m_21),0,"P c m 21,P 2c -2 (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_m_a),0,"P 21 m a,P 2c -2 (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_a_m),0,"P 21 a m,P 2c -2 (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new P_b_21_m),0,"P b 21 m,P 2c -2 (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_m_21_b),0,"P m 21 b,P 2c -2 (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_c_c_2),27,"P c c 2,P 2 -2c");
	addsymmetry_(SymmetryImplementationPtr(new P_2_a_a),0,"P 2 a a,P 2 -2c (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_b_2_b),0,"P b 2 b,P 2 -2c (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_m_a_2),28,"P m a 2,P 2 -2a");
	addsymmetry_(SymmetryImplementationPtr(new P_b_m_2),0,"P b m 2,P 2 -2a (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_2_m_b),0,"P 2 m b,P 2 -2a (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_2_c_m),0,"P 2 c m,P 2 -2a (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new P_c_2_m),0,"P c 2 m,P 2 -2a (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_m_2_a),0,"P m 2 a,P 2 -2a (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_c_a_21),29,"P c a 21,P 2c -2ac");
	addsymmetry_(SymmetryImplementationPtr(new P_b_c_21),0,"P b c 21,P 2c -2ac (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_a_b),0,"P 21 a b,P 2c -2ac (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_c_a),0,"P 21 c a,P 2c -2ac (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new P_c_21_b),0,"P c 21 b,P 2c -2ac (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_b_21_a),0,"P b 21 a,P 2c -2ac (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_n_c_2),30,"P n c 2,P 2 -2bc");
	addsymmetry_(SymmetryImplementationPtr(new P_c_n_2),0,"P c n 2,P 2 -2bc (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_2_n_a),0,"P 2 n a,P 2 -2bc (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_2_a_n),0,"P 2 a n,P 2 -2bc (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new P_b_2_n),0,"P b 2 n,P 2 -2bc (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_n_2_b),0,"P n 2 b,P 2 -2bc (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_m_n_21),31,"P m n 21,P 2ac -2");
	addsymmetry_(SymmetryImplementationPtr(new P_n_m_21),0,"P n m 21,P 2ac -2 (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_m_n),0,"P 21 m n,P 2ac -2 (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_n_m),0,"P 21 n m,P 2ac -2 (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new P_n_21_m),0,"P n 21 m,P 2ac -2 (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_m_21_n),0,"P m 21 n,P 2ac -2 (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_b_a_2),32,"P b a 2,P 2 -2ab");
	addsymmetry_(SymmetryImplementationPtr(new P_2_c_b),0,"P 2 c b,P 2 -2ab (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_c_2_a),0,"P c 2 a,P 2 -2ab (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_n_a_21),33,"P n a 21,P 2c -2n");
	addsymmetry_(SymmetryImplementationPtr(new P_b_n_21),0,"P b n 21,P 2c -2n (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_n_b),0,"P 21 n b,P 2c -2n (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_c_n),0,"P 21 c n,P 2c -2n (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new P_c_21_n),0,"P c 21 n,P 2c -2n (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_n_21_a),0,"P n 21 a,P 2c -2n (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_n_n_2),34,"P n n 2,P 2 -2n");
	addsymmetry_(SymmetryImplementationPtr(new P_2_n_n),0,"P 2 n n,P 2 -2n (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_n_2_n),0,"P n 2 n,P 2 -2n (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new C_m_m_2),35,"C m m 2,C 2 -2");
	addsymmetry_(SymmetryImplementationPtr(new A_2_m_m),0,"A 2 m m,C 2 -2 (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new B_m_2_m),0,"B m 2 m,C 2 -2 (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new C_m_c_21),36,"C m c 21,C 2c -2");
	addsymmetry_(SymmetryImplementationPtr(new C_c_m_21),0,"C c m 21,C 2c -2 (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new A_21_m_a),0,"A 21 m a,C 2c -2 (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new A_21_a_m),0,"A 21 a m,C 2c -2 (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new B_b_21_m),0,"B b 21 m,C 2c -2 (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new B_m_21_b),0,"B m 21 b,C 2c -2 (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new C_c_c_2),37,"C c c 2,C 2 -2c");
	addsymmetry_(SymmetryImplementationPtr(new A_2_a_a),0,"A 2 a a,C 2 -2c (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new B_b_2_b),0,"B b 2 b,C 2 -2c (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new A_m_m_2),38,"A m m 2,A 2 -2");
	addsymmetry_(SymmetryImplementationPtr(new B_m_m_2),0,"B m m 2,A 2 -2 (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new B_2_m_m),0,"B 2 m m,A 2 -2 (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new C_2_m_m),0,"C 2 m m,A 2 -2 (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new C_m_2_m),0,"C m 2 m,A 2 -2 (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new A_m_2_m),0,"A m 2 m,A 2 -2 (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new A_b_m_2),39,"A b m 2,A 2 -2b");
	addsymmetry_(SymmetryImplementationPtr(new B_m_a_2),0,"B m a 2,A 2 -2b (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new B_2_c_m),0,"B 2 c m,A 2 -2b (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new C_2_m_b),0,"C 2 m b,A 2 -2b (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new C_m_2_a),0,"C m 2 a,A 2 -2b (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new A_c_2_m),0,"A c 2 m,A 2 -2b (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new A_m_a_2),40,"A m a 2,A 2 -2a");
	addsymmetry_(SymmetryImplementationPtr(new B_b_m_2),0,"B b m 2,A 2 -2a (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new B_2_m_b),0,"B 2 m b,A 2 -2a (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new C_2_c_m),0,"C 2 c m,A 2 -2a (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new C_c_2_m),0,"C c 2 m,A 2 -2a (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new A_m_2_a),0,"A m 2 a,A 2 -2a (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new A_b_a_2),41,"A b a 2,A 2 -2ab");
	addsymmetry_(SymmetryImplementationPtr(new B_b_a_2),0,"B b a 2,A 2 -2ab (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new B_2_c_b),0,"B 2 c b,A 2 -2ab (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new C_2_c_b),0,"C 2 c b,A 2 -2ab (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new C_c_2_a),0,"C c 2 a,A 2 -2ab (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new A_c_2_a),0,"A c 2 a,A 2 -2ab (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new F_m_m_2),42,"F m m 2,F 2 -2");
	addsymmetry_(SymmetryImplementationPtr(new F_2_m_m),0,"F 2 m m,F 2 -2 (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new F_m_2_m),0,"F m 2 m,F 2 -2 (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new F_d_d_2),43,"F d d 2,F 2 -2d");
	addsymmetry_(SymmetryImplementationPtr(new F_2_d_d),0,"F 2 d d,F 2 -2d (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new F_d_2_d),0,"F d 2 d,F 2 -2d (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new I_m_m_2),44,"I m m 2,I 2 -2");
	addsymmetry_(SymmetryImplementationPtr(new I_2_m_m),0,"I 2 m m,I 2 -2 (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new I_m_2_m),0,"I m 2 m,I 2 -2 (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new I_b_a_2),45,"I b a 2,I 2 -2c");
	addsymmetry_(SymmetryImplementationPtr(new I_2_c_b),0,"I 2 c b,I 2 -2c (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new I_c_2_a),0,"I c 2 a,I 2 -2c (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new I_m_a_2),46,"I m a 2,I 2 -2a");
	addsymmetry_(SymmetryImplementationPtr(new I_b_m_2),0,"I b m 2,I 2 -2a (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new I_2_m_b),0,"I 2 m b,I 2 -2a (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new I_2_c_m),0,"I 2 c m,I 2 -2a (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new I_c_2_m),0,"I c 2 m,I 2 -2a (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new I_m_2_a),0,"I m 2 a,I 2 -2a (-x,z,y)");

}

}} //ns
