
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/////////////////////////////////////////////////////////
// 
// This file was autogenerated from a slightly modified 
// version of the ccp4 syminfo.lib.
// 
//            http://www.ccp4.ac.uk/main.html
// 
/////////////////////////////////////////////////////////

/*
  author: Andreas Schenk
*/

#include <iplt/symmetry_impl/symmetry_impl_holder.hh>
#include <iplt/symmetry_impl/symmetry_impl.hh>
#include "PG2_s_m.hh"

namespace iplt {  namespace symmetry {

	
void SymmetryImplementationHolder::create_PG2_s_m()
{
	addsymmetry_(SymmetryImplementationPtr(new P_1_2_s_m_1),10,"P 1 2/m 1,-P 2y");
	addsymmetry_(SymmetryImplementationPtr(new P_1_1_2_s_m),1010,"P 1 1 2/m,-P 2y (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_2_s_m_1_1),0,"P 2/m 1 1,-P 2y (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_21_s_m_1),11,"P 1 21/m 1,-P 2yb");
	addsymmetry_(SymmetryImplementationPtr(new P_1_1_21_s_m),1011,"P 1 1 21/m,-P 2yb (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_s_m_1_1),0,"P 21/m 1 1,-P 2yb (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new C_1_2_s_m_1),12,"C 1 2/m 1,-C 2y");
	addsymmetry_(SymmetryImplementationPtr(new A_1_2_s_m_1),0,"A 1 2/m 1,-C 2y (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new I_1_2_s_m_1),0,"I 1 2/m 1,-C 2y (x,y,-x+z)");
	addsymmetry_(SymmetryImplementationPtr(new A_1_1_2_s_m),0,"A 1 1 2/m,-C 2y (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new B_1_1_2_s_m),1012,"B 1 1 2/m,-C 2y (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new I_1_1_2_s_m),0,"I 1 1 2/m,-C 2y (-x+z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new B_2_s_m_1_1),0,"B 2/m 1 1,-C 2y (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new C_2_s_m_1_1),0,"C 2/m 1 1,-C 2y (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new I_2_s_m_1_1),0,"I 2/m 1 1,-C 2y (y,-x+z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_2_s_c_1),13,"P 1 2/c 1,-P 2yc");
	addsymmetry_(SymmetryImplementationPtr(new P_1_2_s_n_1),0,"P 1 2/n 1,-P 2yc (x-z,y,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_2_s_a_1),0,"P 1 2/a 1,-P 2yc (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_1_2_s_a),0,"P 1 1 2/a,-P 2yc (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_1_2_s_n),0,"P 1 1 2/n,-P 2yc (z,x-z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_1_2_s_b),1013,"P 1 1 2/b,-P 2yc (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_2_s_b_1_1),0,"P 2/b 1 1,-P 2yc (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_2_s_n_1_1),0,"P 2/n 1 1,-P 2yc (y,z,x-z)");
	addsymmetry_(SymmetryImplementationPtr(new P_2_s_c_1_1),0,"P 2/c 1 1,-P 2yc (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_21_s_c_1),14,"P 1 21/c 1,-P 2ybc");
	addsymmetry_(SymmetryImplementationPtr(new P_1_21_s_n_1),0,"P 1 21/n 1,-P 2ybc (x-z,y,z)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_21_s_a_1),0,"P 1 21/a 1,-P 2ybc (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_1_21_s_a),0,"P 1 1 21/a,-P 2ybc (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_1_21_s_n),0,"P 1 1 21/n,-P 2ybc (z,x-z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_1_1_21_s_b),1014,"P 1 1 21/b,-P 2ybc (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_s_b_1_1),0,"P 21/b 1 1,-P 2ybc (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_s_n_1_1),0,"P 21/n 1 1,-P 2ybc (y,z,x-z)");
	addsymmetry_(SymmetryImplementationPtr(new P_21_s_c_1_1),0,"P 21/c 1 1,-P 2ybc (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new C_1_2_s_c_1),15,"C 1 2/c 1,-C 2yc");
	addsymmetry_(SymmetryImplementationPtr(new A_1_2_s_n_1),0,"A 1 2/n 1,-C 2yc (z,y,-x+z)");
	addsymmetry_(SymmetryImplementationPtr(new I_1_2_s_a_1),0,"I 1 2/a 1,-C 2yc (x+z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new A_1_2_s_a_1),0,"A 1 2/a 1,-C 2yc (z,y,-x)");
	addsymmetry_(SymmetryImplementationPtr(new C_1_2_s_n_1),0,"C 1 2/n 1,-C 2yc (x+1/4,y-1/4,z)");
	addsymmetry_(SymmetryImplementationPtr(new I_1_2_s_c_1),0,"I 1 2/c 1,-C 2yc (x,y,-x+z)");
	addsymmetry_(SymmetryImplementationPtr(new A_1_1_2_s_a),0,"A 1 1 2/a,-C 2yc (z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new B_1_1_2_s_n),0,"B 1 1 2/n,-C 2yc (-x+z,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new I_1_1_2_s_b),0,"I 1 1 2/b,-C 2yc (-x,x+z,y)");
	addsymmetry_(SymmetryImplementationPtr(new B_1_1_2_s_b),1015,"B 1 1 2/b,-C 2yc (-x,z,y)");
	addsymmetry_(SymmetryImplementationPtr(new A_1_1_2_s_n),0,"A 1 1 2/n,-C 2yc (z,x-z,y)");
	addsymmetry_(SymmetryImplementationPtr(new I_1_1_2_s_a),0,"I 1 1 2/a,-C 2yc (-x+z,x,y)");
	addsymmetry_(SymmetryImplementationPtr(new B_2_s_b_1_1),0,"B 2/b 1 1,-C 2yc (y,z,x)");
	addsymmetry_(SymmetryImplementationPtr(new C_2_s_n_1_1),0,"C 2/n 1 1,-C 2yc (y,-x+z,z)");
	addsymmetry_(SymmetryImplementationPtr(new I_2_s_c_1_1),0,"I 2/c 1 1,-C 2yc (y,-x,x+z)");
	addsymmetry_(SymmetryImplementationPtr(new C_2_s_c_1_1),0,"C 2/c 1 1,-C 2yc (y,-x,z)");
	addsymmetry_(SymmetryImplementationPtr(new B_2_s_n_1_1),0,"B 2/n 1 1,-C 2yc (y,z,x-z)");
	addsymmetry_(SymmetryImplementationPtr(new I_2_s_b_1_1),0,"I 2/b 1 1,-C 2yc (y,-x+z,x)");

}

}} //ns
