
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/////////////////////////////////////////////////////////
// 
// This file was autogenerated from a slightly modified 
// version of the ccp4 syminfo.lib.
// 
//            http://www.ccp4.ac.uk/main.html
// 
/////////////////////////////////////////////////////////

/*
  author: Andreas Schenk
*/

#ifndef IPLT_EX_SYMMETRY_PG4mm_H
#define IPLT_EX_SYMMETRY_PG4mm_H

#include "PG1.hh"

namespace iplt {  namespace symmetry {



class P_4_m_m: public SymmetryImplementation{
public:
  P_4_m_m();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class P_4_b_m: public SymmetryImplementation{
public:
  P_4_b_m();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class P_42_c_m: public SymmetryImplementation{
public:
  P_42_c_m();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class P_42_n_m: public SymmetryImplementation{
public:
  P_42_n_m();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class P_4_c_c: public SymmetryImplementation{
public:
  P_4_c_c();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class P_4_n_c: public SymmetryImplementation{
public:
  P_4_n_c();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class P_42_m_c: public SymmetryImplementation{
public:
  P_42_m_c();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class P_42_b_c: public SymmetryImplementation{
public:
  P_42_b_c();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class I_4_m_m: public SymmetryImplementation{
public:
  I_4_m_m();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class I_4_c_m: public SymmetryImplementation{
public:
  I_4_c_m();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class I_41_m_d: public SymmetryImplementation{
public:
  I_41_m_d();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};

class I_41_c_d: public SymmetryImplementation{
public:
  I_41_c_d();
  virtual bool IsInAsymmetricUnit(ReflectionIndex index);
};
}} //ns

#endif

