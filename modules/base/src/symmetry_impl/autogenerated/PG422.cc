
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/////////////////////////////////////////////////////////
// 
// This file was autogenerated from a slightly modified 
// version of the ccp4 syminfo.lib.
// 
//            http://www.ccp4.ac.uk/main.html
// 
/////////////////////////////////////////////////////////

/*
  author: Andreas Schenk
*/


#include "PG422.hh"

namespace iplt {  namespace symmetry {

P_4_2_2::P_4_2_2():SymmetryImplementation(89,"P 4 2 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1)));
}

bool P_4_2_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

P_4_21_2::P_4_21_2():SymmetryImplementation(90,"P 4 21 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,0.0/1.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,0.0/1.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,0.0/1.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,0.0/1.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1)));
}

bool P_4_21_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

P_41_2_2::P_41_2_2():SymmetryImplementation(91,"P 41 2 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/4.0)));
}

bool P_41_2_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

P_41_21_2::P_41_21_2():SymmetryImplementation(92,"P 41 21 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
}

bool P_41_21_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

P_42_2_2::P_42_2_2():SymmetryImplementation(93,"P 42 2 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
}

bool P_42_2_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

P_42_21_2::P_42_21_2():SymmetryImplementation(94,"P 42 21 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1)));
}

bool P_42_21_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

P_43_2_2::P_43_2_2():SymmetryImplementation(95,"P 43 2 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,3.0/4.0)));
}

bool P_43_2_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

P_43_21_2::P_43_21_2():SymmetryImplementation(96,"P 43 21 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
}

bool P_43_21_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

I_4_2_2::I_4_2_2():SymmetryImplementation(97,"I 4 2 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1)));
}

bool I_4_2_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

I_41_2_2::I_41_2_2():SymmetryImplementation(98,"I 41 2 2","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,1.0/2.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(1.0/2.0,0.0/1.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1),Vec3(0.0/1.0,1.0/2.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1),Vec3(1.0/2.0,0.0/1.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1)));
}

bool I_41_2_2::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

SG1094::SG1094():SymmetryImplementation(1094,"SG1094","PG422")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,1.0/2.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,0.0/1.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(1.0/2.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(1,0,0,0,-1,0,0,0,-1),Vec3(1.0/2.0,0.0/1.0,0.0/1.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,1,0,0,0,0,-1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,1,0,0,0,-1),Vec3(0.0/1.0,1.0/2.0,0.0/1.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,-1,0,0,0,0,-1),Vec3(1.0/2.0,1.0/2.0,1.0/2.0)));
}

bool SG1094::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetH()>=index.GetK() && index.GetK()>=0 && index.GetZStar()>=0;
}

}} //ns
