
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/////////////////////////////////////////////////////////
// 
// This file was autogenerated from a slightly modified 
// version of the ccp4 syminfo.lib.
// 
//            http://www.ccp4.ac.uk/main.html
// 
/////////////////////////////////////////////////////////

/*
  author: Andreas Schenk
*/


#include "PG4.hh"

namespace iplt {  namespace symmetry {

P_4::P_4():SymmetryImplementation(75,"P 4","PG4")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1)));
}

bool P_4::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

P_41::P_41():SymmetryImplementation(76,"P 41","PG4")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,3.0/4.0)));
}

bool P_41::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

P_42::P_42():SymmetryImplementation(77,"P 42","PG4")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
}

bool P_42::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

P_43::P_43():SymmetryImplementation(78,"P 43","PG4")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,3.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(0.0/1.0,0.0/1.0,1.0/4.0)));
}

bool P_43::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

I_4::I_4():SymmetryImplementation(79,"I 4","PG4")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1)));
}

bool I_4::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

I_41::I_41():SymmetryImplementation(80,"I 41","PG4")
{
	opvec_.push_back(SymmetryOperator(Mat3(0,-1,0,1,0,0,0,0,1),Vec3(0.0/1.0,1.0/2.0,1.0/4.0)));
	opvec_.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,1),Vec3(1.0/2.0,1.0/2.0,1.0/2.0)));
	opvec_.push_back(SymmetryOperator(Mat3(0,1,0,-1,0,0,0,0,1),Vec3(1.0/2.0,0.0/1.0,3.0/4.0)));
}

bool I_41::IsInAsymmetricUnit(ReflectionIndex index)
{
  return index.GetZStar()>=0 && ((index.GetH()>=0 && index.GetK()>0) || (index.GetH()==0 && index.GetK()==0));
}

}} //ns
