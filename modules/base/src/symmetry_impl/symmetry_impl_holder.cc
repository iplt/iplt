//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#include <iostream>
#include <sstream>
#include <iplt/symmetry_exception.hh>

#include "symmetry_impl_holder.hh"

namespace iplt {  namespace symmetry {


SymmetryImplementationHolder& SymmetryImplementationHolder::Instance()
{
  static SymmetryImplementationHolder instance;
  return instance;
}

SymmetryImplementationHolder::SymmetryImplementationHolder():
  idmappingtable_(),
  namemappingtable_(),
  symlist_()
{
  create_PG1();
  create_PG222();
  create_PG23();
  create_PG2();
  create_PG2_s_m();
  create_PG312();
  create_PG31m();
  create_PG321();
  create_PG32();
  create_PG3();
  create_PG3m1();
  create_PG3m();
  create_PG422();
  create_PG432();
  create_PG4();
  create_PG4mm();
  create_PG4_s_m();
  create_PG4_s_mmm();
  create_PG622();
  create_PG6();
  create_PG6mm();
  create_PG6_s_m();
  create_PG6_s_mmm();
  create_PG_m_1();
  create_PG_m_31m();
  create_PG_m_3();
  create_PG_m_3m1();
  create_PG_m_3m();
  create_PG_m_42m();
  create_PG_m_43m();
  create_PG_m_4();
  create_PG_m_4m2();
  create_PG_m_62m();
  create_PG_m_6();
  create_PG_m_6m2();
  create_PGm();
  create_PGmm2();
  create_PGm_m_3();
  create_PGm_m_3m();
  create_PGmmm();
}

std::vector<unsigned int> SymmetryImplementationHolder::GetImplementedSymmetries() const
{
  std::vector<unsigned int> nrvo;
  for(IdTable::const_iterator it=idmappingtable_.begin();it!=idmappingtable_.end();++it) {
    nrvo.push_back(it->first);
  }
  return nrvo;
}

SymmetryImplementationPtr SymmetryImplementationHolder::GetSymmetryImplementation(const String& name)
{
  NameTable::const_iterator it=namemappingtable_.find(name);
  if(it==namemappingtable_.end()){
	  std::stringstream s; 
	  s<< "symmetry not found: "<< name;
    throw SymmetryException(s.str());
  }
  return symlist_.at(it->second);
}

SymmetryImplementationPtr SymmetryImplementationHolder::GetSymmetryImplementation(unsigned id)
{
  IdTable::iterator it=idmappingtable_.find(id);
  if(it==idmappingtable_.end()){
	  std::stringstream s; 
	  s<< "symmetry not found: "<< id;
	  throw SymmetryException(s.str());
  }
  return symlist_.at(it->second);
}

void SymmetryImplementationHolder::addsymmetry_(const SymmetryImplementationPtr& sp,unsigned id, std::string names)
{
  assert(this);
  unsigned pos=symlist_.size();
  namemappingtable_.insert(std::make_pair(names,pos));
  boost::tokenizer<boost::escaped_list_separator<char> > tok(names);
  for(boost::tokenizer<boost::escaped_list_separator<char> >::iterator beg=tok.begin(); beg!=tok.end();++beg){
    String nam=*beg;
    namemappingtable_.insert(std::make_pair(nam,pos));
  }
  if(id>0){
    idmappingtable_.insert(std::make_pair(id,pos));
  }
  symlist_.push_back(sp);
}
}} //ns
