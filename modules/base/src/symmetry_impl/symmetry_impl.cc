//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#include <ost/message.hh>
#include <ost/img/phase.hh>
#include <iplt/symmetry_exception.hh>

#include "symmetry_impl.hh"

namespace iplt {  namespace symmetry {



SymmetryImplementation::SymmetryImplementation(unsigned int number,String name,String pgname):
	number_(number),
	name_(name),
	pgname_(pgname)
{
}

void SymmetryImplementation::Init()
{
    setup_centric_zones_();
}
	

std::vector<ReflectionIndex> SymmetryImplementation::GenerateSymmetryRelatedPoints(ReflectionIndex p,bool create_friedelmates)
{
  std::vector<ReflectionIndex> result;
  result.push_back(p);
  for(Opvec::const_iterator it=opvec_.begin(); it!=opvec_.end(); it++)
  {
    result.push_back(it->Rotate(p));
  }
  if(create_friedelmates){
    SymmetryOperator hermitian(Mat3(-1,0,0,0,-1,0,0,0,-1),Vec3(0,0,0),-1.0);
    std::vector<ReflectionIndex> result2;
    for(std::vector<ReflectionIndex>::const_iterator it=result.begin(); it!=result.end(); it++)
    {
      result2.push_back(*it);
      result2.push_back(hermitian.Rotate(*it));
    }
    return result2;
  }else{
    return result;
  }
}
std::vector<Vec3> SymmetryImplementation::GenerateSymmetryRelatedPositions(Vec3 p)
{
  std::vector<Vec3> result;
  result.push_back(p);
  for(Opvec::const_iterator it=opvec_.begin(); it!=opvec_.end(); it++)
  {
    geom::Vec3 t=it->rotation_*p+it->translation_;
    t[0]=t[0]-floor(t[0]);
    t[1]=t[1]-floor(t[0]);
    t[2]=t[2]-floor(t[0]);
    if(t[0]<0.0){
      t[0]=t[0]+1.0;
    }
    if(t[1]<0.0){
      t[1]=t[1]+1.0;
    }
    if(t[2]<0.0){
      t[2]=t[2]+1.0;
    }
    result.push_back(t);
  }
  return result;
}

bool SymmetryImplementation::IsCentric(const ReflectionIndex& r) const
{
  for(Opvec::const_iterator it=opvec_.begin(); it!=opvec_.end(); it++) {
    ReflectionIndex p=it->Rotate(r);
    if(r==-p) {
      return true;
    }
  }
  return false;
}


ReflectionIndex SymmetryImplementation::Symmetrize(ReflectionIndex r)
{
  //code duplicated 
  Opvec opvechermitian(opvec_);
  opvechermitian.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,-1),Vec3(0,0,0),-1.0));
  unsigned long maxnum=1;
  for(unsigned int i=0;i<=opvechermitian.size();++i){
	  maxnum*=2;
  }
  for(unsigned long num=0;num<maxnum;++num){
    ReflectionIndex newr=r;
    for(unsigned int j=0;j<opvechermitian.size();++j){
    	if((1<<j & num)){
    	      newr=opvechermitian[j].Rotate(newr);
        }
    }
    if(IsInAsymmetricUnit(newr)){
        return newr;
    }
  }
  throw(SymmetryIndexException(r));
  return r;
}
Real SymmetryImplementation::Symmetrize(ReflectionIndex r,Real phase)
{
  //code duplicated 
  Opvec opvechermitian(opvec_);
  opvechermitian.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,-1),Vec3(0,0,0),-1.0));
  unsigned long maxnum=1;
  for(unsigned int i=0;i<=opvechermitian.size();++i){
	  maxnum*=2;
  }
  for(unsigned long num=0;num<maxnum;++num){
    ReflectionIndex newr=r;
    Real newphase=phase;
    for(unsigned int j=0;j<opvechermitian.size();++j){
    	if((1<<j & num)){
          newr=opvechermitian[j].Rotate(newr);
          newphase=opvechermitian[j].Translate(newr,newphase);
        }
    }
    if(IsInAsymmetricUnit(newr)){
      return newphase;
    }
  }
  throw(SymmetryIndexException(r));
  return phase;
}

unsigned int SymmetryImplementation::GetSpacegroupNumber(){return number_;}
String SymmetryImplementation::GetSpacegroupName(){return name_;}
String SymmetryImplementation::GetPointgroupName(){return pgname_;}

SymmetryReflection SymmetryImplementation::Reduce(const SymmetryReflection& reflection)
{
  Opvec opvechermitian(opvec_);
  opvechermitian.push_back(SymmetryOperator(Mat3(-1,0,0,0,-1,0,0,0,-1),Vec3(0,0,0),-1.0));
  unsigned long maxnum=1;// number of possible combinations of SymmetryOperations
  for(unsigned int i=0;i<=opvechermitian.size();++i){
	  maxnum*=2;
  }
  for(unsigned long num=0;num<maxnum;++num){
	ReflectionIndex newr=reflection.GetReflectionIndex();
    Real newphase=reflection.GetPhase();
    for(unsigned int j=0;j<opvechermitian.size();++j){
    	if((1<<j & num)){ // bit j from the right encodes if SymmetryOperation[j] should be used in this combination
    	      newr=opvechermitian[j].Rotate(newr);
    	      newphase=opvechermitian[j].Translate(newr,newphase);
        }
    }
    if(IsInAsymmetricUnit(newr)){
        return SymmetryReflection(newr,reflection.GetAmplitude(),newphase);
    }
  }
  throw(SymmetryIndexException(reflection.GetReflectionIndex()));
  return SymmetryReflection();
}

SymmetryReflectionList SymmetryImplementation::Expand(const SymmetryReflection& reflection,bool create_friedelmates)
{
  SymmetryReflectionList result;
  result.push_back(reflection);
  for(Opvec::const_iterator it=opvec_.begin(); it!=opvec_.end(); it++)
  {
    Real phase=reflection.GetPhase();
    ReflectionIndex idx=reflection.GetReflectionIndex();
    result.push_back(SymmetryReflection(it->Rotate(idx),reflection.GetAmplitude(),it->Translate(idx,reflection.GetPhase())));
  }
  if(create_friedelmates){
    SymmetryOperator hermitian(Mat3(-1,0,0,0,-1,0,0,0,-1),Vec3(0,0,0),-1.0);
    SymmetryReflectionList result2;
    for(SymmetryReflectionList::const_iterator it=result.begin(); it!=result.end(); it++)
    {
      result2.push_back(*it);
      result2.push_back(SymmetryReflection(hermitian.Rotate(it->GetReflectionIndex()),it->GetAmplitude(),hermitian.Translate(it->GetReflectionIndex(),it->GetPhase())));
    }
    return result2;
  }else{
    return result;
  }
}

SymmetryReflection SymmetryImplementation::Restrict(const SymmetryReflection& reflection)
{
  
  SymmetryReflection result(reflection);
  ReflectionIndex index=reflection.GetReflectionIndex();
  if(IsSystematicAbsence(index)){
    result.SetValue(Complex(0.0,0.0));
    return result;
  }
  for (unsigned int i = 0; i < 12; ++i){
    if (centrics_[i]){ 
      if (is_in_centric_zone_(i,index)) {
        int isym = centrics_[i];
        ost::img::Phase centric_phase(-opvec_[isym-1].Translate(index,0.0)/2.0);
        if(abs(static_cast<Real>(centric_phase-ost::img::Phase(result.GetPhase()))) < abs(static_cast<Real>(centric_phase+ost::img::Phase(M_PI)-ost::img::Phase(result.GetPhase()))) ){
          result.SetPhase(static_cast<Real>(centric_phase));
        }else{
          result.SetPhase(static_cast<Real>(centric_phase+ost::img::Phase(M_PI)));
        }
        return result;
      }
    }
  }
  return reflection;
}

SymmetryReflectionList SymmetryImplementation::Reduce(const SymmetryReflectionList& reflectionlist)
{
  SymmetryReflectionList result;
  for(SymmetryReflectionList::const_iterator it=reflectionlist.begin(); it!=reflectionlist.end(); it++){
    result.push_back(Reduce(*it));
  }
  return result;
}
SymmetryReflectionList SymmetryImplementation::Expand(const SymmetryReflectionList& reflectionlist,bool create_friedelmates)
{
  SymmetryReflectionList result;
  for(SymmetryReflectionList::const_iterator it=reflectionlist.begin(); it!=reflectionlist.end(); it++){
    SymmetryReflectionList tmp=Expand(*it, create_friedelmates);
    result.insert(result.end(),tmp.begin(),tmp.end());
  }
  return result;
}
SymmetryReflectionList SymmetryImplementation::Restrict(const SymmetryReflectionList& reflectionlist)
{
  SymmetryReflectionList result;
  for(SymmetryReflectionList::const_iterator it=reflectionlist.begin(); it!=reflectionlist.end(); it++){
    result.push_back(Restrict(*it));
  }
  return result;
}

bool SymmetryImplementation::IsInAsymmetricUnit(const SymmetryReflection& reflection)
{
  return IsInAsymmetricUnit(reflection.GetReflectionIndex());
}
bool SymmetryImplementation::IsCentric(const SymmetryReflection& reflection) const
{
  return IsCentric(reflection.GetReflectionIndex());
}

void SymmetryImplementation::setup_centric_zones_()
{
  std::vector<ReflectionIndex> indexes;
  indexes.push_back(ReflectionIndex(0,1,2));
  indexes.push_back(ReflectionIndex(1,0,2));
  indexes.push_back(ReflectionIndex(1,2,0));
  indexes.push_back(ReflectionIndex(1,1,10));
  indexes.push_back(ReflectionIndex(1,10,1));
  indexes.push_back(ReflectionIndex(10,1,1));
  indexes.push_back(ReflectionIndex(1,-1,10));
  indexes.push_back(ReflectionIndex(1,10,-1));
  indexes.push_back(ReflectionIndex(10,1,-1));
  indexes.push_back(ReflectionIndex(-1,2,10));
  indexes.push_back(ReflectionIndex(2,-1,10));
  indexes.push_back(ReflectionIndex(1,4,8));

  /* loop over all possible centric zones */
  for (unsigned int i = 0; i < 12; ++i) {
    centrics_[i] = 0;
    for (unsigned int j = 0; j < opvec_.size(); ++j) {
      ReflectionIndex newindex=opvec_[j].Rotate(indexes[i]);
      if(newindex==-indexes[i]){
        centrics_[i] = j+1;
        break;
      }
    }
  }
}
bool SymmetryImplementation::is_in_centric_zone_(unsigned int zone,const ReflectionIndex& r)
{
  switch (zone) {
  case 0:
    return r.GetH()==0;
  case 1:
    return r.GetK()==0;
  case 2:
    return r.GetZStar()==0;
  case 3:
    return r.GetH() - r.GetK()==0;
  case 4:
    return r.GetH() - r.GetZStar()==0;
  case 5:
    return r.GetK() - r.GetZStar()==0;
  case 6:
    return r.GetH() + r.GetK()==0;
  case 7:
    return r.GetH() + r.GetZStar()==0;
  case 8:
    return r.GetK() + r.GetZStar()==0;
  case 9:
    return 2*r.GetH() + r.GetK()==0;
  case 10:
    return r.GetH() + 2*r.GetK()==0;
  case 11:
    return true;
  }
  throw("Invalid nzone !");
  return 0;
}

bool SymmetryImplementation::IsSystematicAbsence(const ReflectionIndex& index)
{
  int j,hnew,knew,lnew;
  float del_phas;
  if (opvec_.size() > 1) {
   for (j = 0; j < opvec_.size(); ++j) {
     ReflectionIndex newindex=opvec_[j].Rotate(index);
     if(index==newindex){
       if(abs(opvec_[j].Translate(index,0.0))>1e-10){
         return true;
       }
     }
   }
  }
  return false;

}


}} //ns
