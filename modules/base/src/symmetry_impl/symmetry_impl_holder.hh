//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#ifndef IPLT_EX_SYMMETRY_IMPL_HOLDER_H
#define IPLT_EX_SYMMETRY_IMPL_HOLDER_H

#include <map>
#include <vector>
#include "symmetry_impl.hh"
#include <string>
#include<boost/tokenizer.hpp>

  
namespace iplt {  namespace symmetry {

class SymmetryImplementationHolder
{
  typedef std::map<unsigned,unsigned> IdTable;
  typedef std::map<String,unsigned> NameTable;
  typedef std::vector<SymmetryImplementationPtr> Symlist;

public:
  static SymmetryImplementationHolder& Instance();
  SymmetryImplementationPtr GetSymmetryImplementation(const String& name);
  SymmetryImplementationPtr GetSymmetryImplementation(unsigned id);

  std::vector<unsigned int> GetImplementedSymmetries() const;

private:
  SymmetryImplementationHolder();
  SymmetryImplementationHolder(const SymmetryImplementationHolder& h) {}
  SymmetryImplementationHolder& operator=(const SymmetryImplementationHolder& h) {return *this;}

  void addsymmetry_(const SymmetryImplementationPtr&,unsigned id, std::string names);
  void create_PG1();
  void create_PG222();
  void create_PG23();
  void create_PG2();
  void create_PG2_s_m();
  void create_PG312();
  void create_PG31m();
  void create_PG321();
  void create_PG32();
  void create_PG3();
  void create_PG3m1();
  void create_PG3m();
  void create_PG422();
  void create_PG432();
  void create_PG4();
  void create_PG4mm();
  void create_PG4_s_m();
  void create_PG4_s_mmm();
  void create_PG622();
  void create_PG6();
  void create_PG6mm();
  void create_PG6_s_m();
  void create_PG6_s_mmm();
  void create_PG_m_1();
  void create_PG_m_31m();
  void create_PG_m_3();
  void create_PG_m_3m1();
  void create_PG_m_3m();
  void create_PG_m_42m();
  void create_PG_m_43m();
  void create_PG_m_4();
  void create_PG_m_4m2();
  void create_PG_m_62m();
  void create_PG_m_6();
  void create_PG_m_6m2();
  void create_PGm();
  void create_PGmm2();
  void create_PGm_m_3();
  void create_PGm_m_3m();
  void create_PGmmm();
  IdTable idmappingtable_;
  NameTable namemappingtable_;
  Symlist symlist_;
};

}} //ns

#endif
