//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/


#include <cmath>
#include <complex>
#include "symmetry_reflection.hh"


namespace iplt { 

SymmetryReflection::SymmetryReflection():
  index_(0,0,0.0),
  val_()
{
}

SymmetryReflection::SymmetryReflection(int h,int k,Real zstar, Complex val):
  index_(h,k,zstar),
  val_(val)
{
}

SymmetryReflection::SymmetryReflection(int h,int k,Real zstar, Real amp, Real phase):
  index_(h,k,zstar),
  val_(std::polar(amp,phase))
{
}

SymmetryReflection::SymmetryReflection(const ReflectionIndex& idx, Complex val):
  index_(idx),
  val_(val)
{
}

SymmetryReflection::SymmetryReflection(const ReflectionIndex& idx, Real amp, Real phase):
  index_(idx),
  val_(std::polar(amp,phase))
{
}

Real SymmetryReflection::GetAmplitude() const
{
  return abs(val_);
}

Real SymmetryReflection::GetPhase() const
{
  return arg(val_);
}

Complex SymmetryReflection::GetValue() const
{
  return val_;
}

ReflectionIndex SymmetryReflection::GetReflectionIndex() const
{
  return index_;
}

void SymmetryReflection::SetAmplitude(Real amplitude)
{
  val_=std::polar(amplitude,arg(val_));
}

void SymmetryReflection::SetPhase(Real phase)
{
  val_=std::polar(abs(val_),phase);
}

void SymmetryReflection::SetValue(Complex value)
{
  val_=value;
}

void SymmetryReflection::SetReflectionIndex(const ReflectionIndex& idx)
{
  index_=idx;
}


SymmetryReflection SymmetryReflection::operator+(const SymmetryReflection& rhs)
{
  if(index_!=rhs.GetReflectionIndex()){
    throw("differen indexes");
  }
  return SymmetryReflection(index_,val_+rhs.GetValue());
}

SymmetryReflection SymmetryReflection::operator/(Real rhs)
{
  return SymmetryReflection(index_,val_/rhs);
}



} //ns


