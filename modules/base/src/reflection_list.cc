//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Reflection list 

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <sstream>
#include <ost/message.hh>

#include "reflection_list.hh"
#include "reflection_algorithm.hh"


namespace iplt { 

using namespace reflection_detail;

ReflectionList::ReflectionList():
  lptr_(new ListImpl())
{}

ReflectionList::ReflectionList(const Lattice& lat):
  lptr_(new ListImpl(lat))
{}

ReflectionList::ReflectionList(const SpatialUnitCell& cell):
  lptr_(new ListImpl(cell))
{}

ReflectionList::ReflectionList(const Lattice& lat, const SpatialUnitCell& cell):
  lptr_(new ListImpl(lat,cell))
{}

ReflectionList::ReflectionList(const SpatialUnitCell& cell, const Lattice& lat):
  lptr_(new ListImpl(lat,cell))
{}

ReflectionList::ReflectionList(const ReflectionList& l):
  lptr_(new ListImpl(*l.lptr_)){}

ReflectionList::ReflectionList(const ReflectionList& l, bool copy):
  lptr_(new ListImpl(*l.lptr_,copy)){}

ReflectionList& ReflectionList::operator=(const ReflectionList& l)
{
  if(&l!=this) {
    lptr_ = ListPtr(new ListImpl(*l.lptr_));
  }
  return *this;
}

void ReflectionList::CopyProperties(const ReflectionList& l)
{
  assert(lptr_);
  lptr_->CopyProperties(*l.lptr_);
}

unsigned int ReflectionList::AddProperty(const String& prop)
{
  return (*lptr_).AddProperty(prop,PT_REAL);
}
void ReflectionList::DeleteProperty(const String& prop)
{
  (*lptr_).DeleteProperty(prop);
}

unsigned int ReflectionList::AddProperty(const String& prop, PropertyType t)
{
  return (*lptr_).AddProperty(prop,t);
}

bool ReflectionList::HasProperty(const String& prop) const
{
  return (*lptr_).HasProperty(prop);
}

bool ReflectionList::HasPropertyType(PropertyType ptype) const
{
  return (*lptr_).HasPropertyType(ptype);
}
  
String ReflectionList::GetPropertyNameByType(PropertyType ptype) const
{
  return (*lptr_).GetPropertyNameByType(ptype);
}

void ReflectionList::RenameProperty(const String& from, const String& to)
{
  (*lptr_).RenameProperty(from,to);
}


unsigned int ReflectionList::GetPropertyCount() const
{
  return (*lptr_).GetPropertyCount();
}
  
String ReflectionList::GetPropertyName(int n) const
{
  return (*lptr_).GetPropertyName(n);
}

PropertyType ReflectionList::GetPropertyType(int n) const
{
  return (*lptr_).GetPropertyType(n);
}

void ReflectionList::SetPropertyType(const String& prop, PropertyType type)
{
  (*lptr_).SetPropertyType(prop,type);
}

PropertyType ReflectionList::GetPropertyType(const String& name) const
{
  return (*lptr_).GetPropertyType(name);
}


unsigned int ReflectionList::GetPropertyIndex(const String& name) const
{
  return (*lptr_).GetPropertyIndex(name);
}

std::vector<String> ReflectionList::GetPropertyList() const
{
  std::vector<String> nrvo;
  for(int i=0;i<lptr_->GetPropertyCount();++i) {
    nrvo.push_back(lptr_->GetPropertyName(i));
  }
  return nrvo;
}


ReflectionProxyter ReflectionList::AddReflection(const ReflectionIndex& indx)
{
  return ReflectionProxyter(lptr_,(*lptr_).AddIndex(indx));
}

ReflectionProxyter ReflectionList::AddReflection(const ReflectionIndex& indx, const ReflectionProxyter& rp)
{
  ReflectionProxyter rpnew(lptr_,(*lptr_).AddIndex(indx));
  rpnew.Set(rp);
  return rpnew;
}

ReflectionProxyter ReflectionList::AddProxyter(const ReflectionProxyter& prox)
{
  ReflectionProxyter nprox = ReflectionProxyter(lptr_,(*lptr_).AddIndex(prox.GetIndex()));
  unsigned int pcount=lptr_->GetPropertyCount();
  for(unsigned int c=0;c<pcount;++c) {
    try {
      String pname=lptr_->GetPropertyName(c);
      nprox.Set(pname,0.0);
      nprox.Set(pname,prox.Get(pname));
    } catch (ReflectionException& e) {
      // ignore
    }
  }
  return nprox;
}

ReflectionProxyter ReflectionList::CopyProxyter(const ReflectionProxyter& prox)
{
  ReflectionProxyter nprox = ReflectionProxyter(lptr_,(*lptr_).AddIndex(prox.GetIndex()));
  unsigned int pcount=lptr_->GetPropertyCount();
  for(unsigned int c=0;c<pcount;++c) {
    nprox.Set(c,prox.Get(c));
  }
  return nprox;
}

void ReflectionList::Merge(const ReflectionList& list)
{
  for(ReflectionProxyter it=list.Begin();!it.AtEnd();++it){
    AddProxyter(it);
  }
}

ReflectionProxyter ReflectionList::Begin() const
{
  return ReflectionProxyter(lptr_,(*lptr_).FirstIndex());
}

ReflectionProxyter ReflectionList::Find(const ReflectionIndex& i) const
{
  return ReflectionProxyter(lptr_,(*lptr_).Find(i));
}

ReflectionProxyter ReflectionList::FindFirst(const ost::img::Point& hk) const
{
  return ReflectionProxyter(lptr_,(*lptr_).FindFirst(hk));
}

unsigned int ReflectionList::NumReflections() const
{
  return (*lptr_).GetEntryCount();
}

void ReflectionList::ClearReflections()
{
  (*lptr_).ClearEntries();
}

void ReflectionList::SetLattice(const Lattice& l)
{
  (*lptr_).SetLattice(l);
}

Lattice ReflectionList::GetLattice() const
{
  return (*lptr_).GetLattice();
}

void ReflectionList::SetUnitCell(const SpatialUnitCell& cell)
{
  (*lptr_).SetUnitCell(cell);
}

SpatialUnitCell ReflectionList::GetUnitCell() const
{
  return (*lptr_).GetUnitCell();
}

Symmetry ReflectionList::GetSymmetry() const
{
  return (*lptr_).GetSymmetry(); 
}

void ReflectionList::SetSymmetry(const String& sym)
{
  (*lptr_).SetSymmetry(sym);
}

void ReflectionList::SetSymmetry(const Symmetry& sym)
{
  (*lptr_).SetSymmetry(sym);
}

String ReflectionList::Dump() const
{
  std::stringstream ss;
  for(ReflectionProxyter rp=Begin(); !rp.AtEnd(); ++rp) {
    ss << rp.GetIndex() << ": ";
    for(unsigned int i=0;i<GetPropertyCount();++i) {
      ss << rp.Get(GetPropertyName(i)) << " ";
    }
    ss << std::endl;
  }
  ss << DumpHeader();
  return ss.str();
}
String ReflectionList::DumpFormatted() const
{
  std::stringstream ss;
  for(ReflectionProxyter rp=Begin(); !rp.AtEnd(); ++rp) {
    ss << rp.GetIndex() << ": ";
    for(unsigned int i=0;i<GetPropertyCount();++i) {
      ss.width(10);
      ss << rp.Get(GetPropertyName(i)) << " ";
    }
    ss << std::endl;
  }
  ss << DumpHeader();
  return ss.str();
 }

String ReflectionList::DumpHeader() const
{
  std::stringstream ss;
  ss << "Header:" << std::endl;
  
  for(unsigned int i=0;i<GetPropertyCount();++i) {
    ss << GetPropertyName(i) << " ";
  }
  ss << std::endl;
  for(unsigned int i=0;i<GetPropertyCount();++i) {
    ss << GetPropertyType(i) << " ";
  }
  ss << std::endl;
  ss <<"Lattice: "<< (*lptr_).GetLattice() <<  std::endl;
  ss <<"Unit cell: "<< (*lptr_).GetUnitCell() <<  std::endl;
  ss << std::endl;
  return ss.str();
}


void ReflectionList::Apply(ReflectionNonModAlgorithm& a) const
{
  a.Visit(*this);
}

ReflectionList ReflectionList::Apply(ReflectionModIPAlgorithm& a) const
{
  ReflectionList nrvo=*this;
  a.Visit(nrvo);
  return nrvo;
}

void ReflectionList::ApplyIP(ReflectionModIPAlgorithm& a)
{
  a.Visit(*this);
}

ReflectionList ReflectionList::Apply(const ReflectionConstModIPAlgorithm& a) const
{
  ReflectionList nrvo=*this;
  a.Visit(nrvo);
  return nrvo;
}

void ReflectionList::ApplyIP(const ReflectionConstModIPAlgorithm& a)
{
  a.Visit(*this);
}

ReflectionList ReflectionList::Apply(ReflectionModOPAlgorithm& a) const
{
  return a.Visit(*this);
}

void ReflectionList::ApplyIP(ReflectionModOPAlgorithm& a)
{
  ReflectionList tmp=a.Visit(*this);
  Swap(tmp);
}

ReflectionList ReflectionList::Apply(const ReflectionConstModOPAlgorithm& a) const
{
  return a.Visit(*this);
}

void ReflectionList::ApplyIP(const ReflectionConstModOPAlgorithm& a)
{
  ReflectionList tmp=a.Visit(*this);
  Swap(tmp);
}


void ReflectionList::Swap(ReflectionList& rl)
{
  std::swap(lptr_, rl.lptr_);
}

Real ReflectionList::GetResolution(const ReflectionIndex& idx) const
{
  assert(lptr_);
  return lptr_->GetResolution(idx);
}
// stand alone functions

ReflectionList ReIndex(const ReflectionList& r_in, const Mat2& m)
{
  int m00=static_cast<int>(m(0,0));
  int m01=static_cast<int>(m(0,1));
  int m10=static_cast<int>(m(1,0));
  int m11=static_cast<int>(m(1,1));

  Mat2 im=Invert(m);

  Lattice lat=r_in.GetLattice();
  Lattice newlat(lat.GetFirst()*im(0,0)+lat.GetSecond()*im(1,0),
		 lat.GetFirst()*im(0,1)+lat.GetSecond()*im(1,1),
		 lat.GetOffset());
		 
  ReflectionList r_out(newlat);
  r_out.CopyProperties(r_in);
  r_out.SetLattice(newlat);
  for(ReflectionProxyter rp=r_in.Begin();!rp.AtEnd();++rp) {
    int h=rp.GetIndex().GetH();
    int k=rp.GetIndex().GetK();
    Real zstar=rp.GetIndex().GetZStar();
    ReflectionProxyter rp2 = r_out.AddReflection(ReflectionIndex(h*m00+k*m01,h*m10+k*m11,zstar),rp);
  }

  return r_out;
}

ReflectionList ReIndex(const ReflectionList& r_in, const Mat3& m)
{
  int m00=static_cast<int>(m(0,0));
  int m01=static_cast<int>(m(0,1));
  int m02=static_cast<int>(m(0,2));
  int m10=static_cast<int>(m(1,0));
  int m11=static_cast<int>(m(1,1));
  int m12=static_cast<int>(m(1,2));
  int m20=static_cast<int>(m(2,0));
  int m21=static_cast<int>(m(2,1));
  int m22=static_cast<int>(m(2,2));

  ReflectionList r_out;
  r_out.CopyProperties(r_in);

  for(ReflectionProxyter rp=r_in.Begin();!rp.AtEnd();++rp) {
    int h=rp.GetIndex().GetH();
    int k=rp.GetIndex().GetK();
    Real zsampling=r_in.GetUnitCell().GetC();
    Real l=rp.GetIndex().GetZStar()*zsampling;
    ReflectionProxyter rp2 = r_out.AddReflection(ReflectionIndex(static_cast<int>(h*m00+k*m01+l*m02),static_cast<int>(h*m10+k*m11+m12*l),(h*m20+k*m21+m22*l)/zsampling),rp);
  }

  return r_out;
}

ReflectionList ReIndex(const ReflectionList& r_in, const geom::Vec2& v)
{
  int h_add=static_cast<int>(round(v[0]));
  int k_add=static_cast<int>(round(v[1]));

  Lattice lat=r_in.GetLattice();
  Lattice newlat(lat.GetFirst(),
		 lat.GetSecond(),
		 lat.GetOffset()+lat.GetFirst()*static_cast<Real>(h_add)+lat.GetSecond()*static_cast<Real>(k_add));

  ReflectionList r_out(newlat);
  r_out.CopyProperties(r_in);
  r_out.SetLattice(newlat);

  for(ReflectionProxyter rp=r_in.Begin();!rp.AtEnd();++rp) {
    int h=rp.GetIndex().GetH();
    int k=rp.GetIndex().GetK();
    Real zstar=rp.GetIndex().GetZStar();
    ReflectionProxyter rp2 = r_out.AddReflection(ReflectionIndex(h-h_add,k-k_add,zstar),rp);
  }

  return r_out;
}

ReflectionList ReIndex(const ReflectionList& r_in, const geom::Vec3& v)
{
  int h_add=static_cast<int>(v[0]);
  int k_add=static_cast<int>(v[1]);
  Real z_add=v[2]/r_in.GetUnitCell().GetC();
  
  ReflectionList r_out;
  r_out.CopyProperties(r_in);

  for(ReflectionProxyter rp=r_in.Begin();!rp.AtEnd();++rp) {
    ReflectionProxyter rp2 = r_out.AddReflection(ReflectionIndex(rp.GetIndex().GetH()+h_add,rp.GetIndex().GetK()+k_add,rp.GetIndex().GetZStar()+z_add),rp);
  }

  return r_out;
}


} //ns
