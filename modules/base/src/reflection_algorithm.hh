//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_REFLECTION_ALGORITHM_HH
#define IPLT_EX_REFLECTION_ALGORITHM_HH

/*
  collection of algorithms that can be applied
  to a ReflectionList

  Author: Andreas Schenk
*/

#include "reflection_list.hh"	

namespace iplt { 

class DLLEXPORT_IPLT_BASE ReflectionNonModAlgorithm {
public:
  virtual ~ReflectionNonModAlgorithm() {}
  virtual void Visit(const ReflectionList&) = 0;
};

class DLLEXPORT_IPLT_BASE ReflectionModIPAlgorithm {
public:
  virtual ~ReflectionModIPAlgorithm() {}
  virtual void Visit(ReflectionList&) = 0;
};

class DLLEXPORT_IPLT_BASE ReflectionModOPAlgorithm {
public:
  virtual ~ReflectionModOPAlgorithm() {}
  virtual ReflectionList Visit(const ReflectionList&) = 0;
};

class DLLEXPORT_IPLT_BASE ReflectionConstModIPAlgorithm {
public:
  virtual ~ReflectionConstModIPAlgorithm() {}
  virtual void Visit(ReflectionList&) const = 0;
};

class DLLEXPORT_IPLT_BASE ReflectionConstModOPAlgorithm {
public:
  virtual ~ReflectionConstModOPAlgorithm() {}
  virtual ReflectionList Visit(const ReflectionList&) const = 0;
};

} //ns

#endif
