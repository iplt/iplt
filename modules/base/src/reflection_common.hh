//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_REFLECTION_COMMON_HH
#define IPLT_EX_REFLECTION_COMMON_HH

/*
  common properties for reflection list and proxyters
  
  Author: Ansgar Philippsen
*/

namespace iplt { 

// implemented as column type chars in the mtz file  
enum PropertyType {
  PT_REAL=0, // R
  PT_INDEX, // H
  PT_INTENSITY, // J
  PT_AMPLITUDE, // F,
  PT_ANOMALOUS_DIFFERENCE, // D
  PT_STDDEV, // Q
  PT_AMPLITUDE2, // G
  PT_STDDEVA2, // L
  PT_INTENSITY2, // K
  PT_STDDEVI2, // M
  PT_NORM_AMPLITUDE, // E
  PT_PHASE, // P
  PT_WEIGHT, // W
  PT_PHASE_PROB, // A
  PT_BATCH, // B
  PT_MISYM, // Y
  PT_INTEGER // I
};

} //ns

#endif
