//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#ifndef CTF_FUNC_BASE_HH_
#define CTF_FUNC_BASE_HH_

#include <ost/geom/vec2.hh>
#include <iplt/tcif_data.hh>
#include <iplt/module_config.hh>


namespace iplt {

class DLLEXPORT_IPLT_BASE CTFFuncBase
{
public:
	CTFFuncBase(const TCIFData& tcifdata=TCIFData());
	virtual ~CTFFuncBase();
  Real CTF(geom::Vec2 xy) const;
  Real DampedCTF(geom::Vec2 xy) const;
  Real SpatialEnvelope(geom::Vec2 xy) const;
  Real TemporalEnvelope(geom::Vec2 xy) const;
  Real CalcDefocusFromThonRing(geom::Vec2 k,int x) const;
  geom::Vec2List CalcThonRingPosition(Real df, unsigned int x) const;
  Real GetDefocusX() const;
  Real GetDefocusY() const;
  void SetDefocusX(Real defocus);
  void SetDefocusY(Real defocus);
  Real GetAstigmatismAngle() const;
  void SetAstigmatismAngle(Real angle);
  TCIFData GetTCIFData() const;
  void SetTCIFData(const TCIFData& data);
private:
  Real defocus_(Real arg) const;
  TCIFData data_;
  Real Q_; // precomputed amplitude contrast term
};

}
#endif /*CTF_FUNC_BASE_HH_*/
