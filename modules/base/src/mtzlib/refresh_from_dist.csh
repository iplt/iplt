#!/bin/csh -f
if ( $#argv<1) then
    echo "expected CCP4 src dir as first argument"
    exit
endif

set base=$argv[1]/lib/src/

foreach f (\
    ccp4_array.c ccp4_array.h \
    ccp4_errno.h \
    ccp4_file_err.h \
    ccp4_general.h \
    ccp4_parser.c ccp4_parser.h \
    ccp4_spg.h \
    ccp4_sysdep.h \
    ccp4_types.h \
    ccp4_unitcell.c ccp4_unitcell.h \
    ccp4_utils.h \
    ccp4_vars.h \
    cmtzlib.c \
    cmtzlib.h \
    cvecmat.h \
    cvecmat.c \
    library_err.c \
    library_file.c library_file.h \
    library_utils.c \
    mtzdata.h \
    )
    echo "copying " $f
    cp ${base}/${f} .
end

sed -i 's/exit(1);/catch_ccp4_exception();/g' library_err.c
sed -i 's/#include "ccp4_errno.h"/#include "ccp4_errno.h"\n#include "catch_ccp4_exception.hh"/g' library_err.c
