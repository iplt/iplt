//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include "symmetry.hh"

#include <sstream>


namespace iplt {  
using namespace geom;
using namespace symmetry;

Symmetry::Symmetry(const String& name)
{
  SymmetryImplementationHolder &holder = SymmetryImplementationHolder::Instance();
  impl_=holder.GetSymmetryImplementation(name);
  assert(impl_);
  impl_->Init();
}
Symmetry::Symmetry(unsigned id)
{
  impl_=SymmetryImplementationHolder::Instance().GetSymmetryImplementation(id);
  assert(impl_);
  impl_->Init();
}
Real Symmetry::Symmetrize(const ReflectionIndex& r,Real phase) const
{
  return impl_->Symmetrize(r,phase);
}
ReflectionIndex Symmetry::Symmetrize(const ReflectionIndex& r) const
{
  return impl_->Symmetrize(r);
}
std::vector<ReflectionIndex> Symmetry::GenerateSymmetryRelatedPoints(const ReflectionIndex& p,bool createfriedel_mates) const
{
  return impl_->GenerateSymmetryRelatedPoints(p,createfriedel_mates);
}
std::vector<Vec3> Symmetry::GenerateSymmetryRelatedPositions(const geom::Vec3& p) const
{
  return impl_->GenerateSymmetryRelatedPositions(p);
}
bool Symmetry::IsInAsymmetricUnit(const ReflectionIndex& r) const
{
  return impl_->IsInAsymmetricUnit(r);
}
bool Symmetry::IsCentric(const ReflectionIndex& r) const
{
  return impl_->IsCentric(r);
}

unsigned int Symmetry::GetSpacegroupNumber() const
{
  return impl_-> GetSpacegroupNumber();
}

String Symmetry::GetSpacegroupName() const
{
  return impl_-> GetSpacegroupName();
}

String Symmetry::GetPointgroupName() const
{
  return impl_-> GetPointgroupName();
}

bool Symmetry::IsInAsymmetricUnit(const SymmetryReflection& reflection) const
{
  return impl_->IsInAsymmetricUnit(reflection);
}
bool Symmetry::IsCentric(const SymmetryReflection& reflection) const
{
  return impl_->IsCentric(reflection);
}

SymmetryReflection Symmetry::Reduce(const SymmetryReflection& reflection) const
{
  return impl_-> Reduce(reflection);
}
SymmetryReflectionList Symmetry::Expand(const SymmetryReflection& reflection,bool create_friedelmates) const
{
  return impl_-> Expand(reflection,create_friedelmates);
}
SymmetryReflection Symmetry::Restrict(const SymmetryReflection& reflection) const
{
  return impl_-> Restrict(reflection);
}

SymmetryReflectionList Symmetry::Reduce(const SymmetryReflectionList& reflectionlist) const
{
  return impl_-> Reduce(reflectionlist);
}
SymmetryReflectionList Symmetry::Expand(const SymmetryReflectionList& reflectionlist,bool create_friedelmates) const
{
  return impl_-> Expand(reflectionlist,create_friedelmates);
}
SymmetryReflectionList Symmetry::Restrict(const SymmetryReflectionList& reflectionlist) const
{
  return impl_-> Restrict(reflectionlist);
}

bool Symmetry::IsSystematicAbsence(const SymmetryReflection& reflection) const
{
  return impl_->IsSystematicAbsence(reflection.GetReflectionIndex());
}
bool Symmetry::IsSystematicAbsence(const ReflectionIndex& index) const
{
  return impl_->IsSystematicAbsence(index);
}

std::vector<unsigned int> GetImplementedSymmetries()
{
  return symmetry::SymmetryImplementationHolder::Instance().GetImplementedSymmetries();
}

} //ns
