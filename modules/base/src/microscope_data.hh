//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#ifndef MICROSCOPE_DATA_HH_
#define MICROSCOPE_DATA_HH_

#include <ost/units.hh>
#include <ost/info/info.hh>
#include <iplt/module_config.hh>

namespace iplt { 

class DLLEXPORT_IPLT_BASE MicroscopeData
{
public:
    MicroscopeData(Real cs=2.0*ost::Units::mm,Real cc=2.0*ost::Units::mm, Real ca=0.07,Real de=0.3*ost::Units::eV,Real alpha=0.1*ost::Units::mrad,Real kv=200.0*ost::Units::kV);
  
  void SetSphericalAberration(Real cs){cs_=cs;}
  void SetAccelerationVoltage(Real kv){kv_=kv;calcwavelength();}
  void SetChromaticAberration ( Real Cc )       {cc_=Cc;}
  void SetAmplitudeContrast ( Real Ca )       {ca_=Ca;}
  void SetConvergenceAngle ( Real alpha ) {alpha_=alpha;}
  void SetEnergySpread ( Real dE )       {de_=dE;};
  Real GetSphericalAberration() const {return cs_;}
  Real GetAccelerationVoltage() const {return kv_;}
  Real GetChromaticAberration() const {return cc_;}
  Real GetAmplitudeContrast() const {return ca_;}
  Real GetConvergenceAngle() const {return alpha_;}
  Real GetEnergySpread() const {return de_;}
  Real GetWavelength() const {return lambda_;}
  
protected:
  void calcwavelength();
  
  Real cs_;                       // spherical aberration
  Real kv_;                       // acceleration voltage
  Real cc_;                       // chromatic aberration
  Real ca_;                       // amplitude contrast
  Real alpha_;                    // (mrad) convergence angle
  Real de_;                       // (eV) energy spread
  Real lambda_;

  static const Real h_;           // (Js) Plank constant
  static const Real m_;           // (kg) mass of the electron
  static const Real e_;           // (C) charge of the electron
  static const Real c_;           // (m/s) speed of light
};

DLLEXPORT_IPLT_BASE void MicroscopeDataToInfo(const MicroscopeData& md, ost::info::InfoGroup& ig);
DLLEXPORT_IPLT_BASE MicroscopeData InfoToMicroscopeData(const ost::info::InfoGroup& ig);

} //ns

#endif /*MICROSCOPE_DATA_HH_*/

