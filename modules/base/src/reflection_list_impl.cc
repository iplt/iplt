//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Reflections impl

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <ostream>
#include <sstream>
#include <iostream>

#include "reflection_list_impl.hh"
#include "reflection_exception.hh"

namespace iplt {  namespace reflection_detail {

// provide case insensitive lookup
PropertyMap::iterator find(PropertyMap& m, const String& k)
{
  String k2(k);
  std::transform(k2.begin(),k2.end(),k2.begin(),tolower);
  for(PropertyMap::iterator it=m.begin();it!=m.end();++it) {
    String e2=it->first;
    std::transform(e2.begin(),e2.end(),e2.begin(),tolower);
    if(k2==e2) return it;
  }
  return m.end();
}

// provide case insensitive lookup
PropertyMap::const_iterator find(const PropertyMap& m, const String& k)
{
  String k2(k);
  std::transform(k2.begin(),k2.end(),k2.begin(),tolower);
  for(PropertyMap::const_iterator it=m.begin();it!=m.end();++it) {
    String e2=it->first;
    std::transform(e2.begin(),e2.end(),e2.begin(),tolower);
    if(k2==e2) return it;
  }
  return m.end();
}

ListImpl::ListImpl():
  pmap_(),
  imap_(),
  lat_(),
  cell_()
{}

ListImpl::ListImpl(const ListImpl& l):
  pmap_(l.pmap_),
  imap_(l.imap_),
  lat_(l.lat_),
  cell_(l.cell_)
{}

ListImpl::ListImpl(const ListImpl& l, bool copy):
  pmap_(),
  imap_(),
  lat_(l.lat_),
  cell_(l.cell_)
{
  if(copy) {
    pmap_=l.pmap_;
    imap_=l.imap_;
  } else {
    CopyProperties(l);
  }
}

ListImpl::ListImpl(const Lattice& lat):
  pmap_(),
  imap_(),
  lat_(lat),
  cell_()
{}

ListImpl::ListImpl(const SpatialUnitCell& cell):
  pmap_(),
  imap_(),
  lat_(),
  cell_(cell)
{}

ListImpl::ListImpl(const Lattice& lat, const SpatialUnitCell& cell):
  pmap_(),
  imap_(),
  lat_(lat),
  cell_(cell)
{}

ListImpl::ListImpl(const SpatialUnitCell& cell, const Lattice& lat):
  pmap_(),
  imap_(),
  lat_(lat),
  cell_(cell)
{}


ListImpl& ListImpl::operator=(const ListImpl& l)
{
  if(&l!=this) {
    pmap_=l.pmap_;
    imap_=l.imap_;
    lat_=l.lat_;
    cell_=l.cell_;
  }
  return *this;
}

void ListImpl::CopyProperties(const ListImpl& l)
{
  pmap_=l.pmap_;
  imap_=IndexMap();
  lat_=l.lat_;
  cell_=l.cell_;
}

int ListImpl::AddProperty(const String& prop, PropertyType type)
{
  PropertyMap::iterator pit = find(pmap_,prop);
  if(pit==pmap_.end()) {
    /* 
       if property does not yet exist, add a new 
       entry to vlist, and store the index to that
       entry in the property map
    */
    for(IndexMap::iterator it=imap_.begin();it!=imap_.end();++it){
      it->second.push_back(0.0);
    }
    PropertyEntry pentry = {type,pmap_.size()};
    std::pair<PropertyMap::iterator, bool> res = pmap_.insert(PropertyMap::value_type(prop,pentry));
    pit = res.first;
  }
  // return index into vlist
  return pit->second.vindex;
}

void ListImpl::DeleteProperty(const String& prop)
{
  PropertyMap::iterator pit = find(pmap_,prop);
  if(pit==pmap_.end()) {
    throw ReflectionException("PropertyName not found in ListImpl::DeleteProperty");
  }
  for(IndexMap::iterator it=imap_.begin();it!=imap_.end();++it){
    it->second.erase(it->second.begin()+pit->second.vindex);
  }
  pmap_.erase(pit);
  for(PropertyMap::iterator pit2=pmap_.begin();pit2!=pmap_.end();++pit2){
    if(pit2->second.vindex>pit->second.vindex){
      pit2->second.vindex-=1;
    }
  } 
}

void ListImpl::RenameProperty(const String& from,const String& to)
{
  PropertyMap::iterator pos = find(pmap_,from);
  if(pos!=pmap_.end()) {
    pmap_[to] = pos->second;
    pmap_.erase(pos);
  }
}

bool ListImpl::HasProperty(const String& prop) const
{
  return find(pmap_,prop) != pmap_.end();
}

bool ListImpl::HasPropertyType(PropertyType ptype) const
{
  for(PropertyMap::const_iterator it=pmap_.begin();it!=pmap_.end();++it)
  {
    if(it->second.type==ptype) return true;
  }
  return false;
}

String ListImpl::GetPropertyNameByType(PropertyType ptype) const
{
  for(PropertyMap::const_iterator it=pmap_.begin();it!=pmap_.end();++it)
  {
    if(it->second.type==ptype) return it->first;
  }
  throw ReflectionException("PropertyType not found in ListImpl::GetPropertyName");
}

int ListImpl::GetPropertyCount() const
{
  return pmap_.size();
}

String ListImpl::GetPropertyName(unsigned int n) const
{
  for(PropertyMap::const_iterator it=pmap_.begin();it!=pmap_.end();++it)
  {
    if(it->second.vindex==n) return it->first;
  }
  throw ReflectionException("Invalid Property Id in ListImpl::GetPropertyName");
}

int ListImpl::GetPropertyIndex(const String& name) const
{
  for(PropertyMap::const_iterator it=pmap_.begin();it!=pmap_.end();++it)
  {
    if(it->first==name) return it->second.vindex;
  }
  throw ReflectionException("Invalid Property Name in ListImpl::GetPropertyIndex");
}

PropertyType ListImpl::GetPropertyType(unsigned int n) const
{
  for(PropertyMap::const_iterator it=pmap_.begin();it!=pmap_.end();++it)
  {
    if(it->second.vindex==n) return it->second.type;
  }
  throw ReflectionException("Invalid Property Id in ListImpl::GetPropertyName");
}

PropertyType ListImpl::GetPropertyType(const String& prop) const
{
  PropertyMap::const_iterator pos = find(pmap_,prop);
  if(pos!=pmap_.end()) {
    return pos->second.type;
  }
  throw ReflectionException(String("invalid property ")+prop);
}

void ListImpl::SetPropertyType(const String& prop, PropertyType type)
{
  PropertyMap::iterator pos = find(pmap_,prop);
  if(pos!=pmap_.end()) {
    pos->second.type=type;
  }

}

IndexMapIterator ListImpl::AddIndex(const ReflectionIndex& i)
{
  // add new entry to index map
  IndexMapIterator e = imap_.insert(IndexMap::value_type(i,std::vector<Real>(pmap_.size())));  
  return e;
}

IndexMapIterator ListImpl::Find(const ReflectionIndex& i)
{
  return imap_.find(i);
}

IndexMapIterator ListImpl::FindFirst(const ost::img::Point& hk) 
{
  IndexMapIterator it=imap_.begin();
  for(;it!=imap_.end();++it) {
    if(it->first.AsDuplet()==hk) break;
  }
  return it;
}

IndexMapIterator ListImpl::DeleteEntry(const IndexMapIterator& iit)
{
  imap_.erase(iit);
  return imap_.end();
}

IndexMapIterator ListImpl::FirstIndex()
{
  return imap_.begin();
}

bool ListImpl::AtEnd(const IndexMapIterator& indx) const
{
  return indx==imap_.end();
}

IndexMapIterator ListImpl::NextIndex(const IndexMapIterator& indx) const
{
  if(AtEnd(indx)) {
    return indx;
  } else {
    IndexMapIterator it(indx);
    return ++it;
  }
}

Real& ListImpl::Value(const IndexMapIterator& iit, const String& prop)
{
  PropertyMap::iterator pit = find(pmap_,prop);
  if(pit==pmap_.end()) {
    std::ostringstream msg;
    msg << "Property '" << prop << "'not found in ListImpl::Value";
    throw ReflectionException(msg.str());
  }
  return iit->second[pit->second.vindex];
}

Real ListImpl::Value(const IndexMapIterator& iit, const String& prop) const
{
  PropertyMap::const_iterator pit = find(pmap_,prop);
  if(pit==pmap_.end()) {
    std::ostringstream msg;
    msg << "Property '" << prop << "'not found in ListImpl::Value";
    throw ReflectionException(msg.str());
  }
  return iit->second[pit->second.vindex];
}

Real& ListImpl::Value(const IndexMapIterator& iit, unsigned int n)
{
  return iit->second[n];
}

Real ListImpl::Value(const IndexMapIterator& iit, unsigned int n) const
{
  return iit->second[n];
}

int ListImpl::GetEntryCount() const
{
  return imap_.size();
}

void ListImpl::ClearEntries()
{
  imap_.clear();

}

void ListImpl::SetLattice(const Lattice& l)
{
  lat_=l;
}

Lattice ListImpl::GetLattice() const
{
  return lat_;
}

void ListImpl::SetUnitCell(const SpatialUnitCell& cell)
{
  cell_=cell;
}

void ListImpl::SetSymmetry(const String& sym)
{
  cell_.SetSymmetry(sym);
}

void ListImpl::SetSymmetry(const Symmetry& sym)
{
  cell_.SetSymmetry(sym);
}

Symmetry ListImpl::GetSymmetry() const
{
  return cell_.GetSymmetry();
}

SpatialUnitCell ListImpl::GetUnitCell() const
{
  return cell_;
}
Real ListImpl::GetResolution(const ReflectionIndex& idx) const
{
  ReciprocalUnitCell cell=ReciprocalUnitCell(GetUnitCell());
  return 1.0/Length(Vec3( static_cast<Real>(idx.GetH())*cell.GetA()+static_cast<Real>(idx.GetK())*cell.GetB()*cos(cell.GetGamma()),
                          static_cast<Real>(idx.GetK())*cell.GetB()*sin(cell.GetGamma()),
                          idx.GetZStar()));
}


}} //ns detail
