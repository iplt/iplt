//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/


#ifndef IPLT_EX_SYMMETRY_REFLECTION_H
#define IPLT_EX_SYMMETRY_REFLECTION_H

#include <vector>
#include <ost/base.hh>
#include <ost/img/data_types.hh>
#include <iplt/reflection_index.hh>


namespace iplt { 

class DLLEXPORT_IPLT_BASE SymmetryReflection
{
public:
  SymmetryReflection();
  SymmetryReflection(int h,int k,Real zstar, Complex val);
  SymmetryReflection(int h,int k,Real zstar, Real amp, Real phase);
  SymmetryReflection(const ReflectionIndex& idx, Complex val);
  SymmetryReflection(const ReflectionIndex& idx, Real amp, Real phase);
  Real GetAmplitude() const;
  Real GetPhase() const;
  Complex GetValue() const;
  ReflectionIndex GetReflectionIndex() const;
  void SetAmplitude(Real amplitude);
  void SetPhase(Real phase);
  void SetValue(Complex value);
  void SetReflectionIndex(const ReflectionIndex& idx);

  SymmetryReflection operator+(const SymmetryReflection& rhs);
  SymmetryReflection operator/(Real rhs);

protected:
  ReflectionIndex index_;
  Complex val_;
};

typedef std::vector<SymmetryReflection> SymmetryReflectionList;

} //ns

#endif //IPLT_EX_SYMMETRY_REFLECTION_H

