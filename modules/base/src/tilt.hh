//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Tilt Geometry

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#ifndef IPLT_EX_TILT_HH
#define IPLT_EX_TILT_HH


#include <ost/geom/geom.hh>
#include <ost/info/info_fw.hh>

#include "unit_cell.hh"

namespace iplt { 

//! Encapsulates tilt geometry of a sample
/*!
  A tilt geometry encapsulates several pieces
  of information:

  - XAxisAngle: The position of the tilt-axis
  relative to the x-axis, in rad (right-handed
  system), in the range -180 to +180

  - TiltAngle: The (right handed) rotation of the
  sample around the tilt axis. Ranging from -90 to
  +90, but when determined from a lattice the range
  is only from 0 to 90.

  - SpatialTransformation: the 3x3 matrix that
  describes the transformation is real space

  - ReciprocalTransformation: the 3x3 matrix that
  describes the transformation is reciprocal space

*/
class DLLEXPORT_IPLT_BASE TiltGeometry {
public:
  
  //! Default constructor, no tilt at all
  /*!
    both axis and tilt angle are set to zero
   */
  TiltGeometry();
  
  //! Initialize with tilt angle and axis angle (in rad)
  /*!
    The range for AxisAngle is [-pi,pi], and for the
    TiltAngle it is [0,pi/2].
  */
  TiltGeometry(Real tilt_angle, Real x_axis_angle);

  /*! @name Properties
  */
  //@{

  //! Retrieve/Set tilt angle (in radians)
  /*!
    Amount of (right handed) rotation around tilt axis.
  */
  Real GetTiltAngle(void) const;
  void SetTiltAngle(Real ta);

  //! Retrieve/Set axis angle (in radians)
  /*!
    The angle between the x-axis and the tilt axis.
  */
  Real GetXAxisAngle(void) const;
  void SetXAxisAngle(Real xaa);


  //! Retrieve spatial transformation matrix
  /*!
    This matrix describes the transformation
    of a spatial, untilted vector to the
    spatial, tilted vector:

    T = V*M

    where M is the 3x3 transformation matrix,
    V the non-tilted and T the tilted 3D vector.
    The projected vector is simply obtained by
    coverting T into a 2D vector (effectively
    setting the z component to zero).

    The transformation matrix is a composite of
    the rotation: a rotation of -B around the z-axis,
    followed by a rotation of +A around the x-axis,
    and finally a rotation of +B around the z-axis
    again; B is the XAxisAngle, and A the 
    TiltAngle.
  */
  Mat3 GetSpatialTransformation() const;

  //! Retrieve reciprocal transformation matrix
  /*!
    This matrix describes the transformation
    of a reciprocal, untilted vector to the
    reciprocal, tilted vector:

    T = V*M

    similar to the spatial transformation matrix
  */
  Mat3 GetReciprocalTransformation() const;

  //@}

  /*! @name Calculations
  */
  //@{
  //! Apply tilt to a reciprocal lattice or vector to return the projected one
  Lattice ApplyReciprocal(const Lattice& untilted_lattice) const;
  geom::Vec2 ApplyReciprocal(const geom::Vec2& untilted_vec) const;

  //! Apply tilt to a spatial lattice or vector to return the projected one
  Lattice ApplySpatial(const Lattice& untilted_lattice) const;
  geom::Vec2 ApplySpatial(const geom::Vec2& untilted_vec) const;
  //@}

  bool operator==(const TiltGeometry& tg);
  
protected:
  Real tilt_angle_;
  Real x_axis_angle_;
};

//! Encapsulates tilt geometry of a crystalline sample
/*!
  In contrast to the TiltGeometry above LatticeTiltGeometry can be initialized
  using either a spatial or reciprocal lattice and unit cell. To calculate the tilt and
  X-axis angle the classs applies Shaw's algorithm (Shaw & Hills 1981).
  
  Creation of a LatticeTiltGeometry is done using the two Factory functions
  CalcTiltFromReciprocalLattice and CalcTiltFromSpatialLattice
  to prevent ambiguity in the corresponding python interface.
  
  
*/
class DLLEXPORT_IPLT_BASE LatticeTiltGeometry : public TiltGeometry{
  friend DLLEXPORT_IPLT_BASE LatticeTiltGeometry CalcTiltFromReciprocalLattice(const Lattice&, const ReciprocalUnitCell&);
  friend DLLEXPORT_IPLT_BASE LatticeTiltGeometry CalcTiltFromSpatialLattice(const Lattice&, const SpatialUnitCell&);
  friend DLLEXPORT_IPLT_BASE std::ostream& operator<<(std::ostream& o, const LatticeTiltGeometry& g);
public:
  LatticeTiltGeometry(Real tilt_angle=0.0, Real x_axis_angle=0.0, const Lattice& untilted_lattice=Lattice(), bool is_reciprocal=true);

  /*! @name Properties
  */
  //@{

  //! Retrieve/Set the angle between the untilted reciprocal A vector of the lattice and the tilt axis (in radians)
  Real GetAStarVectorAngle() const;
  void SetAStarVectorAngle(Real ava);

  //! Retrieve/Set the angle between the tilted reciprocal A vector of the lattice and the tilt axis (in radians)
  Real GetTiltedAStarVectorAngle() const;
  void SetTiltedAStarVectorAngle(Real tava);
  
  //! Retrieve/Set the angle between the untilted spatial A vector of the lattice and the tilt axis (in radians)
  Real GetAVectorAngle() const;
  void SetAVectorAngle(Real ava);

  //! Retrieve/Set the angle between the tilted spatial A vector of the lattice and the tilt axis (in radians)
  Real GetTiltedAVectorAngle() const;
  void SetTiltedAVectorAngle(Real tava);

  //! Retrieve the untilted/tilted reciprocal lattice
  Lattice GetReciprocalUntiltedLattice() const;
  Lattice GetReciprocalTiltedLattice() const;
  
  //! Retrieve the untilted/tilted spatial lattice
  Lattice GetSpatialUntiltedLattice() const;
  Lattice GetSpatialTiltedLattice() const;

  Real GetZStar(const ost::img::Point& p) const;
  
  //! reciprocal sampling based on tilt geometry, unit cell, and reciprocal lattice
  Real CalcReciprocalSampling(const Lattice& lat);

protected:
  void calc_tilt_shaw(const Lattice& lat, const ReciprocalUnitCell& ref);
  Real x_astar_angle_;
  Real x_bstar_angle_;
  Real astar_length_;
  Real bstar_length_;
  LatticeTiltGeometry(const Lattice& tilted_reciprocal_lattice, const ReciprocalUnitCell& uc);
  LatticeTiltGeometry(const Lattice& tilted_spatial_lattice, const SpatialUnitCell& uc);
};

//! Calculate geometry based on a spatial lattice and a reciprocal unit cell
DLLEXPORT_IPLT_BASE LatticeTiltGeometry CalcTiltFromSpatialLattice(const Lattice& lat, const SpatialUnitCell& ref);

//! Calculate geometry based on a reciprocal lattice and a reciprocal unit cell
DLLEXPORT_IPLT_BASE LatticeTiltGeometry CalcTiltFromReciprocalLattice(const Lattice& lat, const ReciprocalUnitCell& ref);


//! Extract tilt geometry from given info group
DLLEXPORT_IPLT_BASE TiltGeometry InfoToTiltGeometry(const ost::info::InfoGroup& g);

//! Store tilt geometry in given info group
DLLEXPORT_IPLT_BASE void TiltGeometryToInfo(const TiltGeometry& tg, ost::info::InfoGroup g);


//! Extract lattice tilt geometry from given info group
DLLEXPORT_IPLT_BASE LatticeTiltGeometry InfoToLatticeTiltGeometry(const ost::info::InfoGroup& g);

//! Store lattice tilt geometry in given info group
DLLEXPORT_IPLT_BASE void LatticeTiltGeometryToInfo(const LatticeTiltGeometry& tg, ost::info::InfoGroup g);


//! Calculate parallel and perpendicular component of given lattice index
DLLEXPORT_IPLT_BASE geom::Vec2 CalcTiltComponents(const TiltGeometry& tg, const Lattice& lat, const ost::img::Point& index);

//! same as CalcTiltComponents, but returns perpendicular vectors
DLLEXPORT_IPLT_BASE std::pair<geom::Vec2,geom::Vec2> CalcTiltComponentVectors(const TiltGeometry& tg, const Lattice& lat, const ost::img::Point& index);


//! write tilt geometry values to stream
DLLEXPORT_IPLT_BASE std::ostream& operator<<(std::ostream& o, const TiltGeometry& g);
//! write tilt geometry values to stream
DLLEXPORT_IPLT_BASE std::ostream& operator<<(std::ostream& o, const LatticeTiltGeometry& g);

} //ns

#endif
