//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  ReflectionList implementation

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#ifndef IPLT_EX_REFLECTION_IMPL_H
#define IPLT_EX_REFLECTION_IMPL_H

#include <vector>
#include <list>
#include <map>
#include <stack>
#include <stdexcept>

#include <boost/shared_ptr.hpp>

#include <ost/base.hh>

#include "reflection_index.hh"
#include "reflection_exception.hh"
#include "reflection_common.hh"
#include "lattice.hh"
#include "unit_cell.hh"

namespace iplt {  namespace reflection_detail {

struct PropertyEntry {
  PropertyType type;
  int vindex;
};

// associative mapping of property name to property list index
typedef std::map<String, PropertyEntry> PropertyMap;

// associative mapping of reflection index to data vector 
typedef std::multimap<ReflectionIndex,std::vector<Real> > IndexMap;

// 'exported' iterator
typedef IndexMap::iterator IndexMapIterator;

class ListImpl {  
public:
  ListImpl();
  ListImpl(const ListImpl& l);
  ListImpl(const ListImpl& l, bool copy);
  ListImpl& operator=(const ListImpl& l);
  ListImpl(const Lattice& lat);
  ListImpl(const SpatialUnitCell& cell);
  ListImpl(const Lattice& lat, const SpatialUnitCell& cell);
  ListImpl(const SpatialUnitCell& cell, const Lattice& lat);

  void CopyProperties(const ListImpl& l);
  int AddProperty(const String& prop, PropertyType type);
  bool HasProperty(const String& prop) const;
  void RenameProperty(const String& from, const String& to);
  void DeleteProperty(const String& prop);

  int GetPropertyCount() const;
  String GetPropertyName(unsigned int n) const;
  int GetPropertyIndex(const String& name) const;

  PropertyType GetPropertyType(unsigned int n) const;
  void SetPropertyType(const String& prop, PropertyType type);
  PropertyType GetPropertyType(const String& prop) const;

  IndexMapIterator AddIndex(const ReflectionIndex& i);

  IndexMapIterator FirstIndex();

  IndexMapIterator Find(const ReflectionIndex& i);

  IndexMapIterator FindFirst(const ost::img::Point& hk);

  bool AtEnd(const IndexMapIterator& indx) const;
  IndexMapIterator NextIndex(const IndexMapIterator& indx) const;

  Real& Value(const IndexMapIterator& iit, const String& prop);
  Real Value(const IndexMapIterator& iit, const String& prop) const;

  Real& Value(const IndexMapIterator& iit, unsigned int n);
  Real Value(const IndexMapIterator& iit, unsigned int n) const;
  
  IndexMapIterator DeleteEntry(const IndexMapIterator& iit);

  int GetEntryCount() const;

  void ClearEntries();
  bool HasPropertyType(PropertyType ptype) const;
  String GetPropertyNameByType(PropertyType ptype) const;
  //! Set lattice
  void SetLattice(const Lattice& l);
  //! Retrieve lattice
  Lattice GetLattice() const;

  //! Set unit cell
  void SetUnitCell(const SpatialUnitCell& cell);

  //! Retrieve unit cell
  SpatialUnitCell GetUnitCell() const;

  //! Set symmetry
  void SetSymmetry(const String& sym);

  //! Set symmetry
  void SetSymmetry(const Symmetry& sym);

  //! Retrieve symmetry
  Symmetry GetSymmetry() const;

  //! Get Resolution of corresponding ReflectionIndex
  Real GetResolution(const ReflectionIndex& idx) const;

private:
  PropertyMap pmap_;
  IndexMap imap_;
  Lattice lat_;
  SpatialUnitCell cell_;
};

typedef boost::shared_ptr<ListImpl> ListPtr;

}} //ns

#endif
