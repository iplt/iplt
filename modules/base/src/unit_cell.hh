//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Unit Cell

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#ifndef IPLT_EX_UNIT_CELL_H
#define IPLT_EX_UNIT_CELL_H

#include <iosfwd>

#include <ost/geom/geom.hh>
//#include <iplt/dllexport.hh>
#include <iplt/lattice.hh>
#include <iplt/symmetry.hh>

namespace iplt { 

namespace cell_detail {

//! Actual, internal implementation of unit cell functionality
class DLLEXPORT_IPLT_BASE CellImpl {
public:
  CellImpl();
  CellImpl(Real a, Real b, Real c, Real gamma,String symmetry="P 1");
  CellImpl(Real a, Real b, Real c, Real gamma,const Symmetry& s);


  //! returns the A vector
  geom::Vec2 GetVecA() const {return A_;}
  //! returns the B vector
  geom::Vec2 GetVecB() const {return B_;}
  //! returns length of A
  Real GetA() const {return a_;}
  //! returns length of B
  Real GetB() const {return b_;}
  //! returns angle (in rad) between A and B
  Real GetGamma() const {return gamma_;}
  //! returns thickness
  Real GetC() const {return c_;}
  //! sets thickness
  void SetC(Real c) {c_=c;}
  //! returns symmetry
  Symmetry GetSymmetry() const {return symmetry_;}
  //! sets symmetry by name
  void SetSymmetry(const String& sym) {symmetry_=Symmetry(sym);}
  //! sets symmetry by sym object
  void SetSymmetry(const Symmetry& sym) {symmetry_=sym;}

  //! retrieve position at index hk
  geom::Vec2 GetPosition(const ost::img::Point& hk);
  
  CellImpl Invert() const;

  void Print(std::ostream& o) const;

protected:
  CellImpl(const geom::Vec2& a, const geom::Vec2& b, Real c,String symmetry="P 1");
  CellImpl(const geom::Vec2& a, const geom::Vec2& b, Real c,const Symmetry& s);
  Real a_,b_,c_;
  Real gamma_; // in rad

  geom::Vec2 A_,B_; 
  Symmetry symmetry_;
};

} // ns

//fw decl for friend functionality
class ReciprocalUnitCell;

//! Spatial Unit Cell
/*!
  Encapsulates a spatial unit cell, defined by two 2D basis vectors 
  in the xy-plane and a thickness. All values are assumed to be in A. 
  Initialization with a reciprocal unit cell is possible, allowing
  easy conversion between these two representations.
*/
class DLLEXPORT_IPLT_BASE SpatialUnitCell: public cell_detail::CellImpl {
  friend class ReciprocalUnitCell;
public:
  //! Default initialization
  /*!
    A=(1,0), B=(0,1), c=1.0
   */
  SpatialUnitCell();

  //! Initialization with the two vector lenghts, the angle (in rad) between them and (optionally) the thickness
  SpatialUnitCell(Real a, Real b, Real g, Real c=1.0, const String& s="P 1");

  //! Initialization with the two basis vectors and (optionally) the thickness
  SpatialUnitCell(const geom::Vec2& a, const geom::Vec2& b, Real c=1.0, const String& s="P 1");

  //! Initialization with a lattice and the pixel sampling
  SpatialUnitCell(const Lattice& l, const geom::Vec3& spatial_sampling, Real c=1.0, const String& s="P 1");

  //! Copy-ctor
  SpatialUnitCell(const SpatialUnitCell& c);

  //! Pseudo copy-ctor, allows implicit construction from reciprocal unit cell
  SpatialUnitCell(const ReciprocalUnitCell& r);

  //! internal ctor
  SpatialUnitCell(const cell_detail::CellImpl& impl);

protected:
  void check();
};

//! Reciprocal Unit Cell
class DLLEXPORT_IPLT_BASE ReciprocalUnitCell: public cell_detail::CellImpl {
  friend class SpatialUnitCell;
public:
  //! Default initialization
  /*!
    A=(1,0), B=(0,1), c=1.0
   */
  ReciprocalUnitCell();

  //! Initialization with the two basis vectors and (optionally) the thickness
  ReciprocalUnitCell(const geom::Vec2& a, const geom::Vec2& b, Real c=1.0, const String& s="P 1");

  //! Initialization with the two vector lenghts, the angle between them and (optionally) the thickness
  ReciprocalUnitCell(Real a, Real b, Real g, Real c=1.0, const String& s="P 1");

  //! Initialization with a lattice and the pixel sampling
  ReciprocalUnitCell(const Lattice& l, const geom::Vec3& reciprocal_sampling, Real c=1.0, const String& s="P 1");

  //! Copy-ctor
  ReciprocalUnitCell(const ReciprocalUnitCell& c);

  //! Pseudo copy-ctor, allows implicit construction from spatial unit cell
  ReciprocalUnitCell(const SpatialUnitCell& r);

  //! internal ctor
  ReciprocalUnitCell(const cell_detail::CellImpl& impl);

protected:
  void check();
};

DLLEXPORT_IPLT_BASE std::ostream& operator<<(std::ostream& o, const SpatialUnitCell& c);
DLLEXPORT_IPLT_BASE std::ostream& operator<<(std::ostream& o, const ReciprocalUnitCell& c);


} //ns

#endif
