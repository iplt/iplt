//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  2D lattice

  Author: Ansgar Philippsen, Andreas Schenk
*/

#include <cmath>
#include <ost/message.hh>

#include <ost/info/info.hh>

#include <boost/shared_ptr.hpp>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multiroots.h>

#include "lattice.hh"

namespace iplt {

namespace {

Real roundp(Real n,unsigned int precision)
{
  return round(n*static_cast<Real>(precision))/static_cast<Real>(precision);
}
template <class V>
bool colinear(const V& v1,
             const V& v2,
             Real lim=1e-20)
{
  return 1.0-std::fabs(Dot(Normalize(v1),Normalize(v2)))<lim;
}


struct gsl_vector_deleter {
  void operator()(gsl_vector* p) {
    gsl_vector_free(p);
  }
};

typedef boost::shared_ptr<gsl_vector> gsl_vector_ptr;
 
//calc lattice
struct calc_lat_params {
  geom::Vec2* v;
  ost::img::Point* p;
  Real k3;
  Real s3;
};
template <bool b1, bool b2>
int calc_lat_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  calc_lat_params* lat_params = reinterpret_cast<calc_lat_params*>(params);
  Real Ax = gsl_vector_get(x, 0);
  Real Ay = gsl_vector_get(x, 1);
  Real Bx = gsl_vector_get(x, 2);
  Real By = gsl_vector_get(x, 3);
  Real Ox = gsl_vector_get(x, 4);
  Real Oy = gsl_vector_get(x, 5);
  for(int i=0;i<3;i++){
    if(b1) { // function value
      Lattice lattice(geom::Vec2(Ax,Ay),geom::Vec2(Bx,By),geom::Vec2(Ox,Oy));
      lattice.SetBarrelDistortion(lat_params->k3);  
      lattice.SetSpiralDistortion(lat_params->s3);  
      geom::Vec2 calcpos=lattice.CalcPosition(lat_params->p[i]);
      gsl_vector_set(f,i*2+ 0, calcpos[0]-lat_params->v[i][0]);
      gsl_vector_set(f,i*2+ 1, calcpos[1]-lat_params->v[i][1]);
    }
    if(b2) { // derivative values
      Real xcomp = lat_params->p[i][0]*Ax+lat_params->p[i][1]*Bx;
      Real xcomp2 = xcomp*xcomp;
      Real ycomp = lat_params->p[i][0]*Ay+lat_params->p[i][1]*By;
      Real xycomp = xcomp*ycomp;
      Real ycomp2 = ycomp*ycomp;
      Real xycomp2 = xcomp2+ycomp2;
      Real pre1x = 1.0+lat_params->k3*(3.0*xcomp2+xycomp2)+2*lat_params->s3*xycomp;
      Real pre1y = 1.0+lat_params->k3*(3.0*ycomp2+xycomp2)+2*lat_params->s3*xycomp;
      Real pre2 = 2.0*lat_params->k3*xycomp+lat_params->s3*xycomp2;
      gsl_matrix_set(J, i*2+0, 0, lat_params->p[i][0]*pre1x); // dPx/dAx
      gsl_matrix_set(J, i*2+0, 1, lat_params->p[i][0]*pre2); // dPx/dAy
      gsl_matrix_set(J, i*2+0, 2, lat_params->p[i][1]*pre1x); // dPx/dBx
      gsl_matrix_set(J, i*2+0, 3, lat_params->p[i][1]*pre2); // dPx/dBy
      gsl_matrix_set(J, i*2+0, 4, 1.0); // dPx/dOx
      gsl_matrix_set(J, i*2+0, 5, 0.0); // dPx/dOy

      gsl_matrix_set(J, i*2+1, 0, lat_params->p[i][0]*pre2); // dPy/dAx
      gsl_matrix_set(J, i*2+1, 1, lat_params->p[i][0]*pre1y); // dPy/dAy
      gsl_matrix_set(J, i*2+1, 2, lat_params->p[i][1]*pre2); // dPy/dBx
      gsl_matrix_set(J, i*2+1, 3, lat_params->p[i][1]*pre1y); // dPy/dBy
      gsl_matrix_set(J, i*2+1, 4, 0.0); // dPy/dOx
      gsl_matrix_set(J, i*2+1, 5, 1.0); // dPy/dOy
    }
  }
  return GSL_SUCCESS;
}

int calc_lat_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return calc_lat_tmpl<true,false>(x,params,f,0);
}

int calc_lat_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return calc_lat_tmpl<false,true>(x,params,0,J);
}

int calc_lat_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return calc_lat_tmpl<true,true>(x,params,f,J);
}
 

//calc components
struct calc_comp_params {
  const Lattice& lattice;
  geom::Vec2 pos;
};


template <bool b1, bool b2>
int calc_comp_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  const Lattice& lattice = reinterpret_cast<calc_comp_params*>(params)->lattice;
  geom::Vec2 pos = reinterpret_cast<calc_comp_params*>(params)->pos;
  Real u = gsl_vector_get(x, 0);
  Real v = gsl_vector_get(x, 1);
  
  if(b1) { // function value
    geom::Vec2 calcpos=lattice.CalcPosition(geom::Vec2(u,v));
    gsl_vector_set(f, 0, calcpos[0]-pos[0]);
    gsl_vector_set(f, 1, calcpos[1]-pos[1]);
  }
  if(b2) { // derivative values
    geom::Vec2 a=lattice.GetFirst();
    geom::Vec2 b=lattice.GetSecond();
    Real k3=lattice.GetBarrelDistortion();
    Real s3=lattice.GetSpiralDistortion();
    Real x=u*a[0]+v*b[0];
    Real y=u*a[1]+v*b[1];
    Real r2=x*x+y*y;
    Real r=sqrt(r2);
    Real k3prefac=1+k3*r2;
    Real s3prefac=s3*r2;
    Real drdu4r=4*r*(x*a[0]+y*a[1]);
    Real drdv4r=4*r*(x*b[0]+y*b[1]);
    
    gsl_matrix_set(J, 0, 0, a[0]*k3prefac+a[1]*s3prefac+drdu4r*(x*k3+y*s3)); // dPx/du
    gsl_matrix_set(J, 0, 1, b[0]+k3prefac+b[1]+s3prefac+drdv4r*(x*k3+y*s3)); // dPy/du
    gsl_matrix_set(J, 1, 0, a[1]*k3prefac-a[0]*s3prefac+drdu4r*(y*k3-x*s3)); // dPx/dv
    gsl_matrix_set(J, 1, 1, b[1]*k3prefac-b[0]*s3prefac+drdv4r*(y*k3-x*s3)); // dPy/dv
  }
  return GSL_SUCCESS;
}

int calc_comp_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return calc_comp_tmpl<true,false>(x,params,f,0);
}

int calc_comp_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return calc_comp_tmpl<false,true>(x,params,0,J);
}

int calc_comp_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return calc_comp_tmpl<true,true>(x,params,f,J);
}

}// anon ns


Lattice::Lattice():
  vec1_(geom::Vec2(1.0,0.0)),vec2_(geom::Vec2(0.0,1.0)), off_(geom::Vec2(0.0,0.0)),barrel_k3_(0.0),spiral_k3_(0.0)
{}

Lattice::Lattice(const geom::Vec2& v1, const geom::Vec2& v2, const geom::Vec2& offset):
  vec1_(v1), vec2_(v2), off_(offset), barrel_k3_(0.0),spiral_k3_(0.0) 
{
  assert_non_parallel();
}

Lattice::Lattice(const geom::Vec2& v1, const geom::Vec2& v2, const geom::Vec2& offset, Real k3, Real s3):
  vec1_(v1), vec2_(v2), off_(offset), barrel_k3_(k3),spiral_k3_(s3) 
{
  assert_non_parallel();
}

Lattice::Lattice(const geom::Vec2& v1, const ost::img::Point& p1, const geom::Vec2& v2, const ost::img::Point& p2, const geom::Vec2& offset):
  vec1_(),
  vec2_(),
  off_(offset),
  barrel_k3_(0.0),
  spiral_k3_(0.0)
{
  geom::Vec2 d1=v1-offset;
  geom::Vec2 d2=v2-offset;
  Real ax=(p2[1]*d1[0]-p1[1]*d2[0])/(p1[0]*p2[1]-p2[0]*p1[1]);
  Real ay=(p2[1]*d1[1]-p1[1]*d2[1])/(p1[0]*p2[1]-p2[0]*p1[1]);
  vec1_=geom::Vec2(ax,ay);
  Real bx=(p2[0]*d1[0]-p1[0]*d2[0])/(p1[1]*p2[0]-p2[1]*p1[0]);
  Real by=(p2[0]*d1[1]-p1[0]*d2[1])/(p1[1]*p2[0]-p2[1]*p1[0]);
  vec2_=geom::Vec2(bx,by);
}

Lattice::Lattice(const geom::Vec2& v1,
                 const ost::img::Point& p1, 
                 const geom::Vec2& v2, 
                 const ost::img::Point& p2,
                 const geom::Vec2& v3,
                 const ost::img::Point& p3):
  vec1_(),
  vec2_(),
  off_(),
  barrel_k3_(0.0),
  spiral_k3_(0.0)
{
  for(unsigned int i=0;i<2;++i){
    vec1_[i]=(p3[1]*(v2[i]-v1[i])+p2[1]*(v1[i]-v3[i])+p1[1]*(v3[i]-v2[i]))
            /(p3[0]*(p1[1]-p2[1])+p1[0]*(p2[1]-p3[1])+p2[0]*(p3[1]-p1[1]));
    vec2_[i]=(p3[0]*(v1[i]-v2[i])+p1[0]*(v2[i]-v3[i])+p2[0]*(v3[i]-v1[i]))
            /(p3[0]*(p1[1]-p2[1])+p1[0]*(p2[1]-p3[1])+p2[0]*(p3[1]-p1[1]));
    off_[i]=(p3[0]*(p1[1]*v2[i]-p2[1]*v1[i])+p2[0]*(p3[1]*v1[i]-p1[1]*v3[i])+p1[0]*(p2[1]*v3[i]-p3[1]*v2[i]))
           /(p3[0]*(p1[1]-p2[1])+p1[0]*(p2[1]-p3[1])+p2[0]*(p3[1]-p1[1]));
  }
}
Lattice::Lattice(const geom::Vec2& v1,
                   const ost::img::Point& p1, 
                   const geom::Vec2& v2, 
                   const ost::img::Point& p2,
                   const geom::Vec2& v3,
                   const ost::img::Point& p3,
                   Real k3,
                   Real s3):
    vec1_(),
    vec2_(),
    off_(),
    barrel_k3_(k3),
    spiral_k3_(s3)
  {
  int paramN=6;

  gsl_vector_ptr X(gsl_vector_alloc(paramN),gsl_vector_deleter());

  // initial guess
  gsl_vector_set(X.get(),0,1); // Ax
  gsl_vector_set(X.get(),1,0); // Ay 
  gsl_vector_set(X.get(),2,0); // Bx
  gsl_vector_set(X.get(),3,1); // By 
  gsl_vector_set(X.get(),4,0); // Ox
  gsl_vector_set(X.get(),5,0); // Oy 


  gsl_multiroot_function_fdf FDF;
  
  geom::Vec2 v[3];
  ost::img::Point p[3];
  v[0]=v1;   
  v[1]=v2;   
  v[2]=v3;   
  p[0]=p1;   
  p[1]=p2;   
  p[2]=p3;   
  FDF.f = &calc_lat_f;
  FDF.df = &calc_lat_df;
  FDF.fdf = &calc_lat_fdf;
  FDF.n = paramN;
  calc_lat_params params={v,p,k3,s3};
  FDF.params = &params;

  // hybrid solver
  const gsl_multiroot_fdfsolver_type * T = gsl_multiroot_fdfsolver_hybridsj;
  gsl_multiroot_fdfsolver * solver = gsl_multiroot_fdfsolver_alloc (T, paramN);
  gsl_multiroot_fdfsolver_set (solver, &FDF, X.get());

  int iter=0;
  int status=GSL_CONTINUE;
  int max_iter=1000;
  
  while (status == GSL_CONTINUE && iter < max_iter) {
    ++iter;
    gsl_multiroot_fdfsolver_iterate (solver);
    status = gsl_multiroot_test_delta (solver->dx, solver->x, 1e-40,1e-40);
  } 

  SetFirst(geom::Vec2(roundp(gsl_vector_get(solver->x, 0),12),roundp(gsl_vector_get(solver->x,1),12)));
  SetSecond(geom::Vec2(roundp(gsl_vector_get(solver->x, 2),12),roundp(gsl_vector_get(solver->x,3),12)));
  SetOffset(geom::Vec2(roundp(gsl_vector_get(solver->x, 4),12),roundp(gsl_vector_get(solver->x,5),12)));
  gsl_multiroot_fdfsolver_free (solver);
}

/*
  barrel (or pincushion) distortion is defined by
  a cubic offset from the center of the coordinate
  system, ie at a given radius R0, the distorted
  radius R1 = R0 + k3*R0^3.

  as a consequence, the X and Y coordinate of the
  lattice point position are multiplied with a
  distortion term that is composed of the barrel
  constant k3 and the distance from the origin
   X' = X * (1 + k3 * (X^2 + Y^2) )
   Y' = Y * (1 + k3 * (X^2 + Y^2) )
  this gives as a length for the new (X',Y') vector:
   sqrt(X'^2+Y'^2) = sqrt(X^2+Y^2) + k3*sqrt(X^2+Y^2)^3
*/
/*
 Spiral distortion is defined by a cubic offset perpendicular
 to the vector describing the lattice point position 
 */

geom::Vec2 Lattice::CalcPosition(const ost::img::Point& hk) const
{
    return CalcPosition(hk.ToVec2());
}
geom::Vec2 Lattice::CalcPosition(const geom::Vec2& hk) const
{
  geom::Vec2 pos=vec1_*hk[0]+vec2_*hk[1];
  Real boff=Length2(pos)*barrel_k3_;
  Real sfac=Length2(pos)*spiral_k3_;
  geom::Vec2 soff=geom::Vec2(-pos[1],pos[0])*sfac;
  pos*=1.0+boff;
  pos+=soff;
  pos+=off_;
  return pos;
}

geom::Vec2 Lattice::CalcComponents(const ost::img::Point& in) const
{
  
  return CalcComponents(in.ToVec2());
}

geom::Vec2 Lattice::CalcComponents(const geom::Vec2& in) const
{
  if(GetBarrelDistortion()!=0.0 || GetSpiralDistortion()!=0.0){
    return calc_components_with_distortion_(in);
  }else{
    return calc_components_without_distortion_(in);
  }
}

// private methods

geom::Vec2 Lattice::calc_components_without_distortion_(const geom::Vec2& in) const
{
  geom::Vec2 p(in-off_);
  
  Real den = vec1_[1]*vec2_[0]-vec1_[0]*vec2_[1];
  Real nom1 = -vec2_[1]*p[0]+vec2_[0]*p[1];
  Real nom2 = -vec1_[1]*p[0]+vec1_[0]*p[1];

  geom::Vec2 comp(std::fabs(den)>1.0e-20 ? nom1/den : 0.0,std::fabs(den)>1.0e-20 ? nom2/-den : 0.0);

  return comp;
}

geom::Vec2 Lattice::calc_components_with_distortion_(const geom::Vec2& in) const
{ 
    

    
  int paramN=2;

  gsl_vector_ptr X(gsl_vector_alloc(paramN),gsl_vector_deleter());

  // initial guess
  geom::Vec2 guess=calc_components_without_distortion_(in);
  gsl_vector_set(X.get(),0,guess[0]); // u
  gsl_vector_set(X.get(),1,guess[1]); // v


  gsl_multiroot_function_fdf FDF;
     
  FDF.f = &calc_comp_f;
  FDF.df = &calc_comp_df;
  FDF.fdf = &calc_comp_fdf;
  FDF.n = paramN;
  calc_comp_params params={*this,in};
  FDF.params = &params;

  // hybrid solver
  const gsl_multiroot_fdfsolver_type * T = gsl_multiroot_fdfsolver_hybridsj;
  gsl_multiroot_fdfsolver * solver = gsl_multiroot_fdfsolver_alloc (T, paramN);
  gsl_multiroot_fdfsolver_set (solver, &FDF, X.get());

  int iter=0;
  int status=GSL_CONTINUE;
  int max_iter=1000;
  
  while (status == GSL_CONTINUE && iter < max_iter) {
    ++iter;
    gsl_multiroot_fdfsolver_iterate (solver);
    status = gsl_multiroot_test_delta (solver->dx, solver->x, 1e-40,1e-40);
  } 

  geom::Vec2 result(gsl_vector_get(solver->x, 0),gsl_vector_get(solver->x,1));
  gsl_multiroot_fdfsolver_free (solver);
           
  return result;
}

void Lattice::assert_non_parallel()
{
  if(colinear(vec1_,vec2_)) {
    throw ost::Error("lattice vectors are co-linear");
  }
}

bool Lattice::equal(const Lattice& lat) const
{
  return Length2(lat.vec1_-vec1_)<1e-6 &&
    Length2(lat.vec2_-vec2_)<1e-6 &&
    Length2(lat.off_-off_)<1e-6;
    
}

Real Lattice::DistanceTo(const ost::img::Point& p) const
{
  return DistanceTo(p.ToVec2());
}
Real Lattice::DistanceTo(const geom::Vec2& v) const
{
  geom::Vec2 comp=CalcComponents(v);
  geom::Vec2 latpos=CalcPosition(geom::Vec2(round(comp[0]),round(comp[1])));
  return Length(v-latpos);
}

PointList Predict(const Lattice& lattice, const ost::img::Extent&extent)
{
  ost::img::PointList result;

  // use four corner points of 2D extent
  ost::img::Point p1=extent.GetStart();
  ost::img::Point p2=extent.GetEnd();
  ost::img::Point p3(p1[0],p2[1]);
  ost::img::Point p4(p2[0],p1[1]);

  // get components of each
  geom::Vec2 v1(lattice.CalcComponents(p1));
  geom::Vec2 v2(lattice.CalcComponents(p2));
  geom::Vec2 v3(lattice.CalcComponents(p3));
  geom::Vec2 v4(lattice.CalcComponents(p4));

  geom::Vec2 nm_min;
  geom::Vec2 nm_max;

  for(int i=0;i<2;++i) {
    // determine corners
    nm_min[i] = std::min<Real>(v1[i],v2[i]);
    nm_min[i] = std::min<Real>(nm_min[i],v3[i]);
    nm_min[i] = std::min<Real>(nm_min[i],v4[i]);

    nm_max[i] = std::max<Real>(v1[i],v2[i]);
    nm_max[i] = std::max<Real>(nm_max[i],v3[i]);
    nm_max[i] = std::max<Real>(nm_max[i],v4[i]);
  } 
 
  int nstart = static_cast<int>(round(nm_min[0]));
  int nend = static_cast<int>(round(nm_max[0]));
  int mstart = static_cast<int>(round(nm_min[1]));
  int mend = static_cast<int>(round(nm_max[1]));

  geom::Vec2 vec1 = lattice.GetFirst();

  for(int n=nstart;n<=nend; n+=1) {
    for(int m=mstart;m<=mend; m+=1) {
      ost::img::Point indx(n,m);
      geom::Vec2 pos=lattice.CalcPosition(indx);
      if(extent.Contains(ost::img::Point(pos))) {
        result.push_back(indx);
      }
    }
  }

  return result;
}


std::ostream& operator<<(std::ostream& os, const Lattice &l)
{
  os << "[" << l.GetFirst() << "," << l.GetSecond() << "," << l.GetOffset() << "]";
  if(l.GetBarrelDistortion()!=0.0) os << " (k3=" << l.GetBarrelDistortion() << ")" ;
  if(l.GetSpiralDistortion()!=0.0) os << " (s3=" << l.GetSpiralDistortion() << ")" ;
  return os;
}

Lattice InvertLattice(const Lattice& lat)
{
  geom::Vec2 A=lat.GetFirst();
  geom::Vec2 B=lat.GetSecond();
  Real D = A[1]*B[0]-A[0]*B[1];
  return Lattice(geom::Vec2(-B[1]/D,B[0]/D),geom::Vec2(A[1]/D,-A[0]/D));
}

Lattice InfoToLattice(const ost::info::InfoGroup& par)
{
  // requires (ax,ay,bx,by,ox,oy)
  Real va[]={0.0,0.0};
  Real vb[]={0.0,0.0};
  Real vo[]={0.0,0.0};
  Real k3=0.0;
  Real s3=0.0;

  try {
    if(par.HasGroup("Lattice")) {
      ost::info::InfoGroup g = par.GetGroup("Lattice");
      if(g.HasItem("ax") && g.HasItem("ay") &&
         g.HasItem("bx") && g.HasItem("by")) {

        va[0] = g.GetItem("ax").AsFloat();
        va[1] = g.GetItem("ay").AsFloat();
        vb[0] = g.GetItem("bx").AsFloat();
        vb[1] = g.GetItem("by").AsFloat();
  
      } else {
        std::ostringstream m;
        m << "ExtractLattice: no lattice description (ax,ay) (bx,by) found in Lattice group";
        throw ost::Error(m.str());
      }
      if(g.HasItem("ox") && g.HasItem("oy")) { // not mandatory
        vo[0] = g.GetItem("ox").AsFloat();
        vo[1] = g.GetItem("oy").AsFloat();
      }
      if(g.HasItem("k3")) { // not mandatory
        k3 = g.GetItem("k3").AsFloat();
      }
      if(g.HasItem("s3")) { // not mandatory
        s3 = g.GetItem("s3").AsFloat();
      }
    } else {
      std::ostringstream m;
      m << "ExtractLattice: no 'Lattice' group found in group " << par.GetName();
      throw ost::Error(m.str());
    }
       
  } catch(const ost::info::InfoError& e) {
    throw;
  }

  return Lattice(geom::Vec2(va[0],va[1]),geom::Vec2(vb[0],vb[1]),geom::Vec2(vo[0],vo[1]),k3,s3);
}


void LatticeToInfo(const Lattice& lat, ost::info::InfoGroup g)
{
  ost::info::InfoGroup lg=g.RetrieveGroup("Lattice");
  ost::info::InfoItem ii=lg.RetrieveItem("ax");
  ii.SetFloat(lat.GetFirst()[0]);
  ii=lg.RetrieveItem("ay");
  ii.SetFloat(lat.GetFirst()[1]);
  ii=lg.RetrieveItem("bx");
  ii.SetFloat(lat.GetSecond()[0]);
  ii=lg.RetrieveItem("by");
  ii.SetFloat(lat.GetSecond()[1]);
  ii=lg.RetrieveItem("ox");
  ii.SetFloat(lat.GetOffset()[0]);
  ii=lg.RetrieveItem("oy");
  ii.SetFloat(lat.GetOffset()[1]);
  ii=lg.RetrieveItem("k3");
  ii.SetFloat(lat.GetBarrelDistortion());
  ii=lg.RetrieveItem("s3");
  ii.SetFloat(lat.GetSpiralDistortion());
}

} //ns
