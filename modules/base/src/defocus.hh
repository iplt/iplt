//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/
#ifndef DEFOCUS_HH_
#define DEFOCUS_HH_

#include <ost/units.hh>
#include <ost/info/info.hh>
#include <iplt/module_config.hh>

namespace iplt { 

class DLLEXPORT_IPLT_BASE Defocus 
{
public:
    Defocus(Real d=5000.0*ost::Units::A);
  Defocus(Real dx,Real dy,Real angle=0.0*ost::Units::deg);

  void SetDefocusX(Real dx){dx_=dx;}
  void SetDefocusY(Real dy){dy_=dy;}
  void SetMeanDefocus(Real d);
  void SetAstigmatismAngle(Real ang){angle_=ang;}
  Real GetDefocusX() const {return dx_;}
  Real GetDefocusY() const {return dy_;}
  Real GetMeanDefocus() const {return (dx_+dy_)*0.5;}
  Real GetAstigmatismAngle() const {return angle_;}
  
protected:
  Real dx_;
  Real dy_;
  Real angle_;
};

DLLEXPORT_IPLT_BASE void DefocusToInfo(const Defocus& df, ost::info::InfoGroup& ig);
DLLEXPORT_IPLT_BASE Defocus InfoToDefocus(const ost::info::InfoGroup& ig);


} //ns

#endif /*DEFOCUS_HH_*/

