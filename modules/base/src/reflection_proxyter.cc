//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Reflection proxyter

  Author: Ansgar Philippsen
*/

#include <ost/message.hh>
#include <ost/log.hh>

#include "reflection_proxyter.hh"
#include "unit_cell.hh"

namespace iplt { 

using namespace reflection_detail;

ReflectionProxyter::ReflectionProxyter(ListPtr lptr, const IndexMapIterator& indx):
  lptr_(lptr),
  index_(indx)
{}

ReflectionProxyter::ReflectionProxyter(const ReflectionProxyter& p):
  lptr_(p.lptr_),
  index_(p.index_)
{
}

ReflectionProxyter& ReflectionProxyter::operator=(const ReflectionProxyter& p)
{
  if(&p!=this) {
    lptr_=p.lptr_;
    index_=p.index_;
  }
  return *this;
}

Real ReflectionProxyter::Get(const String& name) const
{
  if(!IsValid()){
    throw ReflectionException("Invalid ReflectionProxyter");
  }
  return (*lptr_).Value(index_,name);
}

Real ReflectionProxyter::Get(unsigned int num) const
{
  if(!IsValid()){
    throw ReflectionException("Invalid ReflectionProxyter");
  }
  return (*lptr_).Value(index_,num);
}

void ReflectionProxyter::Set(const String& name, Real value)
{
  if( !IsValid()){
    throw ReflectionException("Invalid ReflectionProxyter");
  }
  (*lptr_).Value(index_,name)=value;
}

void ReflectionProxyter::Set(unsigned int num, Real value)
{
  (*lptr_).Value(index_,num)=value;
}

void ReflectionProxyter::Set(const ReflectionProxyter& rp)
{
  assert(lptr_);
  unsigned int pcount=lptr_->GetPropertyCount();
  for(unsigned int c=0;c<pcount;++c) {
    try {
      String pname=lptr_->GetPropertyName(c);
      //this->Set(pname,0.0);
      this->Set(pname,rp.Get(pname));
    } catch (ReflectionException& e) {
      // ignore
    }
  }
}

Real ReflectionProxyter::GetResolution() const
{
  assert(lptr_);
  return lptr_->GetResolution(GetIndex());
}

ReflectionIndex ReflectionProxyter::GetIndex() const {return index_->first;}

ReflectionIndex ReflectionProxyter::Index() const {
  LOG_ERROR( "warning: ReflectionProxyter::Index() is deprecated, use GetIndex() instead" );
  return this->GetIndex();
}

ReflectionProxyter& ReflectionProxyter::operator++()
{
  inc();
  return *this;
}

ReflectionProxyter ReflectionProxyter::operator++(int)
{
  ReflectionProxyter tmp(*this);
  inc();
  return tmp;
}

bool ReflectionProxyter::AtEnd() const
{
  return (*lptr_).AtEnd(index_);
}

bool ReflectionProxyter::IsValid() const
{
  return !this->AtEnd();
}

void ReflectionProxyter::inc()
{
  index_ = (*lptr_).NextIndex(index_);
}
void ReflectionProxyter::Delete()
{
  index_=(*lptr_).DeleteEntry(index_);
}

} //ns
