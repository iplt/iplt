//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <ost/info/info.hh>

#include <ost/message.hh>
#include <ost/log.hh>
#include <ost/units.hh>

#include "unit_cell.hh"
#include "unit_cell_info.hh"

namespace iplt { 

namespace cell_detail {
CellImpl Extract(const ost::info::InfoGroup& g)
{
  Real a=0.0,b=0.0,gm=0.0,th=0.0;
  String sym="P 1";

if(g.HasItem("a") && g.HasItem("b") && 
	    g.HasItem("gamma") && g.HasItem("thickness")) {
    
    a = ost::info::Extract<Real>(g,"a");
    gm = ost::info::Extract<Real>(g,"gamma");
    b = ost::info::Extract<Real>(g,"b");
    th = ost::info::Extract<Real>(g,"thickness");
  } else {
    std::ostringstream m;
    m << "ExtractUnitCell: no unit cell description found";
    throw ost::Error(m.str());
  }

  if(g.HasItem("symmetry")) {
    sym=g.GetItem("symmetry").GetValue();
  } else {
    LOG_VERBOSE( "ExtractUnitCell: missing symmetry from unit cell entry, assuming P1" );
  }

  LOG_DEBUG( "ExtractUnitCell: extracted " << a << ","  << b  << ","  << th << ","  << gm << "  " << sym );
  
  CellImpl nrvo(a*Units::A,b*Units::A,th*Units::A,gm*Units::deg,sym);
  return nrvo;
}

} // ns

SpatialUnitCell ExtractSpatialUnitCell(const ost::info::InfoGroup& par)
{
  LOG_INFO(  "ExtractSpatialUnitCell is deprecated, please use InfoToSpatialUnitCell" );
  return InfoToSpatialUnitCell(par);
}

ReciprocalUnitCell ExtractReciprocalUnitCell(const ost::info::InfoGroup& par)
{
  LOG_INFO( "ExtractReciprocalUnitCell is deprecated, please use InfoToReciprocalUnitCell" );
  return InfoToReciprocalUnitCell(par);
}

SpatialUnitCell InfoToSpatialUnitCell(const ost::info::InfoGroup& par)
{
  if(par.HasGroup("unitcell")) {
    ost::info::InfoGroup g=par.GetGroup("unitcell");
    if(g.HasAttribute("type")) {
      String ctype = g.GetAttribute("type");
      cell_detail::CellImpl c = cell_detail::Extract(g);
      if(ctype==String("spatial")) {
	return SpatialUnitCell(c);
      } else if(ctype==String("reciprocal")) {
	return SpatialUnitCell(c.Invert());
      } else {
	std::ostringstream m;
	m << "ExtractSpacialUnitCell: 'type' item must have either 'spatial' or 'reciprocal' value, but found '" << ctype << "' instead";
	throw ost::Error(m.str());
      }
    } else {
      std::ostringstream m;
      m << "ExtractSpacialUnitCell: 'unitcell' group is missing 'type' item ";
      throw ost::Error(m.str());
    }
  } else {
    std::ostringstream m;
    m << "ExtractSpacialUnitCell: no 'unitcell' group found in group " << par.GetName();
    throw ost::Error(m.str());
  }
  // should never get here
}

ReciprocalUnitCell InfoToReciprocalUnitCell(const ost::info::InfoGroup& par)
{
  if(par.HasGroup("unitcell")) {
    ost::info::InfoGroup g=par.GetGroup("unitcell");
    if(g.HasAttribute("type")) {
      String ctype = g.GetAttribute("type");
      cell_detail::CellImpl c = cell_detail::Extract(g);
      if(ctype==String("reciprocal")) {
	return ReciprocalUnitCell(c);
      } else if(ctype==String("spatial")) {
	return ReciprocalUnitCell(c.Invert());
      } else {
	std::ostringstream m;
	m << "ExtractReciprocalUnitCell: 'type' item must have either 'spatial' or 'reciprocal' value, but found '" << ctype << "' instead";
	throw ost::Error(m.str());
      }
    } else {
      std::ostringstream m;
      m << "ExtractReciprocalUnitCell: 'unitcell' group is missing 'type' item ";
      throw ost::Error(m.str());
    }
  } else {
    std::ostringstream m;
    m << "ExtractReciprocalUnitCell: no 'unitcell' group found in group " << par.GetName();
    throw ost::Error(m.str());
  }
  // should never get here
}

} //ns
