//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/defocus.hh>
#include <iplt/microscope_data.hh>
#include <iplt/tcif_data.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;

void export_defocus_microscope_tcif()
{
  class_<Defocus>("Defocus", init<Real,Real,optional<Real > >() )
    .def(init<optional<Real > >())
    .def("SetDefocusX",&Defocus::SetDefocusX)
    .def("SetDefocusY",&Defocus::SetDefocusY)
    .def("SetMeanDefocus",&Defocus::SetMeanDefocus)
    .def("SetAstigmatismAngle",&Defocus::SetAstigmatismAngle)
    .def("GetDefocusX",&Defocus::GetDefocusX)
    .def("GetDefocusY",&Defocus::GetDefocusY)
    .def("GetMeanDefocus",&Defocus::GetMeanDefocus)
    .def("GetAstigmatismAngle",&Defocus::GetAstigmatismAngle)
    ;

  class_<MicroscopeData>("MicroscopeData", init<optional<Real,Real,Real,Real,Real,Real> >() )
    .def("SetSphericalAberration",&MicroscopeData::SetSphericalAberration)
    .def("SetAccelerationVoltage",&MicroscopeData::SetAccelerationVoltage)
    .def("SetChromaticAberration",&MicroscopeData::SetChromaticAberration)
    .def("SetAmplitudeContrast",&MicroscopeData::SetAmplitudeContrast)
    .def("SetConvergenceAngle",&MicroscopeData::SetConvergenceAngle)
    .def("SetEnergySpread",&MicroscopeData::SetEnergySpread)
    .def("GetSphericalAberration",&MicroscopeData::GetSphericalAberration)
    .def("GetAccelerationVoltage",&MicroscopeData::GetAccelerationVoltage)
    .def("GetChromaticAberration",&MicroscopeData::GetChromaticAberration)
    .def("GetAmplitudeContrast",&MicroscopeData::GetAmplitudeContrast)
    .def("GetConvergenceAngle",&MicroscopeData::GetConvergenceAngle)
    .def("GetEnergySpread",&MicroscopeData::GetEnergySpread)
    .def("GetWavelength",&MicroscopeData::GetWavelength)
    ;
    
  class_<TCIFData, bases<MicroscopeData,Defocus,TiltGeometry> >("TCIFData", init<optional<const MicroscopeData&,const Defocus& ,const TiltGeometry&> >() )
    .def(init<const TCIFData&>())
    .def("SetDefocus",&TCIFData::SetDefocus)
    .def("SetMicroscopeData",&TCIFData::SetMicroscopeData)
    .def("SetTiltGeometry",&TCIFData::SetTiltGeometry)
    ; 

  def("MicroscopeDataToInfo",&MicroscopeDataToInfo);
  def("InfoToMicroscopeData",&InfoToMicroscopeData);
  def("DefocusToInfo",&DefocusToInfo);
  def("InfoToDefocus",&InfoToDefocus);
  def("TCIFDataToInfo",&TCIFDataToInfo);
  def("InfoToTCIFData",&InfoToTCIFData);
  
}
