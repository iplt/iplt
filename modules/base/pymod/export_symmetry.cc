//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
using namespace boost::python;

#include <iplt/symmetry.hh>
#include <iplt/symmetry_exception.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;

ReflectionIndex (Symmetry::*SymmetrizeIndex)(const ReflectionIndex& r) const = &Symmetry::Symmetrize;
Real  (Symmetry::*SymmetrizePhase)(const ReflectionIndex& r, Real phase) const = &Symmetry::Symmetrize;

bool (Symmetry::*IsInAsymmetricUnit1)(const ReflectionIndex& r) const = &Symmetry::IsInAsymmetricUnit;
bool (Symmetry::*IsInAsymmetricUnit2)(const SymmetryReflection& r) const = &Symmetry::IsInAsymmetricUnit;
bool (Symmetry::*IsCentric1)(const ReflectionIndex& r) const = &Symmetry::IsCentric;
bool (Symmetry::*IsCentric2)(const SymmetryReflection& r) const = &Symmetry::IsCentric;

void SymmetryExceptionTranslator(SymmetryException const& x) {
  PyErr_SetString(PyExc_RuntimeError, x.what());
}

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(genpoints_overloads, GenerateSymmetryRelatedPoints, 1, 2)

list get_implemented_symmetries()
{
  std::vector<unsigned int> res = GetImplementedSymmetries();
  list nrvo;
  for(std::vector<unsigned int>::const_iterator it=res.begin();it!=res.end();++it) {
    nrvo.append(*it);
  }
  return nrvo;
}

void export_symmetry(){
  class_<Symmetry >("Symmetry" ,init<unsigned>())
    .def(init<String>())
    .def("GetSpacegroupNumber",&Symmetry::GetSpacegroupNumber)
    .def("GetSpacegroupName",&Symmetry::GetSpacegroupName)
    .def("GetPointgroupName",&Symmetry::GetPointgroupName)
    .def("IsInAsymmetricUnit",IsInAsymmetricUnit1)
    .def("IsInAsymmetricUnit",IsInAsymmetricUnit2)
    .def("IsCentric",IsCentric1)
    .def("IsCentric",IsCentric2)
    .def("GenerateSymmetryRelatedPoints",&Symmetry::GenerateSymmetryRelatedPoints,genpoints_overloads())
    .def("GenerateSymmetryRelatedPositions",&Symmetry::GenerateSymmetryRelatedPositions)
    .def("Symmetrize",SymmetrizeIndex)
    .def("Symmetrize",SymmetrizePhase)
    ;
    
  def("GetImplementedSymmetries",get_implemented_symmetries);

  class_<std::vector<ReflectionIndex> >("ReflectionIndexVec")
    .def(vector_indexing_suite<std::vector<ReflectionIndex> >())
    ;

  register_exception_translator<SymmetryException>(SymmetryExceptionTranslator);
}
