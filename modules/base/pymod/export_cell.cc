//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/unit_cell.hh>
#include <iplt/unit_cell_info.hh>
#include <ost/info/info.hh>
#include <ost/base.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;

void export_cell()
{
  void (cell_detail::CellImpl::*set_sym1)(const String&) = &cell_detail::CellImpl::SetSymmetry;
  void (cell_detail::CellImpl::*set_sym2)(const Symmetry&) = &cell_detail::CellImpl::SetSymmetry;
  class_<cell_detail::CellImpl, boost::noncopyable>("_CellImpl",no_init)
    .def("GetVecA",&cell_detail::CellImpl::GetVecA)
    .def("GetVecB",&cell_detail::CellImpl::GetVecB)
    .def("GetA",&cell_detail::CellImpl::GetA)
    .def("GetB",&cell_detail::CellImpl::GetB)
    .def("GetGamma",&cell_detail::CellImpl::GetGamma)
    .def("GetC",&cell_detail::CellImpl::GetC)
    .def("SetC",&cell_detail::CellImpl::SetC)
    .def("GetSymmetry",&cell_detail::CellImpl::GetSymmetry)
    .def("SetSymmetry",set_sym1)
    .def("SetSymmetry",set_sym2)
    .def("GetPosition",&cell_detail::CellImpl::GetPosition)
    ;

  class_<SpatialUnitCell, bases<cell_detail::CellImpl> >("SpatialUnitCell",init<Real, Real, Real, optional<Real,const String&> >())
    .def(init<const ReciprocalUnitCell&>())
    .def(init<const Lattice&, const geom::Vec3&, optional<Real,const String&> >())
    .def(self_ns::str(self))
    ;

  class_<ReciprocalUnitCell, bases<cell_detail::CellImpl> >("ReciprocalUnitCell",init<Real, Real, Real, optional<Real,const String&> >())
    .def(init<const SpatialUnitCell&>())
    .def(init<const Lattice&, const geom::Vec3&, optional<Real,const String&> >())
    .def(self_ns::str(self))
    ;

  def("InfoToSpatialUnitCell",InfoToSpatialUnitCell);
  def("InfoToReciprocalUnitCell",InfoToReciprocalUnitCell);

  def("ExtractSpatialUnitCell",ExtractSpatialUnitCell);        // deprecated
  def("ExtractReciprocalUnitCell",ExtractReciprocalUnitCell);  // deprecated

  implicitly_convertible<SpatialUnitCell,ReciprocalUnitCell>();
  implicitly_convertible<ReciprocalUnitCell,SpatialUnitCell>();
}
