//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/tilt.hh>
#include <ost/info/info.hh>
#include <ost/geom/geom.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;

tuple calc_tilt_component_vectors(const TiltGeometry& tg, const Lattice& lat, const ost::img::Point& indx)
{
  std::pair<Vec2,Vec2> ret = CalcTiltComponentVectors(tg,lat,indx);
  // hack for ambigious bug in make_tuple(ret.first,ret.second)
  std::vector<Vec2> ret2;
  ret2.push_back(ret.first);
  ret2.push_back(ret.second);
  return tuple(ret2);
}


void export_tilt()
{
  Lattice (TiltGeometry::*applyspatial1)(const Lattice&) const = &TiltGeometry::ApplySpatial;
  geom::Vec2 (TiltGeometry::*applyspatial2)(const geom::Vec2&) const = &TiltGeometry::ApplySpatial;
  Lattice (TiltGeometry::*applyreciprocal1)(const Lattice&) const = &TiltGeometry::ApplyReciprocal;
  geom::Vec2 (TiltGeometry::*applyreciprocal2)(const geom::Vec2&) const = &TiltGeometry::ApplyReciprocal;

  class_<TiltGeometry>("TiltGeometry", init<>())
    .def(init<Real,Real>())
    .def(init<const TiltGeometry&>())
    .def("GetTiltAngle",&TiltGeometry::GetTiltAngle)
    .def("SetTiltAngle",&TiltGeometry::SetTiltAngle)
    .def("GetXAxisAngle",&TiltGeometry::GetXAxisAngle)
    .def("SetXAxisAngle",&TiltGeometry::SetXAxisAngle)
    .def("GetSpatialTransformation",&TiltGeometry::GetSpatialTransformation)
    .def("GetReciprocalTransformation",&TiltGeometry::GetReciprocalTransformation)
    .def("ApplySpatial",applyspatial1)
    .def("ApplySpatial",applyspatial2)
    .def("ApplyReciprocal",applyreciprocal1)
    .def("ApplyReciprocal",applyreciprocal2)
    .def(self_ns::str(self))
    ;

  class_<LatticeTiltGeometry, bases<TiltGeometry> >("LatticeTiltGeometry", init<optional<Real, Real, const Lattice&, bool > >())
    .def(init<const LatticeTiltGeometry&>())
    .def("GetAStarVectorAngle",&LatticeTiltGeometry::GetAStarVectorAngle)
    .def("SetAStarVectorAngle",&LatticeTiltGeometry::SetAStarVectorAngle)
    .def("GetTiltedAStarVectorAngle",&LatticeTiltGeometry::GetTiltedAStarVectorAngle)
    .def("SetTiltedAStarVectorAngle",&LatticeTiltGeometry::SetTiltedAStarVectorAngle)
    .def("GetAVectorAngle",&LatticeTiltGeometry::GetAVectorAngle)
    .def("SetAVectorAngle",&LatticeTiltGeometry::SetAVectorAngle)
    .def("GetTiltedAVectorAngle",&LatticeTiltGeometry::GetTiltedAVectorAngle)
    .def("SetTiltedAVectorAngle",&LatticeTiltGeometry::SetTiltedAVectorAngle)
    .def("GetReciprocalUntiltedLattice",&LatticeTiltGeometry::GetReciprocalUntiltedLattice)
    .def("GetReciprocalTiltedLattice",&LatticeTiltGeometry::GetReciprocalTiltedLattice)
    .def("GetSpatialUntiltedLattice",&LatticeTiltGeometry::GetSpatialUntiltedLattice)
    .def("GetSpatialTiltedLattice",&LatticeTiltGeometry::GetSpatialTiltedLattice)
    .def("GetZStar",&LatticeTiltGeometry::GetZStar)
    .def("CalcReciprocalSampling",&LatticeTiltGeometry::CalcReciprocalSampling)
    .def(self_ns::str(self))
    ;

  def("CalcTiltFromSpatialLattice",CalcTiltFromSpatialLattice);
  def("CalcTiltFromReciprocalLattice",CalcTiltFromReciprocalLattice);

  def("InfoToTiltGeometry",InfoToTiltGeometry);
  def("TiltGeometryToInfo",TiltGeometryToInfo);
  def("InfoToLatticeTiltGeometry",InfoToLatticeTiltGeometry);
  def("LatticeTiltGeometryToInfo",LatticeTiltGeometryToInfo);

  def("InfoToLatticeTiltGeometry",InfoToLatticeTiltGeometry);
  def("LatticeTiltGeometryToInfo",LatticeTiltGeometryToInfo);

  def("CalcTiltComponents",CalcTiltComponents);
  def("CalcTiltComponentVectors",calc_tilt_component_vectors);

}
