//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
using namespace boost::python;

#include <iplt/lattice.hh>
#include <ost/info/info.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;

namespace {
Vec2    (Lattice::*CalcComponents1 )(const geom::Vec2&) const  = &Lattice::CalcComponents;
Vec2    (Lattice::*CalcComponents2 )(const ost::img::Point&) const  = &Lattice::CalcComponents;
Vec2    (Lattice::*CalcPosition1 )(const geom::Vec2&) const  = &Lattice::CalcPosition;
Vec2    (Lattice::*CalcPosition2 )(const ost::img::Point&) const  = &Lattice::CalcPosition;
Real    (Lattice::*DistanceTo1 )(const geom::Vec2&) const  = &Lattice::DistanceTo;
Real    (Lattice::*DistanceTo2 )(const ost::img::Point&) const  = &Lattice::DistanceTo;
}//ns
void export_lattice()
{
  // lattice
  class_<Lattice>("Lattice",init<>())
    .def(init<const geom::Vec2&, const geom::Vec2&, optional<const geom::Vec2&> >())
    .def(init<const geom::Vec2&, const geom::Vec2&, const geom::Vec2&, Real, Real>())
    .def(init<const geom::Vec2&,const ost::img::Point&, const geom::Vec2&,const ost::img::Point&, const geom::Vec2&,const ost::img::Point&, optional<Real,Real> >())
    .def("SetFirst",&Lattice::SetFirst)
    .def("GetFirst",&Lattice::GetFirst,
	 return_value_policy<copy_const_reference>()) 
    .def("SetSecond",&Lattice::SetSecond)
    .def("GetSecond",&Lattice::GetSecond,
	 return_value_policy<copy_const_reference>())
    .def("SetOffset",&Lattice::SetOffset)
    .def("GetOffset",&Lattice::GetOffset,
	 return_value_policy<copy_const_reference>())
    .def("CalcPosition",CalcPosition1)
    .def("CalcPosition",CalcPosition2)
    .def("CalcComponents",CalcComponents1)
    .def("CalcComponents",CalcComponents2)
    .def("DistanceTo",DistanceTo1)
    .def("DistanceTo",DistanceTo2)
    .def("SetBarrelDistortion",&Lattice::SetBarrelDistortion)
    .def("GetBarrelDistortion",&Lattice::GetBarrelDistortion)
    .def("SetSpiralDistortion",&Lattice::SetSpiralDistortion)
    .def("GetSpiralDistortion",&Lattice::GetSpiralDistortion)
    .def(self_ns::str(self))
    ;

  class_<LatticeList >("LatticeList", init<>() )
    .def(vector_indexing_suite<LatticeList >())
    ;
  def("InvertLattice",InvertLattice);

  def("LatticeToInfo",LatticeToInfo);
  def("InfoToLattice",InfoToLattice);

  def("Predict",Predict);
}
