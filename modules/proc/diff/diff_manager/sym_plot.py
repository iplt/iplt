#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtGui import *
from PyQt4.QtCore import *
from ost.img import *
import ost.gui
from math import pi,sqrt
import iplt,iplt.gui

def calc_iq(iobs,back):
  if iobs <= 0.0:
    return 9
  phase_error = 180.0/pi*abs(back)/iobs
  iq = 1.0 + (phase_error/7.0);
  if iq > 8.0:
    return 8
  else:
    return int(iq)
    
class SymPlot:
  def __init__(self,reflist):
    colors=[QColor(0,0,255),QColor(255,0,0),QColor(255,0,255),QColor(0,255,0)]
    self.overlays_=[]
    for j in range(4):
      self.overlays_.append(ost.gui.PointlistOverlay())
      self.overlays_[-1].SetSymbolSize(2)
      self.overlays_[-1].SetActiveColor(colors[j])
      self.overlays_[-1].SetPassiveColor(colors[j])
    lattice=reflist.GetLattice()
    for rp in reflist:
      iq=calc_iq(rp.Get('iobs'),rp.Get('bg'))
      if iq<1 or iq>3:
        continue 
      iqsize=int((6.0*(3-iq)+2.0)/2.0)
      idx=rp.GetIndex().AsDuplet()
      rp2=reflist.FindFirst(Point(-idx[0],-idx[1]))
      if rp2.IsValid():
        pos=Point(lattice.CalcPosition(rp.GetIndex().AsDuplet()))
        average=(rp.Get('iobs')+rp2.Get('iobs'))/2.0
        diff=abs(rp.Get('iobs')-rp2.Get('iobs'))
        if average<0:
          self.overlays_[0].Add(pos,float(iqsize))
        elif diff>average:
          self.overlays_[1].Add(pos,float(iqsize))
        elif diff>average/2.0:
          self.overlays_[2].Add(pos,float(iqsize))
        else:
          self.overlays_[3].Add(pos,float(iqsize))

  def AddOverlays(self,viewer):    
    for j in range(4):
      viewer.AddOverlay(self.overlays_[j])
