#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtCore import pyqtSlot
from parameter_form_base import ParameterFormBase
from iplt.proc.diff.diff_manager.unit_cell_form_ui import Ui_UnitCellForm
from ost import info
import iplt as ex

class UnitCellForm(ParameterFormBase,Ui_UnitCellForm):
  def __init__(self,parent=None):
    ParameterFormBase.__init__(self,parent)
    self.setupUi(self)
    self.items={'a':'DiffProcessing/OrigUnitCell/unitcell/a',
                'b':'DiffProcessing/OrigUnitCell/unitcell/b',
                'gamma':'DiffProcessing/OrigUnitCell/unitcell/gamma',
                'thickness':'DiffProcessing/OrigUnitCell/unitcell/thickness',
                'symmetry':'DiffProcessing/OrigUnitCell/unitcell/symmetry'}
 
  def UpdateValues(self):
    self.BlockChildren(True)
    sym_list=ex.GetImplementedSymmetries()
    info_symmetry=ex.Symmetry(self.info_.Root().GetItem(self.items['symmetry']).GetValue())
    for sid in sym_list:
      sym=ex.Symmetry(sid)
      symn=sym.GetSpacegroupName()
      self.space_group.addItem(symn)
      if sym.GetSpacegroupNumber()==info_symmetry.GetSpacegroupNumber():
        act=self.space_group.count()-1
    self.space_group.setCurrentIndex(act)
    self.a.setValue(self.info_.Root().GetItem(self.items['a']).AsFloat())
    self.b.setValue(self.info_.Root().GetItem(self.items['b']).AsFloat())
    self.gamma.setValue(self.info_.Root().GetItem(self.items['gamma']).AsFloat())
    self.thickness.setValue(self.info_.Root().GetItem(self.items['thickness']).AsFloat())
    self.BlockChildren(False)

  @pyqtSlot(str)  
  def on_space_group_currentIndexChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['symmetry'],False).SetValue(str(val))
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_a_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['a'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_b_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['b'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_gamma_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['gamma'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_thickness_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['thickness'],False).SetFloat(val)
    self.infoChanged.emit()

  def ResetToDefaults(self):
    self.info_.Root().Remove(self.items['a'])
    self.info_.Root().Remove(self.items['b'])
    self.info_.Root().Remove(self.items['gamma'])
    self.info_.Root().Remove(self.items['thickness'])
    self.BlockChildren(True)
    sym_list=ex.GetImplementedSymmetries()
    info_symmetry=ex.Symmetry(self.info_.GetDefaultItem(self.items['symmetry']).GetValue())
    for sid in sym_list:
      sym=ex.Symmetry(sid)
      symn=sym.GetSpacegroupName()
      self.space_group.addItem(symn)
      if sym.GetSpacegroupNumber()==info_symmetry.GetSpacegroupNumber():
        act=self.space_group.count()-1
    self.space_group.setCurrentIndex(act)
    self.a.setValue(self.info_.Root().GetDefaultItem(self.items['a']).AsFloat())
    self.b.setValue(self.info_.Root().GetDefaultItem(self.items['b']).AsFloat())
    self.gamma.setValue(self.info_.Root().GetDefaultItem(self.items['gamma']).AsFloat())
    self.thickness.setValue(self.info_.Root().GetDefaultItem(self.items['thickness']).AsFloat())
    self.BlockChildren(False)

