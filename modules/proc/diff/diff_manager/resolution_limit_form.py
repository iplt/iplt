#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtCore import pyqtSlot

from parameter_form_base import ParameterFormBase
from iplt.proc.diff.diff_manager.resolution_limit_form_ui import Ui_ResolutionLimitForm

class ResolutionLimitForm(ParameterFormBase,Ui_ResolutionLimitForm):
  def __init__(self,parent=None):
    ParameterFormBase.__init__(self,parent)
    self.setupUi(self)
    self.items={'rlow':'DiffProcessing/ResLim/rlow',
                'rhigh':'DiffProcessing/ResLim/rhigh'}
  
  def UpdateValues(self):
    self.BlockChildren(True)
    self.rlow.setValue(self.info_.Root().GetItem(self.items['rlow']).AsFloat())
    self.rhigh.setValue(self.info_.Root().GetItem(self.items['rhigh']).AsFloat())
    self.BlockChildren(False)
    
  @pyqtSlot(float)  
  def on_rlow_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['rlow'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_rhigh_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['rhigh'],False).SetFloat(val)
    self.infoChanged.emit()

  def ResetToDefaults(self):
    self.info_.Root().Remove(self.items['rlow'])
    self.info_.Root().Remove(self.items['rhigh'])
    self.BlockChildren(True)
    self.rlow.setValue(self.info_.GetDefaultItem(self.items['rlow']).AsFloat())
    self.rhigh.setValue(self.info_.GetDefaultItem(self.items['rhigh']).AsFloat())
    self.BlockChildren(False)
    self.infoChanged.emit()
    
