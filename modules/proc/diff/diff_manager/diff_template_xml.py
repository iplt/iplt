#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


template_xml="""<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<EMDataInfo xmlns:iplt="http://iplt.org">
<DiffProcessing>
  <!-- The mask for the directory names - mandatory -->
  <ImageMask value="%(prefix)s"/>

  <!-- the mask for the file name (default is %%%%.tif) - the %%%% pattern will be replace by the current dataset name, as assembled from the ImageMask entry -->
  <ImageFileMask value="%%%%.%(fileformat)s" type="string"/>

  
  <!-- Parameters for the diff_mask step. NOTE: the beamstop mask itself is defined further below -->
  <FindMask>
    <!-- if set to true, reduce size by factor of 2 to speed up calculation -->
    <PreShrink value="0" type="bool"/>
    <!-- if set to true, use only center 50%%, reducing image by (another) factor of 2 -->
    <PreCut value="0" type="bool"/>
    <!-- if present, clips the image to this minimum value prior to running the algorithms -->    
    <MinCutoff value="0.0" type="float"/>
    <!-- 
      Beamstop Mask Detection Algorithm:
      Mode 0 (default) uses a histogram analysis to determine a min max clipping window, which 
      is then used in the cross-correlation with the artificial beamstop image (useful in most cases)
      Mode 1 applies a simply gaussian filter prior to running the CC
      Mode 2 uses local sigma thresholding to extract the beamstop shape (required when strong falloff
      of pixel values across beamstop)
    -->
    <Mode value="0" type="int"/>
    <!-- tweak factor for mode 0, default is 1.2 -->
    <HistoMultiplier value="1.2" type="float"/>
    <!-- tweak factor for mode 2, default is 4 -->
    <LocalSigmaSize value="4" type="int"/>
    <!-- tweak factor for mode 2, default is 8.0 -->
    <LocalSigmaLevel value="8.0" type="float"/>
  </FindMask>

  <!-- Parameters for the 'iplt_diff search' step -->
  <LatticeSearch>
    <!-- apply a gaussian filter to the diff pattern prior to peak search - set to 0.0 to turn this feature off -->
    <GaussFilterStrength value="1.0" type="float"/>
    <!-- minimal and maximal lattice vectors (in pixel) to consider, especially the min value is sometimes needed -->
    <MinimalLatticeLength value="30.0" type="float"/>
    <MaximalLatticeLength value="1.0e4" type="float"/>
    <PeakSearchParams>
      <!-- see the PeakSearch ctor for a description of these parameters -->
      <OuterWindowSize value="10" type="int"/>
      <OuterWindowSensitivity value="0.2" type="float"/>
      <InnerWindowSize value="4" type="int"/>
      <InnerWindowSensitivity value="0.0" type="float"/>
      <InnerWindowCountLimit value="4" type="int"/>
      <InnerWindowCountSensitivity value="0.5" type="float"/>
    </PeakSearchParams>
  </LatticeSearch>

  <!-- Parameters for the extraction and integration of intensity profiles, i.e. 'iplt_diff extract' and 'iplt_diff integrate' -->
  <LatticeExtract>
    <!-- half-box size, i.e. total integration box will be 2n+1 pixels large -->
    <BoxSize value="7" type="int"/>
    <!-- if this is greater than zero, a box grow algorithm will be employed to find the optimal box size per peak integration -->
    <BoxGrowAmount value="0" type="int"/>
    <!-- size of the background rim, values of 1 or 2 are best -->
    <BackgroundRimSize value="2" type="int"/>
    <!-- flag to turn on the local background correction based on the gaussian fit - recommended -->
    <BackgroundSubtract value="1" type="bool"/>
    <!-- flag to turn on the fit of the spiral and barrel distortion during lattice refinement - not recommended for good datasets-->
    <RefineLatticeDistortions value="0" type="bool"/>
   <CenterMaskRadius type="int" value="0" />
   <PeripheralMaskRadius type="int" value="1024" />
   <ReRefineLattice type="bool" value="0" />
   <!-- Optimize the box size within a given range during peak integration -->
   <OptimizeBoxSize type="bool" value="1" />
   <!-- Range for the box size optimization -->
   <OptimizeBoxSizeRange value="5" type="int"/>
  </LatticeExtract>

  <!-- strong filter for the reflections that are used in the lattice refinement -->
  <StrongFilter>
    <MinimumQuality value="2" type="float"/>
    <MaximumOffset value="2" type="float"/>
    <MaximumHalfWidth value="7.0" type="float"/>
    <MaximumElipticalRatio value="2" type="float"/>
  </StrongFilter>

  <!-- weak filter for the reflections that are subjected to integration -->
  <WeakFilter>
    <MinimumQuality value="1.2" type="float"/>
    <MaximumOffset value="5" type="float"/>
  </WeakFilter>

  <!-- Parameters for 'iplt_diff sym' -->
  <Symmetrize>
    <!-- 0 = do not merge Friedel mates; 1 = merge Friedel mates, keep singular reflections; 2 = merge Friedel mates, throw out singular ones; 3 = MRC style Friedel merging -->
    <FriedelMode value="0" type="int"/>
  </Symmetrize>

  <!-- unit cell parameters and symmetry -->
  <OrigUnitCell>
   <unitcell type="spatial">
    <a value="%(ucell_a)f" type="float"/>
    <b value="%(ucell_b)f" type="float"/>
    <gamma value="%(ucell_g)f" type="float"/>
    <thickness value="%(ucell_c)f" type="float"/>
    <symmetry value="%(spacegroup)s"/>
   </unitcell>
  </OrigUnitCell>

  <!-- pixel sampling of the images in reciprocal angstrom - not really needed -->
  <Sampling value="4.24e-4" type="float"/>

  <!-- global resolution limit of the processing, for individual iplt_diff steps as well as iplt_diff_merge -->
  <ResLim>
   <rlow value="100" type="float"/>
   <rhigh value="2" type="float"/>
  </ResLim>

  <!-- Parameters for the scaling -->
  <Scale>
    <!-- resolution limits -->
    <Resolution>
      <low value="40" type="float"/>
      <high value="2.5" type="float"/>
    </Resolution>
    <!-- number of bins for the weighted bin scaling -->
    <BinCount value="16" type="int"/>
    <!-- cutoffs for the filtering step after scaling, based on the weighted bins -->
    <WBinISigICutoff value="0.1" type="float"/>
    <WBinSigmaCutoff value="10.0" type="float"/>
    <!-- minimum tilt angle (in degree) for which to invoke an anisotropic pre-scaling-->
    <AnisoMinTilt value="90" type="float"/>
  </Scale>

  <!-- Parameters for the lattice line fitting -->
  <LLFit>
    <!-- experimental weighting algorithms, use mode 1 and cutoff 0.0 for all practical purposes -->
    <Weighting>
      <T1 value="10.0" type="float"/>
      <T2 value="40.0" type="float"/>
      <Mode value="1" type="int"/>
      <Cutoff value="0.0" type="float"/>
    </Weighting>
    <!-- iteration limit for the internal non-linear fitting routine -->
    <MaxIterations value="1000" type="int"/>
    <!-- iteration precision for the internal non-linear fitting routine -->
    <IterationPrecision value="1e-2" type="float"/>
    <!-- bootstrapping mode for improved sigma generation, recommended -->
    <BootstrapFlag value="1" type="bool"/>
    <BootstrapIterations value="20" type="int"/>
    <!-- initial guess mode prior to fitting; 0=from weighted bins, 1=random phase, unit amplitude, 2=totally random-->
    <GuessMode value="0" type="int"/>
    <!-- multiple iterations with different starting values, not as powerful as bootstrapping, best kept at 1 -->
    <Iterations value="1" type="int"/>
    <!-- phantom mode includes two additional values at +- sigma z* -->
    <PhantomMode value="0" type="bool"/>
    <Algorithm value="0" type="int"/>
  </LLFit>

  <!-- Parameters for the post llfit refinement -->
  <Refine>
    <!-- lattice line curve fitting mode 0 (based on Imax and z*max) or 1 (bases on sigmaI and sigmaz*) -->
    <CurveFitMode value="0" type="int"/>
    <!-- zstar fudge factor, experimental -->
    <ZWeight value="8.0" type="float"/>
    <!-- flag to turn intensity scale refinement on or off -->
    <RefineScale value="1" type="bool"/>
    <!-- flag to turn tilt geometry refinement on or off -->
    <RefineTilt value="1" type="bool"/>
    <!-- resolution limits to determine new tilt geometry -->
    <RefineTiltRLow value="10" type="float"/>
    <RefineTiltRHigh value="4" type="float"/>     
  </Refine>


  <!-- This will be created automatically by the 'giplt_diff edit_mask' command. Remove the tag if not needed -->
  <BeamStopMask>
  </BeamStopMask>

  </DiffProcessing>
</EMDataInfo>
"""
