#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtCore import pyqtSlot

from parameter_form_base import ParameterFormBase
from iplt.proc.diff.diff_manager.llfit_form_ui import Ui_LLFitForm

class LLFitForm(ParameterFormBase,Ui_LLFitForm):
  def __init__(self,parent=None):
    ParameterFormBase.__init__(self,parent)
    self.setupUi(self)
    self.items={'enable_bootstrapping':'DiffProcessing/LLFit/BootstrapFlag',
                'bootstrapping_iterations':'DiffProcessing/LLFit/BootstrapIterations',
                'fit_iterations':'DiffProcessing/LLFit/MaxIterations',
                'fit_precision':'DiffProcessing/LLFit/IterationPrecision',
                'guess_mode':'DiffProcessing/LLFit/GuessMode',
                'fit_algorithm':'DiffProcessing/LLFit/Algorithm'}
  
  def UpdateValues(self):
    self.BlockChildren(True)
    self.enable_bootstrapping.setChecked(self.info_.Root().GetItem(self.items['enable_bootstrapping']).AsBool())
    self.bootstrapping_iterations.setValue(self.info_.Root().GetItem(self.items['bootstrapping_iterations']).AsInt())
    self.fit_iterations.setValue(self.info_.Root().GetItem(self.items['fit_iterations']).AsInt())
    self.fit_precision.setValue(self.info_.Root().GetItem(self.items['fit_precision']).AsFloat())
    guess_mode=self.info_.Root().GetItem(self.items['guess_mode']).AsInt()
    self.mode0.setChecked(guess_mode==0)
    self.mode1.setChecked(guess_mode==1)
    self.mode2.setChecked(guess_mode==2)
    fit_algorithm=self.info_.Root().GetItem(self.items['fit_algorithm']).AsInt()
    self.alg0.setChecked(fit_algorithm==0)
    self.alg1.setChecked(fit_algorithm==1)
    self.BlockChildren(False)
    
  @pyqtSlot(bool)  
  def on_enable_bootstrapping_toggled(self,val):
    self.info_.Root().RetrieveItem(self.items['enable_bootstrapping'],False).SetBool(val)
    self.infoChanged.emit()

  @pyqtSlot(int)  
  def on_bootstrapping_iterations_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['bootstrapping_iterations'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(int)  
  def on_fit_iterations_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['fit_iterations'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_fit_precision_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['fit_precision'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(bool)  
  def on_mode0_toggled(self,val):
    if val:
      self.info_.Root().RetrieveItem(self.items['guess_mode'],False).SetInt(0)
      self.infoChanged.emit()
  @pyqtSlot(bool)  
  def on_mode1_toggled(self,val):
    if val:
      self.info_.Root().RetrieveItem(self.items['guess_mode'],False).SetInt(1)
      self.infoChanged.emit()
  @pyqtSlot(bool)  
  def on_mode2_toggled(self,val):
    if val:
      self.info_.Root().RetrieveItem(self.items['guess_mode'],False).SetInt(2)
      self.infoChanged.emit()

  @pyqtSlot(bool)  
  def on_alg0_toggled(self,val):
    if val:
      self.info_.Root().RetrieveItem(self.items['fit_algorithm'],False).SetInt(0)
      self.infoChanged.emit()
  @pyqtSlot(bool)  
  def on_alg1_toggled(self,val):
    if val:
      self.info_.Root().RetrieveItem(self.items['fit_algorithm'],False).SetInt(1)
      self.infoChanged.emit()


  def ResetToDefaults(self):
    self.info_.Root().Remove(self.items['enable_bootstrapping'])
    self.info_.Root().Remove(self.items['bootstrapping_iterations'])
    self.info_.Root().Remove(self.items['fit_iterations'])
    self.info_.Root().Remove(self.items['fit_precision'])
    self.info_.Root().Remove(self.items['guess_mode'])
    self.info_.Root().Remove(self.items['fit_algorithm'])
    self.BlockChildren(True)
    self.enable_bootstrapping.setChecked(self.info_.GetDefaultItem(self.items['enable_bootstrapping']).AsBool())
    self.bootstrapping_iterations.setValue(self.info_.GetDefaultItem(self.items['bootstrapping_iterations']).AsInt())
    self.fit_iterations.setValue(self.info_.GetDefaultItem(self.items['fit_iterations']).AsInt())
    self.fit_precision.setValue(self.info_.GetDefaultItem(self.items['fit_precision']).AsFloat())
    guess_mode=self.info_.GetDefaultItem(self.items['guess_mode']).AsInt()
    self.mode0.setChecked(guess_mode==0)
    self.mode1.setChecked(guess_mode==1)
    self.mode2.setChecked(guess_mode==2)
    fit_algorithm=self.info_.GetDefaultItem(self.items['fit_algorithm']).AsInt()
    self.alg0.setChecked(fit_algorithm==0)
    self.alg1.setChecked(fit_algorithm==1)
    self.BlockChildren(False)
    
