#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import os
from PyQt4.QtGui import *
import iplt,iplt.gui
import ost.io
import ost.gui

class TextViewer(QWidget):
  def __init__(self,path,text,parent=None):
    QMainWindow.__init__(self,parent)
    vb=QVBoxLayout()
    textedit=QPlainTextEdit(self)
    textedit.setPlainText(text)
    textedit.setReadOnly(True)
    vb.addWidget(textedit)
    self.setLayout(vb)
    app=ost.gui.GostyApp.Instance()
    app.perspective.main_area.AddWidget(path,self)
    self.show()

class ResultBrowser(QListWidget):
  def __init__(self,parent=None):
    QListWidget.__init__(self,parent)
    self.files_={}
    self.itemDoubleClicked.connect(self.OnItemDoubleClicked)

  def AddFile(self, path, name=None):
    if name==None:
      self.files_[path]=path
      if os.path.isfile(path):
        self.addItem(path)
    else:
      self.files_[name]=path
      if os.path.isfile(path):
        self.addItem(name)
  
  def OnItemDoubleClicked(self,item):
    path=self.files_[str(item.text())]
    if path.endswith(('.txt','.log')):
      f=open(path,'r')
      text=f.read()
      f.close()
      self.tv_=TextViewer(path,text)
    elif path.endswith(('.mrc','.tif','.png')):
      self.filehandle_=ost.io.LoadImage(path)
      iplt.gui.Viewer(self.filehandle_)
    else:
      errordialog=QErrorMessage(self)
      errordialog.showMessage("unknown file format")
      
  def ClearFiles(self):
    self.files_.clear()
    self.clear()
  
  def Update(self):
    self.clear()
    for key in self.files_.keys():
      if(os.path.isfile(self.files_[key])):
        self.addItem(key)
