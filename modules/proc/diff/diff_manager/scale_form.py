#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtCore import pyqtSlot

from parameter_form_base import ParameterFormBase
from iplt.proc.diff.diff_manager.scale_form_ui import Ui_ScaleForm

class ScaleForm(ParameterFormBase,Ui_ScaleForm):
  def __init__(self,parent=None):
    ParameterFormBase.__init__(self,parent)
    self.setupUi(self)
    self.items={'rlow':'DiffProcessing/Scale/Resolution/low',
                'rhigh':'DiffProcessing/Scale/Resolution/high',
                'nbins':'DiffProcessing/Scale/BinCount'}
  
  def UpdateValues(self):
    self.BlockChildren(True)
    self.rlow.setValue(self.info_.Root().GetItem(self.items['rlow']).AsFloat())
    self.rhigh.setValue(self.info_.Root().GetItem(self.items['rhigh']).AsFloat())
    self.nbins.setValue(self.info_.Root().GetItem(self.items['nbins']).AsInt())
    self.BlockChildren(False)
    
  @pyqtSlot(float)  
  def on_rlow_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['rlow'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_rhigh_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['rhigh'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_nbins_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['nbins'],False).SetInt(val)
    self.infoChanged.emit()

  def ResetToDefaults(self):
    self.info_.Root().Remove(self.items['rlow'])
    self.info_.Root().Remove(self.items['rhigh'])
    self.info_.Root().Remove(self.items['nbins'])
    self.BlockChildren(True)
    self.rlow.setValue(self.info_.GetDefaultItem(self.items['rlow']).AsFloat())
    self.rhigh.setValue(self.info_.GetDefaultItem(self.items['rhigh']).AsFloat())
    self.rhigh.setValue(self.info_.GetDefaultItem(self.items['nbins']).AsInt())
    self.BlockChildren(False)
    
