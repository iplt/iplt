#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtCore import *
from PyQt4.QtGui import *
import os

class ImageTracker(QWidget):
    def __init__(self,im_list,parent=None):
        QWidget.__init__(self,parent)
        self.im_list_=im_list
        self.current_=-1
        vb=QVBoxLayout()
        self.nb_=QPushButton("Next Pattern")
        QObject.connect(self.nb_,SIGNAL("clicked()"),self.OnNext)
        vb.addWidget(self.nb_)
        self.pb_=QPushButton("Previous Pattern")
        QObject.connect(self.pb_,SIGNAL("clicked()"),self.OnPrev)
        vb.addWidget(self.pb_)
        self.jb_=QPushButton("Jump to Pattern")
        QObject.connect(self.jb_,SIGNAL("clicked()"),self.OnJump)
        vb.addWidget(self.jb_)
        self.setLayout(vb)
    def OnNext(self):
        self.current_+=1
        self.check_vis()
        if self.current_>=len(self.im_list_):
            self.current_=len(self.im_list_)-1
            return
        self.emit(SIGNAL("LoadImage"),os.getcwd(),self.im_list_[self.current_])

    def OnPrev(self):
        self.current_-=1
        self.check_vis()
        if self.current_<0:
            self.current_=0
            return
        self.emit(SIGNAL("LoadImage"),os.getcwd(),self.im_list_[self.current_])

    def OnJump(self):
        (name,ok)=QInputDialog.getText(self,"Jump to Image","Image Name:")
        if not ok:
          return
        self.DoJump(name)

    def DoJump(self,name):
        i=0
        for imname in self.im_list_:
            if name == imname:
                self.current_=i
                self.check_vis()
                print self.im_list_[self.current_]
                self.emit(SIGNAL("LoadImage"),os.getcwd(),self.im_list_[self.current_])
                return 
            i+=1
        msg_box=QMessageBox()
        msg_box.setText("The image %s could not be found." % (name) )
        msg_box.exec_()
            
    def check_vis(self):
        if self.current_+1>=len(self.im_list_):
            self.nb_.setDisabled(True)
        else:
            self.nb_.setEnabled(True)
        if self.current_<=0:
            self.pb_.setDisabled(True)
        else:
            self.pb_.setEnabled(True)
            
