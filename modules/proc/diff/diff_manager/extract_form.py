#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtGui import *
from PyQt4.QtCore import *
from parameter_form_base import ParameterFormBase
from iplt.proc.diff.diff_manager.extract_form_ui import Ui_ExtractForm

class ExtractForm(ParameterFormBase,Ui_ExtractForm):
  def __init__(self,parent=None):
    ParameterFormBase.__init__(self,parent)
    self.setupUi(self)
    self.items={'box_size':'DiffProcessing/LatticeExtract/BoxSize',
                'grow_size':'DiffProcessing/LatticeExtract/BoxGrowAmount',
                'rim_size':'DiffProcessing/LatticeExtract/BackgroundRimSize',
                'center_radius':'DiffProcessing/LatticeExtract/CenterMaskRadius',
                'peripheral_radius':'DiffProcessing/LatticeExtract/PeripheralMaskRadius',
                'refine_distortions':'DiffProcessing/LatticeExtract/RefineLatticeDistortions',
                'rerefine':'DiffProcessing/LatticeExtract/ReRefineLattice',
                'optimize_box_size':'DiffProcessing/LatticeExtract/OptimizeBoxSize',
                'optimize_box_size_range':'DiffProcessing/LatticeExtract/OptimizeBoxSizeRange',}
  
  def UpdateValues(self):
    self.BlockChildren(True)
    self.box_size.setValue(self.info_.Root().GetItem(self.items['box_size']).AsInt())
    self.grow_size.setValue(self.info_.Root().GetItem(self.items['grow_size']).AsInt())
    self.rim_size.setValue(self.info_.Root().GetItem(self.items['rim_size']).AsInt())
    self.center_radius.setValue(self.info_.Root().GetItem(self.items['center_radius']).AsInt())
    self.peripheral_radius.setValue(self.info_.Root().GetItem(self.items['peripheral_radius']).AsInt())
    self.refine_distortions.setChecked(self.info_.Root().GetItem(self.items['refine_distortions']).AsBool())
    self.rerefine.setChecked(self.info_.Root().GetItem(self.items['rerefine']).AsBool())
    self.optimize_box_size.setChecked(self.info_.Root().GetItem(self.items['optimize_box_size']).AsBool())
    self.optimize_range.setValue(self.info_.Root().GetItem(self.items['optimize_box_size_range']).AsInt())
    self.BlockChildren(False)
    
  @pyqtSlot(int)  
  def on_box_size_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['box_size'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(int)  
  def on_grow_size_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['grow_size'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(int)  
  def on_rim_size_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['rim_size'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(int)  
  def on_center_radius_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['center_radius'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_peripheral_radius_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['peripheral_radius'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(bool)  
  def on_refine_distortions_toggled(self,val):
    self.info_.Root().RetrieveItem(self.items['refine_distortions'],False).SetBool(val)
    self.infoChanged.emit()

  @pyqtSlot(bool)  
  def on_rerefine_toggled(self,val):
    self.info_.Root().RetrieveItem(self.items['rerefine'],False).SetBool(val)
    self.infoChanged.emit()

  @pyqtSlot(bool)
  def on_optimize_box_size_toggled(self,val):
    self.info_.Root().RetrieveItem(self.items['optimize_box_size'],False).SetBool(val)
    self.infoChanged.emit()

  @pyqtSlot(int)
  def optimize_range_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['optimize_box_size_range'],False).SetInt(val)
    self.infoChanged.emit()

  def ResetToDefaults(self):
    self.info_.Root().Remove(self.items['box_size'])
    self.info_.Root().Remove(self.items['grow_size'])
    self.info_.Root().Remove(self.items['rim_size'])
    self.info_.Root().Remove(self.items['center_radius'])
    self.info_.Root().Remove(self.items['peripheral_radius'])
    self.info_.Root().Remove(self.items['refine_distortions'])
    self.info_.Root().Remove(self.items['rerefine'])
    self.info_.Root().Remove(self.items['optimize_box_size'])
    self.info_.Root().Remove(self.items['optimize_box_size_range'])
    self.BlockChildren(True)
    self.box_size.setValue(self.info_.GetDefaultItem(self.items['box_size']).AsInt())
    self.grow_size.setValue(self.info_.GetDefaultItem(self.items['grow_size']).AsInt())
    self.rim_size.setValue(self.info_.GetDefaultItem(self.items['rim_size']).AsInt())
    self.center_radius.setValue(self.info_.GetDefaultItem(self.items['center_radius']).AsInt())
    self.peripheral_radius.setValue(self.info_.GetDefaultItem(self.items['peripheral_radius']).AsInt())
    self.refine_distortions.setChecked(self.info_.GetDefaultItem(self.items['refine_distortions']).AsBool())
    self.rerefine.setChecked(self.info_.GetDefaultItem(self.items['rerefine']).AsBool())
    self.rerefine.setChecked(self.info_.GetDefaultItem(self.items['optimize_box_size']).AsBool())
    self.rerefine.setChecked(self.info_.GetDefaultItem(self.items['optimize_box_size_range']).AsInt())
    self.BlockChildren(False)
    
