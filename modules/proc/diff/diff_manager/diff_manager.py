#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ost.img import *
import sip
from ost import PushVerbosityLevel,PopVerbosityLevel,PushLogSink,PopLogSink,FileLogSink

from diff_manager_form_ui import Ui_DiffManager
from ost import info
from iplt.proc.diff.diff_init import *
from iplt.proc.diff.diff_find_mask import *
from iplt.proc.diff.diff_ori import *
from iplt.proc.diff.diff_search import *
from iplt.proc.diff.diff_extract import *
from iplt.proc.diff.diff_integrate import *
from iplt.proc.diff.diff_tilt import *
from iplt.proc.diff.dm.verify_lattice import *
from iplt.proc.diff.dm.edit_mask import *
from image_tracker import ImageTracker
from sym_plot import SymPlot
from iplt.proc.manager.welcome import *
from diff_template_xml import *
from iplt.proc.manager.new_image_wizard import *
import string
import shutil
from iplt.proc.diff.diff_merge import *
import iplt.alg
import iplt
import iplt.proc.llviewer as llv
from iplt.proc.diff.dm.wbin_scaler import *
from iplt.proc.diff.dm.view_se import *
from info_update_from_template import *
from ost import LogVerbose,LogInfo,LogError

#dummy options class for use in the DiffIterativeSubcommand subclasses
class Options:
  pass

  
class CloseEventFilter(QObject):
  def __init__(self,manager):
    QObject.__init__(self)
    self.manager_=manager
  def eventFilter(self, obj, event):
    if event.type()==QEvent.Close:
      self.manager_.ImageUpdate()
      self.manager_.setEnabled(True)
    return False
      
  
class DiffManager(QWidget, Ui_DiffManager):
  def __init__(self, parent = None):
    QMainWindow.__init__(self, parent)
    self.current_image_=""
    self.current_merge_=""
    self.setupUi(self)
    if not os.path.isfile("project.xml"):
      welcomedialog=WelcomeDialog(self,template_xml)
      if not welcomedialog.exec_():
        sys.exit(-1)
    self.project_info_=info.LoadInfo('project.xml')
    template_info=info.CreateInfo(template_xml)
    UpdateInfoFromTemplate(self.project_info_,template_info)
    self.project_info_modified_=True
    self.image_info_=info.CreateInfo()
    self.image_info_.AddDefault(self.project_info_)
    self.image_info_modified_=False
    self.merge_info_modified_=False

    self.dirmask_ = GetStringInfoItem(self.project_info_.Root(),"DiffProcessing/ImageMask","")
    self.filemask_ = GetStringInfoItem(self.project_info_.Root(),"DiffProcessing/ImageFileMask","%%.tif")
    self.image_table_.SetDirMask(self.dirmask_)
    self.SetupProjectForm(self.unit_cell_form_)
    self.SetupProjectForm(self.resolution_limits_form_)
    self.SetupProjectForm(self.search_form_)
    self.SetupProjectForm(self.extract_form_)
    self.SetupProjectForm(self.scale_form_)
    self.SetupProjectForm(self.llfit_form_)

    self.SetupImageForm(self.image_resolution_limits_form_)
    self.SetupImageForm(self.image_search_form_)
    self.SetupImageForm(self.image_extract_form_)
    
    self.SetupMergeForm(self.merge_scale_form_)
    self.SetupMergeForm(self.merge_llfit_form_)
    self.SetupMergeForm(self.merge_resolution_limits_form_)
    self.merge_table_.CreateMergeList()
    self.ImageUpdate()
    QCoreApplication.instance().aboutToQuit.connect(self.OnApplicationQuit)
    
    #load local extension to diff manager
    try:
      import diff_manager_extension
      diff_manager_extension.init(self)
    except ImportError:
      pass
      
  def SetupProjectForm(self,form):
    form.SetInfo(self.project_info_)
    form.infoChanged.connect(self.onProjectInfoChanged)

  def SetupImageForm(self,form):
    form.infoChanged.connect(self.onImageInfoChanged)
    
  def SetupMergeForm(self,form):
    form.infoChanged.connect(self.onMergeInfoChanged)
    
    
  def DisableUntilClosed(self, viewer):
    self.setEnabled(False)
    self.close_event_filter_=CloseEventFilter(self)
    viewer.qobject.installEventFilter(self.close_event_filter_)
    
  def run_step_(self,step,options,wdir,check_up_to_date=True):
      step.SetWorkingDirectory([os.getcwd(),wdir])
      PushLogSink(FileLogSink(step.GetLogfile()))
      step.PreprocessDirectory(options)
      if step.CheckPreCondition(options):
        if (not step.UpToDate(options)) or (not check_up_to_date):
          step.ProcessDirectory(options)
          step.PostprocessDirectory(options)
      os.chdir('..')
      PopLogSink()
    
  @pyqtSlot()
  def on_project_add_clicked(self):
    imagewizard=NewImageWizard(self.dirmask_,4,string.replace(self.filemask_,"%%.",""))
    if imagewizard.exec_():
      filelist=imagewizard.GetFileList()
      Progress().Register(self,len(filelist))
      for (filename,newdirname,newfilename,convert) in filelist:
        os.mkdir(newdirname)
        if convert:
          itmp=LoadImage(str(filename))
          SaveImage(itmp,os.path.join(str(newdirname),str(newfilename)))
        else:
          shutil.copyfile(str(filename),os.path.join(str(newdirname),str(newfilename)))
        diff_init=DiffInit()
        options=Options()
        options.force_flag=False
        self.run_step_(diff_init,options,str(newdirname),False)
        Progress().AdvanceProgress(self)
      Progress().DeRegister(self)
      self.image_table_.CreateImageList()
      self.ImageUpdate()

    
  @pyqtSlot()
  def on_project_define_beamstop_clicked(self):
    self.ExportInfoFiles()
    imagelist=self.image_table_.GetImageList()
    if len(imagelist)==0:
      msg_box=QMessageBox()
      msg_box.setText("No image found.")
      msg_box.exec_()
      return
    image_tracker=ImageTracker(imagelist)
    self.edit_mask=EditMask(self.project_info_,image_tracker)
    self.DisableUntilClosed(self.edit_mask.v_)
    QObject.connect(image_tracker,SIGNAL("LoadImage"),self.edit_mask.Load)
    image_tracker.DoJump(imagelist[0])
    
  @pyqtSlot()
  def on_project_search_clicked(self):
    self.ExportInfoFiles()
    diff_find_mask=DiffFindMask()
    diff_ori=DiffOri()
    diff_search=DiffSearch()
    options=Options()
    options.skip_mask=False
    options.force_flag=False
    options.force_verified=False
    options.skip_ori=False
    options.fast_ori=False
    imagelist=self.image_table_.GetImageList()
    Progress().Register(self,2*len(imagelist))
    for image in imagelist:
      self.run_step_(diff_find_mask,options,image,True)
      Progress().AdvanceProgress(self)
    for image in imagelist:
      self.run_step_(diff_ori,options,image,True)
      Progress().AdvanceProgress(self)
    for image in imagelist:
      self.run_step_(diff_search,options,image,True)
      Progress().AdvanceProgress(self)
    Progress().DeRegister(self)
    self.ImageUpdate()
    
  @pyqtSlot()
  def on_project_verify_clicked(self):
    self.ExportInfoFiles()
    imagelist=self.image_table_.GetNonVerifiedImageList()
    if len(imagelist)==0:
      msg_box=QMessageBox()
      msg_box.setText("No nonverified lattice present.")
      msg_box.exec_()
      return
    image_tracker=ImageTracker(imagelist)
    self.verify_lattice=VerifyLattice(self.project_info_,image_tracker)
    self.DisableUntilClosed(self.verify_lattice.v_)
    QObject.connect(image_tracker,SIGNAL("LoadImage"),self.verify_lattice.Load)
    image_tracker.DoJump(imagelist[0])
    
  @pyqtSlot()
  def on_project_extract_clicked(self):
    self.ExportInfoFiles()
    diff_extract=DiffExtract()
    diff_integrate=DiffIntegrate()
    diff_tilt=DiffTilt()
    options=Options()
    options.assume_verified=False
    options.force_verified=False
    options.force_flag=False
    options.use_lslat=False
    options.ignore_vtilt=False
    imagelist=self.image_table_.GetImageList()
    Progress().Register(self,3*len(imagelist))
    for image in imagelist:
      self.run_step_(diff_extract,options,image,True)
      Progress().AdvanceProgress(self)
    for image in imagelist:
      self.run_step_(diff_integrate,options,image,True)
      Progress().AdvanceProgress(self)
    for image in imagelist:
      self.run_step_(diff_tilt,options,image,True)
      Progress().AdvanceProgress(self)
    Progress().DeRegister(self)
    self.ImageUpdate()
    
  @pyqtSlot()
  def on_project_merge_clicked(self):
    self.ExportInfoFiles()

    merge_info=os.path.join('merge','info.xml')
    if not os.path.exists('merge'):
      os.mkdir('merge')
    if not os.path.exists(merge_info):
      i=info.CreateInfo()
      i.Export(merge_info)
    args=['all']
    diff_merge=DiffMerge(args)
    diff_merge.Run()
    self.merge_table_.CreateMergeList()
    
  @pyqtSlot()
  def on_image_search_beamstop_clicked(self):
    self.ExportInfoFiles()
    options=Options()
    options.force_flag=False
    options.skip_mask=False
    diff_find_mask=DiffFindMask()
    self.run_step_(diff_find_mask,options,self.current_image_,False)
    self.UpdateCurrentImageData()

  @pyqtSlot()
  def on_image_manual_masks_clicked(self):
    self.ExportInfoFiles()
    self.edit_mask=EditMask(self.project_info_)
    self.DisableUntilClosed(self.edit_mask.v_)
    self.edit_mask.Load(os.getcwd(),self.current_image_)

  @pyqtSlot()
  def on_image_search_origin_clicked(self):
    self.ExportInfoFiles()
    options=Options()
    options.force_flag=False
    options.skip_ori=False
    options.fast_ori=False
    diff_ori=DiffOri()
    self.run_step_(diff_ori,options,self.current_image_,False)
    self.UpdateCurrentImageData()

  @pyqtSlot()
  def on_image_search_lattice_clicked(self):
    self.ExportInfoFiles()
    options=Options()
    options.force_flag=False
    options.skip_ori=False
    options.fast_ori=False
    diff_search=DiffSearch()
    self.run_step_(diff_search,options,self.current_image_,False)
    self.UpdateCurrentImageData()

  @pyqtSlot()
  def on_image_verify_clicked(self):
    self.ExportInfoFiles()
    self.verify_lattice=VerifyLattice(self.project_info_)
    self.DisableUntilClosed(self.verify_lattice.v_)
    self.verify_lattice.Load(os.getcwd(),self.current_image_)

  @pyqtSlot()
  def on_image_display_integrated_clicked(self):
    self.ExportInfoFiles()
    im=CreateImage()
    self.int_viewer_=iplt.gui.CreateDataViewer(im,self.current_image_)
    self.DisableUntilClosed(self.int_viewer_)
    self.int_image_=LoadImage(os.path.join(self.current_image_,string.replace(self.filemask_,"%%",self.current_image_)))
    self.int_viewer_.SetData(self.int_image_)
    rlist=iplt.ImportMtz(os.path.join(self.current_image_,self.current_image_+'_tilt.mtz'))
    symplot=SymPlot(rlist)
    symplot.AddOverlays(self.int_viewer_)

  @pyqtSlot()
  def on_image_extract_clicked(self):
    self.ExportInfoFiles()
    diff_extract=DiffExtract()
    options=Options()
    options.force_flag=False
    options.assume_verified=False
    options.force_verified=False
    options.use_lslat=False
    self.run_step_(diff_extract,options,self.current_image_,False)
    self.UpdateCurrentImageData()

  @pyqtSlot()
  def on_image_integrate_clicked(self):
    self.ExportInfoFiles()
    diff_integrate=DiffIntegrate()
    options=Options()
    options.force_flag=False
    self.run_step_(diff_integrate,options,self.current_image_,False)
    self.UpdateCurrentImageData()

  @pyqtSlot()
  def on_image_tilt_clicked(self):
    self.ExportInfoFiles()
    diff_tilt=DiffTilt()
    options=Options()
    options.force_flag=False
    options.ignore_vtilt=False
    self.run_step_(diff_tilt,options,self.current_image_,False)
    self.UpdateCurrentImageData()

  @pyqtSlot()
  def on_merge_new_merge_clicked(self):
    self.ExportInfoFiles()
    (text,ok)=QInputDialog.getText(self,"New merge directory","Name")
    if ok:
      os.mkdir(str(text))
      i=info.CreateInfo()
      i.Export(os.path.join(str(text),'info.xml'))
      self.merge_table_.CreateMergeList()
      
  @pyqtSlot()
  def on_merge_scale_clicked(self):
    self.ExportInfoFiles()
    args=['-c','%d' % (self.merge_table_.GetCurrentCycle('scale')),'-w','%s' % (self.current_merge_),'merge','scale']
    diff_merge=DiffMerge(args)
    diff_merge.Run()
    self.merge_table_.UpdateCurrentMerge()  
  
  @pyqtSlot()
  def on_merge_check_scaling_clicked(self):
    split_by_id=iplt.alg.RListSplitByProperty("id")
    QApplication.setOverrideCursor(QCursor(Qt.WaitCursor));
    merge_mtz=ex.ImportMtz(os.path.join(self.current_merge_,"merge_%02d.mtz" % (self.merge_table_.GetCurrentCycle('scale')-1) ))
    merge_mtz.Apply(split_by_id)
    image_mtz_map=split_by_id.GetRListMap()
    QApplication.restoreOverrideCursor();
    self.scaler_=WBinScaler(merge_mtz,image_mtz_map)
    self.scaler_.setWindowTitle("WBin Scaling %s"%self.current_merge_)
    self.scaler_.show()

  @pyqtSlot()
  def on_merge_refine_clicked(self):
    self.ExportInfoFiles()
    args=['-c','%d' % (self.merge_table_.GetCurrentCycle('refine')),'-w','%s' % (self.current_merge_),'tgrefine']
    diff_merge=DiffMerge(args)
    diff_merge.Run()
    self.merge_table_.UpdateCurrentMerge()  

  @pyqtSlot()
  def on_merge_llfit_clicked(self):
    self.ExportInfoFiles()
    args=['-c','%d' % (self.merge_table_.GetCurrentCycle('llfit')),'-w','%s' % (self.current_merge_),'llfit']
    diff_merge=DiffMerge(args)
    diff_merge.Run()
    self.merge_table_.UpdateCurrentMerge()  

  @pyqtSlot()
  def on_merge_check_llfit_clicked(self):
    cycle=self.merge_table_.GetCurrentCycle('llfit')-1
    rlist=ex.ImportMtz(os.path.join(self.current_merge_,"merge_llfit%02d.mtz"%cycle))
    dlist=ex.ImportMtz(os.path.join(self.current_merge_,"merge_disc%02d.mtz"%cycle))
    clist=ex.ImportMtz(os.path.join(self.current_merge_,"merge_curve%02d.mtz"%cycle))
    self.llv_=llv.LatticeLineViewer(rlist,dlist,clist)
    self.llv_.DataClicked.connect(self.LLViewDataClicked)
    self.llv_.show()
    
  @pyqtSlot(float,float,QString)
  def LLViewDataClicked(self,x,y,infotext):
    if infotext:
      imdir=self.dirmask_+str(infotext)
      self.view_se_=ViewSearchAndExtract(self.project_info_)
      self.view_se_.Load(os.getcwd(),imdir)
      self.view_se_.int_rov_.HighlightSymmetryRelatedPoints(self.llv_.GetIndexSelection()[0])
    
  @pyqtSlot()
  def on_merge_reset_clicked(self):
    (val,ok)=QInputDialog.getInt(self,"Reset merge to specific round","Round:",0,0,self.merge_table_.GetCurrentCycle('scale')-2)
    if ok:
      print val
      for i in range(val+1,self.merge_table_.GetCurrentCycle('scale')):
        print 'i',i
        for name in ('','curve','disc','llfit','scaled','tgref'):
          fullname=os.path.join(self.current_merge_,"merge_%s%02d.mtz"% (name,i))
          if os.path.exists(fullname):
            os.remove(fullname)
        for name in ('merge','scale','llfit','tgrefine'):
          fullname=os.path.join(self.current_merge_,"run_%s%02d.log"% (name,i))
          if os.path.exists(fullname):
            os.remove(fullname)
      self.merge_table_.UpdateCurrentMerge()  

    
  def onProjectInfoChanged(self):
    self.project_info_modified_=True
    
  def onImageInfoChanged(self):
    self.image_info_modified_=True
    
  def onMergeInfoChanged(self):
    self.merge_info_modified_=True
    
      
  def ExportInfoFiles(self):
    self.ExportImageInfo()
    self.ExportProjectInfo()
    self.ExportMergeInfo()
  def ExportImageInfo(self):
    if self.image_info_modified_:
      self.image_info_.Export(os.path.join(self.current_image_,"info.xml"))
      self.image_info_modified_=False
  def ExportProjectInfo(self):
    if self.project_info_modified_:
      self.project_info_.Export('project.xml') 
      self.project_info_modified_=False
  def ExportMergeInfo(self):
    if self.merge_info_modified_:
      self.merge_info_.Export(os.path.join(self.current_merge_,"info.xml"))
      self.merge_info_modified_=False

  def onCurrentImageChanged(self,image):
    self.ExportImageInfo()
    self.current_image_=str(image)
    self.UpdateCurrentImageData()

  def UpdateCurrentImageData(self):
    self.image_info_=self.LoadImageInfo(self.current_image_)
    self.image_resolution_limits_form_.SetInfo(self.image_info_)
    self.image_search_form_.SetInfo(self.image_info_)
    self.image_extract_form_.SetInfo(self.image_info_)
    self.image_result_browser_.ClearFiles()
    self.image_result_browser_.AddFile(os.path.join(self.current_image_,'diff_mask.log'),'mask log')
    self.image_result_browser_.AddFile(os.path.join(self.current_image_,'diff_ori.log'),'origin log')
    self.image_result_browser_.AddFile(os.path.join(self.current_image_,'diff_search.log'),'search log')
    self.image_result_browser_.AddFile(os.path.join(self.current_image_,'tmp_search_vecimg.png'),'search difference vector image')
    self.image_result_browser_.AddFile(os.path.join(self.current_image_,'diff_extract.log'),'extract log')
    self.image_result_browser_.AddFile(os.path.join(self.current_image_,'diff_integrate.log'),'integrate log')
    self.image_result_browser_.AddFile(os.path.join(self.current_image_,'diff_tilt.log'),'tilt log')
    self.image_result_browser_.AddFile(os.path.join(self.current_image_,'diff_sym.log'),'sym log')
    self.image_table_.UpdateCurrentImage()

  def onCurrentMergeChanged(self,merge):
    self.ExportMergeInfo()
    self.current_merge_=str(merge)
    self.merge_info_=self.LoadImageInfo(self.current_merge_)
    self.merge_resolution_limits_form_.SetInfo(self.merge_info_)
    self.merge_scale_form_.SetInfo(self.merge_info_)
    self.merge_llfit_form_.SetInfo(self.merge_info_)
    self.merge_result_browser_.ClearFiles()
    for i in range(self.merge_table_.GetCurrentCycle('scale')+1):
      self.merge_result_browser_.AddFile(os.path.join(self.current_merge_,'run_merge%02d.log' % (i)),'merge log %02d' % (i))
      self.merge_result_browser_.AddFile(os.path.join(self.current_merge_,'run_scale%02d.log' % (i)),'scale log %02d' % (i))
      self.merge_result_browser_.AddFile(os.path.join(self.current_merge_,'run_tgrefine%02d.log' % (i)),'tgrefine log %02d' % (i))
      self.merge_result_browser_.AddFile(os.path.join(self.current_merge_,'run_llfit%02d.log' % (i)),'llfit log %02d' % (i))

  def LoadImageInfo(self,image):
    iinfo=info.LoadInfo(os.path.join(image,'info.xml'))
    iinfo.AddDefault(self.project_info_)
    return iinfo   
    
  def ImageUpdate(self):  
    self.image_table_.UpdateImageList()
    imagelist=self.image_table_.GetImageList()
    self.total_images.setText(str(len(imagelist)))
    image_by_tilt={}
    for image in imagelist:
      tilt=image[3:5]
      if not tilt in image_by_tilt:
        image_by_tilt[tilt]=[]
      image_by_tilt[tilt].append(image)
    if  len(imagelist)==0:
      self.images_per_tilt.setText("")
    else:
      keys=image_by_tilt.keys()
      keys.sort()
      text=u"%s\u02DA:%4d" %(keys[0],len(image_by_tilt[keys[0]]))
      for key in keys[1:]:
        text+=u"\n%s\u02DA:%4d" %(key,len(image_by_tilt[key]))
      self.images_per_tilt.setText(text)
    non_verified=self.image_table_.GetNonVerifiedImageList()
    self.non_verified_images.setText(str(len(non_verified)))
    self.image_result_browser_.Update()
    self.merge_result_browser_.Update()
    
  def OnApplicationQuit(self):
    self.ExportInfoFiles()
