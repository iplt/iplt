#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtCore import pyqtSlot

from parameter_form_base import ParameterFormBase
from iplt.proc.diff.diff_manager.search_form_ui import Ui_SearchForm

class SearchForm(ParameterFormBase,Ui_SearchForm):
  def __init__(self,parent=None):
    ParameterFormBase.__init__(self,parent)
    self.setupUi(self)
    self.items={'gaussfilter':'DiffProcessing/LatticeSearch/GaussFilterStrength',
                'latmin':'DiffProcessing/LatticeSearch/MinimalLatticeLength',
                'latmax':'DiffProcessing/LatticeSearch/MaximalLatticeLength',
                'outer_size':'DiffProcessing/LatticeSearch/PeakSearchParams/OuterWindowSize',
                'outer_sens':'DiffProcessing/LatticeSearch/PeakSearchParams/OuterWindowSensitivity',
                'inner_size':'DiffProcessing/LatticeSearch/PeakSearchParams/InnerWindowSize',
                'inner_sens':'DiffProcessing/LatticeSearch/PeakSearchParams/InnerWindowSensitivity',
                'inner_count_limit':'DiffProcessing/LatticeSearch/PeakSearchParams/InnerWindowCountLimit',
                'inner_count_sens':'DiffProcessing/LatticeSearch/PeakSearchParams/InnerWindowCountSensitivity'}
  
  def UpdateValues(self):
    self.BlockChildren(True)
    self.gaussfilter.setValue(self.info_.Root().GetItem(self.items['gaussfilter']).AsFloat())
    self.latmin.setValue(self.info_.Root().GetItem(self.items['latmin']).AsFloat())
    self.latmax.setValue(self.info_.Root().GetItem(self.items['latmax']).AsFloat())
    self.outer_size.setValue(self.info_.Root().GetItem(self.items['outer_size']).AsInt())
    self.outer_sens.setValue(self.info_.Root().GetItem(self.items['outer_sens']).AsFloat())
    self.inner_size.setValue(self.info_.Root().GetItem(self.items['inner_size']).AsInt())
    self.inner_sens.setValue(self.info_.Root().GetItem(self.items['inner_sens']).AsFloat())
    self.inner_count_limit.setValue(self.info_.Root().GetItem(self.items['inner_count_limit']).AsInt())
    self.inner_count_sens.setValue(self.info_.Root().GetItem(self.items['inner_count_sens']).AsFloat())
    self.BlockChildren(False)
    
  @pyqtSlot(float)  
  def on_gaussfilter_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['gaussfilter'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_latmin_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['latmin'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_latmax_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['latmax'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(int)  
  def on_outer_size_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['outer_size'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_outer_sens_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['outer_sens'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(int)  
  def on_inner_size_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['inner_size'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_inner_sens_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['inner_sens'],False).SetFloat(val)
    self.infoChanged.emit()

  @pyqtSlot(int)  
  def on_inner_count_limit_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['inner_count_limit'],False).SetInt(val)
    self.infoChanged.emit()

  @pyqtSlot(float)  
  def on_inner_count_sens_valueChanged(self,val):
    self.info_.Root().RetrieveItem(self.items['inner_count_sens'],False).SetFloat(val)
    self.infoChanged.emit()


  def ResetToDefaults(self):
    self.info_.Root().Remove(self.items['gaussfilter'])
    self.info_.Root().Remove(self.items['latmin'])
    self.info_.Root().Remove(self.items['latmax'])
    self.info_.Root().Remove(self.items['outer_size'])
    self.info_.Root().Remove(self.items['outer_sens'])
    self.info_.Root().Remove(self.items['inner_size'])
    self.info_.Root().Remove(self.items['inner_sens'])
    self.info_.Root().Remove(self.items['inner_count_limit'])
    self.info_.Root().Remove(self.items['inner_count_sens'])
    self.BlockChildren(True)
    self.gaussfilter.setValue(self.info_.GetDefaultItem(self.items['gaussfilter']).AsFloat())
    self.latmin.setValue(self.info_.GetDefaultItem(self.items['latmin']).AsFloat())
    self.latmax.setValue(self.info_.GetDefaultItem(self.items['latmax']).AsFloat())
    self.outer_size.setValue(self.info_.GetDefaultItem(self.items['outer_size']).AsInt())
    self.outer_sens.setValue(self.info_.GetDefaultItem(self.items['outer_sens']).AsFloat())
    self.inner_size.setValue(self.info_.GetDefaultItem(self.items['inner_size']).AsInt())
    self.inner_sens.setValue(self.info_.GetDefaultItem(self.items['inner_sens']).AsFloat())
    self.inner_count_limit.setValue(self.info_.GetDefaultItem(self.items['inner_count_limit']).AsInt())
    self.inner_count_sens.setValue(self.info_.GetDefaultItem(self.items['inner_count_sens']).AsFloat())
    self.BlockChildren(False)
    self.infoChanged.emit()

