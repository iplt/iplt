#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtGui import *
from PyQt4.QtCore import *
from iplt.proc.diff.diff_manager.merge_table_ui import Ui_MergeTable
from iplt.proc import dirscan
import os
from ost import info 
import re

class MergeTable(QGroupBox,Ui_MergeTable):
  currentMergeChanged=pyqtSignal(QString)
  def __init__(self,parent=None):
    QGroupBox.__init__(self,parent)
    self.setupUi(self)
    tooltips=["<P>Name",
               "<P>Scaling was run",
               "<P>Refinement was run",
               "<P>Lattice line fit was run"]
    for i in range(len(tooltips)):
      self.table_.horizontalHeaderItem(i).setToolTip(tooltips[i])

  def CreateMergeList(self):
    mergedirs=[]
    for name in os.listdir('.'):
      if name.find("merge")!=-1:
        idir = os.path.join(name,"info.xml")
        if os.path.isfile(idir):
          mergedirs.append(name)
    self.table_.blockSignals(True)
    self.table_.setRowCount(0)
    self.table_.clearContents()
    for d in mergedirs:
      info_name=os.path.join(d,"info.xml")
      row=self.table_.rowCount()
      self.table_.setRowCount(row+1)
      self.table_.setItem(row,0,QTableWidgetItem(d))
      self.table_.setItem(row,1,QTableWidgetItem('X'))
      self.table_.setItem(row,2,QTableWidgetItem('X'))
      self.table_.setItem(row,3,QTableWidgetItem('X'))
      self.UpdateMergeRow(row)
    self.table_.resizeColumnsToContents()
    self.table_.blockSignals(False)
    if self.table_.rowCount()>0:
      self.table_.setCurrentCell(0,0)

  def UpdateMergeList(self):
    for i in range(self.table_.rowCount()):
      self.UpdateMergeRow(i)
      
  def UpdateCurrentMerge(self):
    self.UpdateMergeRow(self.table_.currentRow())
      
    
  def UpdateMergeRow(self,row):
    self.table_.blockSignals(True)
    merge_name=str(self.table_.item(row,0).text())
    dirlist=os.listdir(merge_name)
    scale_num=-1
    refine_num=-1
    fit_num=-1
    for file in dirlist:
      if re.match("merge_scaled..\.mtz$",file):
        scale_num=max(scale_num,int(file.strip('merge_scaled').rstrip('.mtz')))
      if re.match("merge_tgref..\.mtz$",file):
        refine_num=max(refine_num,int(file.strip('merge_tgref').rstrip('.mtz')))
      if re.match("merge_llfit..\.mtz$",file):
        fit_num=max(fit_num,int(file.strip('merge_llfit').rstrip('.mtz')))
    if scale_num>=0:
      self.table_.item(row,1).setText(str(scale_num))
    else:
      self.table_.item(row,1).setText('X')
    if refine_num>=0:
      self.table_.item(row,2).setText(str(refine_num))
    else:
      self.table_.item(row,2).setText('X')
    if fit_num>=0:
      self.table_.item(row,3).setText(str(fit_num))
    else:
      self.table_.item(row,3).setText('X')
    self.table_.blockSignals(False)

  def onCurrentCellChanged(self,row,column,oldrow,oldcolumn):
    if self.table_.rowCount()>0:
      self.currentMergeChanged.emit(self.table_.item(row,0).text())

  def GetCurrentCycle(self,text):
    if text=='scale':
      t=self.table_.item(self.table_.currentRow(),1).text()
      if t=='X':
        return 0
      else:
        return int(t)+1
    elif text=='refine':
      t=self.table_.item(self.table_.currentRow(),2).text()
      if t=='X':
        return 0
      else:
        return int(t)+1
    elif text=='llfit':
      t=self.table_.item(self.table_.currentRow(),3).text()
      if t=='X':
        return 0
      else:
        return int(t)+1
    else:
     raise Error("unkown merge step")