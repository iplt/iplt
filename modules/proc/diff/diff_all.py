#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Authors: Ansgar Philippsen, Andreas Schenk
#

from iplt.proc.subcommand import *
from diff_init import *
from diff_find_mask import *
from diff_ori import *
from diff_search import *
from diff_extract import *
from diff_integrate import *
from diff_tilt import *
from diff_sym import *

class DiffAll(Subcommand):
    def __init__(self):
        Subcommand.__init__(self,"all","apply (in turn) init mask origin search extract integrate tilt sym",None)
        self.diff_init=DiffInit()
        self.diff_find_mask=DiffFindMask()
        self.diff_ori=DiffOri()
        self.diff_search=DiffSearch()
        self.diff_extract=DiffExtract()
        self.diff_integrate=DiffIntegrate()
        self.diff_tilt=DiffTilt()
        self.diff_sym=DiffSym()
        
    def Run(self,dentrylist,args,options):
        self.diff_init.Run(dentrylist,args,options)
        self.diff_find_mask.Run(dentrylist,args,options)
        self.diff_ori.Run(dentrylist,args,options)
        self.diff_search.Run(dentrylist,args,options)
        self.diff_extract.Run(dentrylist,args,options)
        self.diff_integrate.Run(dentrylist,args,options)
        self.diff_tilt.Run(dentrylist,args,options)
        self.diff_sym.Run(dentrylist,args,options)
