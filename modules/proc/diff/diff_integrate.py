#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Integrate diffraction intensities
#
# Authors: Ansgar Philippsen, Andreas Schenk
#

import sys,os,os.path
from ost.img import *
import ost.info as info
import iplt
import iplt.alg
from diff_iterative_subcommand import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose
import iplt.proc.calc_friedel as calc_friedel

def integrate_peaks(image,rlist,boxsize,boxgrow,laxsize,bgsize,method,center_tweak_limit,bgcorr):
  lpl=iplt.alg.ConvertToLatticePointList(rlist)
  LogInfo("DiffInt: running peak integration")
  pi=iplt.alg.PeakIntegration(boxsize,boxsize+boxgrow,laxsize)
  pi.SetBackgroundRimSize(bgsize)
  pi.SetMethod(method)
  sz=pi.GetMaximalSize()
  lattice=lpl.GetLattice()
  offset_a=0.5*lattice.GetFirst()
  offset_b=0.5*lattice.GetSecond()
  for lp in lpl:
    LogVerbose("index: %s"%(str(lp.GetIndex())))
    cen=Point(lp.LPoint().GetPosition())
    subimg=image.Extract(Extent(sz,cen))

    #shift the center of the subimage to the center of the peak for the case of an non perfect lattice
    stat=alg.Stat()
    subimg.Apply(stat)
    com=stat.GetMaximumPosition()
    if Length(com.ToVec2()-cen.ToVec2())<=center_tweak_limit:
        LogVerbose("%s: using new center %s instead of %s"%(str(lp.GetIndex()),str(com),str(cen)))
        cen=com
    else:
        LogVerbose("%s: keeping center %s instead of peakmax at %s"%(str(lp.GetIndex()),str(cen),str(com)))
    subimg=image.Extract(Extent(sz,cen))

    # correction for background plane
    if bgcorr:
        bgimg = subimg.Copy(False)
        gfitu = lp.LPoint().GetFit().GetU()
        gfitp = lp.LPoint().GetFit().GetP()
        gfitc = lp.LPoint().GetFit().GetC()
        for bgp in bgimg.GetExtent():
            bgxy = bgp.ToVec2()-gfitu
            bgval = gfitc + bgxy[0]*gfitp[0]+bgxy[1]*gfitp[1]
            bgimg.SetReal(bgp,bgval)
        subimg-=bgimg
    # peak integration
    subimg.Apply(pi)
    lp.LPoint().SetPI(pi)

    # intercalated background determination
    lattice_pos=lattice.CalcPosition(lp.GetIndex())
    sum=0.0
    count=0.0
    for bg_pos in (lattice_pos+offset_a+offset_b,lattice_pos-offset_a+offset_b,lattice_pos+offset_a-offset_b,lattice_pos-offset_a-offset_b):
      for p in Extent(Point(bg_pos)+Point(-1,-1),Point(bg_pos)+Point(1,1)):
        sum+=image.GetReal(p)
        count+=1.0;
    ave = sum/count
    LogVerbose("DiffInt: ic_background for %s: %g (%g,%g)"%(str(lp.GetIndex()),ave,sum,count))
    lp.LPoint().SetICBG(ave)

    # average background calculation
    lp.LPoint().CalcAveBG(False);

  rlist_out=iplt.alg.ConvertToReflectionList(lpl)
  rlist_out.SetUnitCell(rlist.GetUnitCell())
  return rlist_out



class DiffIntegrate(DiffIterativeSubcommand):
    def __init__(self):
      DiffIterativeSubcommand.__init__(self,"integrate","integrate reflections","diff_integrate.log")
      self.in_name_="_ext.mtz"
      self.out_name_="_int.mtz"

    def ProcessDirectory(self,options):
        LogVerbose("DiffIntegrate: retrieving original unit cell info")
        if not self.GetInfoHandle().Root().HasGroup("DiffProcessing/OrigUnitCell"):
            raise Exception("OrigUnitCell not found in info")
        ig = self.GetInfoHandle().Root().GetGroup("DiffProcessing/OrigUnitCell")
        orig_cell = iplt.ExtractSpatialUnitCell(ig)
        LogInfo("DiffIntegrate: original unit cell: "+str(orig_cell))
        #self.init_info_stuff()
        try:
            os.remove(self.GetNamedFilePath(self.out_name_))
        except OSError:
            pass

        """
        main integration procedure
        """
        image=self.LoadDatasetImage()
        self.init_box()
        rlist=iplt.ImportMtz(self.GetNamedFilePath(self.in_name_))
        rlist.SetUnitCell(orig_cell)
        if not self.optimize_box_size_:
          rlist=integrate_peaks(image,rlist,self.box_size_,self.box_grow_,self.lax_size_,self.bg_size_,self.method_,self.center_tweak_limit_,self.bg_corr_)
        else:
          tg = iplt.CalcTiltFromReciprocalLattice(rlist.GetLattice(),orig_cell)
          optimal_rfriedel=100.0
          optimal_boxsize=0.0
          optimal_rlist=iplt.ReflectionList()
          for boxsize in range(self.box_size_-self.optimize_box_size_range_/2,self.box_size_-self.optimize_box_size_range_/2+self.optimize_box_size_range_):
            rlist_int=integrate_peaks(image,rlist,boxsize,self.box_grow_,self.lax_size_,self.bg_size_,self.method_,self.center_tweak_limit_,self.bg_corr_)
            rlist_tilt=rlist_int.Apply(iplt.alg.Tilter(tg))
            rlist2=iplt.ReflectionList(rlist_tilt,False)
            for rp in rlist_tilt:
              resol=rp.GetResolution()
              if resol<=self.res_low_ and resol>=self.res_high_:
                rlist2.AddReflection(rp.GetIndex(),rp)
            (rfriedel1,rfriedel2)=calc_friedel.calc_friedel(rlist2,"pi_volume","pi_sigma")
            if(optimal_rfriedel>rfriedel1):
              optimal_rfriedel=rfriedel1
              optimal_boxsize=boxsize
              optimal_rlist=rlist_int
          rlist=optimal_rlist
          SetIntInfoItem(self.GetInfoHandle().Root(),
                         "DiffProcessing/LatticeExtract/BoxSize",
                         optimal_boxsize)
          LogInfo("DiffInt: optimal boxsize is %d with RFriedel of %f "%(optimal_boxsize,optimal_rfriedel))
        LogInfo("DiffInt: writing final lattice point list to %s"%(self.GetNamedFilePath(self.out_name_)))
        iplt.ExportMtz(rlist,self.GetNamedFilePath(self.out_name_))
 
    def init_box(self):
        # semi duplicated in diff_extract
        self.box_size_=4
        self.box_grow_=0
        self.lax_size_=2
        self.bg_size_=1
        self.method_=0
        self.bg_corr_=False
        self.center_tweak_limit_=3
        self.optimize_box_size_=True
        self.optimize_box_size_range_=5
        self.res_low_=40.0
        self.res_high_=2.5
        gname = "DiffProcessing/LatticeExtract"

        self.box_size_=GetIntInfoItem(self.GetInfoHandle().Root(),
                                      "DiffProcessing/LatticeExtract/BoxSize",
                                      self.box_size_)
        self.box_grow_=GetIntInfoItem(self.GetInfoHandle().Root(),
                                      "DiffProcessing/LatticeExtract/BoxGrowAmount",
                                      self.box_grow_)
        self.bg_size_=GetIntInfoItem(self.GetInfoHandle().Root(),
                                      "DiffProcessing/LatticeExtract/BackgroundRimSize",
                                      self.bg_size_)
        self.method_=GetIntInfoItem(self.GetInfoHandle().Root(),
                                      "DiffProcessing/LatticeExtract/Method",
                                      self.method_)
        self.bg_corr_=GetBoolInfoItem(self.GetInfoHandle().Root(),
                                      "DiffProcessing/LatticeExtract/BackgroundSubtract",
                                      self.bg_corr_)
        self.center_tweak_limit_=GetFloatInfoItem(self.GetInfoHandle().Root(),
                                                  "DiffProcessing/LatticeExtract/CenterTweakLimit",
                                                  self.center_tweak_limit_)
        self.optimize_box_size_=GetBoolInfoItem(self.GetInfoHandle().Root(),
                                      "DiffProcessing/LatticeExtract/OptimizeBoxSize",
                                      self.optimize_box_size_)
        self.optimize_box_size_range_=GetIntInfoItem(self.GetInfoHandle().Root(),
                                      "DiffProcessing/LatticeExtract/OptimizeBoxSizeRange",
                                      self.optimize_box_size_range_)
        self.res_low_=GetFloatInfoItem(self.GetInfoHandle().Root(),"DiffProcessing/ResLim/rlow",self.res_low_)
        self.res_high_=GetFloatInfoItem(self.GetInfoHandle().Root(),"DiffProcessing/ResLim/res_high_",self.res_high_)

        LogInfo("using box-size of %d, box-grow-amount of %d, and background-rim-size of %d; background subtract %s"%(self.box_size_,self.box_grow_,self.bg_size_,str(self.bg_corr_)))


    def Cleanup(self,wdir):
        try:
            #os.remove(os.path.join(wdir,""))
            pass
        except OSError:
            pass

    def UpToDate(self,options):
        if options.force_flag:
            return False
        return self.FileIsNewer(self.GetNamedFilePath(self.out_name_),self.GetNamedFilePath(self.in_name_))

    def CheckPreCondition(self,options):
        if not os.path.isfile(self.GetFilePath("info.xml")):
            LogInfo("DiffInt: missing info.xml")
            return False
        if not self.GetInfoHandle().Root().HasGroup("DiffProcessingResults/RefinedLattice"):
            LogInfo("DiffInt: missing refined lattice")
            return False
        if not os.path.isfile(self.GetNamedFilePath(self.in_name_)):
            LogInfo("DiffInt: missing refined  _ext.mtz")
            return False
        return True
