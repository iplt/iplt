#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import os,os.path,math,gc
from ost.img import *
import iplt as ex
from iplt.proc import dirscan
from iplt.proc.wbin_scale_base import WbinScale
from iplt.proc.icalc_scale_base import ICalcScaler, ICalcWBinScaler
from iplt.proc.llfitamp_base import LLFit
from diff_merge_base import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost import PushVerbosityLevel,PopVerbosityLevel,PushLogSink,PopLogSink,FileLogSink
from ost.info import *
import iplt



# helper functions
def RefineTilt(rlist_ds,rmerge_calc,tiltgeom,alpha_range,alpha_step, beta_range, beta_step):
  # iterate over tilt range
  alpha_count = int(alpha_range/alpha_step)
  beta_count = int (beta_range/beta_step)


  sym = rlist_ds.GetUnitCell().GetSymmetry()
  LogVerbose("using symmetry %s"%(sym.GetSpacegroupName()))

  rlist_ds_sym = rlist_ds.Apply(ex.alg.Symmetrize(sym))
  rmerge_calc.Apply(rlist_ds_sym,"iobs","sigiobs")

  best_rf = rmerge_calc.GetRF()
  best_wrf = rmerge_calc.GetWRF()
  best_tg = tiltgeom
  best_rlist = rlist_ds_sym

  for beta_i in range(-beta_count,beta_count):
    new_beta = float(beta_i)*beta_step*Units.deg+tiltgeom.GetAStarVectorAngle()
    for alpha_i in range(-alpha_count,alpha_count+1):
      new_alpha = float(alpha_i)*alpha_step*Units.deg+tiltgeom.GetTiltAngle()
      new_tg = ex.LatticeTiltGeometry(tiltgeom)
      new_tg.SetTiltAngle(new_alpha)
      new_tg.SetAStarVectorAngle(new_beta)
      new_rlist_ds = rlist_ds.Apply(ex.alg.Tilter(new_tg))
      new_rlist_ds = new_rlist_ds.Apply(ex.alg.Symmetrize(sym))
      rmerge_calc.Apply(new_rlist_ds,"iobs","sigiobs")
      LogVerbose("%s: %5.3g %5.3g %g %g"%(new_tg,rmerge_calc.GetRF(),rmerge_calc.GetWRF(),new_alpha/Units.deg,new_beta/Units.deg))
      if rmerge_calc.GetRF()<best_rf:
        best_tg = new_tg
        best_rf = rmerge_calc.GetRF()
        best_wrf = rmerge_calc.GetWRF()
        best_rlist = ex.ReflectionList(new_rlist_ds)

  return(best_tg,best_rf,best_wrf,best_rlist)


def ReorderHKL(rlist,irlistlist,ucell):
  """
  re-order a merged reflection list from CCP4 symmetry
  (l>0) to h>0, which allows easier application of the
  lattice line fitting algorithm
  """
  sym=rlist.GetSymmetry()
  LogVerbose("merge: checking hkl sorting based on symmtry %s"%(sym.GetSpacegroupName()))
  rtmp=ex.ReflectionList(ucell)
  for ih in [-1,1]:
    for ik in [-1,1]:
      for zstar in [0.5,-0.5]:
        rtmp.AddReflection(ex.ReflectionIndex(ih,ik,zstar))
  rtmp.ApplyIP(ex.alg.Symmetrize(sym))
  flag=False
  for rp in rtmp:
    if rp.GetIndex().GetH()<0:
      flag=True
      break
  if not flag:
    LogVerbose("merge:: no re-indexing necessary")
    return (flag,rlist,irlistlist)
  LogVerbose("merge: re-indexing from l>0 -> h>0")
  rlist_new = ex.ReflectionList(rlist,False)
  for rp in rlist:
    if rp.GetIndex().GetH()<0:
      rlist_new.AddReflection(-rp.GetIndex(),rp)
    else:
      rlist_new.AddReflection(rp.GetIndex(),rp)

  irlistlist_new={}
  for (irkey,irlist) in irlistlist.iteritems():
    irlist_new = ex.ReflectionList(irlist,False)
    for rp in irlist:
      if rp.GetIndex().GetH()<0:
        irlist_new.AddReflection(-rp.GetIndex(),rp)
      else:
        irlist_new.AddReflection(rp.GetIndex(),rp)
      irlistlist_new[irkey]=ex.ReflectionList(irlist_new)

  return (flag,rlist_new,irlistlist_new)

def FriedelMergeHKL(rlist,keep_nonfriedel=True,diff_sigma=False):
  """
  merge friedel mates I+ and I-, given by the formula

    I = 1/2 (I+ + I-)

    S = 1/2 sqrt(S+^2 + S-^2)

  except for Friedel Mode 3, where the new sigma is given by

    S = | S+ - S- |

  """
  rlist_out=ex.ReflectionList(rlist,False)
  rlist_out.AddProperty("iobs_plus",ex.PT_INTENSITY2)
  rlist_out.AddProperty("iobs_minus",ex.PT_INTENSITY2)
  rlist_out.AddProperty("sigiobs_plus",ex.PT_STDDEVI2)
  rlist_out.AddProperty("sigiobs_minus",ex.PT_STDDEVI2)

  for rp1 in rlist:
    rp2=rlist.FindFirst(-rp1.GetIndex().AsDuplet())
    if rp2.IsValid():
      if rp1.GetIndex().GetZStar()>=0.0:
        i_plus=rp1.Get("iobs")
        i_minus=rp2.Get("iobs")
        i_ave = 0.5*(i_plus + i_minus)
        sig_plus=rp1.Get("sigiobs")
        sig_minus=rp2.Get("sigiobs")
        sig_ave = 0.0
        if diff_sigma:
          sig_ave = abs(i_plus-i_minus)
          if sig_ave<abs(i_ave*0.01):
            sig_ave=abs(i_ave*0.01)
        else:
          sig_ave = 0.5*(math.sqrt(sig_plus*sig_plus+sig_minus*sig_minus))

        rp_out=rlist_out.AddProxyter(rp1)
        rp_out.Set("iobs",i_ave)
        rp_out.Set("sigiobs",sig_ave)
        rp_out.Set("isigi",i_ave/sig_ave)
        rp_out.Set("iobs_plus",i_plus)
        rp_out.Set("sigiobs_plus",sig_plus)
        rp_out.Set("iobs_minus",i_minus)
        rp_out.Set("sigiobs_minus",sig_minus)

    else:
      if keep_nonfriedel:
        rp_out=rlist_out.AddProxyter(rp1)

  return rlist_out


def SymmetrizeHKL(rlist,info_root, run_rsym):
  sym_alg=None
  sym_name="P 1"
  if info_root.HasItem('DiffProcessing/OrigUnitCell/unitcell/symmetry'):
    sym_name = info_root.GetItem('DiffProcessing/OrigUnitCell/unitcell/symmetry').GetValue()
    LogInfo("using symmetry from info file: %s"%sym_name)
  else:
    LogInfo("using default P1 symmetry")

  sym_alg = ex.alg.Symmetrize(sym_name)

  merge_friedel=False
  keep_non_friedel=True
  friedel_diff_sigma=False
  
  if info_root.HasItem("DiffProcessing/Symmetrize/FriedelMode"):
    friedel_mode = info_root.GetItem("DiffProcessing/Symmetrize/FriedelMode").AsInt()
    if friedel_mode == 1:
      merge_friedel=True
      keep_non_friedel=True
      friedel_diff_sigma = False
    elif friedel_mode == 2:
      merge_friedel = True
      keep_non_friedel = False
      friedel_diff_sigma = False
    elif friedel_mode == 3:
      merge_friedel = True
      keep_non_friedel = False
      friedel_diff_sigma = True
    else:
      merge_friedel=False
      keep_non_friedel=True
      friedel_diff_sigma = False

      
  if merge_friedel:
    if keep_non_friedel:
      LogInfo("merging friedel mates, keeping unpaired reflections")
    else:
      LogInfo("merging friedel mates, removing unpaired reflections")
      if friedel_diff_sigma:
        LogInfo("constructing artificial sigma from friedel difference")
    rlist=FriedelMergeHKL(rlist,keep_non_friedel,friedel_diff_sigma)

  # store original indices
  rlist.AddProperty("symh")
  rlist.AddProperty("symk")
  rlist.AddProperty("symzstar")
  for rp in rlist:
    index=rp.GetIndex()
    rp.Set("symh",float(index.GetH()))
    rp.Set("symk",float(index.GetK()))
    rp.Set("symzstar",float(index.GetZStar()))

  if run_rsym:
    LogVerbose("running rsym")
    rsym_zlim = GetFloatInfoItem(info_root,
                                 "DiffProcessing/Symmetrize/zlim",
                                 0.01)

    rsymc = ex.alg.RSymCalculator(ex.Symmetry(sym_name),rsym_zlim,40.0,2.0,16)
    rsymc.Apply(rlist,"iobs","sigiobs")
    rsym = rsymc.GetRF();
    wrsym = rsymc.GetWRF();
    rsym_count = rsymc.GetRCount()

    LogInfo("RSym: %g  wRSym: %g  (%d)"%(rsym,wrsym,rsym_count))
        
    LogInfo("symmetrizing")
    rlist_sym = rlist.Apply(sym_alg)
    return rlist_sym

def ResolutionFilterHKL(rlist,rlow,rhigh):
  rlist_out = ex.ReflectionList(rlist,False)
  for rp in rlist:
    if rp.GetResolution()<rhigh or rp.GetResolution()>rlow:
      continue
    rlist_out.AddProxyter(rp)
  return rlist_out

def RecalcQualityHKL(rlist,cdf_m,cdf_s):
  if not rlist.HasProperty("quality"):
    rlist.AddProperty("quality",ex.PT_WEIGHT)
  for rp in rlist:
    iobs = rp.Get("iobs")
    sigiobs = rp.Get("sigiobs")
    quality=0.0
    if sigiobs<=0.0:
      LogVerbose("warning, sigiobs<=0 for %s, skipping"%(str(rp.GetIndex())))
      continue
    quality = ex.alg.CDF(iobs/sigiobs,cdf_m,cdf_s)
    rp.Set("quality",quality)
  return rlist



class DiffMerge(DiffMergeBase):
  def __init__(self,args):
    DiffMergeBase.__init__(self)
    # parameters from command line args
    self.wdir_ = "merge" # working directory
    self.sdir_list_ = ['.'] # list of data containing directories
    self.verb_ = 3 # verbosity
    self.command_list_ = [] # list of commands to run
    self.latline_ = None # limit llfit to this index
    self.unity_scaling_=False # force scaling factor of 1.0
    self.ref_cycle_ = 0 # global cycle to use
    self.do_tgrefine_ = True # perform tgrefinement
    # parse the command line args
    self.parse_args(args)

    if len(self.command_list_)==0:
      raise Exception("missing command(s)")

    if self.command_list_[0]=="all":
      self.command_list_=["merge","scale","tgrefine","llfit"]
  
    PushVerbosityLevel(self.verb_)

    # determine commands to run
    comm_list=["merge","scale","tgrefine","llfit", "summary","premerge"]
    for command in self.command_list_:
      if not command in comm_list:
        raise Exception("uknown command '%s', must be one of %s" %(command,str(comm_list)))

    # working directory
    self.orig_dir_ = os.getcwd()
    if not self.wdir_[0]=='/':
      self.wdir_ = os.path.join(self.orig_dir_,self.wdir_)

    self.data_dir_=os.path.join(self.wdir_,"datafiles")

    # load local and project-wide info file(s)
    iinfo_file = os.path.join(self.wdir_,"info.xml")
    if os.path.isfile(iinfo_file):
      LogVerbose("loading %s"%iinfo_file)
      self.proj_info_ = LoadInfo(iinfo_file)
    else:
      self.proj_info_ = CreateInfo()

    pinfo=LoadInfo("project.xml")
    self.proj_info_.AddDefault(pinfo)

    # grab params from the base class routine
    self.LoadParameters(self.proj_info_)

  def parse_args(self,args):
    # command line args
    i=0
    while i< len(args):
      if args[i]=="-w":
        self.wdir_ = args[i+1]
        i=i+1
      elif args[i]=="-l":
        tmp=args[i+1].split(",")
        self.latline_=Point(int(tmp[0]),int(tmp[1]))
        i=i+1
      elif args[i]=="-v":
        self.verb_=int(args[i+1])
        i=i+1
      elif args[i]=='-u':
        self.unity_scaling_=True
      elif args[i]=='-d':
        self.sdir_list_.append(args[i+1])
        i=i+1
      elif args[i]=='-c':
        self.ref_cycle_ = int(args[i+1])
        i=i+1
      elif args[i]=='-t':
        self.do_tgrefine_=False
      else:
        if args[i][0]=='-':
          raise Exception("unknown option" ,args[i])
        else:
          self.command_list_.append(args[i])
          
      i+=1


  def Run(self):
    # main entry point into the merge routine
    try:
      if not os.path.exists(self.wdir_):
         os.mkdir(self.wdir_)
      if not os.path.exists(self.data_dir_):
         os.mkdir(self.data_dir_)
    except:
      pass

    # runtime lists
    self.image_iinfo_={}
    self.rlist_merge0_=None
    self.image_rlist0_={}
    self.rlist_merge1_=None
    self.image_rlist1_={}
    self.rlist_merge2_=None
    self.image_rlist2_={}

    for command in self.command_list_:
      self.command_ = command
      logfile = os.path.join(self.wdir_,"run_%s%02d.log"%(self.command_,self.ref_cycle_))
      PushLogSink(FileLogSink(logfile))

      self.dump_parameters()

      try:
        if command=="summary":
          self.RunSummary()
        elif command=="premerge":
          self.RunPreMerge()
        elif command=="merge":
          self.RunMerge()
        elif command=="scale":
          if(self.ref_cycle_==0):
            self.RunScale(False)
          else:
            self.RunScale(True)
        elif command=="tgrefine":
          self.RunTGRefine()
        elif command=="llfit":
          self.RunLLFit()
      except:
        PopLogSink()
        raise
      else:
        PopLogSink()

  def RunMerge(self):
    # assemble data files and pool them together into a single reflection list
    rlist_merge0 = ex.ReflectionList(self["ucell"])
    rlist_merge0.AddProperty("iobs",ex.PT_INTENSITY)
    rlist_merge0.AddProperty("sigiobs",ex.PT_STDDEV)
    rlist_merge0.AddProperty("isigi",ex.PT_WEIGHT)
    rlist_merge0.AddProperty("id")
    rlist_merge0.AddProperty("tilt")
    rlist_merge0.AddProperty("quality",ex.PT_WEIGHT)
    rlist_merge0.AddProperty("symh")
    rlist_merge0.AddProperty("symk")
    rlist_merge0.AddProperty("symzstar")
    rlist_merge0.AddProperty("sigmazstar")

    image_rlist0={}
    image_iinfo={}

    cdf_m = (self["quality_t2"]+self["quality_t1"])*0.5
    cdf_s = self["quality_t2"] - cdf_m

    # loop over all source directories
    try:
      for sdir in self.sdir_list_:
        tdir = os.path.join(self.orig_dir_,sdir)
        if not os.path.isdir(tdir):
          continue
        else:
          LogError("changing to source directory %s"%tdir)
          os.chdir(tdir)
                
        # read initial data, symmetrize it,
        # and create unscaled merge rlist
        for dentry in dirscan.dirscan(self["img_mask"],"."):
          mtz_in = os.path.join(dentry[0],dentry[1],dentry[1]+"_tilt.mtz")
          ignore = os.path.join(dentry[0],dentry[1],"ignore")
          iinfo = LoadInfo(os.path.join(dentry[0],dentry[1],"info.xml"))
                            
          if os.path.exists(mtz_in) and not os.path.exists(ignore):
            img_id=int(dentry[1][3:])
            image_iinfo[img_id]=iinfo
            
            LogVerbose("reading %s (id=%06d)"%(mtz_in,img_id))
            rlist=ex.ImportMtz(mtz_in)

            LogInfo("symmetrizing %06d"%(img_id))
            rlist = SymmetrizeHKL(rlist,self.proj_info_.Root(),True)

            LogVerbose("running resolution filter")
            rlist = ResolutionFilterHKL(rlist,self["rlim_low"],self["rlim_high"])

            LogVerbose("recalculating quality")
            rlist = RecalcQualityHKL(rlist,cdf_m,cdf_s)

            LogVerbose("adding img id and assembling merge")
            if not rlist.HasProperty("id"):
              rlist.AddProperty("id")
            for rp in rlist:
              rp.Set("id",img_id)
              rlist_merge0.AddProxyter(rp)

            # store in image rlist, forcing copy with explicit ctor
            image_rlist0[img_id]=ex.ReflectionList(rlist)
            fname=os.path.join(self.data_dir_,dentry[1]+"_sym%02d.mtz"%self.ref_cycle_)
            LogVerbose("exporting symmetrized reflections as %s"%fname)
            ex.ExportMtz(rlist,fname)

    except:
      os.chdir(self.orig_dir_)
      raise

    os.chdir(self.orig_dir_)

    (reorder_flag,rlist_merge0,image_rlist0) = ReorderHKL(rlist_merge0,image_rlist0,self["ucell"])

    fname = os.path.join(self.wdir_,"merge_%02d.mtz"%self.ref_cycle_)
    LogVerbose("writing %s"%fname)
    ex.ExportMtz(rlist_merge0,fname)
    if self.ref_cycle_==0:  # in round >0 scale has to get llfit.mtz instead of merge.mtz
        self.rlist_merge0_=rlist_merge0
    self.image_rlist0_=image_rlist0
    self.image_iinfo_=image_iinfo


  def RunScale(self,icalc_flag):
    if self.rlist_merge0_==None:
      self.import_state0()

    self.rlist_merge1_ = ex.ReflectionList(self.rlist_merge0_,False)
    self.rlist_merge1_.AddProperty("scale")
    self.rlist_merge1_.AddProperty("sigscale")

    # get approproate wbin scale object
    wbin_scale=None
    if icalc_flag:
      if self["llref_scaling_mode"]==1:
        LogInfo("using direct icalc-iobs scaling from llfit")
        wbin_scale = ICalcScaler(self["scale_rlow"],self["scale_rhigh"],self["scale_bcount"])
      else:
        LogInfo("using wbin reference scaling from llfit derived icalcs")
        rlist_fit = ex.ReflectionList(self.rlist_merge0_,False)
        LogVerbose("assembling reference object")
        for rp in self.rlist_merge0_:
          rp2=rlist_fit.AddProxyter(rp)
          rp2.Set("iobs",rp.Get("icalc"))
          rp2.Set("sigiobs",rp.Get("sigicalc"))
        LogVerbose("initializing WBinScale")
        wbin_scale = WbinScale(rlist_fit,self["scale_bcount"],self["scale_rlow"],self["scale_rhigh"])
    else:
      if self["use_model_ref"]:
        rlist_model = None
        if self["model_mtz"]:
          # use model reflections if available
          LogInfo("using model based wbin scaling")
          fname = os.path.join(self.wdir_,self["model_mtz"])
          if not os.path.exists(fname):
            raise Exception("model mtz %s not found"% (self["model_mtz"]) )
          rlist_model = ex.ImportMtz(fname)
          if not rlist_model.HasProperty("FC"):
            raise Exception("missing property FC in %s"% (self["model_mtz"]) )
          rlist_model.AddProperty("iobs")
          rlist_model.AddProperty("sigiobs")

          LogVerbose("assembling reference object")
          for rp in rlist_model:
            amp = rp.Get("FC")
            sigamp = amp*0.00001 # hack
            rp.Set("iobs",amp*amp)
            rp.Set("sigiobs",math.sqrt(2.0)*sigamp*sigamp)
            
        elif self["model_map"]:
          # use map and llpredict if mtz is not available
          LogVerbose("loading reference map %s"%self["model_map"])
          model_map = LoadImage(self["model_map"])
          LogVerbose("running llpredict")
          llpred = ex.alg.LLPredict(model_map)
          LogVerbose("assembling model reference")
          rlist_model = ex.ReflectionList(self.rlist_merge0_,False)
          total_iobs=0.0
          total_icalc=0.0
          for rp in self.rlist_merge0_:
            total_iobs+=rp.Get("iobs")
            rp2=rlist_model.AddProxyter(rp)
            acalc = llpred.Get(rp.GetIndex())
            icalc = acalc.real*acalc.real+acalc.imag*acalc.imag
            total_icalc+=icalc
            rp2.Set("iobs",icalc)
            rp2.Set("sigiobs",icalc*0.01) # artifial 1% error = hack
          # pre-scaling tweak
          pre_scale = total_iobs/total_icalc
          LogVerbose("pre-scaling model with %g"%pre_scale)
          for rp in rlist_model:
            rp.Set("iobs",rp.Get("iobs")*pre_scale)
            rp.Set("sigiobs",rp.Get("sigiobs")*pre_scale)
        else:
          LogError("error: missing model mtz or model map in project/info xml")
          return;
        
        LogVerbose("initializing WBinScale")
        wbin_scale = WbinScale(rlist_model,self["scale_bcount"],self["scale_rlow"],self["scale_rhigh"])
      else:
        LogInfo("using merge based wbin scaling")
        wbin_scale = WbinScale(self.rlist_merge0_,self["scale_bcount"],self["scale_rlow"],self["scale_rhigh"])
    # endif icalc_flag
    
    wbin_scale.SetWBinSigmaCutoff(self["wbin_sigma_cutoff"])
    wbin_scale.SetWBinISigICutoff(self["wbin_isigi_cutoff"])
    wbin_scale.SetFilterRLim(self["scale_filter_rlow"],self["scale_filter_rhigh"],self["scale_filter_bcount"])

    # iterate over each dataset, determine scale, and create a scaled version
    self.image_rlist1_={}
    
    LogInfo("commencing dataset iterations")
    image_rlist_keys=self.image_rlist0_.keys()
    image_rlist_keys.sort()
    for dataset_id in image_rlist_keys:
      rlist=self.image_rlist0_[dataset_id]
      LogVerbose("isotropic scaling for %06d"%dataset_id)
      rlist = wbin_scale.Apply(rlist,self.unity_scaling_,"%06d"%dataset_id)
      if not rlist:
        LogVerbose("scaling failed")
        continue
      self.image_rlist1_[dataset_id]=ex.ReflectionList(rlist)
      for rp in rlist:
        rp2=self.rlist_merge1_.AddProxyter(rp)
        rp2.Set("id",dataset_id)

    fname = os.path.join(self.wdir_,"merge_scaled%02d.mtz"%self.ref_cycle_)
    LogInfo("scaling done, exporting %s"%fname)
    ex.ExportMtz(self.rlist_merge1_,fname)

    if(self["calc_rmerge"]):
      self.calc_rmerge(self.rlist_merge1_,self.image_rlist1_)

  def RunTGRefine(self):
    if self.rlist_merge1_==None:
      self.import_state1()
    if len(self.image_iinfo_)==0:
      self.import_iinfo(self.image_rlist1_)

    if not self.do_tgrefine_:
      self.rlist_merge2_=ex.ReflectionList(self.rlist_merge1_,True)
      self.image_rlist2_ = self.dissect_merged(self.rlist_merge2_)
      LogInfo("skipping tilt geometry refinement")
      fname = os.path.join(self.wdir_,"merge_tgref%02d.mtz"%self.ref_cycle_)
      LogVerbose("writing %s"%fname)
      ex.ExportMtz(self.rlist_merge2_,fname)
      return

    rmerge_calc = ex.alg.RMergeCalculator(self.rlist_merge1_,"iobs","sigiobs",
                                          self["calc_rmerge_zlim"],self["scale_rlow"],
                                          self["scale_rhigh"],self["scale_bcount"])
    rmerge_calc.SetSigmaRejectionLevel(self["calc_rmerge_rej_level"])

    image_rlist={}

    image_rlist_keys=self.image_rlist1_.keys()
    image_rlist_keys.sort()
    for dataset_id in image_rlist_keys:
      if not self.image_iinfo_.has_key(dataset_id):
        LogInfo("missing info entry for %06d, skipping tg refinement"%dataset_id)
        continue
      rlist=self.image_rlist1_[dataset_id]
      iinfo = self.image_iinfo_[dataset_id]
      LogInfo("tilt geometry refinement for %06d"%dataset_id)
      # obtain tilt geom
      tiltgeom = iplt.InfoToLatticeTiltGeometry(iinfo.Root().GetGroup("DiffProcessingResults/InitialTiltGeometry"))
      lat = iplt.InfoToLattice(iinfo.Root().GetGroup("DiffProcessingResults/RefinedLattice"))
      # set up non symmetrized rlist version with ucell and lattice
      rlist_ds = ex.ReflectionList(rlist,False)
      rlist_ds.SetUnitCell(self["ucell"])
      rlist_ds.SetLattice(lat)
      for rp in rlist:
        ri=ex.ReflectionIndex(int(rp.Get("symh")),int(rp.Get("symk")),rp.Get("symzstar"))
        rlist_ds.AddReflection(ri,rp)

      # create reference rfactors, use symmetrized rlist
      rmerge_calc.Apply(rlist,"iobs","sigiobs")
      pre_rf = rmerge_calc.GetRF()
      pre_wrf = rmerge_calc.GetWRF()
      pre_tg = tiltgeom

      # coarse and fine tilt geometry refinement
      # rlist parameter needs to be un-symmetrized!
      # returned rlist _is_ symmetrized, however
      (best_tg1,best_rf1,best_wrf1,best_rlist1) = RefineTilt(rlist_ds,rmerge_calc,tiltgeom,
                                                             10.0,2.0,180.0,20.0)
      (best_tg2,best_rf2,best_wrf2,best_rlist2) = RefineTilt(rlist_ds,rmerge_calc,best_tg1,
                                                             2.0,0.2,20.0,2.0)

      LogInfo("applying %s symmetry to refined rlist"%(best_rlist2.GetSymmetry().GetSpacegroupName()))
      LogInfo(" pre-ref : %s  rf=%5.3g  wrf=%5.3g"%(pre_tg,pre_rf,pre_wrf))
      LogInfo(" refined1: %s  rf=%5.3g  wrf=%5.3f"%(best_tg1,best_rf1,best_wrf1))
      LogInfo(" refined2: %s  rf=%5.3g  wrf=%5.3f"%(best_tg2,best_rf2,best_wrf2))

      image_rlist[dataset_id]=ex.ReflectionList(best_rlist2)

    # temporarily merge the refined but unscaled images
    rlist_merge = self.compact_merged(image_rlist)

    # now the scaling
    wbin_scale = WbinScale(rlist_merge,self["scale_bcount"],self["scale_rlow"],self["scale_rhigh"])
    wbin_scale.SetWBinSigmaCutoff(self["wbin_sigma_cutoff"])
    wbin_scale.SetWBinISigICutoff(self["wbin_isigi_cutoff"])
    wbin_scale.SetFilterRLim(self["scale_filter_rlow"],self["scale_filter_rhigh"],self["scale_filter_bcount"])

    self.image_rlist2_={}
    self.rlist_merge2_ = ex.ReflectionList(self.rlist_merge1_,False)

    image_rlist_keys=image_rlist.keys()
    image_rlist_keys.sort()
    for dataset_id in image_rlist_keys:
      LogVerbose("isotropic scaling for tg refined %06d"%dataset_id)
      rlist=image_rlist[dataset_id]
      rlist = wbin_scale.Apply(rlist,self.unity_scaling_,"%06d"%dataset_id)
      if not rlist:
        LogVerbose("scaling failed")
        continue
      self.image_rlist2_[dataset_id]=ex.ReflectionList(rlist)
      for rp in rlist:
        rp2=self.rlist_merge2_.AddProxyter(rp)
        rp2.Set("id",dataset_id)

    fname = os.path.join(self.wdir_,"merge_tgref%02d.mtz"%self.ref_cycle_)
    LogVerbose("writing %s"%fname)
    ex.ExportMtz(self.rlist_merge2_,fname)

    if(self["calc_rmerge"]):
      self.calc_rmerge(self.rlist_merge2_,self.image_rlist2_)

  def RunLLFit(self):
    if self.rlist_merge2_==None:
      self.import_state2()
    if len(self.image_iinfo_)==0:
      self.import_iinfo(self.image_rlist2_)

    # set up llfit object
    max_resol=self["scale_rhigh"]
    min_bin_count=4
    thickness=self["ucell"].GetC()
    llfit = LLFit(max_resol,min_bin_count,thickness,self["ucell"])
    llfit.SetLatLine(self.latline_)
    
    llfit.SetMinimumQuality(self["quality_cutoff"])
    llfit.SetWeightingScheme(self["weight_mode"])
    llfit.SetMaxIterations(self["niter"])
    llfit.SetIterationLimits(self["iterlim"])
    llfit.SetGlobalWeight(self["weight_global"])
    
    llfit.SetBootstrapFlag(self["bootstrap_flag"])
    llfit.SetBootstrapIter(self["bootstrap_iter"])
    
    llfit.SetGuessMode(self["llfit_guess_mode"])
    llfit.SetIterations(self["llfit_iter"])
    llfit.SetIterationsUseNonConvergedFlag(self["llfit_iter_conv_flag"])
    
    llfit.SetZIFitFlag(True)
    llfit.ForceHermMode(self["llfit_force_herm"])
    
    llfit.SetBFactor(self["llfit_bfac"])
    
    llfit.SetQualityLimits(self["quality_t1"],self["quality_t2"])
    llfit.SetRelativeZWeight(self["refine_zweight"])
    
    llfit.SetPhantomMode(self["llfit_phantom_mode"])
    llfit.SetCurveFitMode(self["refine_curve_fit_mode"])

    llfit.SetAlg(self["llfit_alg"])

    # do actual fitting
    (rlist_llfit,rlist_disc,curve_map) = llfit.Apply(self.rlist_merge2_)

    # export results
    # disc contains discretized values
    # llfit retains the original reflections with an icalc attribute based on the fit
    ex.ExportMtz(rlist_llfit,os.path.join(self.wdir_,"merge_llfit%02d.mtz"%self.ref_cycle_))
    ex.ExportMtz(rlist_disc,os.path.join(self.wdir_,"merge_disc%02d.mtz"%self.ref_cycle_))

    # make curve for display purposes
    rlist_curve = ex.ReflectionList(self["ucell"])
    rlist_curve.AddProperty("icalc")
    rlist_curve.AddProperty("sigicalc")
    for (hk,c_list) in curve_map.iteritems():
      for cc in c_list:
        rp=rlist_curve.AddReflection(ex.ReflectionIndex(hk[0],hk[1],cc[0]))
        rp.Set("icalc",cc[1])
        rp.Set("sigicalc",cc[2])
    ex.ExportMtz(rlist_curve,os.path.join(self.wdir_,"merge_curve%02d.mtz"%self.ref_cycle_))
    # clean up memory
    rlist_curve=None
    gc.collect()

    # stop here if only a single lattice line was fit
    if self.latline_:
      return

    bin_size = (1.0/(self["rlim_high"]*self["rlim_high"])-1.0/(self["rlim_low"]*self["rlim_low"]))/float(self["scale_bcount"])
    bcount=int((1.0/(self["rlim_high"]*self["rlim_high"])-1.0/(self["rlim_low"]*self["rlim_low"]))/bin_size)
    rcalc_calc = ex.alg.RCalcCalculator("icalc","sigicalc",self["rlim_low"],self["rlim_high"],bcount)

    # export individual fits and print their rfit stats
    image_rlist = self.dissect_merged(rlist_llfit)
    for (dataset_id,dataset_rlist) in image_rlist.iteritems():
      fname=os.path.join(self.data_dir_,"%s%06d_llfit%02d.mtz"%(self["img_mask_pref"],dataset_id,self.ref_cycle_))
      ex.ExportMtz(dataset_rlist,fname)
      LogInfo("Dataset %06d: Rfit values"%(dataset_id))
      rcalc_calc.Apply(dataset_rlist,"iobs","sigiobs")
      LogInfo("  rlow  rhigh  rfit    wrfit    count")
      for bb in range(self["scale_bcount"]):
        LogInfo("  %5.2f %5.2f   %4.4g  %4.4g (%d)"%(rcalc_calc.GetBinLowerResolutionLimit(bb),
                                                     rcalc_calc.GetBinLowerResolutionLimit(bb+1),
                                                     rcalc_calc.GetRFBin(bb),
                                                     rcalc_calc.GetWRFBin(bb),
                                                     rcalc_calc.GetRCountBin(bb)))
      LogInfo("%06d overall:  RFit = %4.4g wRFit =  %4.4g (N=%d)"%(dataset_id,rcalc_calc.GetRF(),
                                                                       rcalc_calc.GetWRF(),
                                                                       rcalc_calc.GetRCount()))
      LogInfo("")
    # end of individual fit iterations

    # finally overall rfit
    rcalc_calc.Apply(rlist_llfit,"iobs","sigiobs")
    LogInfo("Overall RFit for all datasets")
    for bb in range(self["scale_bcount"]):
      LogInfo("  %5.2f %5.2f   %4.4g  %4.4g (%d)"%(rcalc_calc.GetBinLowerResolutionLimit(bb),
                                                   rcalc_calc.GetBinLowerResolutionLimit(bb+1),
                                                   rcalc_calc.GetRFBin(bb),rcalc_calc.GetWRFBin(bb),
                                                   rcalc_calc.GetRCountBin(bb)))
    LogInfo("Total: RFit = %2.4g  wRFit= %2.4g"%(rcalc_calc.GetRF(),rcalc_calc.GetWRF()))

    # done
    self.rlist_merge3_=rlist_llfit
    self.image_rlist3_=image_rlist

  def RunPreMerge(self):
    """
    pre-merge takes only a subset (usually 0deg), assumes z* of zero,
    and dumps out an rmerge per resolution bin
    """
    
  def dump_parameters(self):
    LogInfo("Merge Parameters")
    LogInfo("----------------")
    LogInfo(" overall refinement cycle %02d"%self.ref_cycle_)
    LogInfo(" working dir: %s"%(self.wdir_))
    LogInfo(" additional data dirs: %s"%(self.sdir_list_[1:]))
    LogInfo(" unit cell: %s"%(str(self["ucell"])))
    LogInfo(" rsym cutoff: %f"%(self["rsym_limit"]))
    LogInfo(" resolution limits: %g %g"%(self["rlim_low"],self["rlim_high"]))
    LogInfo(" scale: from %gA to %gA in %d bins"%(self["scale_rlow"],self["scale_rhigh"],self["scale_bcount"]))
    LogInfo(" scale filter: using %d bins in range (%g,%g)"%(self["scale_filter_bcount"],self["scale_filter_rlow"],self["scale_filter_rhigh"]))
    LogInfo(" scale filter: resolution cutoff per image at wbin isigi of %g"%(self["wbin_isigi_cutoff"]))
    LogInfo(" scale filter: outlier rejection per image over %g wbin sigma"%(self["wbin_sigma_cutoff"]))
    if self["use_model_ref"] and self["model_mtz"]:
      LogInfo(" scale: using model reference from %s"%(self["model_mtz"]))
    LogInfo(" scale: rmerge zlim: %g"%(self["calc_rmerge_zlim"]))
    LogInfo(" llfit: max %d iterations, lim %g"%(self["niter"],self["iterlim"]))
    LogInfo(" llfit: weight-mode %d with t1=%g, t2=%g, cutoff=%g and global=%g"%(self["weight_mode"],self["quality_t1"],self["quality_t2"],self["quality_cutoff"],self["weight_global"]))
    LogInfo(" llfit: using bfactor %g"%self["llfit_bfac"])
    if self["bootstrap_flag"]:
      LogInfo(" llfit: boostrapping with %d iterations"%(self["bootstrap_iter"]))
    else:
      LogInfo(" llfit: no bootstrapping")
    if self["llfit_phantom_mode"]:
      LogInfo(" llfit: using phantom data points for sigma zstar inclusion")
    LogInfo(" llfit: guess-mode %d with %d iterations, keeping non-coverged: %s"%(self["llfit_guess_mode"],self["llfit_iter"],str(self["llfit_iter_conv_flag"])))
    LogInfo(" llfit: refining scale with mode %d"%(self["llref_scaling_mode"]))
    LogInfo(" llfit: using algorithm %d"%(self["llfit_alg"]))
    
    LogInfo(" refine: using curve fit mode %d with zstar weight of %g"%(self["refine_curve_fit_mode"],self["refine_zweight"]))
    LogInfo("")

  def import_iinfo(self,image_rlist):
    self.image_iinfo_={}
    for (dataset_id,dataset_rlist) in image_rlist.iteritems():
      self.image_iinfo_[dataset_id]=None
      for sdir in self.sdir_list_:
        base = "%s%06d"%(self["img_mask_pref"],dataset_id)
        tdir = os.path.join(self.orig_dir_,sdir,base)
        if os.path.isdir(tdir):
          self.image_iinfo_[dataset_id]=LoadInfo(os.path.join(tdir,"info.xml"))
          break
      if not self.image_iinfo_[dataset_id]:
        LogInfo("warning: could not find dataset directory for %s"%base)

  def compact_merged(self,image_rlist):
    rlist_merge = ex.ReflectionList(self["ucell"])
    rlist_merge.AddProperty("iobs",ex.PT_INTENSITY)
    rlist_merge.AddProperty("sigiobs",ex.PT_STDDEV)
    rlist_merge.AddProperty("quality",ex.PT_WEIGHT)
    rlist_merge.AddProperty("id")
    rlist_merge.AddProperty("symh")
    rlist_merge.AddProperty("symk")
    rlist_merge.AddProperty("symzstar")
    rlist_merge.AddProperty("sigmazstar")
    for (dataset_id,dataset_rlist) in image_rlist.iteritems():
      for rp in dataset_rlist:
        rp2=rlist_merge.AddProxyter(rp)
        rp2.Set("id",dataset_id)
    return rlist_merge

  def dissect_merged(self,rlist_merge):
    image_rlist = {}
    for rp in rlist_merge:
      dataset_id = rp.Get("id")
      rlist = None
      if not dataset_id in image_rlist:
        rlist = ex.ReflectionList(rlist_merge,False)
        image_rlist[dataset_id]=rlist
      else:
        rlist = image_rlist[dataset_id]
      rlist.AddProxyter(rp)
    return image_rlist
    
  def import_state0(self):
    fname=""
    if(self.ref_cycle_==0):
      fname=os.path.join(self.wdir_,"merge_%02d.mtz"%self.ref_cycle_)
    else:
      fname=os.path.join(self.wdir_,"merge_llfit%02d.mtz"%(self.ref_cycle_-1))
    if not os.path.isfile(fname):
      raise Exception("cannot find %s" %fname)
    LogInfo("importing %s"%fname)
    self.rlist_merge0_ = ex.ImportMtz(fname)
    self.image_rlist0_ = self.dissect_merged(self.rlist_merge0_)

  def import_state1(self):
    fname=os.path.join(self.wdir_,"merge_scaled%02d.mtz"%self.ref_cycle_)
    if not os.path.isfile(fname):
      raise Exception("cannot find %s" %fname)
    LogInfo("importing %s"%fname)
    self.rlist_merge1_ = ex.ImportMtz(fname)
    self.image_rlist1_ = self.dissect_merged(self.rlist_merge1_)

  def import_state2(self):
    fname=os.path.join(self.wdir_,"merge_tgref%02d.mtz"%self.ref_cycle_)
    if not os.path.isfile(fname):
      raise Exception("cannot find %s" %fname)
    LogInfo("importing %s"%fname)
    self.rlist_merge2_ = ex.ImportMtz(fname)
    self.image_rlist2_ = self.dissect_merged(self.rlist_merge2_)

  def import_state3(self):
    fname=os.path.join(self.wdir_,"merge_llfit%02d.mtz"%self.ref_cycle_)
    if not os.path.isfile(fname):
      raise Exception("cannot find %s" %fname)
    LogInfo("importing %s"%fname)
    self.rlist_merge3_ = ex.ImportMtz(fname)
    self.image_rlist3_ = self.dissect_merged(self.rlist_merge3_)

  def calc_rmerge(self,rlist_merge,image_rlist):
    rmerge_calc = ex.alg.RMergeCalculator(rlist_merge,"iobs","sigiobs",
                                          self["calc_rmerge_zlim"],self["scale_rlow"],
                                          self["scale_rhigh"],self["scale_bcount"])
    rmerge_calc.SetSigmaRejectionLevel(self["calc_rmerge_rej_level"])

    image_rlist_keys=image_rlist.keys()
    image_rlist_keys.sort()
    for dataset_id in image_rlist_keys:
      dataset_rlist=image_rlist[dataset_id]
      LogInfo("Dataset %06d: Rmerge values"%(dataset_id))
      rmerge_calc.Apply(dataset_rlist,"iobs","sigiobs")
      LogInfo("  rlow  rhigh  rmerge  wrmerge  count")
      for bb in range(self["scale_bcount"]):
        LogInfo("  %5.2f %5.2f   %4.4g  %4.4g (%d)"%(rmerge_calc.GetBinLowerResolutionLimit(bb),
                                                     rmerge_calc.GetBinLowerResolutionLimit(bb+1),
                                                     rmerge_calc.GetRFBin(bb),
                                                     rmerge_calc.GetWRFBin(bb),
                                                     rmerge_calc.GetRCountBin(bb)))
      LogInfo("%06d overall:  RMerge = %4.4g wRMerge =  %4.4g (N=%d)"%(dataset_id,rmerge_calc.GetRF(),
                                                                       rmerge_calc.GetWRF(),
                                                                       rmerge_calc.GetRCount()))
      LogInfo("")

    rmerge_calc.Apply(rlist_merge,"iobs","sigiobs")
    LogInfo("Overall RMerge for all datasets")
    for bb in range(self["scale_bcount"]):
      LogInfo("  %5.2f %5.2f   %4.4g  %4.4g (%d)"%(rmerge_calc.GetBinLowerResolutionLimit(bb),
                                                   rmerge_calc.GetBinLowerResolutionLimit(bb+1),
                                                   rmerge_calc.GetRFBin(bb),rmerge_calc.GetWRFBin(bb),
                                                   rmerge_calc.GetRCountBin(bb)))
    LogInfo("Total: RMerge = %2.4g  wRMerge= %2.4g"%(rmerge_calc.GetRF(),rmerge_calc.GetWRF()))
