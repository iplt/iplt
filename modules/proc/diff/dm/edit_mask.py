#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sip
import sys,math,os.path,time
from ost.img import *
from view_base import ViewBase
import iplt as ex
import iplt.gui 
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost.geom import *
from ost.info import *
from ost import gui
from ost.info import *
from ost.io import LoadImage


class EditMask(ViewBase):
  def __init__(self,pinfo,task_widget=None):
    ViewBase.__init__(self,pinfo)
    self.pinfo_=pinfo
    self.im_=CreateImage(Size(1,1))
    self.v_=iplt.gui.CreateDataViewer(self.im_,"None")
    self.dock_=QWidget()
    vb=QVBoxLayout()
    b=QPushButton("Clear Beam Stop Mask")
    QObject.connect(b,SIGNAL("clicked()"),self.clear_mask)
    vb.addWidget(b)
    b=QPushButton("Save Global Beam Stop Mask")
    QObject.connect(b,SIGNAL("clicked()"),self.save_global_mask)
    vb.addWidget(b)
    b=QPushButton("Save Local Beam Stop Mask")
    QObject.connect(b,SIGNAL("clicked()"),self.save_local_mask)
    vb.addWidget(b)
    b=QPushButton("Remove Local Beam Stop Mask")
    QObject.connect(b,SIGNAL("clicked()"),self.remove_local_mask)
    vb.addWidget(b)
    b=QPushButton("Clear Exclusion Mask")
    QObject.connect(b,SIGNAL("clicked()"),self.clear_exclusion_mask)
    vb.addWidget(b)
    b=QPushButton("Save Exclusion Mask")
    QObject.connect(b,SIGNAL("clicked()"),self.save_exclusion_mask)
    vb.addWidget(b)
    b=QPushButton("Remove Exlusion Mask from Info")
    QObject.connect(b,SIGNAL("clicked()"),self.remove_exclusion_mask)
    vb.addWidget(b)
    self.dock_.setLayout(vb)

    if task_widget:
      self.task_widget_=task_widget
      self.v_.AddDockWidget(gui.BPQtHandle(sip.unwrapinstance(self.task_widget_)),"Main Tasks")

    self.v_.AddDockWidget(gui.BPQtHandle(sip.unwrapinstance(self.dock_)),"Edit Mask Tasks")


  def Load(self,wdir,base):
    QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
    try:
      self.do_load(wdir,base)
    finally:
      QApplication.restoreOverrideCursor()

  def do_load(self,wdir,base):
    self.info_name_ = os.path.join(base,"info.xml")
    LogVerbose("reading info file: %s"%self.info_name_)
    self.info_ = LoadInfo(self.info_name_)
    self.info_.AddDefault(self.pinfo_)

    self.im_name_ = str(self.GetDatasetImagePath(self.info_,wdir,base))


    LogVerbose("loading %s"%self.im_name_)
    self.im_=LoadImage(self.im_name_)
    self.v_.SetData(self.im_)
    self.v_.Recenter()
    self.v_.SetName(self.im_name_)
    self.v_.Renormalize()
    self.v_.Show()
    self.v_.ClearOverlays()
    self.exclusion_mask_overlay_=gui.MaskOverlay()
    self.exclusion_mask_overlay_.SetName("Exclusion Mask")
    self.mask_overlay_=gui.MaskOverlay()
    self.mask_overlay_.SetName("Beamstop Mask")
    if self.info_.Root().HasGroup("DiffProcessing/BeamStopMask"):
      LogVerbose("reading beam stop mask from info")
      bs_mask = InfoToMask(self.info_.Root().GetGroup("DiffProcessing/BeamStopMask"))
      if self.info_.Root().HasGroup("DiffProcessingResults/DiffFindMask/Shift"):
        g=self.info_.Root().GetGroup("DiffProcessingResults/DiffFindMask/Shift")
        mshift=Vec2(g.GetItem("x").AsFloat(),g.GetItem("y").AsFloat())
        self.mask_overlay_.SetShift(mshift);
      if bs_mask:
        self.mask_overlay_.SetMask(bs_mask)
    if self.info_.Root().HasGroup("DiffProcessing/ExclusionMask"):
      LogVerbose("reading exclusion mask from info")
      exclusion_mask = InfoToMask(self.info_.Root().RetrieveGroup("DiffProcessing/ExclusionMask"))
      self.exclusion_mask_overlay_.SetMask(exclusion_mask)

    self.v_.AddOverlay(self.exclusion_mask_overlay_)
    self.v_.AddOverlay(self.mask_overlay_)

    LogVerbose("done")

  def clear_mask(self):
    self.mask_overlay_.ClearMask()

  def clear_exclusion_mask(self):
    self.exclusion_mask_overlay_.ClearMask()

  def save_global_mask(self):
    mask=self.mask_overlay_.GetMask()
    mshift=self.mask_overlay_.GetShift()
    mask.Shift(mshift)
    if self.pinfo_.Root().HasGroup("DiffProcessing/BeamStopMask"):
      self.pinfo_.Root().Remove("DiffProcessing/BeamStopMask")
    g=self.pinfo_.Root().RetrieveGroup("DiffProcessing/BeamStopMask")
    MaskToInfo(mask,g)
    self.pinfo_.Export("project.xml")

  def save_local_mask(self):
    mask=self.mask_overlay_.GetMask()
    mshift=self.mask_overlay_.GetShift()
    mask.Shift(mshift)
    if self.info_.Root().HasGroup("DiffProcessing/BeamStopMask"):
      self.info_.Root().Remove("DiffProcessing/BeamStopMask")
    g=self.info_.Root().RetrieveGroup("DiffProcessing/BeamStopMask")
    MaskToInfo(mask,g)
    g=self.info_.Root().GetGroup("DiffProcessingResults/DiffFindMask/Shift")
    g.GetItem('x').SetFloat(0)
    g.GetItem('y').SetFloat(0)
    g.SetAttribute("modified",time.asctime())
    self.info_.Export(self.info_name_)
    
  def remove_local_mask(self):
    if self.info_.Root().HasGroup("DiffProcessing/BeamStopMask"):
      self.info_.Root().Remove("DiffProcessing/BeamStopMask")
      self.info_.Export(self.info_name_)

  def save_exclusion_mask(self):
    mask=self.exclusion_mask_overlay_.GetMask()
    mshift=self.exclusion_mask_overlay_.GetShift()
    mask.Shift(mshift)
    if self.info_.Root().HasGroup("DiffProcessing/ExclusionMask"):
      self.info_.Root().Remove("DiffProcessing/ExclusionMask")
    g=self.info_.Root().RetrieveGroup("DiffProcessing/ExclusionMask")
    MaskToInfo(mask,g)
    self.info_.Export(self.info_name_)
    
  def remove_exclusion_mask(self):
    if self.info_.Root().HasGroup("DiffProcessing/ExclusionMask"):
      self.info_.Root().Remove("DiffProcessing/ExclusionMask")
      self.info_.Export(self.info_name_)
    
