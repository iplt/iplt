#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sip
import sys,math,os.path
from ost.img import *
from view_base import ViewBase
import iplt as ex
import iplt.gui as exgui
from ost.geom import *
from ost.info import *
from ost.io import LoadImage
from ost import gui

class ViewSearchAndExtract(ViewBase):
    def __init__(self,pinfo,task_widget=None):
        ViewBase.__init__(self,pinfo)
        self.suc_=ex.InfoToSpatialUnitCell(pinfo.Root().GetGroup("DiffProcessing/OrigUnitCell"))
        self.sampling_=pinfo.Root().GetItem("DiffProcessing/Sampling").AsFloat()
        self.bs_mask_=None
        self.pinfo_=pinfo
        self.excl_mask_=None

        if pinfo.Root().HasGroup("DiffProcessing/ExclusionMask"):
            self.excl_mask_=InfoToMask(pinfo.Root().GetGroup("DiffProcessing/ExclusionMask"))
            
        self.im_=CreateImage(Size(1,1))
        self.v1_=exgui.CreateDataViewer(self.im_)
        self.v2_=None
        self.dv2_=None
        self.vi_=None

        #self.dock_=QWidget()
        #vb=QVBoxLayout()
        #b=QPushButton("Load DiffVec Image")
        #QObject.connect(b,SIGNAL("clicked()"),self.LoadDiffVec)
        #vb.addWidget(b)
        #b=QPushButton("Load BG Corrected Image")
        #QObject.connect(b,SIGNAL("clicked()"),self.LoadTmpInt)
        #vb.addWidget(b)
        #self.dock_.setLayout(vb)

        #if task_widget:
        #    self.task_widget_=task_widget
        #    self.v1_.AddDockWidget(gui.BPQtHandle(sip.unwrapinstance(self.task_widget_)),"Main Tasks")

        #self.v1_.AddDockWidget(gui.BPQtHandle(sip.unwrapinstance(self.dock_)),"Extra Tasks")


    def LoadDiffVec(self):
        if os.path.exists(self.vecimg_name_):
            self.vi_=LoadImage(self.vecimg_name_)
            exgui.CreateDataViewer(self.vi_,"Difference Vector Image")

    def LoadTmpInt(self):
        if os.path.exists(self.intimg_name_):
            self.v2_=LoadImage(self.intimg_name_)
            self.dv2_=exgui.CreateDataViewer(self.v2_,"Background Corrected Integration")
            if self.ext_rov_:
                self.dv2_.AddOverlay(self.ext_rov_)
            if self.int_rov_:
                self.dv2_.AddOverlay(self.int_rov_)

    def Load(self,wdir,base):
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        try:
            self.do_load(wdir,base)
        finally:
            QApplication.restoreOverrideCursor()

    def do_load(self,wdir,base):
        self.dv2_=None
        self.v2_=None
        self.vi_=None
        
        info_name = os.path.join(base,"info.xml")
        inf = LoadInfo(info_name)
        inf.AddDefault(self.pinfo_)
        self.im_name_ = str(self.GetDatasetImagePath(inf,wdir,base))

        mtz_name1 = os.path.join(base,base+"_ext.mtz")
        mtz_name2 = os.path.join(base,base+"_int.mtz")
        plist_name = os.path.join(base,"tmp_search_peaks.dat")

        self.base_=base
        self.vecimg_name_ = os.path.join(base,"tmp_search_vecimg.png")
        self.intimg_name_ = os.path.join(base,"tmp_integrate2.tif")
        self.int_mtz_name_=mtz_name2
        self.int_rov_=None
        self.ext_rov_=None

        lat1=None
        lat2=None
        lat3=None
        if inf.Root().HasGroup("DiffProcessingResults/InitialLattice"):
            lat1 = ex.InfoToLattice(inf.Root().GetGroup("DiffProcessingResults/InitialLattice"))
        if inf.Root().HasGroup("DiffProcessingResults/VerifiedLattice"):
            lat2 = ex.InfoToLattice(inf.Root().GetGroup("DiffProcessingResults/VerifiedLattice"))
        if inf.Root().HasGroup("DiffProcessingResults/RefinedLattice"):
            lat3 = ex.InfoToLattice(inf.Root().GetGroup("DiffProcessingResults/RefinedLattice"))

        pg2d=None
        if inf.Root().HasGroup("DiffProcessingResults/Origin/ParamsGauss2D"):
            pg2d=alg.InfoToGauss2D(inf.Root().GetGroup("DiffProcessingResults/Origin"))

        if inf.Root().HasGroup("DiffProcessing/BeamStopMask"):
            self.bs_mask_=InfoToMask(inf.Root().GetGroup("DiffProcessing/BeamStopMask"))
        mshift=None
        if inf.Root().HasGroup("DiffProcessingResults/DiffFindMask/Shift"):
            g=inf.Root().GetGroup("DiffProcessingResults/DiffFindMask/Shift")
            mshift=Vec2(g.GetItem("x").AsFloat(),g.GetItem("y").AsFloat())

        plist=PointList()
        if os.path.exists(plist_name):
            plist_tmp=ImportPeakList(plist_name)
            for p in plist_tmp:
              plist.append(p)

        mtz_latfit = os.path.join(base,"tmp_latfit.mtz")
        latfitlist = None
        if os.path.isfile(mtz_latfit):
            latfitlist = ex.ImportMtz(mtz_latfit)
        
        im=LoadImage(self.im_name_)
        im.SetPixelSampling(self.sampling_)

        self.v1_.SetData(im)
        self.im_=im
        self.v1_.Recenter()
        self.v1_.SetName(self.im_name_)
        self.v1_.Renormalize()
        self.v1_.Show()
        self.v1_.ClearOverlays()

        pov=gui.PointlistOverlay(plist,"PeakSearch")
        self.v1_.AddOverlay(pov)

        if pg2d:
            go=gui.Gauss2DOverlay(pg2d)
            self.v1_.AddOverlay(go)

        if mshift and self.bs_mask_ and False:
            mask = self.bs_mask_.Clone()
            mask.Shift(mshift)
            #if self.excl_mask_:
            #    mask+=self.excl_mask_
            mo=gui.MaskOverlay()
            mo.SetMask(mask)
            self.v1_.AddOverlay(mo)

        if latfitlist:
            rov3=exgui.RListOverlay(latfitlist,"LatFit Points")
            rov3.SetReferenceCell(self.suc_)
            self.v1_.AddOverlay(rov3)

        if lat1 and False:
            lov1=exgui.LatticeOverlay(lat1,"Initial Lat")
            lov1.SetReferenceCell(self.suc_)
            self.v1_.AddOverlay(lov1)

        if lat2 and False:
            lov2=exgui.LatticeOverlay(lat2,"Verified Lat")
            lov2.SetReferenceCell(self.suc_)
            self.v1_.AddOverlay(lov2)

        if lat3:
            lov3=exgui.LatticeOverlay(lat3,"Refined Lat")
            lov3.SetReferenceCell(self.suc_)
            self.v1_.AddOverlay(lov3)

        if lat3:
            rlist1=ex.ImportMtz(mtz_name1)
            rov1=exgui.RListOverlay(rlist1,"Ext RList")
            rov1.SetReferenceCell(self.suc_)
            self.v1_.AddOverlay(rov1)
            self.ext_rov_=rov1

            rlist2=ex.ImportMtz(mtz_name2)

            #lpl=ex.alg.ConvertToLatticePointList(rlist2)
            #pl=PointList()
            #for lp in lpl:
            #    pl.append(Point(lp.LPoint().GetPosition()))

            #plov = gui.PointlistOverlay(pl,"Int PList")
            #self.v1_.AddOverlay(plov)

            rov2=exgui.RListOverlay(rlist2,"Int RList")
            rov2.SetReferenceCell(self.suc_)
            self.v1_.AddOverlay(rov2)
            self.int_rov_=rov2

       

    
