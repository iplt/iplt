#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sip
import sys,math,os.path,time
from ost.img import *
from view_base import ViewBase
import iplt as ex
import iplt.gui as exgui
from ost.info import *

def create_histogram_plot(rlist,inf):
    res_low = inf.Root().GetGroup("DiffProcessing/Scale/Resolution").GetItem("low").AsFloat()
    res_high = inf.Root().GetGroup("DiffProcessing/Scale/Resolution").GetItem("high").AsFloat()
    bcount = inf.Root().GetGroup("DiffProcessing/Scale").GetItem("BinCount").AsInt()

    blist1 = ex.alg.WeightedBin(bcount,1.0/(res_low*res_low),1.0/(res_high*res_high))
    pd2=gui.PlotData()

    for rp in rlist:
        resol=rp.GetResolution()
        rr=1.0/(resol*resol)
        iobs=rp.Get("iobs")
        sigiobs=rp.Get("sigiobs")
        blist1.Add(rr,iobs,1.0/(sigiobs*sigiobs))
        pd2.AddXYE(rr,iobs,sigiobs,str(rp.GetIndex()))

    pd1=gui.PlotData()
    for n in range(blist1.GetBinCount()):
        if blist1.GetSize(n)>0:
            pd1.AddXYE(blist1.GetLimit(n),blist1.GetAverage(n),blist1.GetStdDev(n))
        else:
            pd1.AddXYE(blist1.GetLimit(n),0.0,0.0)

    pv=gui.CreatePlotViewer()
    pv.AddData(pd2).SetSymbol(gui.PlotViewer.CIRCLE).SetSymbolSize(2).SetLineColour(gui.Colour(0,0,0)).SetFillColour(gui.Colour(255,255,255))
    pv.AddData(pd1).SetMode(gui.PlotViewer.LINE).SetColour(gui.Colour(255,0,127))
    pv.AddData(pd1).SetMode(gui.PlotViewer.CIRCLE).SetSymbolSize(3).SetColour(gui.Colour(255,0,127)).SetErrorMode(gui.PlotViewer.errormodes.LINE)
    return pv


class ViewTilt(ViewBase):
    def __init__(self,pinfo, task_widget=None):
        ViewBase.__init__(self,pinfo)
        self.suc_=ex.ExtractSpatialUnitCell(pinfo.Root().GetGroup("DiffProcessing/OrigUnitCell"))
        self.pv_=None
        self.rov_=None
        self.sampling_=pinfo.Root().GetItem("DiffProcessing/Sampling").AsFloat()
        self.info_=None

        self.im_=CreateImage(Size(1,1))
        self.v_=gui.CreateDataViewer(self.im_,"None")

        self.dock_=QWidget()
        vb=QVBoxLayout()
        b=QPushButton("Intensity Histogram")
        QObject.connect(b,SIGNAL("clicked()"),self.ShowHistogram)
        vb.addWidget(b)
        b=QPushButton("Re-Export Reflections")
        QObject.connect(b,SIGNAL("clicked()"),self.ExportRList)
        vb.addWidget(b)
        self.dock_.setLayout(vb)

        if task_widget:
            self.task_widget_=task_widget
            self.v_.AddDockWidget(gui.BPQtHandle(sip.unwrapinstance(self.task_widget_)),"Main Tasks")

        self.v_.AddDockWidget(gui.BPQtHandle(sip.unwrapinstance(self.dock_)),"View Tasks")


    def AddCustomSteps(self,proc,name):
        proc.AddStep(self.ShowHistogram,"Intensity Histogram",[])
        proc.AddStep(self.ExportRList,"Re-Export RList",[])

    def ShowHistogram(self):
        self.pv_=create_histogram_plot(self.rov_.GetReflectionList(),self.proj_info_)

    def ExportRList(self):
        print "Exporting rlist to %s and updating tilt geometry in %s"%(self.mtz_name_,self.info_name_)
        rl=self.rov_.GetReflectionList()
        ex.ExportMtz(rl,self.mtz_name_)
        tg=ex.CalcTiltFromReciprocalLattice(rl.GetLattice(),rl.GetUnitCell())
        ig=self.info_.Root().RetrieveGroup("DiffProcessingResults/InitialTiltGeometry")
        ig.SetAttribute("modified",time.asctime())
        ex.LatticeTiltGeometryToInfo(tg,ig)
        lat=rl.GetLattice()
        ig=self.info_.Root().RetrieveGroup("DiffProcessingResults/VerifiedLattice")
        ex.LatticeToInfo(lat,ig)
        ig.SetAttribute("modified",time.asctime())
        time.sleep(1) # for the refined lattice time to be a newer
        ig=self.info_.Root().RetrieveGroup("DiffProcessingResults/RefinedLattice")
        ex.LatticeToInfo(lat,ig)
        ig.SetAttribute("modified",time.asctime())
        self.info_.Export(self.info_name_)
            

    def Load(self,wdir,base):
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        try:
            self.do_load(wdir,base)
        finally:
            QApplication.restoreOverrideCursor()

    def do_load(self,wdir,base):
        self.info_name_ = os.path.join(base,"info.xml")
        self.info_ = LoadInfo(self.info_name_)

        im_name = str(self.GetDatasetImagePath(self.info_,wdir,base))

        self.rov_=None
        plot_flag=False
        if self.pv_:
            plot_flag=True
            self.pv_.Close()
            self.pv_=None
        
        self.mtz_name_ = os.path.join(base,base+"_tilt.mtz")

        im=LoadImage(im_name)
        im.SetPixelSampling(self.sampling_)
        rlist=ex.ImportMtz(self.mtz_name_)

        lat=rlist.GetLattice()

        lat_ov=exgui.LatticeOverlay(lat)
        lat_ov.SetReferenceCell(self.suc_)

        self.rov_=exgui.RListOverlay(rlist)
        self.rov_.AddResolutionRing(5.0)
        self.rov_.AddResolutionRing(4.0)
        self.rov_.AddResolutionRing(3.0)
        self.rov_.AddResolutionRing(2.0)
        self.rov_.DisplayResolutionRings(True)
        self.rov_.SetReferenceCell(self.suc_)
        
        self.v_.SetData(im)
        self.im_=im
        self.v_.Recenter()
        self.v_.SetName(im_name)
        self.v_.Renormalize()
        self.v_.Show()

        self.v_.ClearOverlays()
        
        self.v_.AddOverlay(lat_ov)
        self.v_.AddOverlay(self.rov_)
        
        if plot_flag:
            self.pv_=create_histogram_plot(rlist,self.proj_info_)

