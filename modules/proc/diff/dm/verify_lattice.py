#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Authors: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sip
import sys,math,os.path,time
from ost.img import *
from view_base import ViewBase
import iplt as ex
import iplt.gui as exgui
from ost import gui
from ost.info import *
from ost.io import LoadImage
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost.geom import *
from iplt.gui.inputdialog import *

class VerifyLattice(ViewBase):
    def __init__(self,pinfo,task_widget=None):
        ViewBase.__init__(self,pinfo)
        self.pinfo_=pinfo
        self.ucell_ = ex.ExtractSpatialUnitCell(pinfo.Root().GetGroup("DiffProcessing/OrigUnitCell"))
        self.ilname_="DiffProcessingResults/InitialLattice"
        self.rlname_="DiffProcessingResults/RefinedLattice"
        self.vlname_="DiffProcessingResults/VerifiedLattice"

        self.im_=CreateImage(Size(1,1))
        self.bs_mask_=None

        self.v_=exgui.CreateDataViewer(self.im_)

        self.dock_=QWidget()
        vb=QVBoxLayout()
        b=QPushButton("Save Verified Lattice")
        QObject.connect(b,SIGNAL("clicked()"),self.WriteLattice)
        vb.addWidget(b)
        b=QPushButton("Modify Verified Lattice")
        QObject.connect(b,SIGNAL("clicked()"),self.ModifyLattice)
        vb.addWidget(b)
        b=QPushButton("Copy Initial to Verified")
        QObject.connect(b,SIGNAL("clicked()"),self.CopyLattice1)
        vb.addWidget(b)
        b=QPushButton("Copy Refined to Verified")
        QObject.connect(b,SIGNAL("clicked()"),self.CopyLattice2)
        vb.addWidget(b)
        b=QPushButton("Save Mask Shift")
        QObject.connect(b,SIGNAL("clicked()"),self.SaveMaskShift)
        vb.addWidget(b)
        self.dock_.setLayout(vb)

        if task_widget:
            self.task_widget_=task_widget
            self.v_.AddDockWidget(gui.BPQtHandle(sip.unwrapinstance(self.task_widget_)),"Main Tasks")

        self.v_.AddDockWidget(gui.BPQtHandle(sip.unwrapinstance(self.dock_)),"Verify Tasks")

    def Load(self,wdir,base):
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        try:
            self.do_load(wdir,base)
        finally:
            QApplication.restoreOverrideCursor()

    def do_load(self,wdir,base):
        self.info_name_ = os.path.join(base,"info.xml")
        LogVerbose("reading info file: %s"%self.info_name_)
        self.info_ = LoadInfo(self.info_name_)
        self.info_.AddDefault(self.pinfo_)

        self.im_name_ = str(self.GetDatasetImagePath(self.info_,wdir,base))

        self.lat1_=None
        self.lat2_=None
        self.lat3_=None

        ts1=None
        LogVerbose("reading lattice(s) from info")
        if self.info_.Root().HasGroup(self.ilname_):
            gg1=self.info_.Root().GetGroup(self.ilname_)
            self.lat1_ = ex.InfoToLattice(gg1)
            self.lat3_ = self.lat1_
            ts1 = time.strptime(gg1.GetAttribute("modified"))
        if self.info_.Root().HasGroup(self.rlname_):
            gg2=self.info_.Root().GetGroup(self.rlname_)
            self.lat2_ = ex.InfoToLattice(gg2)
            ts2 = time.strptime(gg2.GetAttribute("modified"))
            if not ts1 or ts2>ts1:
                self.lat3_ = self.lat2_
        if self.info_.Root().HasGroup(self.vlname_):
            LogInfo("found verified lattice")
            self.lat3_ = ex.InfoToLattice(self.info_.Root().GetGroup(self.vlname_))
        else:
            LogInfo("no verified lattice found")

        LogVerbose("loading %s"%self.im_name_)
        im=LoadImage(self.im_name_)

        self.v_.SetData(im)
        self.im_=im
        self.v_.Recenter()
        self.v_.SetName(self.im_name_)
        self.v_.Renormalize()
        self.v_.Show()

        self.v_.ClearOverlays()

        self.lov1_=None
        self.lov2_=None
        self.lov3_=None
        mshift=Vec2()
        if self.info_.Root().HasGroup("DiffProcessing/BeamStopMask"):
            self.bs_mask_=InfoToMask(self.info_.Root().GetGroup("DiffProcessing/BeamStopMask"))
        if self.info_.Root().HasGroup("DiffProcessingResults/DiffFindMask/Shift"):
            g=self.info_.Root().GetGroup("DiffProcessingResults/DiffFindMask/Shift")
            mshift=Vec2(g.GetItem("x").AsFloat(),g.GetItem("y").AsFloat())
        self.mo_=None
        if mshift and self.bs_mask_:
            LogVerbose("adding mask overlay")
            self.mo_=gui.MaskOverlay()
            self.mo_.SetMask(self.bs_mask_)
            self.mo_.SetShift(mshift)
            self.v_.AddOverlay(self.mo_)

        if self.lat1_:
            LogVerbose("adding initial lattice overlay")
            self.lov1_=exgui.LatticeOverlay(self.lat1_,"Initial")
            self.lov1_.SetReferenceCell(self.ucell_)
            id=self.v_.AddOverlay(self.lov1_)
            self.v_.GetOverlayManager().SetOverlayVisibility(id,False)
        if self.lat2_:
            LogVerbose("adding refined lattice overlay")
            self.lov2_=exgui.LatticeOverlay(self.lat2_,"Refined")
            self.lov2_.SetReferenceCell(self.ucell_)
            id=self.v_.AddOverlay(self.lov2_)
            self.v_.GetOverlayManager().SetOverlayVisibility(id,False)
        if self.lat3_:
            LogVerbose("adding verified lattice overlay")
            self.lov3_=exgui.LatticeOverlay(self.lat3_,"Verified")
            self.lov3_.SetReferenceCell(self.ucell_)
            self.v_.AddOverlay(self.lov3_)
        LogVerbose("done")

    def WriteLattice(self):
        try:
            vlat=self.lov3_.GetLattice()
            gg=self.info_.Root().RetrieveGroup(self.vlname_)
            ex.LatticeToInfo(vlat,gg)
            gg.SetAttribute("modified",time.asctime())
            self.info_.Export(self.info_name_)
            print "Saving verified lattice to %s: %s"%(self.info_name_,
                                                       str(self.lov3_.GetLattice()))
        except Exception, e:
            LogError("caught exception in WriteLattice: %s"%(str(e)))

    def ModifyLattice(self):
        vlat=self.lov3_.GetLattice()
        idialog=InputDialog("Modify lattice")
        idialog.AddFloat("ax:",-1000.0,1000.0,2,vlat.GetFirst()[0])
        idialog.AddFloat("ay:",-1000.0,1000.0,2,vlat.GetFirst()[1])
        idialog.AddFloat("bx:",-1000.0,1000.0,2,vlat.GetSecond()[0])
        idialog.AddFloat("by:",-1000.0,1000.0,2,vlat.GetSecond()[1])
        idialog.AddFloat("ox:",-10000.0,10000.0,2,vlat.GetOffset()[0])
        idialog.AddFloat("oy:",-10000.0,10000.0,2,vlat.GetOffset()[1])
        idialog.AddFloat("kb (*1e-10):",-1000000.0,1000000.0,2,vlat.GetBarrelDistortion()*1.0e10)
        idialog.AddFloat("ks (*1e-10):",-1000000.0,1000000.0,2,vlat.GetSpiralDistortion()*1.0e10)
        if idialog.exec_():
          data=idialog.GetData()
          vlat.SetFirst(Vec2(data[0],data[1]))
          vlat.SetSecond(Vec2(data[2],data[3]))
          vlat.SetOffset(Vec2(data[4],data[5]))
          vlat.SetBarrelDistortion(data[6]/1.0e10)
          vlat.SetSpiralDistortion(data[7]/1.0e10)
          self.lov3_.SetLattice(vlat)

    def CopyLattice1(self):
        if self.lat1_:
            self.lov3_.SetLattice(self.lat1_)
            self.v_.UpdateView()
            
    def CopyLattice2(self):
        if self.lat2_:
            self.lov3_.SetLattice(self.lat2_)
            self.v_.UpdateView()

    def SaveMaskShift(self):
      if self.mo_:
            shift=self.mo_.GetShift()
            g=self.info_.Root().GetGroup("DiffProcessingResults/DiffFindMask/Shift")
            g.GetItem('x').SetFloat(shift[0])
            g.GetItem('y').SetFloat(shift[1])
            g.SetAttribute("modified",time.asctime())
            self.info_.Export(self.info_name_)
            print "Saving mask shift"
