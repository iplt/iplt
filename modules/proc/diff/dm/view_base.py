#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Diff Viewer base class
#
# Author: Ansgar Philippsen
#
import os.path

class ViewBase:
    def __init__(self,pinfo):
        self.proj_info_=pinfo

    def AddCustomSteps(self,proc,name):
        pass

    def DoCustomStep(self,n):
        pass

    def Load(self,wdir,base,iinfo):
        pass

    def GetDatasetImagePath(self,iinfo,wdir,base):
        image_name=base+".tif" # default
        ifm=None
        if iinfo:
            if iinfo.Root().HasItem("DiffProcessing/ImageFileMask"):
                ifm = iinfo.Root().GetItem("DiffProcessing/ImageFileMask").GetValue()
        if not ifm:
            if self.proj_info_.Root().HasItem("DiffProcessing/ImageFileMask"):
                ifm = self.proj_info_.Root().GetItem("DiffProcessing/ImageFileMask").GetValue()

        if ifm:
            pos = ifm.find("%%")
            if pos>=0:
                image_name=ifm[:pos]+base+ifm[pos+2:]
            else:
                image_name=base+ifm
                
        return os.path.join(wdir,base,image_name)
