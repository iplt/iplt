#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sip
import sys, os, os.path, time, math
from ost.img import *
import iplt as ex
import iplt.proc.wbin_scale_base as wsb
from ost import gui
import iplt.proc.plotviewer


class WBinItem(QListWidgetItem):
    def __init__(self,name,parent):
        QListWidgetItem.__init__(self,name,parent)
        self.name_=name

class WBinScaler(QWidget):
    def __init__(self,rlist,imlist,parent=None):
        QWidget.__init__(self,parent)
        self.imlist_=imlist
        self.rlow_=40.0
        self.rhigh_=4.0
        self.bcount_=12
        self.im_=None
        self.pv1_=None
        self.pv2_=None
        topl=QHBoxLayout()
        ll=QVBoxLayout()
        ll.addLayout(self.build_imlist())
        ll.addLayout(self.build_opt())
        topl.addLayout(ll)
        topl.addLayout(self.build_plot())
        self.setLayout(topl)
        self.wbin_merge_ = wsb.create_wbin(rlist,self.bcount_,self.rlow_,self.rhigh_)
        app=gui.GostyApp.Instance()
        app.perspective.main_area.AddWidget('Scaling', self)

    def OnImage(self,li,old_li):
        if not li:
            return
        if not float(li.name_) in self.imlist_:
            return
        self.list_widget_.setCurrentItem(li)
        self.im_=self.imlist_[float(li.name_)]
        self.plot_data()

    def plot_data(self):
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor));
        try:
            self.plot_data2()
        finally:
            QApplication.restoreOverrideCursor();

    def plot_data2(self):
        if not self.im_:
            return
        wbin_ds = wsb.create_wbin(self.im_,self.bcount_,self.rlow_,self.rhigh_)
        
        pd1=iplt.proc.plotviewer.PlotData()
        pd2=iplt.proc.plotviewer.PlotData()
        pd3=iplt.proc.plotviewer.PlotData()

        linfit=ex.alg.LinearFit()
        linfit.ForceOrigin(False)

        wbin_isigi_lim = 0.1

        for n in range(wbin_ds.GetBinCount()):
            x=wbin_ds.GetLimit(n)
            I2 = wbin_ds.GetAverage(n)
            S2 = wbin_ds.GetStdDev(n)
            I1 = self.wbin_merge_.GetAverage(n)
            S1 = self.wbin_merge_.GetStdDev(n)
            if I1>0.0 and I2>0.0:
                y=math.log(I1/I2)
                sigy=math.sqrt( S1*S1/(I1*I1) + S2*S2/(I2*I2) )
                pd1.x.append(x)
                pd1.y.append(y)
                pd1.ey.append(sigy)
                linfit.AddDatapoint(x,y,1.0/(sigy*sigy))

            pd2.x.append(x)
            pd2.y.append(I1)
            pd2.ey.append(S1)
            pd3.x.append(x)
            pd3.y.append(I2)
            pd3.ey.append(S2)

        linfit.Apply()

        self.pv1_.ClearData()
        self.pv2_.ClearData()


        #self.pv1_.Add(pd1,True).SetName("log I0/I").SetColor(0,0,0).SetSymbolSize(2)
        self.pv1_.scatter(pd1.x,pd1.y,label="log I0/I",color='black')
        self.pv1_.AutoscaleView()
        #self.pv2_.Add(pd2,False).SetName("merge").SetColor(0,0,255).SetMode(gui.PlotViewer.LINES)
        self.pv2_.plot(pd2.x,pd2.y,label="merge",color='blue')
        #self.pv2_.Add(pd3,True).SetName("orig").SetColor(255,0,0).SetMode(gui.PlotViewer.LINES)
        self.pv2_.plot(pd3.x,pd3.y,label="orig",color='red')
        self.pv2_.AutoscaleView()
        pdfunc=iplt.proc.plotviewer.PlotData()
        pdfunc.x.append(pd1.x[0])
        pdfunc.y.append(linfit.Estimate(pd1.x[0])[0])
        pdfunc.x.append(pd1.x[-1])
        pdfunc.y.append(linfit.Estimate(pd1.x[-1])[0])
        #self.pv1_.Add(myfunc).SetName("fit").SetColor(127,0,0)
        self.pv1_.plot(pdfunc.x,pdfunc.y,label="fit",color='darkred')
        self.pv1_.SetMaximumX(max(pd1.x)) # workaround for matplotlib bug in autoscale when hline present

        rlist_scaled=iplt.ReflectionList(self.im_) # explicit copy for scaled rlist
        for rp in rlist_scaled:
            resol=rp.GetResolution()
            (logF,siglogF) = linfit.Estimate(1.0/(resol*resol))
            F = math.exp(logF)
            rp.Set("iobs",rp.Get("iobs")*F)
            rp.Set("sigiobs",rp.Get("sigiobs")*F)

        wbin_ds2 = wsb.create_wbin(rlist_scaled,self.bcount_,self.rlow_,self.rhigh_)

        pd5=iplt.proc.plotviewer.PlotData()

        for n in range(wbin_ds2.GetBinCount()):
            x=wbin_ds2.GetLimit(n)
            I2 = wbin_ds2.GetAverage(n)
            S2 = wbin_ds2.GetStdDev(n)
            #pd5.AddXYE(x,I2,S2)
            pd5.x.append(x)
            pd5.y.append(I2)
            pd5.ey.append(S2)
            
        #self.pv2_.Add(pd5,False).SetName("scaled").SetColor(0,127,0).SetMode(gui.PlotViewer.LINES)
        self.pv2_.plot(pd5.x,pd5.y,label="scaled",color='green')
        bfactor=linfit.GetScale()
        scalefactor=math.exp(linfit.GetOffset())
        self.scalefactor_.setText("%.3f" % (scalefactor))
        self.bfactor_.setText("%.3f" % (bfactor))
        self.pv2_.SetMaximumX(max(pd2.x)) # workaround for matplotlib bug in autoscale when hline present
        
    def build_imlist(self):
        lw=QListWidget()
        for item in self.imlist_:
            li=WBinItem("%06d" % item.key(),lw)
            lw.addItem(li)
        QObject.connect(lw,SIGNAL("currentItemChanged(QListWidgetItem*,QListWidgetItem*)"),self.OnImage)
        vb=QVBoxLayout()
        vb.addWidget(lw)
        self.list_widget_=lw
        return vb

    def on_rlow(self,value):
        self.rlow_=value
        self.plot_data()

    def on_rhigh(self,value):
        self.rhigh_=value
        self.plot_data()

    def on_bins(self,value):
        self.bcount_=value
        self.plot_data()
        
    def build_opt(self):
        g=QGridLayout()
        g.addWidget(QLabel("Low Resolution Cutoff"),0,0)
        sb=QDoubleSpinBox()
        sb.setDecimals(1)
        sb.setRange(1.1,1000.0)
        sb.setSuffix("A")
        sb.setSingleStep(1)
        sb.setValue(self.rlow_)
        QObject.connect(sb,SIGNAL("valueChanged(double)"),self.on_rlow)
        g.addWidget(sb,0,1)
        g.addWidget(QLabel("High Resolution Cutoff"),1,0)
        sb=QDoubleSpinBox()
        sb.setDecimals(1)
        sb.setRange(1.0,999.9)
        sb.setSuffix("A")
        sb.setSingleStep(0.1)
        sb.setValue(self.rhigh_)
        sb.setValue(self.rhigh_)
        QObject.connect(sb,SIGNAL("valueChanged(double)"),self.on_rhigh)
        g.addWidget(sb,1,1)
        g.addWidget(QLabel("Resolution Bins"),2,0)
        bin_sb=QSpinBox()
        bin_sb.setRange(3,50)
        bin_sb.setValue(self.bcount_)
        QObject.connect(bin_sb,SIGNAL("valueChanged(int)"),self.on_bins)
        g.addWidget(bin_sb,2,1)
        hb=QHBoxLayout()
        b=QPushButton("Accept")
        hb.addWidget(b)
        b=QPushButton("Revert")
        hb.addWidget(b)
        g.addLayout(hb,3,0,1,2)
        return g

    def build_plot(self):
        vb=QVBoxLayout()
        self.pv1_=iplt.proc.plotviewer.PlotViewer()
        self.pv2_=iplt.proc.plotviewer.PlotViewer()
        vb.addWidget(self.pv1_)
        vb.addWidget(self.pv2_)
        g=QGridLayout()
        g.setColumnStretch(1,1)
        g.setColumnStretch(3,1)
        g.addWidget(QLabel("Scalefactor: "),0,0)
        self.scalefactor_=QLabel("")
        g.addWidget(self.scalefactor_,0,1)
        g.addWidget(QLabel("Bfactor: "),0,2)
        self.bfactor_=QLabel("")
        g.addWidget(self.bfactor_,0,3)
        vb.addLayout(g)
        return vb

