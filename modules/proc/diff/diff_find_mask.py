#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# find predefined beamstop mask in diff pattern
# 
# Author: Andreas Schenk, Ansgar Philippsen
#

from ost.img import *
from diff_iterative_subcommand import *
import time
from ost import LogVerbose,LogInfo,LogError,LogVerbose

class DiffFindMask(DiffIterativeSubcommand):
    def __init__(self):
        DiffIterativeSubcommand.__init__(self,"mask","generate mask for beam stop","diff_mask.log")

    def ExtendParser(self,command):
        command.AddOption("--clear-mask",
                          action="store_true", default=False,dest="skip_mask",
                          help="clear mask and skip detection")
        
    def ProcessDirectory(self,options):
        info_root = self.GetInfoHandle().Root()

        if options.skip_mask:
            ig=info_root.RetrieveGroup("DiffProcessingResults/DiffFindMask/Shift")
            ig.SetAttribute("modified",time.asctime())
            ig.RetrieveItem("x").SetFloat(0.0)
            ig.RetrieveItem("y").SetFloat(0.0)
            return

        
        local_sigma_size = GetIntInfoItem(info_root,
                                          "DiffProcessing/FindMask/LocalSigmaSize",
                                          4)
        local_sigma_level = GetFloatInfoItem(info_root,
                                             "DiffProcessing/FindMask/LocalSigmaLevel",
                                             8.0)
        fast_flag = GetBoolInfoItem(info_root,
                                    "DiffProcessing/FindMask/PreShrink",
                                    False)
        
        cut_flag = GetBoolInfoItem(info_root,
                                    "DiffProcessing/FindMask/PreCut",
                                    False)
        
        mode = GetIntInfoItem(info_root,
                              "DiffProcessing/FindMask/Mode",
                              0)
        invert_flag = GetBoolInfoItem(info_root,
                                      "DiffProcessing/FindMask/Invert",
                                      False)

        histo_multiplier = GetFloatInfoItem(info_root,
                                            "DiffProcessing/FindMask/HistoMultiplier",
                                            1.2)

        min_cutoff_flag = info_root.HasItem("DiffProcessing/FindMask/MinCutoff")
        if min_cutoff_flag:
            min_cutoff = GetFloatInfoItem(info_root,
                                          "DiffProcessing/FindMask/MinCutoff",
                                          0.0)
        
        if mode==2 and (local_sigma_size==0 or local_sigma_level==0.0):
            LogInfo("FindMask: local sigma size or level is zero, reverting to mode 0")
            mode = 0

        
        try:
            pmax=Point()
            LogVerbose("FindMask: Loading mask")
            mask = self.GetBeamStopMask()
            if mask:
                mask=~mask
                LogVerbose("FindMask: Loading image")
                image=self.LoadDatasetImage()
                if invert_flag:
                    image*=-1
                stat=alg.Stat()
                image.Apply(stat)

                if min_cutoff_flag:
                    LogVerbose("FindMask: alg.ClipMinMax(%f,%f)"%(min_cutoff,stat.GetMaximum()))
                    image.ApplyIP(alg.ClipMinMax(min_cutoff,stat.GetMaximum()))
                    image.Apply(stat)
                
                LogVerbose("FindMask: CreateImage")
                imask=CreateImage(image.GetExtent())
                LogVerbose("FindMask: alg.Fill")
                imask.ApplyIP(alg.Fill(stat.GetMean()))
                LogVerbose("FindMask: alg.MaskImage")
                imask.ApplyIP(alg.MaskImage(mask))
                
                if fast_flag:
                    LogVerbose("FindMask: alg.DiscreteShrink(2,2)")
                    imask.ApplyIP(alg.DiscreteShrink(Size(2,2)))
                    image.ApplyIP(alg.DiscreteShrink(Size(2,2)))

                if cut_flag:
                    new_size = Size(image.GetExtent().GetSize()[0]/2,
                                    image.GetExtent().GetSize()[1]/2)
                    new_ext = Extent(new_size,image.GetExtent().GetCenter())
                    LogVerbose("FindMask: resizing to %s"%(str(new_ext)))
                    image = image.Extract(new_ext)
                    imask = imask.Extract(new_ext)
                                    

                if mode==0:
                    # new histogram based mode, default
                    LogVerbose("FindMask: determining limits")
                    nrval=2500
                    histo=alg.Histogram(nrval,stat.GetMinimum(),stat.GetMaximum())
                    image.Apply(histo)
                    maxbin=0
                    bincount=0
                    maxbincount=0
                    for bin in histo.GetBins():
			if bin>=maxbin:
                            maxbin=bin
                            maxbincount=bincount
                        bincount+=1

                    clmin = stat.GetMinimum()
                    clmax = histo_multiplier*(stat.GetMaximum()-stat.GetMinimum())/float(nrval)*float(maxbincount-1)+stat.GetMinimum()
                    LogVerbose("FindMask: alg.ClipMinMax(%f,%f)"%(clmin,clmax))
                    image.ApplyIP(alg.ClipMinMax(clmin,clmax))

                elif mode==1:
                    # old CC with only a gaussian smoothing on the mask
                    LogVerbose("FindMask: alg.GaussianFilter on mask")
                    imask.ApplyIP(alg.GaussianFilter(1.0))
                    
                elif mode==2:
                    # new LocalSigmaThreshold mode
                    LogVerbose("FindMask: alg.LocalSigmaThreshold(%d,%f)"%(local_sigma_size,
                                                                        local_sigma_level))
                    image.ApplyIP(alg.LocalSigmaThreshold(local_sigma_size,
                                                          local_sigma_level))
                    imask.ApplyIP(alg.LocalSigmaThreshold(local_sigma_size,0.1))
                else:
                    raise Error("FindMask: unsupported mode %d"%(mode))

                #if GetVerbosityLevel()>3:
                #    SaveImage(image,self.GetFilePath("tmp_mask_img.tif"))
                #    SaveImage(imask,self.GetFilePath("tmp_mask_mask.tif"))


                image.CenterSpatialOrigin()
                imask.CenterSpatialOrigin()

                LogVerbose("FindMask: alg.CrossCorrelate")
                cc=alg.CrossCorrelate(imask)
                if invert_flag:
                    image*=-1
                ccim=image.Apply(cc)
                ccim.CenterSpatialOrigin()
                #if GetVerbosityLevel()>3:
                #    SaveImage(ccim,self.GetFilePath("tmp_mask_cc.tif"))

                LogVerbose("FindMask: extracting shift")
                s=alg.Stat()
                ccim=ccim.Extract(Extent(Size(ccim.GetExtent().GetSize()[0]/2,ccim.GetExtent().GetSize()[1]/2),Point(0,0)))
                ccim.Apply(s)
                pmax=s.GetMaximumPosition()
                if fast_flag:
                    pmax=Point(pmax[0]*2,pmax[1]*2)

                LogVerbose("FindMask: found %s"%str(pmax))
            else:
                pass
                LogVerbose("FindMask: no beamstop mask found in project.xml, skipping")


            ig=self.GetInfoHandle().Root().RetrieveGroup("DiffProcessingResults/DiffFindMask/Shift")
            ig.SetAttribute("modified",time.asctime())
            iit=ig.RetrieveItem("x")
            iit.SetFloat(pmax[0])
            iit=ig.RetrieveItem("y")
            iit.SetFloat(pmax[1])
        except Exception, e:
            LogError("caught exception in DiffMask: %s"%(str(e)))
            pass
    def UpToDate(self,options):
        if options.skip_mask:
            return False
        """return true if origin state is up to date"""
        return self.info_handle_.Root().HasGroup("DiffProcessingResults/DiffFindMask/Shift") and not options.force_flag
        # todo: handle modified tag

    def CheckPreCondition(self,options):
        """
        returns true if pre-condition is met to run find mask
        """
        #if not os.path.isfile(self.GetFilePath("info.xml")):
        #    LogInfo("DiffMask: missing info.xml")
        #    return False
        #if self.info_handle_.Root().HasGroup("DiffProcessing/BeamStopMask"):
        #    return True
        #else:
        #    LogInfo("DiffMask: missing mask definition")
        #    return False
        return True
    
