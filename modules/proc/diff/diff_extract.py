#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Gaussian profiles of diffraction peaks
# and subsequent lattice refinement
#
# Authors: Ansgar Philippsen, Andreas Schenk
#
import sys,os,os.path
from ost.img import *
import ost.info as info
import iplt as ex
from diff_iterative_subcommand import *
import time
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost.geom import *

class DiffExtract(DiffIterativeSubcommand):
    def __init__(self):
      DiffIterativeSubcommand.__init__(self,"extract","extract reflections based on lattice","diff_extract.log")

    def ExtendParser(self,command):
        command.AddOption("--assume-verified-lattice",
                          action="store_true",default=False,dest="assume_verified",
                          help="assume initial lattice is verified")
        command.AddOption("--force-verified-lattice",
                          action="store_true",default=False,dest="force_verified",
                          help="force use of verified lattice, even if older than initial")
        command.AddOption("--ignore-refined-lattice",
                          action="store_true", default=False,dest="use_lslat",
                          help="ignore refined lattice when re-running extraction")

    def init_info_stuff(self):
        info_root=self.GetInfoHandle().Root()
        LogVerbose("DiffExtract: retrieving original unit cell info")
        if not info_root.HasGroup("DiffProcessing/OrigUnitCell"):
            raise Exception("OrigUnitCell not found in info")
        ig = info_root.GetGroup("DiffProcessing/OrigUnitCell")
        self.orig_cell_ = ex.ExtractSpatialUnitCell(ig)
        LogInfo("DiffExtract: original unit cell: "+str(self.orig_cell_))
        self.center_mask_radius_ = GetIntInfoItem(info_root,
                                                 "DiffProcessing/LatticeExtract/CenterMaskRadius",
                                                 0)
        self.peripheral_mask_radius_ = GetIntInfoItem(info_root,
                                                 "DiffProcessing/LatticeExtract/PeripheralMaskRadius",
                                                 0)
        LogInfo("using central mask radius of %d"%(self.center_mask_radius_))
        self.refine_lattice_distortions_ = GetBoolInfoItem(info_root,
                                                          "DiffProcessing/LatticeExtract/RefineLatticeDistortions",
                                                          False)
        if self.refine_lattice_distortions_:
            LogInfo("refining lattice barrel and pincushion distortions")
        else:
            LogInfo("not refining lattice barrel and pincushion distortions")

        self.re_refine_lattice_ = GetBoolInfoItem(info_root,
                                                  "DiffProcessing/LatticeExtract/ReRefineLattice",
                                                  False)

    def init_box(self):
        # semi duplicated in diff_integrate!
        self.box_size_=4
        self.box_grow_=0
        self.bg_size_=1
        gname = "DiffProcessing/LatticeExtract"
        if self.GetInfoHandle().Root().HasGroup(gname):
            ig=self.GetInfoHandle().Root().GetGroup(gname)
            if ig.HasItem("BoxSize"):
                self.box_size_=ig.GetItem("BoxSize").AsInt()
            if ig.HasItem("BoxGrowAmount"):
                self.box_grow_=ig.GetItem("BoxGrowAmount").AsInt()
            if ig.HasItem("BackgroundRimSize"):
                self.bg_size_=ig.GetItem("BackgroundRimSize").AsInt()
        LogInfo("using box-size of %d, box-grow-amount of %d, and background-rim-size of %d"%(self.box_size_,self.box_grow_,self.bg_size_))


    def ProcessDirectory(self,options):   
        self.init_info_stuff()
        image=self.LoadDatasetImage()
        mask=self.GetTotalMask()
        self.init_box()
        (lattice,lattice_type)=self.get_extract_lattice(options)
        if lattice==None:
            LogError("DiffExtract: Error: no lattice found in info file")
            return
        LogInfo("DiffExtract: lattice: %s"%lattice)
        LogInfo("DiffExtract: running lattice gaussian fit")
        lpl_gf = self.run_gaussian_fits(image,mask,lattice)
        total_box_size=self.box_size_+self.box_grow_+self.bg_size_
        lpl_gf = self.filter_area_and_mask(lpl_gf,image.GetExtent(),mask,total_box_size+2)
        if lattice_type==3 and not self.re_refine_lattice_:
            LogInfo("skipping re-refinement of lattice")
        else:
            LogInfo("DiffExtract: running lattice refinement")
            lpl_strongf = self.strong_filter_gf(lpl_gf)
            if len(lpl_strongf)>10:
                # always refine origin (last True)
                refined_lattice = ex.alg.RefineLattice(lpl_strongf,self.refine_lattice_distortions_,True)
                lpl_gf.SetLattice(refined_lattice)
                # rerun this filter after lattice refinement
                lpl_gf = self.filter_area_and_mask(lpl_gf,image.GetExtent(),mask,total_box_size+2)
                LogInfo("DiffExtract: refined lattice: %s"%(str(refined_lattice)))
                g=self.GetInfoHandle().Root().RetrieveGroup("DiffProcessingResults/RefinedLattice")
                ex.LatticeToInfo(refined_lattice,g)
                g.SetAttribute("modified",time.asctime())
                #if GetVerbosityLevel()>3:
                #    tmp_rlist = ex.alg.ConvertToReflectionList(lpl_strongf)
                #    ex.ExportMtz(tmp_rlist,self.GetFilePath("tmp_latfit.mtz"))

            else:
                LogError("DiffExtract: Warning: not enough points (%d/%d) to perform lattice refinement, using verified lattice" % (len(lpl_strongf),10))
                # remove refined lattice from info file
                if self.GetInfoHandle().Root().HasGroup("DiffProcessingResults/RefinedLattice"):
                    self.GetInfoHandle().Root().Remove("DiffProcessingResults/RefinedLattice")
            
        rlist=ex.alg.ConvertToReflectionList(lpl_gf)
        rlist.SetUnitCell(self.orig_cell_)
        ex.ExportMtz(rlist,self.GetNamedFilePath("_ext.mtz"))


    def get_extract_lattice(self,options):
        (lat1,ts1)=self.get_lattice_("DiffProcessingResults/InitialLattice")
        (lat2,ts2)=self.get_lattice_("DiffProcessingResults/VerifiedLattice")
        (lat3,ts3)=self.get_lattice_("DiffProcessingResults/RefinedLattice")
        if not options.assume_verified:
            if not lat2:
                return (None,0)
            
        if not lat3 or options.use_lslat:
            # ignore refined lattice entry
            if lat2 and (ts2>ts1 or options.force_verified):
                # verified lattice exists and is newer than initial
                LogVerbose("DiffExtract: using verified lattice")
                return (lat2,2)
            LogVerbose("DiffExtract: using initial lattice")
            return (lat1,1)
        else:
            if lat2:
                if ts2>ts3:
                    if ts2>ts1 or options.force_verified:
                        LogVerbose("DiffExtract: using verified lattice")
                        return (lat2,2)
                    else:
                        LogVerbose("DiffExtract: using initial lattice")
                        return (lat1,1)
                else:
                    LogVerbose("DiffExtract: using refined lattice")
                    return (lat3,3)
            else:
                if ts3>ts1:
                    LogVerbose("DiffExtract: using refined lattice")
                    return (lat3,3)
                else:
                    LogVerbose("DiffExtract: using initial lattice")
                    return (lat1,1)
        # should never get here
        return (lat1,1)
        
    def run_gaussian_fits(self,image,mask,lattice):
        """
        run a gaussian fit on each predicted lattice point
        and return the resulting lattice point list
        """
        # default lattice extract object
        LogInfo("DiffExtract: creating predicted lattice point list over image extent")
        lpl=ex.alg.PredictLatticePoints(lattice,image.GetExtent())

        LogInfo("DiffExtract: removing masked reflections")
        total_box_size=self.box_size_+self.box_grow_+self.bg_size_
        lpl_masked = self.filter_area_and_mask(lpl,image.GetExtent(),mask,total_box_size+2)

        LogInfo("DiffExtract: running actual extraction")
        lgextr = ex.alg.LatticeGaussianExtract(lpl_masked,total_box_size)
        image.Apply(lgextr)
        return lgextr.GetList()

    def weak_filter_gf(self,lpl):
        gname="DiffProcessing/WeakFilter"
        minimum_quality=None
        maximum_offset=None
        maximum_half_width=None
        maximum_elliptical_ratio=None
        if self.GetInfoHandle().Root().HasGroup(gname):
            g=self.GetInfoHandle().Root().GetGroup(gname)
            if g.HasItem("MinimumQuality"):
                minimum_quality=g.GetItem("MinimumQuality").AsFloat()
            if g.HasItem("MaximumOffset"):
                maximum_offset=g.GetItem("MaximumOffset").AsFloat()
            if g.HasItem("MaximumHalfWidth"):
               maximum_half_width=g.GetItem("MaximumHalfWidth").AsFloat()
            if g.HasItem("MaximumEllipticalRatio"):
               maximum_elliptical_ratio=g.GetItem("MaximumEllipticalRatio").AsFloat()
        else:
           minimum_quality=1.2
           maximum_offset=5.0
        return self.filter_gf(lpl,minimum_quality,maximum_offset,maximum_half_width,maximum_elliptical_ratio) 

    def strong_filter_gf(self,lpl):
        gname="DiffProcessing/StrongFilter"
        minimum_quality=None
        maximum_offset=None
        maximum_half_width=None
        maximum_elliptical_ratio=None
        if self.GetInfoHandle().Root().HasGroup(gname):
            g=self.GetInfoHandle().Root().GetGroup(gname)
            if g.HasItem("MinimumQuality"):
                minimum_quality=g.GetItem("MinimumQuality").AsFloat()
            if g.HasItem("MaximumOffset"):
                maximum_offset=g.GetItem("MaximumOffset").AsFloat()
            if g.HasItem("MaximumHalfWidth"):
               maximum_half_width=g.GetItem("MaximumHalfWidth").AsFloat()
            if g.HasItem("MaximumEllipticalRatio"):
               maximum_elliptical_ratio=g.GetItem("MaximumEllipticalRatio").AsFloat()
        else:
           minimum_quality=2.0
           maximum_offset=2.0
           maximum_half_width=7.0
           maximum_elliptical_ratio=2.0
        return self.filter_gf(lpl,minimum_quality,maximum_offset,maximum_half_width,maximum_elliptical_ratio)
               
            
    def filter_gf(self,lpl,minimum_quality=None,maximum_offset=None,maximum_half_width=None,maximum_elliptical_ratio=None):
        res_lpl = ex.alg.LatticePointList(lpl.GetLattice())

        global_offset=Vec2(0.0,0.0)
        global_weight=0.0
        for pp in lpl:
            off=pp.LPoint().GetFit().GetU()-pp.LPoint().GetPosition()
            wei=pp.LPoint().GetFit().GetQuality()
            global_offset+=(off*wei)
            global_weight+=wei
        global_offset/=global_weight
        LogVerbose("DiffExtract: global offset: %s"%(str(global_offset)))
        
        reject=[0,0,0,0,0,0]
        inc_count=0
        all_count=0;
        for pp in lpl:
            all_count+=1
            p=pp.LPoint()
            if minimum_quality!=None and p.GetFit().GetQuality()<minimum_quality: # goodness of fit
                reject[0]+=1
                continue
            # global offset not working as intended
            # off=p.GetFit().GetU()-p.GetPosition()-global_offset
            off=p.GetFit().GetU()-p.GetPosition()
            dx=off[0]
            dy=off[1]
            if maximum_offset!=None and (dx*dx+dy*dy)>maximum_offset*maximum_offset: # offset from predicted center
                reject[1]+=1
                continue
            bx=p.GetFit().GetBx()
            if maximum_half_width!=None and bx>maximum_half_width: # half-width of peak
                reject[2]+=1
                continue
            by=p.GetFit().GetBy()
            if maximum_half_width!=None and by>maximum_half_width: # half-width of peak
                reject[2]+=1
                continue
            bratio=bx/by
            if maximum_elliptical_ratio!=None and  (bratio>maximum_elliptical_ratio or bratio<1.0/maximum_elliptical_ratio): # elliptical ratio
                reject[3]+=1
                continue
            if p.GetFit().GetA()<0.0:
                reject[4]+=1
                continue
            if p.GetFit().GetIterationCount()==p.GetFit().GetMaxIter():
                reject[5]+=1
                continue
		
            inc_count+=1
            res_lpl.Add(pp.GetIndex(),p)
        LogVerbose("DiffExtract: filter_gf: kept %d of %d reflections"%(inc_count,all_count))
        if minimum_quality:
            LogVerbose("DiffExtract: rejected "+str(reject[0])+" due to quality<%f"% (minimum_quality))
        if maximum_offset:
            LogVerbose("DiffExtract: rejected "+str(reject[1])+" due to offset>%f"% (maximum_offset))
        if maximum_half_width:
            LogVerbose("DiffExtract: rejected "+str(reject[2])+" due to half-width>%f"% (maximum_half_width))
        if maximum_elliptical_ratio:
            LogVerbose("DiffExtract: rejected "+str(reject[3])+" due to ellipse-ratio>%f"% (maximum_elliptical_ratio))
        LogVerbose("DiffExtract: rejected "+str(reject[4])+" due to negative amplitude")
        LogVerbose("DiffExtract: rejected "+str(reject[5])+" because maximum number of iterations were reached.")
        return res_lpl

    def Cleanup(self,wdir):
        try:
            #os.remove(os.path.join(wdir,""))
            pass
        except OSError:
            pass

    def UpToDate(self,options):
        """
        returns true if diff extract state is up to date
        """
        if options.force_flag:
            return False
        if not os.path.isfile(self.GetNamedFilePath("_ext.mtz")):
            LogVerbose("missing _ext.mtz")
            return False
        ilatname="DiffProcessingResults/InitialLattice"
        vlatname="DiffProcessingResults/VerifiedLattice"
        rlatname="DiffProcessingResults/RefinedLattice"
        if self.GetInfoHandle().Root().HasGroup(rlatname):
            gg1=self.GetInfoHandle().Root().GetGroup(ilatname)
            gg2=self.GetInfoHandle().Root().GetGroup(rlatname)
            if self.InfoIsNewer(gg1,gg2):
                LogVerbose("DiffExtract: InitialLattice is newer than RefinedLattice")
                return False
            if self.GetInfoHandle().Root().HasGroup(vlatname):
                gg1=self.GetInfoHandle().Root().GetGroup(vlatname)
                if self.InfoIsNewer(gg1,gg2):
                        LogVerbose("DiffExtract: VerifiedLattice is newer than RefinedLattice")
                        return False
            return True #reflat exists and is newer than both initial and verified lat
        return False # reflat does not exist

    def CheckPreCondition(self,options):
        """
        returns true if pre-condition is met to run diff extract
        """
        if not os.path.isfile(self.GetFilePath("info.xml")):
            LogInfo("DiffExtract: missing info.xml")
            return False
        if not options.assume_verified:
            if self.GetInfoHandle().Root().HasGroup("DiffProcessingResults/VerifiedLattice"):
                if self.InfoIsNewer(self.GetInfoHandle().Root().GetGroup("DiffProcessingResults/VerifiedLattice"),
                                self.GetInfoHandle().Root().GetGroup("DiffProcessingResults/InitialLattice")):
                    return True
                else:
                    if options.force_verified:
                        return True
                    else:
                        LogInfo("DiffExtract: verified lattice older than initial lattice. Please recheck verified lattice.")
                        return False
            else:
                LogInfo("DiffExtract: missing verified lattice, skipping")
                return False
        else:
            if self.GetInfoHandle().Root().HasGroup("DiffProcessingResults/InitialLattice"):
                return True
            else:
                LogInfo("DiffExtract: missing lattice, skipping")
                return False

    def filter_area_and_mask(self,lpl,ext,mask,rad):
        res_lpl = ex.alg.LatticePointList(lpl.GetLattice())
        count1=0
        count2=0
        count3=0
        count4=0
        rad2 = self.center_mask_radius_*self.center_mask_radius_
        prad2 = self.peripheral_mask_radius_*self.peripheral_mask_radius_
        latori = lpl.GetLattice().GetOffset()
        for pp in lpl:
            pos=pp.LPoint().GetPosition()
            # check center and all corner points
            if (mask.IsInside(pos) or
                mask.IsInside(pos+Vec2(rad,rad)) or
                mask.IsInside(pos+Vec2(-rad,rad)) or
                mask.IsInside(pos+Vec2(rad,-rad)) or
                mask.IsInside(pos+Vec2(-rad,-rad))):
                count1+=1
                continue
            if not (ext.Contains(Point(pos)) and
                    ext.Contains(Point(pos+Vec2(rad,rad))) and
                    ext.Contains(Point(pos+Vec2(-rad,rad))) and
                    ext.Contains(Point(pos+Vec2(rad,-rad))) and
                    ext.Contains(Point(pos+Vec2(-rad,-rad)))):
                count2+=1
                continue
            dpos = pos-latori
            dd2 = dpos[0]*dpos[0]+dpos[1]*dpos[1]
            if dd2<rad2:
                count3+=1
                continue
            if dd2>prad2 and prad2>0:
                count4+=1
                continue
                
            res_lpl.Add(pp.GetIndex(),pp.LPoint())
        LogVerbose("DiffBase: removed %d reflections in masked region(s) (%d,%d,%d,%d)"%(count1+count2+count3+count4,count1,count2,count3,count4))
        return res_lpl
