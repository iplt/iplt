#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# plot synthetic diff image from reflection list
#

import math
from ost.img import *
import iplt as ex
from diff_iterative_subcommand import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose
import iplt


def make_diff_plot(rlist,lattice,size,factor,spread,powf):
    img = CreateImage(Size(factor*size,factor*size))
    for rp in rlist:
        pos = float(factor)*lattice.CalcPosition(rp.GetIndex().AsDuplet())
        amp2 = rp.Get("iobs")
        if amp2>0.0:
            v=math.pow(amp2,powf)
            sg = alg.SpreadGauss2D(pos,v,spread*float(factor))
            img.ApplyIP(sg)
    return img

class DiffPlot(DiffIterativeSubcommand):
    def __init__(self):
        DiffIterativeSubcommand.__init__(self,"plot","plot synthetic diffraction pattern","diff_plot.log")
        self.in_name_ = "_tilt.mtz"

    def ProcessDirectory(self,options):
        info_root=self.GetInfoHandle().Root()
        self.diff_plot_spread_ = GetFloatInfoItem(info_root,
                                                  "DiffProcessing/Plot/spread",
                                                  5.0)
        self.diff_plot_size_ = GetIntInfoItem(info_root,
                                              "DiffProcessing/Plot/size",
                                              2048)
        self.diff_plot_factor_ = GetIntInfoItem(info_root,
                                                "DiffProcessing/Plot/factor",
                                                1)
        self.diff_plot_pow_ = GetFloatInfoItem(info_root,
                                               "DiffProcessing/Plot/pow",
                                               0.5)
        LogInfo("DiffPlot: reading %s"%(self.GetNamedFilePath(self.in_name_)))
        rlist = ex.ImportMtz(self.GetNamedFilePath(self.in_name_))

        lattice = iplt.InfoToLattice(info_root.GetGroup("DiffProcessingResults/RefinedLattice"))

        LogInfo("DiffPlot: generating synthetic diff")
        diff_plot_img = make_diff_plot(rlist,lattice,
                                       self.diff_plot_size_,self.diff_plot_factor_,
                                       self.diff_plot_spread_,self.diff_plot_pow_)
        diff_plot_img*=-1 # inverting contrast
        SaveImage(diff_plot_img,self.GetNamedFilePath("_calc.png"))

    def UpToDate(self,options):
        return False

    def CheckPreCondition(self,options):
        if not os.path.isfile(self.GetFilePath("info.xml")):
            LogInfo("DiffPlot: missing info.xml")
            return False
        if not os.path.isfile(self.GetNamedFilePath(self.in_name_)):
            LogInfo("DiffPlot: missing tilt mtz file")
            return False
        return True


