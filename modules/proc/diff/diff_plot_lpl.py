#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


"""
plot resolution bins of given mtz file
"""

import sys,os,os.path
from ost.img import *
import iplt as ex
import iplt.proc.plot_fit as plot_fit

sys.argv=sys.argv[1:]

if len(sys.argv)<3:
    raise Exception("missing parameter: mtzin img target_dir")

mtzin=sys.argv[0]
dir=sys.argv[2]

try:
    os.makedirs(dir)
except OSError:
    pass

lpl=ex.alg.ConvertToLatticePointList(ex.ImportMtz(mtzin))
lat=lpl.GetLattice()

img=LoadImage(sys.argv[1])
#img.CenterSpatialOrigin()

for lpp in lpl:
    indx=lpp.GetIndex()
    #e=Extent(Size(15,15),Point(lat.CalcPosition(indx)))
    e=Extent(Size(21,21),Point(lpp.LPoint().GetPosition()))
    lpoint=lpp.LPoint()
    lpoint.SetRegion(e)
    fname=os.path.join(dir+"/lp_"+str(indx[0])+"_"+str(indx[1]))
    print "writing ",fname
    #try:
    plot_fit.PlotLatticePoint(lpoint,indx,img,fname)
    #except:
    #    print "some error occured, ignored"
