#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Symmetrize diffration dataset
#
# Authors: Ansgar Philippsen, Andreas Schenk
#

import os,os.path,sys,math
from ost.img import *
import iplt as ex
from diff_iterative_subcommand import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose

# helper functions

def do_friedel_merge(rlist,keep_nonfriedel=True,diff_sigma=False):
    """
    merge friedel mates I+ and I-, given by the formula

      I = 1/2 (I+ + I-)

      S = 1/2 sqrt(S+^2 + S-^2)

    except for Friedel Mode 3, where the new sigma is given by

      S = | S+ - S- |

    """
    rlist_out=ex.ReflectionList(rlist,False)
    rlist_out.AddProperty("iobs_plus",ex.PT_INTENSITY2)
    rlist_out.AddProperty("iobs_minus",ex.PT_INTENSITY2)
    rlist_out.AddProperty("sigiobs_plus",ex.PT_STDDEVI2)
    rlist_out.AddProperty("sigiobs_minus",ex.PT_STDDEVI2)
    tmp_img=None
    #if GetVerbosityLevel()>3:
    #    tmp_img=CreateImage(Size(80,80),Point(0,0))

    for rp1 in rlist:
        rp2=rlist.FindFirst(-rp1.GetIndex().AsDuplet())
        if rp2.IsValid():
            if rp1.GetIndex().GetZStar()>=0.0:
                i_plus=rp1.Get("iobs")
                i_minus=rp2.Get("iobs")
                i_ave = 0.5*(i_plus + i_minus)
                sig_plus=rp1.Get("sigiobs")
                sig_minus=rp2.Get("sigiobs")
                sig_ave = 0.0
                if diff_sigma:
                    sig_ave = abs(i_plus-i_minus)
                    if sig_ave<abs(i_ave*0.01):
                        sig_ave=abs(i_ave*0.01)
                else:
                    sig_ave = 0.5*(math.sqrt(sig_plus*sig_plus+sig_minus*sig_minus))

                rp_out=rlist_out.AddProxyter(rp1)
                rp_out.Set("iobs",i_ave)
                rp_out.Set("sigiobs",sig_ave)
                rp_out.Set("isigi",i_ave/sig_ave)
                rp_out.Set("iobs_plus",i_plus)
                rp_out.Set("sigiobs_plus",sig_plus)
                rp_out.Set("iobs_minus",i_minus)
                rp_out.Set("sigiobs_minus",sig_minus)

                if GetVerbosityLevel()>3:
                 #   if i_ave!=0.0:
                  #      tmp_img.SetReal(rp1.GetIndex().AsDuplet(),(i_plus-i_ave)/i_ave)
                   #     tmp_img.SetReal(rp2.GetIndex().AsDuplet(),(i_minus-i_ave)/i_ave)
                        
                    LogVerbose("%s: ip: %g (%g) im: %g (%g) iave: %g (%g %g)"%(str(rp1.GetIndex()),i_plus,sig_plus,i_minus,sig_minus,i_ave,sig_ave,i_ave/sig_ave))
                
        else:
            if keep_nonfriedel:
                rp_out=rlist_out.AddProxyter(rp1)

    if tmp_img:
        SaveImage(tmp_img,"tmp_sym_diff.tif")
    return rlist_out


def do_weighted_friedel_merge(rlist,keep_nonfriedel=True):
    """
    merge friedel mates I+ and I-, given by the formula

      I = Ip * Wp + Im * Wm

    where the weight is the normalized reciprocal of the
    variance, ie

      W+ = (sig-^2)/(sig+^2+sig-^2)
      W- = (sig+^2)/(sig+^2+sig-^2)
               ^-> sign reversal is correct!

    the stddev is then given by

      sigma = sqrt[ ( Wp * (Ip-I)^2 + Wm * (Im-I)^2 ) / (Wp+Wm) ]

    """
    rlist_out=ex.ReflectionList(rlist,False)
    rlist_out.AddProperty("iobs_plus",ex.PT_INTENSITY2)
    rlist_out.AddProperty("iobs_minus",ex.PT_INTENSITY2)
    rlist_out.AddProperty("sigiobs_plus",ex.PT_STDDEVI2)
    rlist_out.AddProperty("sigiobs_minus",ex.PT_STDDEVI2)
    for rp1 in rlist:
        rp2=rlist.FindFirst(-rp1.GetIndex().AsDuplet())
        if rp2.IsValid():
            if rp1.GetIndex().GetZStar()>=0.0:
                i_plus=rp1.Get("iobs")
                i_minus=rp2.Get("iobs")
                sig_plus=rp1.Get("sigiobs")
                sig_minus=rp2.Get("sigiobs")

                # normalized weights
                sig2sum = sig_plus*sig_plus+sig_minus*sig_minus
                w_plus=sig_minus*sig_minus/sig2sum
                w_minus=sig_plus*sig_plus/sig2sum

                # weighted average
                i_ave=(i_plus*w_plus+i_minus*w_minus)
                d_plus=i_ave-i_plus
                d_minus=i_ave-i_minus
                sig_ave = math.sqrt((w_plus*d_plus*d_plus+w_minus*d_minus*d_minus))
                
                rp_out=rlist_out.AddProxyter(rp1)
                rp_out.Set("iobs",i_ave)
                rp_out.Set("sigiobs",sig_ave)
                rp_out.Set("isigi",i_ave/sig_ave)
                rp_out.Set("iobs_plus",i_plus)
                rp_out.Set("sigiobs_plus",sig_plus)
                rp_out.Set("iobs_minus",i_minus)
                rp_out.Set("sigiobs_minus",sig_minus)
                LogVerbose("%s: ip: %g (%g) im: %g (%g) iave: %g (%g %g)"%(str(rp1.GetIndex()),i_plus,sig_plus,i_minus,sig_minus,i_ave,sig_ave,i_ave/sig_ave))

        else:
            if keep_nonfriedel:
                rp_out=rlist_out.AddProxyter(rp1)


    return rlist_out


class DiffSym(DiffIterativeSubcommand):
    def __init__(self):
        DiffIterativeSubcommand.__init__(self,"sym","apply symmetry to individual image dataset","diff_sym.log")
        self.in_name_ = "_tilt.mtz"
        self.out_name_ = "_sym.mtz"

    def ProcessDirectory(self,options):
        sym_alg=None
        sym_name="P 1"
        if self.GetInfoHandle().Root().HasItem('DiffProcessing/OrigUnitCell/unitcell/symmetry'):
            sym_name = self.GetInfoHandle().Root().GetItem('DiffProcessing/OrigUnitCell/unitcell/symmetry').GetValue()
            LogInfo("using symmetry from info file: %s"%sym_name)
        else:
            LogInfo("using default P1 symmetry")

        sym_alg = ex.alg.Symmetrize(sym_name)

        merge_friedel=False
        keep_non_friedel=True
        friedel_diff_sigma=False
            
        if self.GetInfoHandle().Root().HasItem("DiffProcessing/Symmetrize/FriedelMode"):
            friedel_mode = self.GetInfoHandle().Root().GetItem("DiffProcessing/Symmetrize/FriedelMode").AsInt()
            if friedel_mode == 1:
                merge_friedel=True
                keep_non_friedel=True
                friedel_diff_sigma = False
            elif friedel_mode == 2:
                merge_friedel = True
                keep_non_friedel = False
                friedel_diff_sigma = False
            elif friedel_mode == 3:
                merge_friedel = True
                keep_non_friedel = False
                friedel_diff_sigma = True
            else:
                merge_friedel=False
                keep_non_friedel=True
                friedel_diff_sigma = False


        LogVerbose("importing %s"%self.in_name_)
        rlist = ex.ImportMtz(self.GetNamedFilePath(self.in_name_))

        rsym_zlim = GetFloatInfoItem(self.GetInfoHandle().Root(),
                                     "DiffProcessing/Symmetrize/zlim",
                                     0.01)
        
        if merge_friedel:
            if keep_non_friedel:
                LogInfo("merging friedel mates, keeping unpaired reflections")
            else:
                LogInfo("merging friedel mates, removing unpaired reflections")
            if friedel_diff_sigma:
                LogInfo("constructing artificial sigma from friedel difference")
            rlist=do_friedel_merge(rlist,keep_non_friedel,friedel_diff_sigma)

        # store original indices
        rlist.AddProperty("symh")
        rlist.AddProperty("symk")
        rlist.AddProperty("symzstar")
        for rp in rlist:
            index=rp.GetIndex()
            rp.Set("symh",float(index.GetH()))
            rp.Set("symk",float(index.GetK()))
            rp.Set("symzstar",float(index.GetZStar()))

        LogVerbose("running rsym")

        rsymc = ex.alg.RSymCalculator(ex.Symmetry(sym_name),rsym_zlim,40.0,2.0,16)
        rsymc.Apply(rlist,"iobs","sigiobs")
        rsym = rsymc.GetRF();
        wrsym = rsymc.GetWRF();
        rsym_count = rsymc.GetRCount()

        LogInfo("Rsym: %g  wRsym: %g  (%d)"%(rsym,wrsym,rsym_count))

        ig=self.GetInfoHandle().Root().RetrieveGroup("DiffProcessingResults")
        ig.SetAttribute("modified",time.asctime())
        ig.RetrieveItem("RSym").SetFloat(rsym)
        ig.RetrieveItem("WRSym").SetFloat(wrsym)
        ig.RetrieveItem("RSymCount").SetInt(rsym_count)

        LogInfo("symmetrizing")
        rlist_sym = rlist.Apply(sym_alg)
        LogVerbose("exporting %s"%self.out_name_)
        ex.ExportMtz(rlist_sym,self.GetNamedFilePath(self.out_name_))

    def Cleanup(self,wdir):
        try:
            #os.remove(os.path.join(wdir,""))
            pass
        except OSError:
            pass
        
    def UpToDate(self,options):
        if options.force_flag:
            return False
        return self.FileIsNewer(self.GetNamedFilePath(self.out_name_),self.GetNamedFilePath(self.in_name_))

    def CheckPreCondition(self,options):
        if not os.path.isfile(self.GetFilePath("info.xml")):
            LogInfo("DiffSym: missing info.xml")
            return False
        if not os.path.isfile(self.GetNamedFilePath(self.in_name_)):
            LogInfo("DiffSym: missing _tilt.mtz")
            return False
        return True
