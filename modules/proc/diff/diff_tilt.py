#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# calculate tilt geometry from lattice
# and original unit cell, add zstar and
# resol properties to reflection list
#
# Authors: Ansgar Philippsen, Andreas Schenk
#

import math,re
import sys,os,os.path,time
from ost.img import *
import iplt as ex
import ost.info as info
import iplt.proc.calc_friedel as calc_friedel
from diff_iterative_subcommand import *
from diff_plot import make_diff_plot
from ost import LogVerbose,LogInfo,LogError,LogVerbose

class DiffTilt(DiffIterativeSubcommand):
    def __init__(self):
        DiffIterativeSubcommand.__init__(self,"tilt","determine tilt geometry and generate z*","diff_tilt.log")
        self.in_name_ = "_int.mtz"
        self.out_name_ = "_tilt.mtz"

    def ExtendParser(self,command):
        command.AddOption("--ignore-verified-tilt",
                          action="store_true", default=False,dest="ignore_vtilt",
                          help="ignore verified tilt when running diff_tilt")

    def ProcessDirectory(self,options):
        try:
            rlist_ori = ex.ImportMtz(self.GetNamedFilePath(self.in_name_))
            try:
                os.remove(self.GetNamedFilePath(self.out_name_))
            except OSError:
                pass
            self.init_info_stuff()

            tg=None
            # the verified tilt geometry is only used when it is newer
            # than the initial one
            iinfo = self.GetInfoHandle().Root()
            
            if iinfo.HasGroup("DiffProcessingResults/VerifiedTiltGeometry") and not options.ignore_vtilt:
                ig=iinfo.RetrieveGroup("DiffProcessingResults/VerifiedTiltGeometry")
                if iinfo.HasGroup("DiffProcessingResults/InitialTiltGeometry"):
                    ig2=iinfo.GetGroup("DiffProcessingResults/InitialTiltGeometry")
                    if self.InfoIsNewer(ig,ig2):
                        tg=ex.InfoToLatticeTiltGeometry(ig)
                        LogVerbose("using verified tilt geometry")
                else:
                    tg=ex.InfoToLatticeTiltGeometry(ig)
                    LogVerbose("using verified tilt geometry")

            if not tg:
                tg = ex.CalcTiltFromReciprocalLattice(self.lattice_,self.orig_cell_)
                LogVerbose("re-calculating tilt geometry from lattice %s"%(self.lattice_))
                ig=iinfo.RetrieveGroup("DiffProcessingResults/InitialTiltGeometry")
                ig.SetAttribute("modified",time.asctime())
                ex.LatticeTiltGeometryToInfo(tg,ig)

                

            # calculates zstar and resol
            rlist_ori.SetLattice(self.lattice_) # just... to make sure
            LogVerbose("using tilt geometry: %s and lattice %s to generate z*"%(tg,rlist_ori.GetLattice()))
            rlist_ori_z = rlist_ori.Apply(ex.alg.Tilter(tg))

            rlist_final=ex.ReflectionList(self.lattice_,self.orig_cell_)
            rlist_final.AddProperty("iobs", ex.PT_INTENSITY)
            rlist_final.AddProperty("sigiobs",ex.PT_STDDEV)
            rlist_final.AddProperty("isigi",ex.PT_WEIGHT)
            rlist_final.AddProperty("weight",ex.PT_WEIGHT)
            rlist_final.AddProperty("bg",ex.PT_REAL)
            rlist_final.AddProperty("sigbg",ex.PT_REAL)
            rlist_final.AddProperty("averatio",ex.PT_REAL)
            rlist_final.AddProperty("sigmazstar",ex.PT_REAL)
            for rp in rlist_ori_z:
                idx=rp.GetIndex()
                if idx.GetH()==0 and idx.GetK()==0:
                    continue
                resol=rp.GetResolution()
                if resol<=self.res_low_ and resol>=self.res_high_:
                    iobs=rp.Get("pi_volume")
                    sigiobs=rp.Get("pi_sigma")    
                    rpnew=rlist_final.AddReflection(idx)
                    rpnew.Set("iobs",iobs)
                    rpnew.Set("sigiobs",sigiobs)
                    if sigiobs>0.0:
                        rpnew.Set("isigi",iobs/sigiobs)
                    else:
                        rpnew.Set("isigi",0.0)
                    rpnew.Set("weight",1.0) # not calculated here
                    rpnew.Set("bg",rp.Get("pi_background"))
                    rpnew.Set("sigbg",rp.Get("pi_bgsigma"))
                    rpnew.Set("averatio",rp.Get("pi_averatio"))
                    rpnew.Set("sigmazstar",rp.Get("sigmazstar"))

            (rf1,rf2) = calc_friedel.calc_friedel(rlist_final,"iobs","sigiobs")
            LogInfo("DiffTilt: RFriedel (weighted, non-weighted) from %d reflections: %f, %f"%(len(rlist_final),rf1,rf2))

            ex.ExportMtz(rlist_final,self.GetNamedFilePath(self.out_name_))
            ig=iinfo.RetrieveGroup("DiffProcessingResults/InitialTiltGeometry")
            ig.SetAttribute("modified",time.asctime())
            ex.LatticeTiltGeometryToInfo(tg,ig)
            ig=iinfo.RetrieveGroup("DiffProcessingResults")
            ig.SetAttribute("modified",time.asctime())
            ig.RetrieveItem("RFriedel").SetFloat(rf1)
            ig.RetrieveItem("RFriedel2").SetFloat(rf2)
        except Exception,inst:
            LogError("DiffTilt: caught exception during tilt for "+self.GetWorkingDirectory()+": "+str(inst))
            raise

    def init_info_stuff(self):
            info_root=self.GetInfoHandle().Root()
            LogVerbose("DiffTilt: retrieving original unit cell info")
            ig = info_root.GetGroup("DiffProcessing/OrigUnitCell")
            self.orig_cell_ = ex.ExtractSpatialUnitCell(ig)
            self.thickness_ = self.orig_cell_.GetC()
            LogInfo("DiffTilt: original unit cell: "+str(self.orig_cell_))
            LogVerbose("DiffTilt: retrieving sampling info")
            try:
                self.sampling_ = float(info_root.GetItem("DiffProcessing/Sampling").GetValue())
            except:
                raise Exception("sampling entry not found in info")

            LogVerbose("DiffTilt: retrieving resolution limit info")
            try:
                rlim = info_root.GetGroup("DiffProcessing/ResLim")
            except:
                raise Exception("ResLim entry not found in info")
            self.res_low_=float(rlim.GetItem("rlow").GetValue())
            self.res_high_=float(rlim.GetItem("rhigh").GetValue())
            (self.lattice_,dummy)=self.get_lattice_("DiffProcessingResults/RefinedLattice")
            if not self.lattice_:
                raise Exception("missing refined lattice")
            LogVerbose("DiffTilt: lattice: %s"%(str(self.lattice_)))

    def Cleanup(self,wdir):
        try:
            #os.remove(os.path.join(wdir,""))
            pass
        except OSError:
            pass

    def UpToDate(self,options):
        if options.force_flag:
            return False
        return self.FileIsNewer(self.GetNamedFilePath(self.out_name_),self.GetNamedFilePath(self.in_name_))

    def CheckPreCondition(self,options):
        if not os.path.isfile(self.GetFilePath("info.xml")):
            LogInfo("DiffTilt: missing info.xml")
            return False
        info_root=self.GetInfoHandle().Root()
        if not info_root.HasGroup("DiffProcessing/OrigUnitCell"):
            LogInfo("OrigUnitCell not found in info")
            return False
        if not info_root.HasGroup("DiffProcessingResults/RefinedLattice"):
            LogInfo("missing lattice")
            return False
        if not os.path.isfile(self.GetNamedFilePath(self.in_name_)):
            LogInfo("DiffTilt: missing integrated mtz file, skipping")
            return False
        return True

