#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

import os,os.path
from ost.img import *
import iplt as ex

class DiffImgBase:
  def __init__(self):
    self.pdict_={}

  def __getitem__(self,x):
    return self.pdict_[x]

  def __setitem__(self,x,v):
    self.pdict_[x]=v

  def LoadParameters(self,pinfo):

    info_root=pinfo.Root()
    
    # grab parameters from info
    # required: the image mask to work with
    if not info_root.HasItem("DiffProcessing/ImageMask"):
      raise Exception("ImageMask not found in info")
    self["img_mask"] = info_root.GetItem("DiffProcessing/ImageMask").GetValue()
    self["img_mask_pref"] = self["img_mask"][:3]

    # required: the unit cell
    if not info_root.HasGroup("DiffProcessing/OrigUnitCell"):
      raise Exception("OrigUnitCell not found in info")
    ig= info_root.GetGroup("DiffProcessing/OrigUnitCell")
    self["ucell"] = ex.InfoToSpatialUnitCell(ig)

    # global resolution limit
    self["rlow"] = GetFloatInfoItem(info_root,
                                      "DiffProcessing/ResLim/rlow",
                                      100.0)
    self["rhigh"] = GetFloatInfoItem(info_root,
                                       "DiffProcessing/ResLim/rhigh",
                                       2.0)

    # lattice search stuff
    gname="DiffProcessing/LatticeSearch/"
    self["ls_gauss_filter_strength"] = GetFloatInfoItem(info_root,
                                                          gname+"GaussFilterStrength",
                                                          1.0)
    self["ls_min_lat_length"] = GetFloatInfoItem(info_root,
                                                   gname+"MinimalLatticeLength",
                                                   3.0)
    self["ls_max_lat_length"] = GetFloatInfoItem(info_root,
                                                   gname+"MaximalLatticeLength",
                                                   1e10)
    gname="DiffProcessing/LatticeSearch/PeakSearchParams/"
    self["ls_outer_win_size"] = GetIntInfoItem(info_root,
                                                 gname+"OuterWindowSize",
                                                 10)
    self["ls_outer_win_sens"] = GetFloatInfoItem(info_root,
                                                   gname+"OuterWindowSensitivity",
                                                   0.01)
    self["ls_inner_win_size"] = GetIntInfoItem(info_root,
                                                 gname+"InnerWindowSize",
                                                 5)
    self["ls_inner_win_sens"] = GetFloatInfoItem(info_root,
                                                   gname+"InnerWindowSensitivity",
                                                   0.0)
    gname="DiffProcessing/DifferenceVectorImage/"
    self["dv_gauss_filter_strength"] = GetFloatInfoItem(info_root,
                                                          gname+"GaussFilterStrength",
                                                          0.5)
    gname="DiffProcessing/DifferenceVectorImage/PeakSearchParams/"
    self["dv_outer_win_size"] = GetIntInfoItem(info_root,
                                                 gname+"OuterWindowSize",
                                                 3)
    self["dv_outer_win_sens"] = GetFloatInfoItem(info_root,
                                                   gname+"OuterWindowSensitivity",
                                                   10.0)
    self["dv_inner_win_size"] = GetIntInfoItem(info_root,
                                                 gname+"InnerWindowSize",
                                                 1)
    self["dv_inner_win_sens"] = GetFloatInfoItem(info_root,
                                                   gname+"InnerWindowSensitivity",
                                                   0.0)

    # lattice extract stuff
    gname="DiffProcessing/LatticeExtract/"
    self["le_center_mask_radius"] = GetIntInfoItem(info_root,
                                                     gname+"CenterMaskRadius",
                                                     0)
    self["le_peripheral_mask_radius"] = GetIntInfoItem(info_root,
                                                         gname+"PeripheralMaskRadius",
                                                         0)
    self["le_refine_lattice_distortions"] = GetBoolInfoItem(info_root,
                                                              gname+"RefineLatticeDistortions",
                                                              False)
    self["le_re_refine_lattice"] = GetBoolInfoItem(info_root,
                                                     gname+"ReRefineLattice",
                                                     False)
    self["le_box_size"] = GetIntInfoItem(info_root,
                                           gname+"BoxSize",
                                           4)
    self["le_box_grow"] = GetIntInfoItem(info_root,
                                           gname+"BoxGrowAmount",
                                           0)
    self["le_bg_size"] = GetIntInfoItem(info_root,
                                          gname+"BackgroundRimSize",
                                          1)
    self["le_method"] = GetIntInfoItem(info_root,
                                         gname+"Method",
                                         0)

    self["le_bg_corr"] = GetBoolInfoItem(info_root,
                                           gname+"BackgroundSubtract",
                                           False)
    self["le_center_tweak_limit"] = GetFloatInfoItem(info_root,
                                                       gname+"CenterTweakLimit",
                                                       3)
    

    gname="DiffProcessing/WeakFilter/"
    self["wf_min_qual"] = GetFloatInfoItem(info_root,
                                             gname+"MinimumQuality",
                                             1.2)
    self["wf_max_offset"] = GetFloatInfoItem(info_root,
                                               gname+"MaximumOffset",
                                               5.0)
    self["wf_max_halfwidth"] = GetFloatInfoItem(info_root,
                                                  gname+"MaximumHalfWidth",
                                                  1000.0)
    self["wf_max_eratio"] = GetFloatInfoItem(info_root,
                                               gname+"MaximumEllipticalRatio",
                                               1000.0)
    gname="DiffProcessing/StrongFilter/" 
    self["sf_min_qual"] = GetFloatInfoItem(info_root,
                                             gname+"MinimumQuality",
                                             2.0)
    self["sf_max_offset"] = GetFloatInfoItem(info_root,
                                               gname+"MaximumOffset",
                                               2.0)
    self["sf_max_halfwidth"] = GetFloatInfoItem(info_root,
                                                  gname+"MaximumHalfWidth",
                                                  1000.0)
    self["sf_max_eratio"] = GetFloatInfoItem(info_root,
                                               gname+"MaximumEllipticalRatio",
                                               1000.0)
    
    self["sampling"] = GetFloatInfoItem(info_root,
                                          "DiffProcessing/Sampling",
                                          1e-4)

