#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


# all wx stuff here
import wx
import images
import os

class ColumnEntry:
  """
  Entry for each column with cells in ProjView, columns[].
  """
  def __init__(self,name,cmd_list,callback):
    self.name = name
    self.cmd_list = cmd_list
    self.cb = callback

class ColumnEntryRB:
  """
  Entry for each column with radio buttons in ProjView, columnsrb[].
  """
  def __init__(self,name,callback):
    self.name = name
    self.cb = callback

class EntryMapEntry:
  """
  Entry for each row in ProjView, entryMap
  """
  def __init__(self, text, cmd_list, callback):
    self.text = text # non-editable wx.textCtrl for explanation purposes
    self.cmd_list = cmd_list # list of menu-item names
    self.cb = callback

class BuildList:
  """
  Storing maps with pointers
  """
  def __init__(self):
    self.checkBoxMap = {} # map of checkbox-pointers, key = image
    self.cellMap = {} # map of cell-lists, key = image

class ProjView(wx.Frame):
  """
  Gives an interface to create a Project Viewer. Initialize with adding columns(AddColumn),
  entrys(AddEntry), menus(AddMenu). Build() needs to be called before further usage.
  """
  def __init__(self, title):
    wx.Frame.__init__(self, None, wx.ID_ANY, title, pos=(150, 150), size=(500, 600))

    self.title = title
    self.columns = []
    self.columnsrb = []
    self.entryMap = {}
    self.buildList = BuildList
    self.menu_cmd_list = []
    self.menu_count = 0

    # Menu-/Status-bar
    self.CreateStatusBar()
    self.menuBar = wx.MenuBar()
    menuFile = wx.Menu()
    menuFile.AppendSeparator()
    menuFile.Append(101, "&Quit\tCtrl+Q", "Quit the program")
    self.menuBar.Append(menuFile, "&File")
    menuHelp = wx.Menu()
    menuHelp.AppendSeparator()
    menuHelp.Append(102, "&About", "About this application")
    self.menuBar.Append(menuHelp, "&Help")
    self.SetMenuBar(self.menuBar)

    # Events
    self.Bind(wx.EVT_MENU, self.CloseWindow, id=self.MenuId())
    self.Bind(wx.EVT_MENU, self.OnAbout, id=self.MenuId())

    self.canv = ProjViewCanvas(self)

    self.Show(True)

  # Allows dynamic use of menus
  def MenuId(self):
    self.menu_count += 1
    return self.menu_count + 100

  # Events
  def CloseWindow(self, event):
    self.Close()

  def OnAbout(self, event):
    dlg = ProjViewAbout(self)
    dlg.CenterOnScreen()
    dlg.Show(True)

  # Add a menu to the project viewer
  def AddMenu(self, name, menu_cmd_list):
    self.menu_cmd_list = menu_cmd_list # lists in the form of ["name", "statusbar tooltip", "callback"]
    menuGeneric = wx.Menu()
    menuGeneric.AppendSeparator()
    cur_id = self.MenuId()
    for i in range(len(menu_cmd_list)):
      self.Bind(wx.EVT_MENU, self.MenuGeneric, id=cur_id+i)
      menuGeneric.Append(cur_id+i, menu_cmd_list[i][0], menu_cmd_list[i][1])
    self.menuBar.Insert(1, menuGeneric, name)

  def MenuGeneric(self, event):
    # [event.GetId() - 100 - self.menu_count] = menuitem, [2] = callback
    self.menu_cmd_list[event.GetId() - 100 - self.menu_count][2]()

  def AddColumn(self, name, cmd_list, callback):
    self.columns.append(ColumnEntry(name,cmd_list,callback))

  def AddRBColumn(self, name, callback):
    self.columnsrb.append(ColumnEntryRB(name, callback))

  def AddEntry(self, image, cmd_list, callback):
    self.entryMap[image] = EntryMapEntry("Default", cmd_list, callback)

  # Descriptive text for a specific row
  def SetRowText(self, image, text):
    self.entryMap[image].text = text

  """
  Clear state of cell[image][column], "all" gives the option to only show confirmation once
  ClearState() should be used to clear state, SetCell() should be used to set state to True
  """
  def ClearState(self, image, column, all):
    if all:
      val = wx.ID_YES
    else:
      dlg = wx.MessageDialog(self, "This will rename files!", "Delete confirmation",
      style = wx.YES_NO | wx.NO_DEFAULT)
      dlg.CenterOnScreen()
      val = dlg.ShowModal()
    if val == wx.ID_YES:
      if self.SetCell(image, column, False):
        return True
    dlg.Destroy()
  def SetCell(self, image, column, state):
    for j in range(len(self.columns)):
      if self.columns[j].name == column:
        if self.buildList.cellMap[image][j].state == state:
          return False
        if state == True:
          if j != 0:
            if self.buildList.cellMap[image][j-1].state == True:
              self.buildList.cellMap[image][j].SetState(state)
              return True
          else:
            self.buildList.cellMap[image][j].SetState(state)
            return True
        else:
          if j != len(self.columns)-1:
            if self.buildList.cellMap[image][j+1].state == False:
              self.buildList.cellMap[image][j].SetState(state)
              return True
          else:
            self.buildList.cellMap[image][j].SetState(state)
            return True
    return False

  def SetRowIgnored(self, image, z):
    self.buildList.checkBoxMap[image].SetState(z)

  def Clear(self):
    self.entryMap = {}
    self.canv.Destroy()
    self.canv = ProjViewCanvas(self)

  # Combine with Clear() if rebuilding
  def Build(self):
    buildList = self.canv.BuildGrid(self.columns, self.entryMap, self.columnsrb)
    self.buildList.checkBoxMap = buildList.checkBoxMap
    self.buildList.cellMap = buildList.cellMap

  def ProjViewLog(self, log_file):
    self.log = ProjViewLog(self, log_file)

  def ProjViewAskDialog(self, title, info, value):
    dlg = wx.TextEntryDialog(self, info, title, value, style = wx.OK | wx.CANCEL)
    dlg.CenterOnScreen()
    val = dlg.ShowModal()
    if val == wx.ID_OK:
      temp = [True, dlg.GetValue()]
      return temp
    else:
      temp = [False]
      return temp
    dlg.Destroy()

  def SetMask(self, mask):
    self.SetTitle(self.title + " : mask('" + mask + "')")

class ProjViewCell(wx.PyWindow):
  """
  ColumnCell that can show different bitmaps depending on the state.
  Right click will bring up a columnspecific popupmenu.
  """
  def __init__(self, parent, image, cmd_list, cb, pos=wx.DefaultPosition, size=wx.DefaultSize):
    wx.PyWindow.__init__(self, parent, -1,style=wx.SIMPLE_BORDER)
    self.image = image
    self.cmd_list = cmd_list
    self.cb = cb
    self.state = False
    self.parent = parent

    yes = images.getYesBitmap()
    self.yes = yes

    self.width = 25
    self.height = 25
    self.bestsize = (self.width,self.height)
    self.SetSize(self.bestsize)
    self.Bind(wx.EVT_PAINT, self.OnPaint)
    self.Bind(wx.EVT_SIZE, self.OnSize)
    self.Bind(wx.EVT_RIGHT_DOWN, self.OnRightButton)
    for i in range(len(self.cmd_list)):
      self.Bind(wx.EVT_MENU, self.Menu101, id=100+i)

  def GetParentProjectView(self):
    return self.parent.GetParentProjectView()

  def SetState(self, state):
    self.state = state
    self.Refresh()

  def Menu101(self, event):
    self.cb(self.image, self.cmd_list[event.GetId() - 100])

  def OnPaint(self, evt):
    self.Refresh()

  def OnSize(self, evt):
    self.Refresh()

  def OnRightButton(self, evt):
    menu = wx.Menu(self.image)
    for i in range(len(self.cmd_list)):
      menu.Append(100+i, self.cmd_list[i])
    self.PopupMenu(menu)

  def Refresh(self):
    dc = wx.PaintDC(self)
    dc.Clear()
    sz = self.GetSize()
    if self.state == True:
      dc.DrawBitmap(self.yes, (self.width - self.yes.GetWidth()) / 2, (self.height - self.yes.GetHeight()) / 2, True)
"""
class ProjViewRadioButton(wx.PyWindow):
  def __init__(self, parent, image, cb, label1, label2, pos=wx.DefaultPosition, size=wx.DefaultSize):
    wx.PyWindow.__init__(self, parent, -1,style=wx.SIMPLE_BORDER)
    self.image = image
    self.cb = cb
    self.label1 = label1
    self.label2 = label2

    self.sizer = wx.BoxSizer(wx.VERTICAL)
    self.radio1 = wx.RadioButton(self, -1, label1, style = wx.RB_GROUP)
    self.radio2 = wx.RadioButton(self, -1, label2)
    self.sizer.Add(self.radio1, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
    self.sizer.Add(self.radio2, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
    self.SetSizer(self.sizer)
    self.Fit()
    self.sizer.Layout()

    self.Bind(wx.EVT_RADIOBUTTON, self.OnSelect1, self.radio1)
    self.Bind(wx.EVT_RADIOBUTTON, self.OnSelect2, self.radio2)

  def OnSelect1(self, event):
    radio_selected = event.GetEventObject()
    print radio_selected.GetLabel()

  def OnSelect2(self, event):
    radio_selected = event.GetEventObject()
    print radio_selected.GetLabel()
"""
class ProjViewRadioButton(wx.RadioBox):
  def __init__(self, parent, image, cb, pos=wx.DefaultPosition, size=wx.DefaultSize):
    wx.RadioBox.__init__(self, parent, -1, "", wx.DefaultPosition, wx.DefaultSize,["",""],
                     2, wx.RA_SPECIFY_COLS)
    self.image = image
    self.cb = cb

    self.Bind(wx.EVT_RADIOBOX, self.EvtRadioBox, self)

  def EvtRadioBox(self, event):
    self.cb(self.image, event.GetInt())

class ProjViewText(wx.StaticText):
  """
  Used as a label.
  """
  def __init__(self, parent, text):
    wx.StaticText.__init__(self, parent, -1, text, wx.DefaultPosition, wx.DefaultSize, wx.SUNKEN_BORDER)
    self.text = text

class ProjViewImage(wx.StaticText):
  """
  Label that have it's own popupmenu on Rightclick.
  """
  def __init__(self, parent, image, cmd_list, cb):
    wx.StaticText.__init__(self, parent, -1, image, wx.DefaultPosition, wx.DefaultSize, wx.SUNKEN_BORDER)
    self.parent = parent
    self.image = image
    self.cmd_list = cmd_list
    self.cb = cb

    self.Bind(wx.EVT_RIGHT_DOWN, self.OnRightButton)
    for i in range(len(self.cmd_list)):
      self.Bind(wx.EVT_MENU, self.Menu101, id=100+i)

  def GetParentProjectView(self):
    return self.parent.GetParentProjectView()

  def Menu101(self, event):
    self.cb(self.image, self.cmd_list[event.GetId() - 100],
            self.GetParentProjectView().columns)

  def OnRightButton(self, evt):
    menu = wx.Menu(self.image)
    for i in range(len(self.cmd_list)):
      menu.Append(100+i, self.cmd_list[i])
    self.PopupMenu(menu)

class ProjViewInfo(wx.TextCtrl):
  """
  Used as an informative read-only textctrl.
  """
  def __init__(self, parent, text):
    wx.TextCtrl.__init__(self, parent, -1, text, wx.DefaultPosition, (200, -1), wx.TE_READONLY)
    self.text = text

class ProjViewCheckBox(wx.CheckBox):
  """
  A checkbox that creates/removes an ignore-file in its' corresponding image-directory.
  On init will set it's value corresponding to if an ignore-file exists or not.
  """
  def __init__(self, parent, image):
    wx.CheckBox.__init__(self, parent, -1, "", wx.DefaultPosition, wx.DefaultSize)
    self.parent = parent
    self.image = image
    ignore_file = os.path.join(".",self.image,"ignore")
    if os.path.isfile(ignore_file):
      self.SetValue(True)
    else:
      self.SetValue(False)

    self.Bind(wx.EVT_CHECKBOX, self.OnCheckBox, self)

  def SetState(self, z):
    if z == True:
      self.SetValue(True)
      ignore_file = os.path.join(".",self.image,"ignore")
      if os.path.isfile(ignore_file):
        pass
      else:
        os.mknod(ignore_file)
    else:
      ignore_file = os.path.join(".",self.image,"ignore")
      if os.path.isfile(ignore_file):
#On Windows, attempting to remove a file that is in use causes an exception to be raised
        try:
          os.remove(ignore_file)
          self.SetValue(False)
        except Exception, e:
          pass

  def OnCheckBox(self, event):
    self.parent.SetRowIgnored(self.image, self.GetValue())

class ProjViewCanvas(wx.ScrolledWindow):
  """
  Canvas of ProjView. 
  """
  def __init__(self, parent):
    wx.ScrolledWindow.__init__(self, parent, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.VSCROLL | wx.HSCROLL)
    self.parent = parent
    self.SetScrollRate(10,10)

    self.sizer = wx.BoxSizer(wx.HORIZONTAL)
    self.SetSizer(self.sizer)

  def Refresh(self):
    self.Fit()
    self.sizer.Layout()

  def BuildGrid(self, columns, entryMap, columnsrb):
    """
    Build the gridinterface defined in columns and entryMap.
    """
    checkBoxMap = {}
    cellMap = {}

    gridSizer = wx.FlexGridSizer(len(entryMap) + 1,len(columns)+len(columnsrb)+3,-1,5) # rows, cols, hgap, vgap
    self.sizer.Add(gridSizer, 0)
    gridSizer.Add(ProjViewText(self, "Ignore: "), 0, wx.ALIGN_CENTER_VERTICAL)
    gridSizer.Add(ProjViewText(self, "Image: "), 0, wx.ALIGN_CENTER_VERTICAL)

    for col in columns:
      gridSizer.Add(ProjViewText(self, col.name), 0, wx.ALIGN_CENTER)
    for col in columnsrb:
      gridSizer.Add(ProjViewText(self, col.name), 0, wx.ALIGN_CENTER)
    gridSizer.Add((60, 20), 0, wx.EXPAND)

    for image in sorted(entryMap.iterkeys()):
      check_box = ProjViewCheckBox(self, image)
      gridSizer.Add(check_box, 0, wx.ALIGN_CENTER_VERTICAL)
      checkBoxMap[image] = check_box
      gridSizer.Add(ProjViewImage(self, image, entryMap[image].cmd_list, entryMap[image].cb),
                                  0, wx.ALIGN_CENTER_VERTICAL)
      colList = []
      for col in columns:
        cell = ProjViewCell(self, image, col.cmd_list, col.cb)
        gridSizer.Add(cell, 0, wx.ALIGN_CENTER)
        colList.append(cell)
      for col in columnsrb:
        gridSizer.Add(ProjViewRadioButton(self, image, col.cb), 0, wx.ALIGN_CENTER_VERTICAL)
      gridSizer.Add(ProjViewInfo(self, entryMap[image].text), 0, wx.ALIGN_CENTER_VERTICAL)
      cellMap[image] = colList
      
    self.Refresh()
    buildList = BuildList
    buildList.checkBoxMap = checkBoxMap
    buildList.cellMap = cellMap
    return buildList

  def SetRowIgnored(self, image, z):
    self.parent.SetRowIgnored(image, z)

  def GetParentProjectView(self):
    return self.parent

class ProjViewLog(wx.Frame):
  """
  Displays a project log file in a simple scrolled window.
  """
  def __init__(self, parent, file_name):
    wx.Frame.__init__(self, parent, wx.ID_ANY, title=file_name, pos=(-1, -1), size=(-1, -1))

    self.Show(True)
    f = open(file_name, 'r')
    wx.TextCtrl(self, -1, f.read(), wx.DefaultPosition, (-1, -1), wx.TE_MULTILINE | wx.TE_READONLY)
    f.close()
    self.CenterOnScreen()

class ProjViewAbout(wx.Dialog):
  """
  About dialog for the project viewer.
  """
  def __init__(self, parent):
    wx.Dialog.__init__(self, parent, wx.ID_ANY, title="About ProjectView", pos=(-1, -1), size=(-1, -1))

    sizer = wx.BoxSizer(wx.VERTICAL)
    self.SetSizer(sizer)

    gridSizer = wx.FlexGridSizer(3,2, -1, 5) # rows, cols, hgap, vgap
    sizer.Add(gridSizer, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL, 5)
    gridSizer.Add(ProjViewText(self, "Run: "), 0, wx.ALIGN_CENTER_VERTICAL)
    cell1 = ProjViewCell(self, "abc1", "abc", "abc")
    cell1.SetState(True)
    gridSizer.Add(cell1, 0, wx.ALIGN_CENTER)
    gridSizer.Add(ProjViewText(self, "Run with error: "), 0, wx.ALIGN_CENTER_VERTICAL)
    gridSizer.Add(ProjViewCell(self, "abc2", "abc", "abc"), 0, wx.ALIGN_CENTER)
    gridSizer.Add(ProjViewText(self, "Not yet run: "), 0, wx.ALIGN_CENTER_VERTICAL)
    gridSizer.Add(ProjViewCell(self, "abc3", "abc", "abc"), 0, wx.ALIGN_CENTER)

    sizer.Add(wx.Button(self, wx.ID_OK), 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL, 5)
    self.Fit()
