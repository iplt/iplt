#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


"""Plot LatticePoint fits to PostScript

Several routines are available:

PlotLatticePoint: plots a single lattice point
PlotLatticePointList: plots summary of a lattice point list
PlotGauss2DFit:
PlotRBin: resolution bin plot

Requires pyx version >0.7.1 (!)

Authors: Andreas Schenk, Ansgar Philippsen
"""

try:
    from pyx import *
    import pyx.document as document
except ImportError:
    print "pyx module could not be imported, Plot* not available"
    has_pyx=False
else:
    has_pyx=True

import iplt, iplt.ex, iplt.alg
from math import radians,sqrt,log

def PlotLatticePointList(lpl,fname):
    """
    Plot summary of a complete lattice point list
    Parameters:
     - lpl   instance of LatticePointList
     - fname destination file
    """
    if not has_pyx: return
    c=canvas.canvas()
    l=[]
    for lpp in lpl:
        l.append(lpp.LPoint().GetPI().GetVolume())
    if max(l)==0:
        sf=8.0
    else:
        sf=8.0/max(l)

    for lpp in lpl:
        c=add_point(c,lpp.LPoint(),sf)
    tr=trafo.trafo().scaled(sx=0.01,sy=0.01).translated(x=2,y=2)
    d=canvas.canvas()
    d.insert(c,[tr])
    d.writeEPSfile(fname,paperformat='A4',fittosize=1)

def PlotGauss2DFit(fit,img,fname):
    """plot gauss fit of an image
    """
    if not has_pyx: return
    stat=iplt.alg.Stat()
    img.Apply(stat)
    tr1=trafo.translate(x=0,y=10)
    tr2=trafo.translate(x=3,y=1)
    c=canvas.canvas()
    pnt=img.GetExtent().GetCenter()
    c.insert(plot_image(img,stat,(pnt[0],pnt[1]),fit),[tr1])
    c.insert(plot_gauss(img,fit),[tr2])
    c.text(14,7.0,'Gauss Volume:%6.4e' % (fit.GetVolume(),) )
    c.text(14,6.0,'AbsQuality:  %6.4e' % (fit.GetAbsQuality(),) )
    c.text(14,5.5,'RelQuality:  %6.4e' % (fit.GetRelQuality(),) )
    c.text(14,5.0,'GOF:         %6.4e' % (fit.GetGOF(),) )
    c.text(14,4.5,'Chi:         %6.4e' % (fit.GetChi(),) )
    c.text(14,3.5,'A:           %6.4e' % (fit.GetA(),) )
    c.text(14,3.0,'B:           %6.4e,%6.4e' % (fit.GetBx(),fit.GetBy()) )
    c.text(14,2.5,'U:           %6.4e,%6.4e' % (fit.GetUx(),fit.GetUy()) )
    c.text(14,2.0,'W:           %6.4e' % (fit.GetW(),) )
    c.text(14,1.5,'P:           %6.4e,%6.4e' % (fit.GetPx(),fit.GetPy()) )
    c.text(14,1.0,'C:           %6.4e' % (fit.GetC(),) )
    c.writeEPSfile(fname,paperformat=document.paperformat.A4,fittosize=1)
    

def PlotLatticePoint(lp,indx,img,fname):
    """
    Plot individual lattice point with all gory details.
    Required params:
     - lp    instance of LatticePoint
     - indx  Point instance denoting index
     - img   original image
     - fname destination file
    """
    if not has_pyx: return
    subimg = img.Copy(lp.GetRegion())
    stat=iplt.alg.Stat()
    subimg.Apply(stat)
    tr1=trafo.translate(x=0,y=10)
    tr2=trafo.translate(x=3,y=1)
    c=canvas.canvas()
    fit=lp.GetFit()
    pi=lp.GetPI()
    pnt=iplt.Point(lp.GetPosition())
    c.insert(plot_image(subimg,stat,(pnt[0],pnt[1]),fit),[tr1])
    c.insert(plot_gauss(subimg,fit),[tr2])
    c.text(14,8.5,'Index:       (%5d,%5d)' % (indx[0],indx[1]),[text.size.large] )
    c.text(14,7.0,'Gauss Volume:%6.4e' % (fit.GetVolume(),) )
    c.text(14,6.5,'Int Volume:  %6.4e' % (pi.GetVolume(),) )
    c.text(14,6.0,'AbsQuality:  %6.4e' % (fit.GetAbsQuality(),) )
    c.text(14,5.5,'RelQuality:  %6.4e' % (fit.GetRelQuality(),) )
    c.text(14,5.0,'GOF:         %6.4e' % (fit.GetGOF(),) )
    c.text(14,4.5,'Chi:         %6.4e' % (fit.GetChi(),) )
    c.text(14,3.5,'A:           %6.4e' % (fit.GetA(),) )
    c.text(14,3.0,'B:           %6.4e,%6.4e' % (fit.GetBx(),fit.GetBy()) )
    c.text(14,2.5,'U:           %6.4e,%6.4e' % (fit.GetUx(),fit.GetUy()) )
    c.text(14,2.0,'W:           %6.4e' % (fit.GetW(),) )
    c.text(14,1.5,'P:           %6.4e,%6.4e' % (fit.GetPx(),fit.GetPy()) )
    c.text(14,1.0,'C:           %6.4e' % (fit.GetC(),) )
    c.writeEPSfile(fname,paperformat=document.paperformat.A4,fittosize=1)

def PlotRBin(rbin,eps_file):
    """
    plot a resolution bin instance
    """
    if not has_pyx: return
    # number of bins
    nbin=rbin.GetBinCount()
    ticks=[]
    for b in range(nbin):
        rl=float(int(rbin.GetBinResolution(b)*100.0))/100.0
        ticks.append(graph.axis.tick.tick(b,label=str(rl)))
    
    c=canvas.canvas()

    ax=graph.axis.linear(parter=None,manualticks=ticks)
    g=graph.graphxy(width=8,x=ax)
    l=[]
    for b in range(nbin):
        # average value for this bin
        val=rbin.GetBinAverage(b)
        sd=rbin.GetBinStdDev(b)
        rl=rbin.GetBinResolution(b)
        l.append([b,val,sd])
    g.plot(graph.data.list(l,x=1,y=2,dy=3),
           [graph.style.symbol(),graph.style.errorbar()])
    c.insert(g)
    c.writeEPSfile(eps_file,paperformat=document.paperformat.A4,fittosize=1)

def PlotLatticeLine(rlist,indx,dir,pmin=-0.2,pmax=0.2):
    """
    plots the lattice line of the given reflection list and index
    assumes that directory exists and is writeable
    """
    l=[]
    rp=rlist.Find(indx)
    if not rp.IsValid():
        return
    sigmax=0
    ri=rp.__iter__()
    while(rp.IsValid() and rp.GetIndex()==indx):
        l.append([rp.Get("zstar"),rp.Get("fobs"),rp.Get("fobs")/rp.Get("sigfobs")])
	sigmax=max(sigmax,rp.Get("fobs")/rp.Get("sigfobs"))
        rp=ri.next()
    print "plotting %d points for index %s"%(len(l),str(indx))
    if len(l)>0:
        try:
            eps_file="%s/ll_%d_%d.eps"%(dir,indx[0],indx[1])
            c=canvas.canvas()
            g=graph.graphxy(width=8,
                            x=graph.axis.linear(min=pmin, max=pmax,title="z*"),
                            y=graph.axis.linear(min=0,title="A"))
	    for item in l:
            	g.plot(graph.data.list([item],x=1,y=2),[graph.style.symbol(symbol=graph.style.symbol.circle,size=item[2]/sigmax*0.06,symbolattrs=[deco.filled])])
            c.insert(g)
            c.text(0,5.5,"%d %d"%(indx[0],indx[1]))
            c.writeEPSfile(eps_file,paperformat=document.paperformat.A4,fittosize=1)
        except OverflowError:
            try:
                os.remove(eps_file)
            except OSError:
                pass
    
def plot_image(image,statm,predicted_center,fit):
    start=image.GetExtent().GetStart()
    imsize=image.GetExtent().GetSize()[0]
    rel_fitted_center=[fit.GetUx()-start[0]+0.5,fit.GetUy()-start[1]+0.5]
    rel_pred_center=[predicted_center[0]-start[0]+0.5,predicted_center[1]-start[1]+0.5]
    d=canvas.canvas()
    c=canvas.canvas()
    tr=trafo.trafo().scaled(sx=0.5).translated(x=1,y=1)
    immax=statm.GetMaximum()
    c.stroke(path.rect(0,0,imsize,imsize),[color.rgb.green])
    if immax>0:
        for px in iplt.ExtentIterator(image.GetExtent()):
            c.fill(path.rect(px[0]-start[0],px[1]-start[1],1,1),[color.gray(image.GetReal(px)/immax)])
        
    c.stroke(path.line(-1,rel_pred_center[1],imsize+1,rel_pred_center[1]))
    c.stroke(path.line(rel_pred_center[0],-1,rel_pred_center[0],imsize+1))
    bboxpath=c.bbox().path()
    c.stroke(path.line(-1,rel_fitted_center[1],imsize+1,rel_fitted_center[1]),[color.rgb.red,style.linestyle.dashed])
    c.stroke(path.line(rel_fitted_center[0],-1,rel_fitted_center[0],imsize+1),[color.rgb.red,style.linestyle.dashed])
    c.stroke(path.circle(0,0,1),[trafo.trafo().scaled(sx=fit.GetBx()*sqrt(log(2)),sy=fit.GetBy()*sqrt(log(2))).rotated(fit.GetW()).translated(x=rel_fitted_center[0],y=rel_fitted_center[1]),color.rgb.green])
    d.insert(c,[tr,canvas.clip(bboxpath)])
    d.text(imsize/2+3,imsize/2-0.0,'Minimum:            %6.4e' % (statm.GetMinimum(),) )
    d.text(imsize/2+3,imsize/2-0.5,'Maximum:            %6.4e' % (statm.GetMaximum(),) )
    d.text(imsize/2+3,imsize/2-1.0,'Mean:               %6.4e' % (statm.GetMean(),) )
    d.text(imsize/2+3,imsize/2-1.5,'Standard Deviation: %6.4e' % (statm.GetStandardDeviation(),) )
    d.text(imsize/2+3,imsize/2-2.0,'Average Deviation:  %6.4e' % (statm.GetAverageDeviation(),) )
    d.text(imsize/2+3,imsize/2-2.5,'Variance:           %6.4e' % (statm.GetVariance(),) )
    
    return d

def plot_gauss(image,gaussfit):
    c=canvas.canvas()
    gx=c.insert(plot_gauss_sub(image,gaussfit,0))
    tr=trafo.translate(0,gx.height+0.5)
    c.insert(plot_gauss_sub(image,gaussfit,1),[tr])
    return c


def plot_gauss_sub(image,gaussfit,flag):
    intfitcenter=[]
    intfitcenter.append(int(gaussfit.GetUx()))
    intfitcenter.append(int(gaussfit.GetUy()))
    function='y=%(A)s*exp(-(((%(x)s-%(Ux)s)*cos(%(W)s)-(%(y)s-%(Uy)s)*sin(%(W)s))/%(Bx)s)**2-(((%(y)s-%(Uy)s)*cos(%(W)s)+(%(x)s-%(Ux)s)*sin(%(W)s))/%(By)s)**2)+%(Px)s*(%(x)s-%(Ux)s)+%(Py)s*(%(y)s-%(Uy)s)+%(C)s'
    fparams={}
    fparams['A']=str(gaussfit.GetA())
    fparams['Bx']=str(gaussfit.GetBx())
    fparams['By']=str(gaussfit.GetBy())
    fparams['Ux']=str(gaussfit.GetUx())
    fparams['Uy']=str(gaussfit.GetUy())
    fparams['W']=str(radians(gaussfit.GetW()))
    fparams['Px']=str(gaussfit.GetPx())
    fparams['Py']=str(gaussfit.GetPy())
    fparams['C']=str(gaussfit.GetC())
    l=[]
    l2=[]
    start=image.GetExtent().GetStart()
    end=image.GetExtent().GetEnd()
    
    if flag==0:
        fparams['x']='x2'
        fparams['y']=str(intfitcenter[1])
        for i in range(start[flag],end[flag]+1):
            l.append([i,image.GetReal(iplt.Point(i,intfitcenter[1]))])  
            l2.append([image.GetReal(iplt.Point(i,intfitcenter[1]))])  
    else:
        fparams['x']=str(intfitcenter[0])
        fparams['y']='x2'
        for i in range(start[flag],end[flag]+1):
            l.append([i,image.GetReal(iplt.Point(intfitcenter[0],i))])  
            l2.append([image.GetReal(iplt.Point(intfitcenter[0],i))])  
  
    f=function % fparams
    bap=graph.axis.painter.bar
    a=graph.axis.bar(painter=bap(nameattrs=None))
    a2=graph.axis.linear(min=start[flag],max=end[flag])
    lmax=max(l2)[0]
    if lmax==0:
        lmax=1
    #print lmax
    ay=graph.axis.linear(min=0,max=lmax)
    g=graph.graphxy(width=8, height=4, x=a,x2=a2,y=ay)
    g.plot(graph.data.list(l,xname=1,y=2),[graph.style.bar()])
    g.plot(graph.data.function(f),[graph.style.line([color.rgb.green])])
    
    return g

def plot_intvol(intvols,gaussvol):
    s=''
    c=canvas.canvas()
    g=graph.graphxy(x=graph.axis.linear(min=0,max=len(intvols)))
    for i in range(len(intvols)):
        s=s+'%d %20.10f\n' % (i,intvol[i])  
    g.plot(graph.data.list(s))
    g.plot(graph.data.function("y=%20.10f" % gaussvol))
    c.insert(g)
    return c


  
def add_point(can,lp,sf):
  fit=lp.GetFit()
  ratio=abs(fit.GetBx()/fit.GetBy())
  vol=lp.GetPI().GetVolume()*sf
  if vol>0:
    tr=trafo.trafo().scaled(sx=sqrt(ratio),sy=1/sqrt(ratio)).scaled(sx=vol,sy=vol).rotated(fit.GetW()).translated(x=lp.GetPosition()[0],y=lp.GetPosition()[1])
    can.fill(path.circle(0,0,1),[tr,color.rgb.black])
  return can
