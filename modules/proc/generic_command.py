#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------
#
#
# Authors: Ansgar Philippsen, Andreas Schenk
#

# system specific imports
import sys,os.path,re,copy,Queue
from threading import Thread
from optparse import OptionParser

from ost import PushVerbosityLevel

# iplt imports
from iplt.proc.dirscan import dirscan

class CommandThread(Thread):
    def __init__(self,queue,command,verbosity):
        Thread.__init__(self)
        self.queue_=queue
        self.command_=command
        self.verbosity_=verbosity
        
    def run(self):
      PushVerbosityLevel(self.verbosity_)
      try:
        while 1:
          dentry=self.queue_.get(False)
          try:
            self.command_(dentry[0],dentry[1])
          except Exception,e:
            print "exception during thread:",e
            raise
      except Queue.Empty, e:
        pass

class GenericCommand:
  def __init__(self,name):
      self.name_=name
      self.command_list_={}
      self.command_help_=[]
      self.command_prep_={}
      self.command_list_no_threads={}
      self.parser_=OptionParser()
      self.options_=[]
      self.args_=[]
  def SetImageMask(self,mask):
      self.img_mask_=mask
  def GetOptions(self):
          return self.options_
  def GetArgs(self):
      return self.args_
  def AddCommand(self,name,command,help="",use_threads=True,prep=None):
      if use_threads:
          self.command_list_[name]=command
      else:
          self.command_list_no_threads[name]=command
      self.command_help_.append(name+": "+help)
      self.command_prep_[name]=prep

  def AddOption(self,short,long=None,type=None,default=None,dest=None,help=None,action=None):
      self.parser_.add_option(short,long,type=type,default=default,dest=dest,help=help,action=action)
     
  def Parse(self):
    usage = self.name_+" [options] COMMAND COMMAND_OPTIONS\n"
    usage += "\nwhere COMMAND is one of:\n"
    for uc in self.command_help_:
      usage += "   "+uc+"\n"
    usage += "\nand COMMAND_OPTIONS depends on the command"
    self.parser_.set_usage(usage)
    
    self.parser_.add_option("-v", "--verbosity",
                      type="int", default=5, dest="verbosity",
                      help="Set verbosity level (default=1)")
    self.parser_.add_option("-j", "--threads",
                      type="int", default=1, dest="num_threads",
                      help="Set number of threads (default=1)")
    self.parser_.add_option("-f", "--force",
                      action="store_true",default=False,dest="force_flag",
                      help="force processing even when up-to-date results are present")
    self.parser_.add_option("--force-ignored",
                      action="store_true",default=False,dest="force_ignored_flag",
                      help="force processing of directories even if ignore file is present")
    self.parser_.add_option("-i", "--image-mask",
                      type="str", default=".*",dest="dir_mask",
                      help="image mask that particular processing will be applied to, defaults to *")

    (self.options_,self.args_) = self.parser_.parse_args()

    if len(self.args_)<2:
      self.parser_.print_help()
      sys.exit(-1)

  def Run(self):
    self.Parse()
    PushVerbosityLevel(self.options_.verbosity)

    # the working dir comes from the caller
    work_dir_ = self.args_[0]
    commandname = self.args_[1]
    if  commandname in self.command_list_.keys():
        command=self.command_list_[commandname]
        multithreaded=True
    elif  commandname in self.command_list_no_threads.keys():
        command=self.command_list_no_threads[commandname]
        multithreaded=False
    else:
      print "unrecognized command:",commandname
      self.parser_.print_help()
      sys.exit(-1)
    if self.command_prep_[commandname]:
        self.command_prep_[commandname](work_dir_)

    dir_mask_re = re.compile(self.options_.dir_mask)

    # prepare image directory queue
    dentryqueue=Queue.Queue()
    dentrylist=[]
    for dentry in dirscan(self.img_mask_,work_dir_):
      if not dir_mask_re.match(dentry[1]):
        continue
      if(not self.GetOptions().force_ignored_flag and os.path.exists(os.path.join(dentry[0],dentry[1],"ignore") )):
          print "ignoring "+dentry[1]
          continue
      dentryqueue.put(dentry)
      dentrylist.append(dentry)

    # start threads
    threadlist=[]
    try:
        if multithreaded:
            for i in range(self.options_.num_threads):
                threadlist.append(CommandThread(dentryqueue,command,self.options_.verbosity))
                threadlist[i].start()
            for i in range(self.options_.num_threads):
                threadlist[i].join()
        else:
            command(dentrylist)
    except Exception, e:
      print "caught exception during %s: %s, %s"%(command,str(e),sys.exc_info())
      raise


 
