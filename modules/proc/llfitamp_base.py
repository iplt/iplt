#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import math,sys,copy
from random import random
from ost.img import *
import iplt as ex
import time
import gc
from ost import LogVerbose,LogInfo,LogError,LogVerbose

def find_resolution_limit(data,N,max_resol,min_count):
    wb=ex.alg.WeightedBin(N/2,0.0,1.0/max_resol)
    for d in data:
        # using abs(zstar) is a hack to simplify this routine
        # and make it independent of the herm mode
        wb.Add(abs(d[0]),d[1],1.0)
    for n in range(N/2+1):
        bin=N/2-n
        if wb.GetSize(bin)>=min_count:
            return wb.GetUpperLimit(bin)
        LogVerbose("skipping bin %d (%g %g) due to insufficient values (%d<%d)"%(bin,wb.GetLowerLimit(bin),wb.GetUpperLimit(bin),wb.GetSize(bin),min_count))
    return 0.0
        
    
def handle_empty_bins(data,N,thickness):
    # heuristic for filling empty bins
    rdelta=1.0/thickness
    emptybins=[]
    rtotal=0
    sigsum=0.0
    for nn in range(0,N/2):
        rlim2=float(nn)*rdelta
        rlim1=rlim2+rdelta
        rcount=0
        for d in data:
            if d[0]<rlim1 and d[0]>=rlim2:
                sigsum+=d[2]
                rcount+=1
        rtotal+=rcount
        if rcount==0:
            LogVerbose("empty bin: %d ( zstar %f to %f)" % (nn,rlim2,rlim1))
            emptybins.append(nn)
        else:
            LogVerbose("full bin: %d ( zstar %f to %f)" % (nn,rlim2,rlim1))

    sigave = sigsum/float(rtotal+1)

    for bin in emptybins:
        LogVerbose("adding %g (%g) to empty bin %d"%(0,sigave,bin))
        data.append( ((float(bin)+0.5)*rdelta,0.0,sigave,0.1) )
    return data

class LLFit:
    def __init__(self,max_resol,min_bin_count,thickness,unitcell):
        self.current_rp_=None
        self.iobs_name_="iobs"
        self.sigiobs_name_="sigiobs"
        self.quality_name_="quality"
        self.max_resol_=max_resol
        self.min_bin_count_=min_bin_count
        self.niter_=1000
        self.nlim_=1e-5
        self.thickness_=thickness
        sampling = max_resol/2.0
        self.N_ = int(thickness/sampling)
        self.uc_ = unitcell
        self.min_qual_ = 0.0
        self.wscheme_ = 1
        self.latline_ = None
        self.global_weight_=1.0
        self.bootstrap_flag_ = True
        self.bootstrap_iter_ = 20
        self.guess_mode_ = 0
        self.iter_ = 1
        self.iter_conv_flag_=True
        self.zifit_flag_=False
        self.force_non_herm_=False
        self.force_herm_=-1
        self.relative_zweight_=4.0
        self.bfac_=0.0
        self.quality_t1_=1.0
        self.quality_t2_=10.0
        self.phantom_mode_=False
        self.curve_fit_mode_=1
        self.alg_=0
        self.rlow_=100.0
        self.rhigh_=1.0

    def SetCurveFitMode(self,m):
        self.curve_fit_mode_=m

    def SetPhantomMode(self,m):
        self.phantom_mode_=m

    def SetLatLine(self,ll):
        self.latline_=ll

    def SetQualityLimits(self,t1,t2):
        self.quality_t1_=t1
        self.quality_t2_=t2

    def SetMinimumQuality(self,w):
        self.min_qual_=w

    def SetWeightingScheme(self,s):
        self.wscheme_=s

    def SetMaxIterations(self,n):
        self.niter_=n

    def SetIterationLimits(self,l):
        self.nlim_=l

    def SetGlobalWeight(self,w):
        self.global_weight_=w

    def SetBootstrapFlag(self,f):
        self.bootstrap_flag_=f

    def SetBootstrapIter(self,n):
        self.bootstrap_iter_=n

    def SetGuessMode(self,n):
        self.guess_mode_=n

    def SetIterations(self,n):
        self.iter_=n

    def SetIterationsUseNonConvergedFlag(self,f):
        self.iter_conv_flag_=f

    def SetZIFitFlag(self,f):
        self.zifit_flag_=f

    def SetRelativeZWeight(self,f):
        self.relative_zweight_=f

    def ForceNonHerm(self,f):
        self.force_non_herm_=f

    def ForceHermMode(self,m):
        self.force_herm_=m

    def SetBFactor(self,b):
        if b<0.0:
            b=0.0
        self.bfac_=b

    def SetAlg(self,a):
        self.alg_=a

    def SetRLim(self,rlow,rhigh):
        self.rlow_=rlow
        self.rhigh_=rhigh

    def Apply(self,rlist):
        rlist_out_merge = ex.ReflectionList(rlist)
        rlist_out_merge.AddProperty("icalc")
        rlist_out_merge.AddProperty("sigicalc")
        
        rlist_out_merge.AddProperty("cflag")
        rlist_out_merge.AddProperty("ifit")
        rlist_out_merge.AddProperty("sigifit")
        rlist_out_merge.AddProperty("zfit")

        curve_map={}

        rlist_out_disc = ex.ReflectionList(self.uc_)
        rlist_out_disc.AddProperty(self.iobs_name_)
        rlist_out_disc.AddProperty(self.sigiobs_name_)
        rlist_out_disc.AddProperty("sinc_amp")
        rlist_out_disc.AddProperty("sinc_phi")
        rlist_out_disc.AddProperty("bin_ave")

        rlist_tmp = ex.ReflectionList(rlist_out_merge,False)

        rcalc = ex.alg.RCalcCalculator("icalc","sigicalc",self.rlow_,self.rhigh_,0)

        self.current_rp_ = rlist_out_merge.Begin()
        if self.latline_:
            self.current_rp_=rlist_out_merge.FindFirst(self.latline_)
        # main outer loop over all reflections
        while self.current_rp_.IsValid():
            if self.latline_ and not self.current_rp_.GetIndex().AsDuplet()==self.latline_:
                break
            try:
                hk = self.current_rp_.GetIndex().AsDuplet()
                LogVerbose("working on (%d,%d)"%(hk[0],hk[1]))
                tm0 = time.time()
                # store for later
                rp = ex.ReflectionProxyter(self.current_rp_)

                herm_mode = self.get_herm_mode(self.uc_.GetSymmetry(),hk)
                LogVerbose("using herm_mode %d"%(herm_mode))
                # this routine updates current_rp_ and
                # thereby contributes to the main loop
                llfit_index = hk
                (llfit,guess)=self.process_next(hk,herm_mode)
                if not llfit:
                    continue
                
                self.assemble_discretized(llfit,guess,rlist_out_disc,hk,herm_mode)
                
                cflag=True
                if llfit.GetIterCount()==self.niter_:
                    LogVerbose("llfit did not converge")
                    cflag=False
                else:
                    LogVerbose("llfit converged after %d iterations"%(llfit.GetIterCount()))

                tm1 = time.time()
                LogVerbose("elapsed time: %fs"%(tm1-tm0))

                # assemble merged dataset
                LogVerbose("assembling merged dataset")
                llfit.SetRelativeZWeight(self.relative_zweight_)
                rlist_tmp.ClearReflections()
                while rp.IsValid() and rp.GetIndex().AsDuplet()==hk:
                    zstar=rp.GetIndex().GetZStar()
                    if herm_mode==2:
                        zstar=-zstar
                    (icalc,sigicalc)=llfit.CalcIntensAt(zstar)
                    rp.Set("icalc",icalc)
                    rp.Set("sigicalc",sigicalc)
                    rp.Set("cflag",cflag)
                    rlist_tmp.AddProxyter(rp)
                    if self.zifit_flag_:
                        if self.curve_fit_mode_==0:
                            cp =llfit.RelativeClosestIntensAt(zstar,rp.Get("iobs"))
                        else:
                            sigzstar = rp.Get("sigmazstar")
                            zstar_w = 1.0
                            if sigzstar>0.0:
                                zstar_w = 1.0/sigzstar
                            sigiobs = rp.Get("sigiobs")
                            iobs_w=1.0
                            if sigiobs>0.0:
                                iobs_w=1.0/sigiobs
                            cp = llfit.ClosestIntensAt(zstar,zstar_w,rp.Get("iobs"),iobs_w)
                        rp.Set("zfit",cp.zfit)
                        rp.Set("ifit",cp.ifit)
                        rp.Set("sigifit",cp.sigifit)
                    else:
                        rp.Set("ifit",rp.Get("iobs"))
                        rp.Set("sigifit",rp.Get("sigiobs"))
                        rp.Set("zfit",zstar)
                    rp.Inc()

                LogVerbose("assembling curve")
                curve_count = 1000
                curve_f = 0.5/llfit.GetSampling()/float(curve_count)
                curve_list=(curve_count*2+1)*[(0,0,0)]
                for n in range(-curve_count,curve_count+1):
                    zstar = float(n)*curve_f
                    (icalc,sigicalc) = llfit.CalcIntensAt(zstar)
                    curve_list[n+curve_count]=(zstar,icalc,sigicalc)
                curve_map[hk]=curve_list

                # garbage collection tweak
                llfit=None
                gc.collect()

                rcalc.Apply(rlist_tmp,"iobs","sigiobs")
                LogError("CalcRFactor %s:  RFit= %2.4g  wRFit= %2.4g"%(str(hk),rcalc.GetRF(),rcalc.GetWRF()))

                  
            except Exception, e:
                print "caught exception in main llfit loop: %s"%(str(e))
                raise
                #return (None,None)

        return (rlist_out_merge,rlist_out_disc,curve_map)

    def get_herm_mode(self,sym,hk):
        """
        the hermitian mode depending on the symmetry and the current
        index.
          0 = full blown non-hermitian from -z* ... +z*
          1 = normal hermitian symmetry
          2 = 'negative' hermitian symmetry, all zstar are mapped to positive
        """
        if self.force_herm_>-1:
            return min(2,self.force_herm_)
        LogVerbose("using %s (%s)"%(sym.GetPointgroupName(),sym.GetSpacegroupName()))
        if sym.GetPointgroupName()=="PG1":
            return 0
        elif sym.GetPointgroupName()=="PG2":
            if hk[1]==0:
                return 0
            elif hk[1]<0:
                return 2
            else:
                return 1
        else:
            return 1
                

    def assemble_discretized(self,llfit,guess,rlist_out_disc,hk,herm_mode):
        sincc = llfit.GetComponents()

        LogVerbose("assembling discretized dataset")
        ir=0
        for zstar in sincc:
            rp_out=rlist_out_disc.AddReflection(ex.ReflectionIndex(hk[0],hk[1],zstar))
            sinc_amp = math.sqrt(sincc[zstar].real*sincc[zstar].real+sincc[zstar].imag*sincc[zstar].imag)
            sinc_phi = math.atan2(sincc[zstar].imag,sincc[zstar].real)
            
            (icalc,sigicalc)=llfit.CalcIntensAt(zstar)
            LogVerbose(" c=%d  zstar=%f  iobs=%g  sigiobs=%g"%(ir,zstar,icalc,sigicalc))

            rp_out.Set(self.iobs_name_,icalc)
            rp_out.Set(self.sigiobs_name_,sigicalc)
            rp_out.Set("sinc_amp",sinc_amp)
            rp_out.Set("sinc_phi",sinc_phi)
            if ir<len(guess):
                rp_out.Set("bin_ave",guess[ir])
            else:
                rp_out.Set("bin_ave",0.0)
            ir+=1


    def run_llfit(self,data,guess,best_N,sampling,hk,herm_mode):
        # lattice line fit object
        llfit = None
        if self.alg_==1:
            llfit = ex.alg.LLFitAmpSincIntpolSQ(best_N,sampling)
        else:
            if herm_mode==0:
                LogVerbose("using non-hermitian symmetry algorithm")
                llfit = ex.alg.LLFitAmpSincIntpol(best_N,sampling)
            else:
                if self.force_non_herm_:
                    LogVerbose("forcing non-hermitian symmetry algorithm for hermitian data")
                    llfit = ex.alg.LLFitAmpSincIntpol(best_N,sampling)
                    guess2=copy.copy(guess)
                    guess.reverse()
                    guess.extend(guess2[1:])
                else:
                    if herm_mode==1:
                        LogVerbose("using hermitian symmetry algorithm")
                    else:
                        LogVerbose("using 'inverted' hermitian symmetry algorithm")
                    llfit = ex.alg.LLFitAmpSincIntpolHerm(best_N,sampling)

#        llfit.SetIntensGuess(guess)

        count=0
        for d in data:
            # sanity check
            if d[2]>0.0:
                count+=1
                # this weight is on the order of 1/variance
                we = self.calc_weight(d)
                sigi = math.sqrt(1.0/we)
                zstar = d[0]
                if herm_mode==2:
                    zstar=-d[0]
                llfit.AddIntens(zstar,d[1],sigi)
                if self.force_non_herm_:
                    llfit.AddIntens(-zstar,d[1],sigi)
                    count+=1
                if self.alg_==1 and herm_mode==1:
                    llfit.AddIntens(-zstar,d[1],sigi)
                    count+=1


        if count<best_N+2:
            LogVerbose("not enough points, skipping")
            return (None,guess)

        LogVerbose("running sinc llfit for %s with N=%d and %gA sampling based on %d data points"%(str(hk),best_N,sampling,count))
        llfit.SetMaxIter(self.niter_)
        llfit.SetIterationLimits(self.nlim_,self.nlim_)
        llfit.SetBFactor(self.bfac_)
        try:
            llfit.Apply();
        except Exception, e:
            LogError("caught exception: %s"%(str(e)))
            return (None,guess)
        
        return (llfit,guess)

    def process_next(self,hk,herm_mode):
        LogVerbose("assembling data")
        #
        # the layout of each data element is
        # [0] zstar
        # [1] amplitude
        # [2] sigma
        # [3] quality
        #
        (data,max_zstar) = self.assemble_next(hk)

        LogVerbose("finding resolution limit")
        best_zstar = find_resolution_limit(data,
                                           self.N_,
                                           self.max_resol_,
                                           self.min_bin_count_)

        if best_zstar<=0.0:
            LogVerbose("best_zstar is zero, skipping")
            return (None,None)
        

        best_sampling = 0.5/best_zstar # sampling is always 1/2 resol
        best_N = min(int(self.thickness_/best_sampling),self.N_) # herm = N/2

        if best_N&0x1: # odd
            best_N+=1

        if best_N==0:
            LogVerbose("best_N is zero, skipping")
            return (None,None)
        
        sampling = self.thickness_/float(best_N)

        LogVerbose("detected: best_N=%d; sampling=%g"%(best_N,sampling))

        LogVerbose("handling empty bins")
        data=handle_empty_bins(data,best_N,self.thickness_)

        guess_bin_count=0
        if herm_mode==0:
            guess_bin_count = best_N # non hermitian
        else:
            guess_bin_count = best_N/2+1 # hermitian
        guess_bin_delta = 1.0/(float(best_N)*sampling)
        # correction to place sinc frequencies into middle of bin
        guess_res_low = 0.0-guess_bin_delta*0.5
        guess_res_hi = 0.5/sampling+guess_bin_delta*0.5
        LogVerbose("making guess (%d %f %f)"%(guess_bin_count,guess_res_low,guess_res_hi))
        blist = ex.alg.WeightedBin(guess_bin_count,guess_res_low,guess_res_hi)
        for d in data:
            try:
                we=self.calc_weight(d)
                if we==we: # avoid NaN
                    blist.Add(d[0],d[1],self.calc_weight(d))
            except ZeroDivisionError:
                pass

        guess=[]
        wbin_guess=[]
        for n in range(guess_bin_count):
            guess_amp2=blist.GetAverage(n)
            if guess_amp2==guess_amp2:
                wbin_guess.append(guess_amp2)
            else:
                wbin_guess.append(0.0)
        if self.guess_mode_==0:
            for n in range(guess_bin_count):
                guess.append(wbin_guess[n])
        elif self.guess_mode_==1:
            for n in range(guess_bin_count):
                guess.append(1.0)
        else:
            for n in range(guess_bin_count):
                guess.append(random())
            

        if self.iter_<1:
            self.iter_=1

        if self.bootstrap_flag_==False or self.bootstrap_iter_<=1:
            if self.iter_<=1:
                (llfit,guess) = self.run_llfit(data,guess,best_N,sampling,hk,herm_mode)
                return (llfit,wbin_guess)
            else:
                cllfit = ex.alg.LLFitAmpComposite()
                for n in range(self.iter_):
                    (llfit,guess) = self.run_llfit(data,guess,best_N,sampling,hk,herm_mode)
                    if not llfit:
                        LogVerbose("skipping this composite llfit")
                        return (None,wbin_guess)
                    if llfit.GetIterCount()<self.niter_:
                        LogVerbose("adding converged (%d) iteration to cumulative llfit"%llfit.GetIterCount())
                        cllfit.Add(llfit)
                    elif self.iter_conv_flag_:
                        LogVerbose("adding non-converged iteration to cumulative llfit")
                        cllfit.Add(llfit)
                        
                return (cllfit,wbin_guess)
        else:
            # TODO: combine bootstraping with normal iterations ?
            rmult = float(len(data))
            cllfit = ex.alg.LLFitAmpComposite()
            for bn in range(self.bootstrap_iter_):
		LogVerbose("bootstrap iteration %d"%bn)
                temp_data=[]
                for dn in range(len(data)):
                    index = int(random()*rmult)
                    temp_data.append(data[index])
                (llfit,guess) = self.run_llfit(temp_data,guess,best_N,sampling,hk,herm_mode)
                if not llfit:
                    return (None,wbin_guess)
                cllfit.Add(llfit)
            return (cllfit,wbin_guess)
                

    def calc_weight(self,d):
        # d[1] is the amplitude
        # d[2] is the standard deviation of the amplitude
        # d[3] capped quality (0 to 1)
        we = 1e-20
        if self.wscheme_==1:
            # standard reciprocal variance
            if d[2]>1e-10:
                we = 1.0/(d[2]*d[2])
        elif self.wscheme_==2:
            # standard reciprocal variance scaled
            # with the quality
            if d[2]>1e-10:
                we = d[3]/(d[2]*d[2])
        elif self.wscheme_==3:
            # sort of a heuristic hack
            # sigma is calculated using the cap
            # introduced by the quality
            if abs(d[1])>1:
                we = d[3]*d[3]/(d[1]*d[1])

        return we * self.global_weight_

    def assemble_next(self,hk):
        """
        works with all herm_modes
        """
        data=[]
        min_zstar = 1e10
        max_zstar = -1e10
        cdf_m = (self.quality_t2_+self.quality_t1_)*0.5
        cdf_s = self.quality_t2_ - cdf_m
        while self.current_rp_.IsValid() and self.current_rp_.GetIndex().AsDuplet()==hk:
            LogVerbose("working on %s"%(self.current_rp_.GetIndex()))
            zstar=self.current_rp_.GetIndex().GetZStar()
            min_zstar=min(min_zstar,zstar)
            max_zstar=max(max_zstar,zstar)
            iobs=self.current_rp_.Get(self.iobs_name_)
            sigi=self.current_rp_.Get(self.sigiobs_name_)
            if sigi<=0.0:
                self.current_rp_.Inc()
                continue
            quality = ex.alg.CDF(iobs/sigi,cdf_m,cdf_s)
            if quality<1e-10:
                quality=1e-10
            self.current_rp_.Set(self.quality_name_,quality)
            if quality>=self.min_qual_:
                data.append((zstar,iobs,sigi,quality))
                if self.phantom_mode_:
                    sigmazstar=self.current_rp_.Get("sigmazstar")
                    data.append((zstar+sigmazstar,iobs,sigi,quality))
                    data.append((zstar-sigmazstar,iobs,sigi,quality))
                    
            self.current_rp_.Inc()

        return (data,max(abs(max_zstar),abs(min_zstar)))


