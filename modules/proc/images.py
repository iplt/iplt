#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from wx import ImageFromStream, BitmapFromImage
import cStringIO, zlib

# Generated with img2py
# Displays a check

def getYesData():
    return zlib.decompress(
'x\xda\xeb\x0c\xf0s\xe7\xe5\x92\xe2b``\xe0\xf5\xf4p\t\x02\xd2\x02@\xcc\xcf\
\xc1\x06$\xdf\x16\xfb\xeb\x03)\x96b\'\xcf\x10\x0e \xa8\xe1H\xe9\x00\xf2\xcb=\
]\x1cC4&&\xa7\xa4$\xa5-`1\x98\xb8\xea`u\xe7\xf3\xfa\xf7\xaa\xf3\xfe\x9f4c\
\xc8\x0bP>"0\x89\xa7\xe1v\xcd\xf4\xc6\xc4\xec\xbfen]1\x0c\xcc\x1al\x0b~\xf7O\
xk\x11\xcb\xf0Y\xfe\xa4\xde\r)f\xd3H\xc9\xab\x021\xdb\x1e\x970j6\x08F;9m\x9b\
\xe8(\xa9\xd5q\xb4\xec\xd6N\x86\xa3\xa2\x159\xc7_3U4\x08\x950\t\x1b\xc7\xbd\
\xb9\xfc\xe6\xb3\xfcz)\xafbe\x06\xc6\xc9\xfaE"\x9a\r\xe1@\'0x\xba\xfa\xb9\
\xacsJh\x02\x00rP?\xfd' )

def getYesBitmap():
    return BitmapFromImage(getYesImage())

def getYesImage():
    stream = cStringIO.StringIO(getYesData())
    return ImageFromStream(stream)
