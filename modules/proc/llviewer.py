#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Authors: Ansgar Philippsen, Andreas Schenk
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import iplt.gui.rlistgrid as rlistgrid
from ost import gui
import math
import sip
from ost import LogVerbose,LogInfo,LogError,LogVerbose
import iplt.proc.plotviewer
import ost.gui

def sinc(x,w):
    if x==0.0:
        return 1.0
    pwx = math.pi*w*x
    return math.sin(pwx)/pwx


class LatticeLineViewer(QMainWindow): 
    DataClicked = pyqtSignal(float,float,str)
    def __init__(self,rlist,flist=None,clist=None,wlim=0.0,restrict=None):
        QMainWindow.__init__(self)
        LogVerbose("LLV: init")
        self.rlist_=rlist
        self.flist_=flist
        self.clist_=clist
        self.restrict_=restrict
        self.has_id_=rlist.HasProperty("id")
        self.has_quality_=rlist.HasProperty("quality")
        self.has_sigmaz_=rlist.HasProperty("sigmazstar")
        if self.has_sigmaz_:
            LogVerbose("found property sigmazstar")
        self.p_iobs_="iobs"
        self.p_sigiobs_="sigiobs"
        self.p_icalc_="icalc"
        self.p_sigicalc_="sigicalc"
        self.has_bin_ave_=False
        self.has_ifit_=rlist.HasProperty("ifit")
        if self.has_ifit_:
            LogVerbose("found property ifit")
        if flist:
            self.has_bin_ave_ = flist.HasProperty("bin_ave")
            LogVerbose("found property bin_ave")
        self.has_icalc_=rlist.HasProperty("icalc")
        if self.has_icalc_:
            LogVerbose("found property icalc")
        self.has_sigicalc_=rlist.HasProperty("sigicalc")
        self.wlim_=wlim
        self.pdi_iobs_q_=None
        self.pdi_iobs_e_=None
        self.pdi_iobs_f_=None
        self.pdi_icalc_=None
        self.pdi_curve_=None
        self.pdi_disc_=None
        self.pdi_bin_ave_=None

        self.flist2_=None
        self.pdi_disc2_=None
        self.clist2_=None
        self.pdi_curve2_=None

        self.grid_=rlistgrid.RlistGrid(None,rlist)
        QObject.connect(self.grid_,SIGNAL("SelectionChanged"),self.Refresh)
        self.grid_.setWindowTitle("Lattice Line Viewer")
        self.grid_.show()

        self.plotview_=iplt.proc.plotviewer.PlotViewer()

        self.plotview_.DataPicked.connect(self.DataClicked)
        self.setCentralWidget(self.plotview_)
        dw=QDockWidget(self)
        dw.setWidget(self.grid_)
        dw.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        dw.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.addDockWidget(Qt.RightDockWidgetArea, dw)
        app=ost.gui.GostyApp.Instance()
        app.perspective.main_area.AddWidget('lattice line viewer', self)

        self.DrawData(self.grid_.GetSelection())

    def SetComparisonFit(self,flist,clist):
        self.flist2_=flist
        self.clist2_=clist
    
    def GetIndexSelection(self):
        return self.grid_.GetSelection()

    def Refresh(self,selection):
        self.plotview_.ClearData()
        self.DrawData(selection)
    
    def DrawData(self,selection):
        if len(selection)==0:
          LogError("Empty selection")          
          return
        point=selection[0]
        LogVerbose("DrawData ")
        # draw datapoints
        count=0
        pdata_iobs_q = iplt.proc.plotviewer.PlotData()
        pdata_iobs_e = iplt.proc.plotviewer.PlotData()
        pdata_iobs_f = None
        if self.has_ifit_:
            pdata_iobs_f = iplt.proc.plotviewer.PlotData()
            
        pdata_icalc=None
        if self.has_icalc_:
            pdata_icalc = iplt.proc.plotviewer.PlotData()
            pdata_icalc.converged = 0.0

        rprox=self.rlist_.FindFirst(point)

        while rprox.IsValid() and rprox.GetIndex().AsDuplet()==point:
            flag = True
            if self.restrict_ and self.restrict_!=int(rprox.Get("id")):
                flag=False
            
            vx=rprox.GetIndex().GetZStar()
            vy1=rprox.Get(self.p_iobs_)
            sigvy1=rprox.Get(self.p_sigiobs_)
            vy2=0.0
            id=""
            w=1.0
            if self.has_id_:
                id="%06d"%(int(rprox.Get("id")))
            if self.has_quality_:
                w=rprox.Get("quality")
            if w<self.wlim_:
                flag=False

            if flag:
                pdata_iobs_q.x.append(vx) 
                pdata_iobs_q.y.append(vy1) 
                pdata_iobs_q.q.append(w)
                pdata_iobs_q.info.append(id) 
                if self.has_sigmaz_:
                    sigvx=rprox.Get("sigmazstar")
                    pdata_iobs_e.x.append(vx) 
                    pdata_iobs_e.y.append(vy1) 
                    pdata_iobs_e.ex.append(sigvx) 
                    pdata_iobs_e.ey.append(sigvy1) 
                    pdata_iobs_e.info.append('') 
                else:
                    pdata_iobs_e.x.append(vx) 
                    pdata_iobs_e.ex.append(0) 
                    pdata_iobs_e.y.append(vy1) 
                    pdata_iobs_e.ey.append(sigvy1) 
                    pdata_iobs_e.info.append('') 
                if self.has_ifit_:
                    ifit = rprox.Get("ifit")
                    zfit = rprox.Get("zfit")
                    pdata_iobs_f.x.append(vx) 
                    pdata_iobs_f.ex.append(zfit-vx) 
                    pdata_iobs_f.y.append(vy1) 
                    pdata_iobs_f.ey.append(ifit-vy1) 
                    pdata_iobs_f.info.append('') 
                    
                if self.has_icalc_:
                    vy2=rprox.Get(self.p_icalc_)
                    pdata_icalc.x.append(vx) 
                    pdata_icalc.y.append(vy2) 
                    pdata_icalc.info.append('') 
                    pdata_icalc.converged=rprox.Get('cflag')
                    if self.has_sigicalc_:
                        sigvy2=rprox.Get(self.p_sigicalc_)
                        pdata_icalc.ey.append(sigvy2) 
                    else:
                        pdata_icalc.q.append(w) 
                count+=1
            rprox.Inc()

        LogVerbose( "using %d reflections for %s"%(count,point))

        # iobs w plot
        pdi_tmp=self.plotview_.scatter(pdata_iobs_q.x,pdata_iobs_q.y,label='iobs w',infotext=pdata_iobs_q.info,color='blue')
        self.plotview_.AutoscaleView()
        self.pdi_iobs_q_=pdi_tmp

        # iobs sig plot
        pdi_tmp=self.plotview_.errorbar(pdata_iobs_e.x,pdata_iobs_e.y,xerr=pdata_iobs_e.ex,yerr=pdata_iobs_e.ey,fmt='o',label='iobs sig',color='blue',infotext=pdata_iobs_e.info)
        self.pdi_iobs_e_=pdi_tmp

        # ifit plot
        if self.has_ifit_:
            pdi_tmp=self.plotview_.scatter(pdata_iobs_f.x,pdata_iobs_f.y,label='ifit',infotext=pdata_iobs_f.info,color='cyan')
            self.pdi_iobs_f_=pdi_tmp

        # icalc plot
        if self.has_icalc_:
            if pdata_icalc.converged:
              icalc_color='darkgreen'
            else:
              icalc_color='darkred'
            pdi_tmp=self.plotview_.scatter(pdata_icalc.x,pdata_icalc.y,label='icalc',infotext=pdata_icalc.info,color=icalc_color)
            self.pdi_icalc_=pdi_tmp

        # draw curve if present
        if self.clist_:
            rp = self.clist_.FindFirst(point)
            if rp.IsValid():
                LogVerbose("drawing curve")
            else:
                LogVerbose("no curve found for this index")
                
            if rp.IsValid() and self.clist_.HasProperty("icalc"):
                plot_fd4=iplt.proc.plotviewer.PlotData()
                plot_fd4.converged=0.0
                while rp.GetIndex().AsDuplet()==point:
                    plot_fd4.x.append(rp.GetIndex().GetZStar())
                    plot_fd4.y.append(rp.Get("icalc"))
                    plot_fd4.ey.append(rp.Get("sigicalc"))
                    #plot_fd4.converged=rp.Get('cflag')
                    rp.Inc()

                if pdata_icalc.converged:
                  icalc_color='green'
                else:
                  icalc_color='red'
                pdi_tmp=self.plotview_.plot(plot_fd4.x,plot_fd4.y,label='curve',infotext=plot_fd4.info,color=icalc_color)
                self.pdi_curve_=pdi_tmp
            

        # draw fit if present
        if self.flist_:
            rp = self.flist_.FindFirst(point)
            if rp.IsValid():
                LogVerbose("drawing fit")
            else:
                LogVerbose("no fit found for this index")
            if rp.IsValid():
                has_sinc_amp = self.flist_.HasProperty("sinc_amp")
                
                sc=[]
                plot_fd1=iplt.proc.plotviewer.PlotData()
                plot_fd1.converged= 0
                plot_fd3=iplt.proc.plotviewer.PlotData()
                while rp.GetIndex().AsDuplet()==point:
                    iobs = rp.Get("iobs")
                    sigiobs = rp.Get("sigiobs")
                    if sigiobs!=sigiobs: # check for NaN
                        sigiobs = 1e100
                    plot_fd1.x.append(rp.GetIndex().GetZStar())
                    plot_fd1.y.append(iobs)
                    plot_fd1.ey.append(sigiobs)
                    #plot_fd1.converged=rp.Get('cflag')
                    if self.has_bin_ave_:
                        plot_fd3.x.append(rp.GetIndex().GetZStar())
                        plot_fd3.y.append(rp.Get("bin_ave"))
                    if has_sinc_amp:
                        sc_amp = rp.Get("sinc_amp")
                        sc_phi = rp.Get("sinc_phi")
                        sc.append(complex(sc_amp*math.cos(sc_phi),sc_amp*math.sin(sc_phi)))
                    rp.Inc()
                if pdata_icalc.converged:
                  disc_color='green'
                else:
                  disc_color='red'
                pdi_tmp=self.plotview_.scatter(plot_fd1.x,plot_fd1.y,label='disc',infotext=plot_fd1.info,color=disc_color)
                self.pdi_disc_=pdi_tmp

                
                if self.has_bin_ave_:
                    pdi_tmp=self.plotview_.scatter(plot_fd3.x,plot_fd3.y,label='bin_ave',infotext=plot_fd3.info,color='grey')
                    self.pdi_bin_ave_=pdi_tmp

        # comparison lists
        if self.clist2_:
            rp = self.clist2_.FindFirst(point)
            if rp.IsValid() and self.clist2_.HasProperty("icalc"):
                plot_fd42=iplt.proc.plotviewer.PlotData()
                while rp.GetIndex().AsDuplet()==point:
                    plot_fd42.x.append(rp.GetIndex().GetZStar())
                    plot_fd42.y.append(rp.Get("icalc"))
                    plot_fd42.ey.append(rp.Get("sigicalc"))
                    rp.Inc()

                pdi_tmp=self.plotview_.plot(plot_fd42.x,plot_fd42.y,label='curve2',infotext=plot_fd42.info,color='darkred')
                self.pdi_curve2_=pdi_tmp
                
        if self.flist2_:
            rp = self.flist2_.FindFirst(point)
            if rp.IsValid():
                plot_fd12=iplt.proc.plotviewer.PlotData()
                while rp.GetIndex().AsDuplet()==point:
                    iobs = rp.Get("iobs")
                    sigiobs = rp.Get("sigiobs")
                    if sigiobs!=sigiobs: # check for NaN
                        sigiobs = 1e100
                    plot_fd12.x.append(rp.GetIndex().GetZStar())
                    plot_fd12.y.append(iobs)
                    plot_fd12.ey.append(sigiobs)
                    rp.Inc()
                pdi_tmp=self.plotview_.scatter(plot_fd12.x,plot_fd12.y,label='disc2',infotext=plot_fd12.info,color='magenta')
                self.pdi_disc2_=pdi_tmp

      
