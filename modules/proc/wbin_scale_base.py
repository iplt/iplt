#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import math
from ost.img import *
import iplt as ex
from ost import LogVerbose,LogInfo,LogError,LogVerbose

#   a note on error propagation:
#   standard deviation for ln(I1/I2) is given by
#
#     sqrt( (S1/I1)^2 + (S2/I2)^2 )
#
#   which is passed to the linear fit routine (as weight=1/sig^2)
#   the estimate method returns the multiplication factor F as
#   logF and siglogF, from which F and its sigma can be calculated as
#
#     F = exp(logF)
#     SF = F * siglogF
#
#   the scaled intensity is the given by
#    I2 * F
#
#   and the corresponding sigma by
#    sqrt( (SF/F)^2 + (S2/I2)^2 ) * I2 * F
#
#   UPDATE: conceptually, this seems wrong. the scaled sigma should not
#   contain the error from the scaling factor, hence
#     SIGI2F = SIGI2 * F


# helper functions
def create_wbin2(rlist,bcount,rlow,rhigh):
    wbin = ex.alg.WeightedBin(bcount,
                              1.0/(rlow*rlow),
                              1.0/(rhigh*rhigh))
    for rp in rlist:
        resol=rp.GetResolution()
        rr=1.0/(resol*resol)
        iobs=rp.Get("iobs")
        sigiobs=rp.Get("sigiobs")
        if sigiobs>0.0:
            wbin.Add(rr,iobs,1.0/(sigiobs*sigiobs))
    return wbin

def create_wbin(rlist,bcount,rlow,rhigh):
    return ex.alg.RListWBin(rlist,bcount,rlow,rhigh,"iobs","sigiobs")


class WBinScaleBase:
    def __init__(self,reference,bcount,rlow,rhigh):
        self.bcount_=bcount
        self.rlow_=rlow
        self.rhigh_=rhigh
        self.ref_wbin_=create_wbin(reference,self.bcount_,self.rlow_,self.rhigh_)
        self.wbin_isigi_cutoff_=0.0
        self.wbin_sigma_cutoff_=0.0
        bin_size = (1.0/(rhigh*rhigh)-1.0/(rlow*rlow))/float(bcount)
        self.filter_rlow_=rlow
        self.filter_rhigh_=2.0
        self.filter_bcount_=1.0/(self.filter_rhigh_*self.filter_rhigh_)-1.0/(self.filter_rlow_*self.filter_rlow_)/bin_size

    def SetWBinISigICutoff(self,c):
        self.wbin_isigi_cutoff_=c

    def SetWBinSigmaCutoff(self,c):
        self.wbin_sigma_cutoff_=c

    def SetFilterRLim(self,rlow,rhigh,bcount):
        self.filter_rlow_=rlow
        self.filter_rhigh=rhigh
        self.filter_bcount_=bcount


    def resolution_limit_filter(self,rlist,name):
        LogVerbose("rlim filter: %d %g %g"%(self.filter_bcount_,self.filter_rlow_,self.filter_rhigh_))
        wbin = create_wbin(rlist,self.filter_bcount_,self.filter_rlow_,self.filter_rhigh_)

        # first get resolution limit of highest non-empty bin
        resolution_limit = 0.0
        for n in range(wbin.GetBinCount()):
            x=wbin.GetLimit(n)
            if wbin.GetSize(n)>0:
                resolution_limit = math.sqrt(1.0/x)

        # then determine limit by additional filter criteria
        for n in range(wbin.GetBinCount()):
            x=wbin.GetLimit(n)
            I2 = wbin.GetAverage(n)
            S2 = wbin.GetStdDev(n)
            if S2<=0.0:
                continue
            if n>0 and abs(I2/S2)<self.wbin_isigi_cutoff_: # n>0 is a heuristic hack
                resolution_limit = math.sqrt(1.0/x)
                break
        LogVerbose("resolution limit at %gA (bin %d)"%(resolution_limit,n))
        rlist_new = ex.ReflectionList(rlist,False)
        for rp in rlist:
            indx=str(rp.GetIndex())
            res=rp.GetResolution()
            if res<resolution_limit:
                LogVerbose(" %s r=%g not used (resolution limit check)"%(indx,res))
                continue
            bin = wbin.CalcBin(1.0/(res*res))
            if bin==-1:
                LogVerbose(" %s r=%g not used (resolution limit check)"%(indx,res))
                continue
            wbin_sigma = wbin.GetStdDev(bin)
            wbin_i = wbin.GetAverage(bin)
            if wbin_sigma<0.0:
                LogVerbose(" %s r=%g not used (wbin sigma<0)"%(indx,res))
                continue
# this doesn't make sense (I2-wbin_sigma) should id be (iobs- wbin_sigma)?              
#            if wbin_sigma>0 and (abs(I2-wbin_i)/wbin_sigma)>self.wbin_sigma_cutoff_:
#                LogVerbose(" %s r=%g not used (wbin sigma limit: (%g - %g) > %g * %g)"%(indx,res,I2,wbin_i,self.wbin_sigma_cutoff_,wbin_sigma))
#                continue
            if wbin_sigma>0 and (abs(rp.Get('iobs')-wbin_i)/wbin_sigma)>self.wbin_sigma_cutoff_:
                LogVerbose(" %s r=%g not used (wbin sigma limit: (%g - %g) > %g * %g)"%(indx,res,I2,wbin_i,self.wbin_sigma_cutoff_,wbin_sigma))
                continue

            rlist_new.AddProxyter(rp)

        LogVerbose("%s: using %d of %d reflections for wbin scaling"%(name,rlist_new.NumReflections(),rlist.NumReflections()))
        return rlist_new



class AnisoWbinScale(WBinScaleBase):
    def __init__(self,reference,bcount,rlow,rhigh,swidth):
        WBinScaleBase.__init__(self,reference,bcount,rlow,rhigh)
        self.swidth_=swidth

    def Apply(self,rlist2,tg,name=""):
        rlist=self.resolution_limit_filter(rlist2,name)
        
        uc = rlist.GetUnitCell()
        lat = rlist.GetLattice()
        rsampling = ex.CalcReciprocalSampling(tg,uc,lat)

        ascale=ex.alg.AnisoScaling()

        # this variant uses each reflection individually
        # instead of pre-binning the data
        for rp in rlist:
            #point=rp.GetIndex().AsDuplet()
            point=Point(int(rp.Get("symh")),int(rp.Get("symk")))
            tc = rsampling*ex.CalcTiltComponents(tg,lat,point)
            iobs = rp.Get("iobs")
            sigiobs = rp.Get("sigiobs")
            if sigiobs<=0.0:
                continue
            we=1.0/(sigiobs*sigiobs)
            # lookup appropriate wbin entry of reference for iref
            ir2=1.0/(rp.GetResolution()*rp.GetResolution())
            ref_bin_n = self.ref_wbin_.CalcBin(ir2)
            if ref_bin_n>=0:
                # todo: use linear interpolation between bins
                # for a better estimate ?
                iref = self.ref_wbin_.GetAverage(ref_bin_n)
                if iobs>0.0 and iref>0.0:
                    ascale.Add(math.log(iref/iobs),tc[0],tc[1],we)


        ascale.Apply()
        LogVerbose("%s: scale: %g  bfacs: %g %g %g  est(0,0): %g"%(name,math.exp(ascale.GetA()),ascale.GetB1(),ascale.GetB2(),ascale.GetB3(),ascale.Estimate(0.0,0.0)))

        # copy original rlist
        rlist3 = ex.ReflectionList(rlist2)

        if not rlist3.HasProperty("scale"):
            rlist3.AddProperty("scale")
        if not rlist3.HasProperty("sigscale"):
            rlist3.AddProperty("sigscale")

        # apply scaling to complete dataset, not only filtered one
        for rp in rlist3:
            #point=rp.GetIndex().AsDuplet()
            point=Point(int(rp.Get("symh")),int(rp.Get("symk")))
            tc = rsampling*ex.CalcTiltComponents(tg,lat,point)
            logF = ascale.Estimate(tc[0],tc[1])
            F = math.exp(logF)
            I2 = rp.Get("iobs")
            I2F = I2*F
            rp.Set("iobs",I2F)
            S2 = rp.Get("sigiobs")
            S2F = S2*F
            rp.Set("sigiobs",S2F)
            rp.Set("scale",F)
            LogVerbose(" %s tc=%s  logF=%g  F=%g  I2=%g (%g)  I2F=%g (%g)"%(str(rp.GetIndex()),str(tc),logF,F,I2,S2,I2F,S2F))

        return rlist3



class WbinScale(WBinScaleBase):
    def __init__(self,reference,bcount,rlow,rhigh):
        WBinScaleBase.__init__(self,reference,bcount,rlow,rhigh)
        
    def Apply(self,rlist2,unity_scaling,name=""):

        rlist=self.resolution_limit_filter(rlist2,name)

        if unity_scaling:
            if not rlist.HasProperty("scale"):
                rlist.AddProperty("scale")
            if not rlist.HasProperty("sigscale"):
                rlist.AddProperty("sigscale")
            for rp in rlist:
                rp.Set("scale",1.0)
                rp.Set("sigscale",0.0)
            return rlist
            
        wbin = create_wbin(rlist,self.bcount_,self.rlow_,self.rhigh_)
        linfit=ex.alg.LinearFit()
        linfit.ForceOrigin(False)

        count=0
        for n in range(wbin.GetBinCount()):
            count += wbin.GetSize(n)
            x = wbin.GetLimit(n)
            I2 = wbin.GetAverage(n)
            S2 = wbin.GetStdDev(n)
            I1 = self.ref_wbin_.GetAverage(n)
            S1 = self.ref_wbin_.GetStdDev(n)
            if I1>0.0 and I2>0.0:
                y=math.log(I1/I2)
                sigy2=( S1*S1/(I1*I1) + S2*S2/(I2*I2) )
                if sigy2==0.0:
                  linfit.AddDatapoint(x,y,1e12) # hack to avoid division by 0
                else:
                  linfit.AddDatapoint(x,y,1.0/sigy2)

        if linfit.PointCount()<3:
            LogInfo("%s: scale: not enough bins for scaling, rejecting"%(name))
            return None

        linfit.Apply()
        bfactor=linfit.GetScale()
        bfactor_sig=math.sqrt(linfit.GetScaleVariance())
        scale=math.exp(linfit.GetOffset())
        scale_sig=math.sqrt(linfit.GetOffsetVariance())*scale

        LogInfo("%s: scale: %g (%g)  bfac: %g (%g)  est(0): %g"%(name,scale,scale_sig,bfactor,bfactor_sig,(linfit.Estimate(0.0))[0]))

        # copy original rlist
        rlist3 = ex.ReflectionList(rlist2)

        if not rlist3.HasProperty("scale"):
            rlist3.AddProperty("scale")
        if not rlist3.HasProperty("sigscale"):
            rlist3.AddProperty("sigscale")


        # apply scaling to complete dataset, not only filtered one
        for rp in rlist3:
            indx = "(%d,%d,%g"%((int(rp.Get("symh")),int(rp.Get("symk")),rp.Get("symzstar")))
            res=rp.GetResolution()
            (logF,siglogF) = linfit.Estimate(1.0/(res*res))
            F = math.exp(logF)
            I2 = rp.Get("iobs")
            sigF = siglogF * F
            S2 = rp.Get("sigiobs")
            I2F = I2*F
            rp.Set("iobs",I2F)
            S2F = S2*F
            rp.Set("sigiobs",S2F)
            rp.Set("scale",F)
            rp.Set("sigscale",sigF)
            LogVerbose(" %s r=%g  logF=%g (%g)  F=%g (%g)  I2=%g (%g)  I2F=%g (%g)"%(indx,res,logF,siglogF,F,sigF,I2,S2,I2F,S2F))

        return rlist3

