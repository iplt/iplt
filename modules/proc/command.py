#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Andreas Schenk
#
# system specific imports
import os,re,sys
from optparse import OptionParser,OptionGroup

#ost imports
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost import PushVerbosityLevel

# iplt imports
from iplt.proc.dirscan import dirscan
from ost.img import *

class Command:
    def __init__(self,name):
        self.name_=name
        self.subcommand_list_={}
        self.parser_=OptionParser()
        self.options_=[]
        self.args_=[]
        self.img_mask_=""
        self.parser_.add_option("-v", "--verbosity",
                                type="int", default=3, dest="verbosity",
                                help="Set verbosity level (default=3)")
        self.parser_.add_option("-a","--all",
                                action="store_true",default=False,dest="process_ignore",
                                help="process even ignored datasets")
        self.parser_.add_option("-i", "--image-mask",
                                type="str", default=".*",dest="dir_mask",
                                help="image mask that particular processing will be applied to, defaults to *")
    def AddSubCommand(self,subcommand):
        self.subcommand_list_[subcommand.GetName()]=subcommand
        og=OptionGroup(self.parser_,title=subcommand.GetName()+" options")
        og.AddOption=og.add_option
        subcommand.ExtendParser(og)
        if len(og.option_list)>0:
                self.parser_.add_option_group(og)

    def AddOption(self,short,long=None,type=None,default=None,dest=None,help=None,action=None):
        self.parser_.add_option(short,long,type=type,default=default,dest=dest,help=help,action=action)
    def GetOptions(self):
        return self.options_
    def GetArgs(self):
        return self.args_

    def Parse(self):
        usage = self.name_+" [options] COMMAND COMMAND_OPTIONS\n"
        usage += "\nwhere COMMAND is one of:\n"
        for key in self.subcommand_list_:
            usage += "   "+key+": "+self.subcommand_list_[key].GetHelp()+"\n"
        usage += "\nand COMMAND_OPTIONS depends on the command"
        self.parser_.set_usage(usage)   
        (self.options_,self.args_) = self.parser_.parse_args()
        if len(self.args_)<1:
            self.parser_.print_help()
            sys.exit(-1)
            
    def SetImageMask(self,mask):
        self.img_mask_=mask
        
    def Run(self):
        self.Parse()
        PushVerbosityLevel(self.options_.verbosity)
        work_dir_=os.getcwd()
        for subcommandname in self.args_:
            if subcommandname=="clean_all":
                subcommand="clean_all"
            elif  subcommandname in self.subcommand_list_.keys():
                subcommand=self.subcommand_list_[subcommandname]
            else:
                LogError("unrecognized command: "+subcommandname)
                self.parser_.print_help()
                sys.exit(-1)
            self.Run2(subcommand,work_dir_)

    def Run2(self,subcommand,work_dir_):
        mask_list = self.options_.dir_mask.split(',')
        final_mask = ""
        for mm in mask_list:
            final_mask+=("(%s)|"%mm)
        final_mask=final_mask[:-1]
        LogVerbose("using image mask: %s"%(final_mask))
        dir_mask_re = re.compile(final_mask)
        # prepare image directory queue
        dentrylist=[]
        for dentry in dirscan(self.img_mask_,work_dir_):
            if not dir_mask_re.match(dentry[1]):
                continue
            if(not self.GetOptions().process_ignore and os.path.exists(os.path.join(dentry[0],dentry[1],"ignore") )):
                LogError( "ignoring "+dentry[1])
                continue
            dentrylist.append(dentry)
        if subcommand=="clean_all":
            for d in dentrylist:
                wdir=os.path.join(d[0],d[1])
                for key in self.subcommand_list_:
                    self.subcommand_list_[key].Cleanup(wdir)
        else:
            subcommand.Run(dentrylist,self.GetArgs()[1:],self.GetOptions())
