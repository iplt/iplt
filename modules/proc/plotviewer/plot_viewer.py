#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtGui import *
from PyQt4.QtCore import *
from plot_viewer_form_ui import Ui_PlotViewerForm
from matplotlib.lines import *
import ost.gui as gui

class PlotData:
  def __init__(self):
    self.x=[]
    self.y=[]
    self.ex=[]
    self.ey=[]
    self.q=[]
    self.info=[]


class PlotViewer(QMainWindow,Ui_PlotViewerForm):
  DataPicked = pyqtSignal(float,float,str)
  def __init__(self,title='PlotViewer',parent=None):
    QMainWindow.__init__(self,parent)
    self.setupUi(self)
    self.datasets_=[]
    self.viewer_panel_.DataPicked.connect(self.DataPicked)
    self.viewer_panel_.axes_.axhline(color='black',linewidth='0.5')
    self.viewer_panel_.axes_.axvline(color='black',linewidth='0.5')
   # app=gui.GostyApp.Instance()
   # app.perspective.main_area.AddWidget(title, self)

  def errorbar(self,*args,**kwargs):
    infotext=self.prepare_kwargs(kwargs)
    self.datasets_.append(self.viewer_panel_.errorbar(*args,**kwargs))
    self.datasets_[-1][0].infotext_=infotext
    self.Update()
    return self.datasets_[-1]

  def plot(self,*args,**kwargs):
    infotext=self.prepare_kwargs(kwargs)
    self.datasets_.append(self.viewer_panel_.plot(*args,**kwargs))
    self.datasets_[-1][0].infotext_=infotext
    self.Update()
    return self.datasets_[-1]

  def scatter(self,*args,**kwargs):
    infotext=self.prepare_kwargs(kwargs)
    self.datasets_.append([self.viewer_panel_.scatter(*args,**kwargs)])
    self.datasets_[-1][0].infotext_=infotext
    self.Update()
    return self.datasets_[-1][0]
    
  def Update(self):
    self.UpdateDatasets()
    self.UpdateLegend()
    self.viewer_panel_.draw()
    self.legend_panel_.draw()
    
  def UpdateDatasets(self):
    self.dataset_list_.blockSignals(True)
    self.dataset_list_.clear()
    for dataset in self.datasets_:
      mainartist=dataset[0]
      item=QListWidgetItem(mainartist.get_label(),self.dataset_list_)
      if mainartist.get_visible():
        item.setCheckState(Qt.Checked)
      else:
        item.setCheckState(Qt.Unchecked)
    self.dataset_list_.blockSignals(False)
          
  def UpdateLegend(self):
    h,l=self.viewer_panel_.axes_.get_legend_handles_labels()
    self.legend_panel_.SetHandles(h,l)
    
  def OnDatasetActivated(self,item):
    dataset=self.datasets_[self.dataset_lsit_.row(item)]
    
  def OnDatasetChanged(self,item):
    row=self.dataset_list_.row(item)
    data=self.datasets_[row]
    visible=item.checkState()==Qt.Checked
    self.set_data_visible(data,visible)
    self.viewer_panel_.draw()
    
  def set_data_visible(self,data,visible):
    if type(data)== list or type(data)== tuple:
      for child in data:
       self.set_data_visible(child,visible)
    else:
      data.set_visible(visible)
    
  def prepare_kwargs(self,kwargs):
    kwargs['picker']=True
    if not 'label' in kwargs:
      kwargs['label']='Dataset'+str(len(self.datasets_))
    if 'infotext' in kwargs:
      infotext=kwargs['infotext']
      del kwargs['infotext']
      return infotext
    else:
      return []

  def ClearData(self):
    self.datasets_=[]
    self.viewer_panel_.Clear()
    self.viewer_panel_.axes_.axhline(color='black',linewidth='0.5')
    self.viewer_panel_.axes_.axvline(color='black',linewidth='0.5')
    self.Update()
  

  def OnSavePlot(self):
    pass

  def OnShowLegendTriggered(self,flag):
    pass

  def OnShowDatasetsTriggered(self,flag):
    pass
  
  def GetAxes(self):
    return self.viewer_panel_.GetAxes()
    
  def SetMinimumX(self,x):
    self.viewer_panel_.SetMinimumX(x)

  def SetMaximumX(self,x):
    self.viewer_panel_.SetMaximumX(x)

  def SetMinimumY(self,y):
    self.viewer_panel_.SetMinimumY(y)

  def SetMaximumY(self,y):
    self.viewer_panel_.SetMaximumY(y)

  def AutoscaleView(self):
    self.viewer_panel_.AutoscaleView()    