#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from math import exp
import matplotlib.font_manager 

class LegendPanel(FigureCanvas):
  def __init__(self, parent=None):
    self.figure_ = Figure(figsize=(2,1), dpi=100,facecolor='white')
    FigureCanvas.__init__(self, self.figure_)
    self.setParent(parent)
    self.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
    self.figure_.gca().set_axis_off()

  def SetHandles(self,h,l):
    self.figure_.gca().legend_=None
    if len(h)>0:
      prop = matplotlib.font_manager.FontProperties(size=10) 
      self.legend_=self.figure_.gca().legend(h,l,loc=10,prop=prop)
      self.legend_.draw_frame(False)
      self.draw_idle()

