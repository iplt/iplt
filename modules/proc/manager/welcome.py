#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Andreas Schenk
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import os,os.path
import iplt as ex
from ost import Units
from iplt.proc.manager.manager_params import *


class ProjectParams(ManagerParamsForm):
  def __init__(self,parent=None):
    ManagerParamsForm.__init__(self,"Project",None,parent)
    format_list=QComboBox()
    format_list.addItem("tif")
    format_list.addItem("dm3")
    format_list.addItem("mrc")
    self.Add("Raw Image Format",
             format_list,
             "Image format of the raw image")
    prefix_edit=QLineEdit("ABC")
    self.Add("Project Prefix",
             prefix_edit,
             "Three letter code for the project")


class NewProjectWizard(QWizard):
  def __init__(self,parent):
    QWizard.__init__(self,parent)
    self.addPage(self.CreateDirPage())
    self.addPage(self.CreateProjectSettingsPage())
    self.addPage(self.CreateCellSettingsPage())

  def CreateDirPage(self):
    page=QWizardPage()
    page.setTitle("Directory")
    page.setSubTitle("Choose directory for new project")
    hb=QHBoxLayout()

    self.dirpathedit_=QLineEdit(os.getcwd())  
    hb.addWidget(self.dirpathedit_)

    browsebutton=QPushButton("Browse ...")
    QObject.connect(browsebutton,SIGNAL("clicked()"),self.BrowseDir)
    hb.addWidget(browsebutton)
    page.setLayout(hb)
    return page
    
  def CreateProjectSettingsPage(self):
    page=QWizardPage()
    page.setTitle("Project Settings")
    page.setSubTitle("Please enter the basic project information")
    vb=QVBoxLayout()
    self.project_params_=ProjectParams()
    vb.addWidget(self.project_params_)
    page.setLayout(vb)
    return page

  def CreateCellSettingsPage(self):
    page=QWizardPage()
    page.setTitle("Unit Cell Settings")
    page.setSubTitle("Please enter theunit cell information")
    vb=QVBoxLayout()
    self.cell_params_=CellParams(ex.SpatialUnitCell(100*Units.A,100*Units.A,90*Units.deg,140*Units.A))
    vb.addWidget(self.cell_params_)
    page.setLayout(vb)
    return page

  def BrowseDir(self):
    filedialog=QFileDialog(self)
    filedialog.setDirectory(self.dirpathedit_.text())
    filedialog.setFileMode(QFileDialog.Directory)
    if filedialog.exec_():
      self.dirpathedit_.setText(filedialog.selectedFiles()[0])

  def GetProjectDirectory(self):
    return str(self.dirpathedit_.text())

  def GetProjectSettings(self):
    pdict=self.cell_params_.pdict_
    #todo implement proper getters instead od using private variables
    self.project_params_.grid_.itemAtPosition(0,2).widget().currentText()
    pdict["fileformat"]=str(self.project_params_.grid_.itemAtPosition(0,2).widget().currentText())
    pdict["prefix"]=str(self.project_params_.grid_.itemAtPosition(1,2).widget().text())
    pdict["spacegroup"]=str(self.cell_params_.grid_.itemAtPosition(0,2).widget().currentText())
    return pdict

class WelcomeDialog(QDialog):
  def __init__(self,parent,template_xml):
    QDialog.__init__(self,parent)
    self.setWindowTitle("Welcome")
    self.template_xml_=template_xml

    vb=QVBoxLayout()

    openbutton=QPushButton("Open existing project")
    QObject.connect(openbutton,SIGNAL("clicked()"),self.OpenProject)
    vb.addWidget(openbutton)
    newbutton=QPushButton("Create new project")
    QObject.connect(newbutton,SIGNAL("clicked()"),self.NewProject)
    vb.addWidget(newbutton)
    cancelbutton=QPushButton("Cancel")
    QObject.connect(cancelbutton,SIGNAL("clicked()"),self.reject)
    vb.addWidget(cancelbutton)
    self.setLayout(vb)

  def OpenProject(self):
    filedialog=QFileDialog(self)
    filedialog.setFileMode(QFileDialog.ExistingFile)
    filedialog.setNameFilter("Project file (*.xml *.XML)")
    if filedialog.exec_():
      os.chdir(os.path.dirname(str(filedialog.selectedFiles()[0])))
      self.accept()

  def NewProject(self):
    self.newprojectwizard_=NewProjectWizard(self)
    QObject.connect(self.newprojectwizard_.button(QWizard.FinishButton),SIGNAL("clicked()"),self.CreateProject)
    self.newprojectwizard_.show()
    
  def CreateProject(self):
    if not os.path.isdir(self.newprojectwizard_.GetProjectDirectory()):
      try:
        os.mkdir(self.newprojectwizard_.GetProjectDirectory())
      except:
        dialog=QMessageBox()
        dialog.setText("Could not create directory: %s" %self.newprojectwizard_.GetProjectDirectory() )
        dialog.exec_()
        return
    os.chdir(self.newprojectwizard_.GetProjectDirectory())
    f=open("project.xml",'w')
    f.write(self.template_xml_ % self.newprojectwizard_.GetProjectSettings())
    f.close()
    self.accept()


