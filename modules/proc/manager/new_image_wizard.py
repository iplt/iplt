#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Andreas Schenk
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import math 

class NewImageWizard(QDialog):
  def __init__(self,prefix, numberlength,filesuffix):
    QDialog.__init__(self)
    self.filelist_=[]
    self.prefix_=prefix
    self.filesuffix_=filesuffix
    self.numberlength_=numberlength
    self.setWindowTitle("Import new images into project")
    vb=QVBoxLayout()
    hb=QHBoxLayout()
    button=QPushButton("Set Tilt Angle")
    QObject.connect(button,SIGNAL("clicked()"),self.OnSetTiltAngle)
    hb.addWidget(button)
    button=QPushButton("Renumber")
    QObject.connect(button,SIGNAL("clicked()"),self.OnRenumber)
    hb.addWidget(button)
    vb.addLayout(hb)
    self.imagelist_=QTreeWidget(self)
    headers=["Filename","Tilt angle","Image number","Final name"]
    self.imagelist_.setHeaderLabels(headers)
    self.imagelist_.setColumnCount(4)
    vb.addWidget(self.imagelist_)
    buttonbox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    vb.addWidget(buttonbox)
    QObject.connect(buttonbox,SIGNAL("accepted()"),self.accept)
    QObject.connect(buttonbox,SIGNAL("rejected()"),self.reject)
    QObject.connect(self.imagelist_,SIGNAL("itemDoubleClicked(QTreeWidgetItem*,int)"),self.EditImagelist)
    QObject.connect(self.imagelist_,SIGNAL("itemChanged(QTreeWidgetItem*,int)"),self.OnItemChanged)
    self.setLayout(vb)

  def exec_(self):
    self.filelist_=[]
    filedialog=QFileDialog(self)
    filedialog.setFileMode(QFileDialog.ExistingFiles)
    filedialog.setNameFilter("Images in project format (*.%s)" % self.filesuffix_)
    if filedialog.exec_():
      filenames = filedialog.selectedFiles();
      for filename in filenames:
        imagename=filename.split('/')[-1]
        ti=QTreeWidgetItem()
        ti.setText(0,imagename)
        if imagename.size()>=4 and imagename[-4]==".":
          #remove suffix
          imagename.chop(4)
        imnumberstr=QString()
        for c in imagename:
          if QChar(c).isDigit():
            imnumberstr.append(c)
        imnumber=imnumberstr.toUInt()[0]
        maxnum=10**self.numberlength_
        if imnumber>=maxnum:
          tilt=int(math.floor(imnumber/float(maxnum)))
          imnumber=int(imnumber-tilt*float(maxnum))
        else:
          tilt=0
          imnumber=int(imnumber)
        newdirname=QString("%1%2%3").arg(self.prefix_).arg(tilt,2,10,QChar('0')).arg(imnumber,self.numberlength_,10,QChar('0'))
        newfilename=newdirname+"."+self.filesuffix_
        self.filelist_.append([filename,newdirname,newfilename,filename.split('.')[-1] != self.filesuffix_])
        ti.setText(1,QString("%1").arg(tilt,2,10,QChar('0')))
        ti.setText(2,QString("%1").arg(imnumber,self.numberlength_,10,QChar('0')))
        ti.setText(3,newfilename)
        self.imagelist_.addTopLevelItem(ti)
      for i in range(4):
        self.imagelist_.resizeColumnToContents(i)
      return QDialog.exec_(self)
    else:
      return False

  def EditImagelist(self,item,column):
    if column>0 and column < 5:
      flags=item.flags()
      item.setFlags(flags | Qt.ItemIsEditable)
      self.imagelist_.editItem(item,column)
      item.setFlags(flags)


  def OnItemChanged(self,ti,column=0):
    index=self.imagelist_.indexOfTopLevelItem(ti)
    newdirname=QString("%1%2%3").arg(self.prefix_).arg(ti.text(1)).arg(ti.text(2))
    newfilename=newdirname+"."+self.filesuffix_
    self.filelist_[index][1]=newdirname
    self.filelist_[index][2]=newfilename
    self.filelist_[index][3]=ti.text(0).split('.')[-1] != self.filesuffix_
    ti.setText(3,newfilename)

  def GetFileList(self):
    return self.filelist_



  def OnSetTiltAngle(self):
    (val,ok)=QInputDialog.getInt(self,"Set Tilt Angle","Tilt angle",0,0,90)
    if ok:
      for i in range(self.imagelist_.topLevelItemCount()):
        self.imagelist_.topLevelItem(i).setText(1,QString("%1").arg(val,2,10,QChar('0')))
        self.OnItemChanged(self.imagelist_.topLevelItem(i),1)

  def OnRenumber(self):
    (val,ok)=QInputDialog.getInt(self,"Renumber","Starting Number",0,0,10**self.numberlength_-1)
    if ok:
      for i in range(self.imagelist_.topLevelItemCount()):
        self.imagelist_.topLevelItem(i).setText(2,QString("%1").arg(val+i,self.numberlength_,10,QChar('0')))
        self.OnItemChanged(self.imagelist_.topLevelItem(i),2)
  
