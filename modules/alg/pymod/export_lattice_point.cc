//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <ost/img/image.hh>
#include <ost/geom/geom.hh>
#include <iplt/lattice.hh>
#include <iplt/reflection_list.hh>
#include <iplt/alg/lattice_point.hh>

using namespace ost::img;
using namespace iplt;
using namespace iplt::alg;
using namespace ost::img::alg;

namespace {

class LatticePointIterator {
public:
  LatticePointIterator(const LatticePointProxyter& p): proxyter_(p) {}

  LatticePointProxyter& Iter() {
    return proxyter_;
  }

  LatticePointProxyter Next() {
    if(proxyter_.AtEnd()) {
      PyErr_SetObject(PyExc_StopIteration, Py_None);
      throw_error_already_set();
    }

    return proxyter_++;
  }


private:
  LatticePointProxyter proxyter_;
};

LatticePointIterator IteratorFromProxyter(LatticePointProxyter& p)
{
  return LatticePointIterator(p);
}

LatticePointIterator IteratorFromList(LatticePointList& l)
{
  return LatticePointIterator(l.Begin());
}

int lattice_point_list_len(const LatticePointList& lpl) 
{
  return lpl.NumEntries();
}

} //ns

void export_lattice_point()
{
  class_<LatticePoint>("LatticePoint",
		       init<optional<const geom::Vec2&,
		                     const ost::img::Extent&,
		                     const Stat&,
		                     const FitGauss2D&,
		                     const PeakIntegration&,
		                     Real,Real,Real>
		           >())
    .def("GetPosition",&LatticePoint::GetPosition)
    .def("SetPosition",&LatticePoint::SetPosition)
    .def("GetFit",&LatticePoint::GetFit)
    .def("SetFit",&LatticePoint::SetFit)
    .def("GetRegion",&LatticePoint::GetRegion)
    .def("SetRegion",&LatticePoint::SetRegion)
    .def("GetStat",&LatticePoint::GetStat)
    .def("SetStat",&LatticePoint::SetStat)
    .def("GetPI",&LatticePoint::GetPI)
    .def("SetPI",&LatticePoint::SetPI)
    .def("GetICBG",&LatticePoint::GetICBG)
    .def("SetICBG",&LatticePoint::SetICBG)
    .def("GetAveBG",&LatticePoint::GetAveBG)
    .def("SetAveBG",&LatticePoint::SetAveBG)
    .def("GetAveSigBG",&LatticePoint::GetAveSigBG)
    .def("SetAveSigBG",&LatticePoint::SetAveSigBG)
    .def("MemSize",&LatticePoint::MemSize)
    .def("CalcAveBG",&LatticePoint::CalcAveBG)
    ;


  implicitly_convertible<LatticePoint,Point>();

  //LatticePoint (LatticePointProxyter::*get_lp1)() const = &LatticePointProxyter::LPoint;
  LatticePoint& (LatticePointProxyter::*get_lp2)() = &LatticePointProxyter::LPoint;

  class_<LatticePointProxyter>("LatticePointProxyter",no_init)
    .def("GetIndex",&LatticePointProxyter::GetIndex)
    /*.def("LPoint",get_lp1)*/
    .def("LPoint",get_lp2,
	 return_internal_reference<>())
    .def("AtEnd",&LatticePointProxyter::AtEnd)
    .def("IsValid",&LatticePointProxyter::IsValid)
    .def("__iter__", &IteratorFromProxyter)
   ;

  class_<LatticePointList>("LatticePointList",init<const Lattice&>())
    .def("GetLattice",&LatticePointList::GetLattice)
    .def("SetLattice",&LatticePointList::SetLattice)
    .def("Add",&LatticePointList::Add)
    .def("Begin",&LatticePointList::Begin)
    .def("Find",&LatticePointList::Find)
    .def("Clear",&LatticePointList::Clear)
    .def("MemSize",&LatticePointList::MemSize)
    .def("NumEntries",&LatticePointList::NumEntries)
    //.def("__len__", &LatticePointList::Size)
    .def("__iter__", &IteratorFromList)
    .def("__len__",lattice_point_list_len)
   ;

  class_<LatticePointIterator>("LatticePointIterator", no_init)
    .def("__iter__",&LatticePointIterator::Iter,
	 return_internal_reference<>() )
    .def("next",&LatticePointIterator::Next)
    ;

  def("PredictLatticePoints",PredictLatticePoints);

  def("ConvertToReflectionList",ConvertToReflectionList);
  def("ConvertToLatticePointList",ConvertToLatticePointList);
}
