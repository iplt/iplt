//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/peak_search.hh>

using namespace ost::img;
using namespace iplt::alg;

void export_PeakSearch()
{

  def("ImportPeakList",ImportPeakList);
  def("ExportPeakList",ExportPeakList);

  {
    scope ps=class_<PeakSearch, bases<NonModAlgorithm> >("PeakSearch", 
		  			     init<optional<int, Real, int, Real, int, Real, unsigned int> >() )
      .def("GetPeakList", &PeakSearch::GetPeakList)
      .def("ClearPeakList", &PeakSearch::ClearPeakList)
      .def("SetThreshold", &PeakSearch::SetThreshold)
      .def("SetOuterWindowSize", &PeakSearch::SetOuterWindowSize)
      .def("GetOuterWindowSize", &PeakSearch::GetOuterWindowSize)
      .def("SetOuterSensitivity", &PeakSearch::SetOuterSensitivity)
      .def("GetOuterSensitivity", &PeakSearch::GetOuterSensitivity)
      .def("SetInnerWindowSize", &PeakSearch::SetInnerWindowSize)
      .def("GetInnerWindowSize", &PeakSearch::GetInnerWindowSize)
      .def("SetInnerSensitivity", &PeakSearch::SetInnerSensitivity)
      .def("GetInnerSensitivity", &PeakSearch::GetInnerSensitivity)
      .def("SetInnerCountLimit", &PeakSearch::SetInnerCountLimit)
      .def("GetInnerCountLimit", &PeakSearch::GetInnerCountLimit)
      .def("SetInnerCountSensitivity", &PeakSearch::SetInnerCountSensitivity)
      .def("GetInnerCountSensitivity", &PeakSearch::GetInnerCountSensitivity)
      .def("AddExclusion", &PeakSearch::AddExclusion)
      .def("SetMode", &PeakSearch::SetMode)
      .def("GetMode", &PeakSearch::GetMode)
    ;
    enum_<PeakSearch::modes>("modes")
      .value("ABSOLUTE",PeakSearch::ABSOLUTE)
      .value("RELATIVE_TO_PEAK", PeakSearch::RELATIVE_TO_PEAK)
      .value("RELATIVE_TO_STDEV", PeakSearch::RELATIVE_TO_STDEV)
      .export_values()
    ;
  }
}
