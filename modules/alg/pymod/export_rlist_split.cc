//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <boost/python.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
using namespace boost::python;

#include <iplt/alg/rlist_split.hh>

using namespace iplt::alg;
using namespace iplt;
using namespace ost::img;
using namespace ost;


void export_rlist_split()
{
  {
    scope s=class_<RListSplitByProperty, bases<ReflectionNonModAlgorithm> >("RListSplitByProperty", init<const String&>())
	    .def("GetRListMap",&RListSplitByProperty::GetRListMap)
      ;

      class_<RListSplitByProperty::PropertyReflectionListMap >("PropertyReflectionListMap")
        .def(map_indexing_suite<RListSplitByProperty::PropertyReflectionListMap>())
        ;
  }
  {
    scope s=class_<RListSplitByIndex, bases<ReflectionNonModAlgorithm> >("RListSplitByIndex", init<>())
            .def("GetRListMap",&RListSplitByIndex::GetRListMap)
      ;
    class_<RListSplitByIndex::IndexReflectionListMap >("IndexReflectionListMap")
            .def(map_indexing_suite<RListSplitByIndex::IndexReflectionListMap>())
        ;
  }
}



