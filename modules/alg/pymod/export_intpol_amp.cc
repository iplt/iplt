//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/reflection.hh>

#include <iplt/alg/intpol_amp.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;
using namespace iplt::alg;

void export_intpol_amp()
{
  class_<IntpolAmp>("IntpolAmp", init<ReflectionList&, const String&, const String&, const String&, const String&, Real>())
    .def("SetWindow",&IntpolAmp::SetWindow)
    .def("GetInterpolatedValue",&IntpolAmp::GetInterpolatedValue)
    .def("Dump",&IntpolAmp::Dump)
    ;
}
