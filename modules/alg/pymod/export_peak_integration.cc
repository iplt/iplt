//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/peak_integration.hh>

using namespace ost::img;
using namespace iplt;
using namespace iplt::alg;

namespace {
  Real calc_piw1(Real isigi) {return CalcPeakIntegrationWeight(isigi);}
  Real calc_piw2(Real isigi,Real s) {return CalcPeakIntegrationWeight(isigi,s);}
}

void export_peak_integration()
{
  class_<PeakIntegration, bases<NonModAlgorithm> >("PeakIntegration", init<optional<int,int,int> >() )
    .def("GetVolume",&PeakIntegration::GetVolume)
    .def("SetVolume",&PeakIntegration::SetVolume)
    .def("GetSigma",&PeakIntegration::GetSigma)
    .def("SetSigma",&PeakIntegration::SetSigma)
    .def("GetBackground",&PeakIntegration::GetBackground)
    .def("SetBackground",&PeakIntegration::SetBackground)
    .def("GetQuality",&PeakIntegration::GetQuality)
    .def("GetFinalSize",&PeakIntegration::GetFinalSize)
    .def("GetStartSize",&PeakIntegration::GetStartSize)
    .def("GetEndSize",&PeakIntegration::GetEndSize)
    .def("SetBackgroundRimSize",&PeakIntegration::SetBackgroundRimSize)
    .def("GetMaximalSize",&PeakIntegration::GetMaximalSize)
    .def("SetMethod",&PeakIntegration::SetMethod)
    ;

  def("CalcPeakIntegrationWeight",calc_piw1);
  def("CalcPeakIntegrationWeight",calc_piw2);
  def("CalcPeakIntegrationWeight",CalcPeakIntegrationWeight);
}
