//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/exp_fit.hh>
#include <iplt/alg/log_fit.hh>
#include <iplt/alg/nexp_fit.hh>

using namespace ost::img;
using namespace iplt;
using namespace iplt::alg;

namespace {
  void (ExpFit::*add1)(Real, Real) = &ExpFit::Add;
  void (ExpFit::*add2)(Real, Real, Real) = &ExpFit::Add;

  void (NExpFit::*nadd1)(Real, Real) = &NExpFit::Add;
  void (NExpFit::*nadd2)(Real, Real, Real) = &NExpFit::Add;

  void (LogFit::*ladd1)(Real, Real) = &LogFit::Add;
  void (LogFit::*ladd2)(Real, Real, Real) = &LogFit::Add;
  
  list get_s_vec(NExpFit& fit){
    list result;
    std::vector<Real> svec=fit.GetSVec();
    for(std::vector<Real>::const_iterator it=svec.begin();it!=svec.end();++it){
      result.append(*it);
    }
    return result;
  }

  list get_b_vec(NExpFit& fit){
    list result;
    std::vector<Real> bvec=fit.GetBVec();
    for(std::vector<Real>::const_iterator it=bvec.begin();it!=bvec.end();++it){
      result.append(*it);
    }
    return result;
  }

  tuple get_s_sigs(const ExpFit& f) {
    std::pair<Real,Real> res = f.GetSsigS();
    return make_tuple(res.first,res.second);
  }

  tuple get_b_sigb(const ExpFit& f) {
    std::pair<Real,Real> res = f.GetBsigB();
    return make_tuple(res.first,res.second);
  }

  tuple get_c_sigc(const ExpFit& f) {
    std::pair<Real,Real> res = f.GetCsigC();
    return make_tuple(res.first,res.second);
  }

  tuple estimate(ExpFit& f,Real x) {
    std::pair<Real,Real> r=f.Estimate(x);
    return make_tuple(r.first,r.second);
  }
}

void export_exp_fit()
{
  class_<ExpFit>("ExpFit",init<optional<bool> >())
    .def("Add",add1)
    .def("Add",add2)
    .def("GetS",&ExpFit::GetS)
    .def("GetSsigS",get_s_sigs)
    .def("GetB",&ExpFit::GetB)
    .def("GetBsigB",get_b_sigb)
    .def("GetC",&ExpFit::GetC)
    .def("GetCsigC",get_c_sigc)
    .def("GetChi",&ExpFit::GetChi)
    .def("Apply",&ExpFit::Apply)
    .def("SetMaxIter",&ExpFit::SetMaxIter)
    .def("SetLimits",&ExpFit::SetLimits)
    .def("Estimate",estimate)
    ;

  class_<LogFit>("LogFit",init<optional<bool> >())
    .def("Add",ladd1)
    .def("Add",ladd2)
    .def("GetS",&LogFit::GetS)
    .def("GetB",&LogFit::GetB)
    .def("GetC",&LogFit::GetC)
    .def("GetChi",&LogFit::GetChi)
    .def("Apply",&LogFit::Apply)
    .def("SetMaxIter",&LogFit::SetMaxIter)
    .def("SetLimits",&LogFit::SetLimits)
    ;

  class_<NExpFit>("NExpFit",init< optional<unsigned int,bool> >())
    .def("Add",nadd1)
    .def("Add",nadd2)
    .def("GetSVec",get_s_vec)
    .def("GetBVec",get_b_vec)
    .def("GetC",&NExpFit::GetC)
    .def("GetChi",&NExpFit::GetChi)
    .def("Apply",&NExpFit::Apply)
    ;
}
