//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/linfit.hh>

using namespace iplt;
using namespace iplt::alg;

namespace {

tuple apply(LinearFit& lf) {
  std::pair<double,double> r=lf.Apply();
  return make_tuple(r.first,r.second);
}

tuple estimate(LinearFit& lf,double x) {
  std::pair<double,double> r=lf.Estimate(x);
  return make_tuple(r.first,r.second);
}

tuple estimate2d(LinearFit2D& lf,double x1, double x2) {
  std::pair<double,double> r=lf.Estimate(x1,x2);
  return make_tuple(r.first,r.second);
}

}

void (LinearFit::*add_datapoint2)(double, double) = &LinearFit::AddDatapoint;
void (LinearFit::*add_datapoint3)(double, double, double) = &LinearFit::AddDatapoint;
void (LinearFit::*add_datapoint4)(double, double, double, double) = &LinearFit::AddDatapoint;
void (LinearFit2D::*add_datapoint2d1)(double, double, double) = &LinearFit2D::AddDatapoint;
void (LinearFit2D::*add_datapoint2d2)(double, double, double, double) = &LinearFit2D::AddDatapoint;

void export_linfit()
{
  class_<LinearFit>("LinearFit", init<>())
    .def("AddDatapoint",add_datapoint2)
    .def("AddDatapoint",add_datapoint3)
    .def("AddDatapoint",add_datapoint4)
    .def("Apply",apply)
    .def("Estimate",estimate)
    .def("PointCount",&LinearFit::PointCount)
    .def("Clear",&LinearFit::Clear)
    .def("ForceOrigin",&LinearFit::ForceOrigin)
    .def("GetScale",&LinearFit::GetScale)
    .def("GetOffset",&LinearFit::GetOffset)
    .def("GetGOF",&LinearFit::GetGOF)
    .def("GetWGOF",&LinearFit::GetWGOF)
    .def("GetQ",&LinearFit::GetQ)
    .def("GetRSQ",&LinearFit::GetRSQ)
    .def("GetScaleVariance",&LinearFit::GetScaleVariance)
    .def("GetOffsetVariance",&LinearFit::GetOffsetVariance)
    ;

  class_<LinearFit2D>("LinearFit2D", init<>())
    .def("AddDatapoint",add_datapoint2d1)
    .def("AddDatapoint",add_datapoint2d2)
    .def("Apply",&LinearFit2D::Apply)
    .def("Estimate",estimate2d)
    .def("PointCount",&LinearFit2D::PointCount)
    .def("Clear",&LinearFit2D::Clear)
    .def("ForceOrigin",&LinearFit2D::ForceOrigin)
    .def("GetScale1",&LinearFit2D::GetScale1)
    .def("GetScale2",&LinearFit2D::GetScale2)
    .def("GetOffset",&LinearFit2D::GetOffset)
    .def("GetGOF",&LinearFit2D::GetGOF)
    ;
}
