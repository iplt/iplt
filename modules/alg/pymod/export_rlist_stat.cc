//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/rlist_stat.hh>

using namespace iplt::alg;
using namespace iplt;
using namespace ost::img;


void export_rlist_stat()
{
  class_<RListStat, bases<ReflectionNonModAlgorithm> >("RListStat", init<>())
	.def("GetMinimum",&RListStat::GetMinimum)
	.def("GetMaximum",&RListStat::GetMaximum)
	.def("GetMean",&RListStat::GetMean)
	.def("GetMinimumH",&RListStat::GetMinimumH)
	.def("GetMaximumH",&RListStat::GetMaximumH)
	.def("GetMeanH",&RListStat::GetMeanH)
	.def("GetMinimumK",&RListStat::GetMinimumK)
	.def("GetMaximumK",&RListStat::GetMaximumK)
	.def("GetMeanK",&RListStat::GetMeanK)
	.def("GetMinimumZStar",&RListStat::GetMinimumZStar)
	.def("GetMaximumZStar",&RListStat::GetMaximumZStar)
  .def("GetMeanZStar",&RListStat::GetMeanZStar)
  .def("GetIndexList",&RListStat::GetIndexList)
    ;
 
}



