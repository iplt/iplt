//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <boost/python.hpp>
#include <iplt/alg/lattice_filters.hh>

void export_lattice_filters()
{

  using namespace boost::python;
  using namespace ost::img;
  using namespace iplt::alg;
  using namespace iplt;

  class_<LatticeFilter, bases<ConstModIPAlgorithm> >("LatticeFilter",init<Lattice,unsigned int>())
  ;
  class_<GaussianLatticeFilter, bases<ConstModIPAlgorithm> >("GaussianLatticeFilter",init<Lattice,unsigned int>())
  ;

}
