//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/llfit_base.hh>
#include <iplt/alg/llfitamp_gauss_sum.hh>
#include <iplt/alg/llfitamp_sinc_intpol.hh>
#include <iplt/alg/llfitamp_sinc_intpol_sq.hh>
#include <iplt/alg/llfitamp_composite.hh>

using namespace ost::img;
using namespace iplt;
using namespace iplt::alg;

namespace {


tuple calc_intens_at(LLFitBase& llfit,Real zstar)
{
  std::pair<Real,Real> res=llfit.CalcIntensAt(zstar);
  return make_tuple(res.first,res.second);
}

tuple calc_phase_at(LLFitBase& llfit,Real zstar)
{
  std::pair<Real,Real> res=llfit.CalcPhaseAt(zstar);
  return make_tuple(res.first,res.second);
}

dict get_comp(LLFitBase& llfit)
{
  std::map<Real,Complex> res=llfit.GetComponents();
  dict nrvo;
  for(std::map<Real,Complex>::const_iterator it=res.begin();it!=res.end();++it) {
    nrvo[it->first]=it->second;
  }
  return nrvo;
}

}

void export_llfit()
{
  class_<LLFitAmpCurvePoint>("LLFitAmpCurvePoint")
    .add_property("zfit",&LLFitAmpCurvePoint::zfit)
    .add_property("ifit",&LLFitAmpCurvePoint::ifit)
    .add_property("sigifit",&LLFitAmpCurvePoint::sigifit)
    ;

  {
  scope scope_llfitbase=class_<LLFitBase, LLFitBasePtr, boost::noncopyable>("LLFitBase",no_init)
    .def("AddIntens",&LLFitBase::AddIntens)
    .def("AddPhase",&LLFitBase::AddPhase)
    .def("Clear",&LLFitBase::Clear)
    .def("SetMaxIter",&LLFitBase::SetMaxIter)
    .def("GetIterCount",&LLFitBase::GetIterCount)
    .def("SetIterationLimits",&LLFitBase::SetIterationLimits)
    .def("GetCount",&LLFitBase::GetCount)
    .def("CalcIntensAt",calc_intens_at)
    .def("CalcPhaseAt",calc_phase_at)
    .def("ClosestIntensAt",&LLFitBase::ClosestIntensAt)
    .def("RelativeClosestIntensAt",&LLFitBase::RelativeClosestIntensAt)
    .def("Apply",&LLFitBase::Apply)
    .def("GetComponents",get_comp)
    .def("GetSampling",&LLFitBase::GetSampling)
    .def("SetRelativeZWeight",&LLFitBase::SetRelativeZWeight)
    .def("Cleanup",&LLFitBase::Cleanup)
    .def("SetBFactor",&LLFitBase::SetBFactor)
    .def("GetBFactor",&LLFitBase::GetBFactor)
    ;
  enum_<LLFitBase::guessmode>("guessmode")
  .value("GUESSMODE_BINAVERAGE",LLFitBase::GUESSMODE_BINAVERAGE)
  .value("GUESSMODE_CONST",LLFitBase::GUESSMODE_CONST)
  .value("GUESSMODE_RANDOM",LLFitBase::GUESSMODE_RANDOM)
  .export_values();
  }
  class_<LLFitAmpGaussianSum, bases<LLFitBase> >("LLFitAmpGaussianSum",init<int,Real,optional<LLFitBase::guessmode> >())
    ;
  class_<LLFitAmpComplexGaussianSum, bases<LLFitBase> >("LLFitAmpComplexGaussianSum",init<int,Real,optional<LLFitBase::guessmode> >())
    ;

  class_<LLFitAmpSincIntpol, bases<LLFitBase> >("LLFitAmpSincIntpol",init<int,Real,optional<LLFitBase::guessmode> >())
    ;

  class_<LLFitAmpSincIntpolHerm, bases<LLFitBase> >("LLFitAmpSincIntpolHerm",init<int,Real,optional<LLFitBase::guessmode> >())
    ;

  class_<LLFitAmpSincIntpolSQ, bases<LLFitBase> >("LLFitAmpSincIntpolSQ",init<int,Real>())
    ;

  class_<LLFitAmpComposite, bases<LLFitBase> >("LLFitAmpComposite",init<>())
    .def("Add",&LLFitAmpComposite::Add)
    ;
}

   
