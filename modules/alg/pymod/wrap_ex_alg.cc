//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

extern void export_fourier_mask();
extern void export_lattice_search();
extern void export_lattice_extract();
extern void export_lattice_gaussian_extract();
extern void export_lattice_point();
extern void export_peak_integration();
extern void export_intpol_amp();
extern void export_linfit();
extern void export_symmetrize();
extern void export_reduce();
extern void export_restrict();
extern void export_expand();
extern void export_unique();
extern void export_exp_fit();
extern void export_rbin();
extern void export_wbin();
extern void export_split_gauss2d();
extern void export_background_fit();
extern void export_lattice_filters();
extern void export_ctf();
extern void export_llfit();
extern void export_rfactors();
extern void export_rlist_stat();
extern void export_rlist_index_stat();
extern void export_rlist_range_filter();
extern void export_rlist_resolution_filter();
extern void export_rlist_property_filter();
extern void export_rlist_llfit();
extern void export_rlist_split();
extern void export_Gauss2D();
extern void export_PeakSearch();

#include <iplt/alg/refine_lattice.hh>
#include <iplt/alg/tilter.hh>
#include <iplt/alg/ewald_tilter.hh>
#include <iplt/alg/aniso_scaling.hh>
#include <iplt/alg/create_image.hh>
#include <iplt/alg/llpredict.hh>
#include <iplt/alg/rlist_wbin.hh>
#include <iplt/alg/sobel.hh>
#include <iplt/alg/hysteresis.hh>
#include <iplt/alg/nonmaxsupressor.hh>
#include <iplt/alg/canny.hh>

#include <gsl/gsl_cdf.h>

using namespace ost;
using namespace ost::img;
using namespace iplt;
using namespace iplt::alg;

namespace {

BOOST_PYTHON_FUNCTION_OVERLOADS(refine_overloads, RefineLattice, 1, 3)
BOOST_PYTHON_FUNCTION_OVERLOADS(CreateImageFromReflectionList_overloads, CreateImageFromReflectionList, 1, 3)

Real CDF(Real x, Real m, Real s)
{
  return gsl_cdf_ugaussian_P((x-m)/s);
}

}

BOOST_PYTHON_MODULE(_iplt_alg)
{
  export_fourier_mask();
  export_lattice_point();
  export_lattice_search();
  export_lattice_extract();
  export_lattice_gaussian_extract();
  export_peak_integration();
  export_intpol_amp();
  export_linfit();
  export_symmetrize();
  export_reduce();
  export_restrict();
  export_expand();
  export_unique();
  export_exp_fit();
  export_rbin();
  export_wbin();
  export_Gauss2D();
  export_split_gauss2d();
  export_background_fit();
  export_lattice_filters();
  export_ctf();
  export_llfit();
  export_rfactors();
  export_rlist_stat();
  export_rlist_index_stat();
  export_rlist_range_filter();
  export_rlist_resolution_filter();
  export_rlist_property_filter();
  export_rlist_llfit();
  export_rlist_split();
  export_PeakSearch();
  
  
  class_<Canny, bases<ConstModOPAlgorithm> >("Canny" ,init<Real,Real>())
    ;
  class_<Sobel, bases<ConstModOPAlgorithm> >("Sobel", init<>() )
    ;
  class_<NonMaxSupressor, bases<ConstModOPAlgorithm> >("NonMaxSupressor", init<>() )
    ;
  class_<Hysteresis, bases<ConstModIPAlgorithm> >("Hysteresis",init<Real,Real>() )
    ;
  
  def("RefineLattice",&RefineLattice,refine_overloads());

  def("CDF",CDF);

  class_<Tilter, bases<ReflectionConstModOPAlgorithm> >("Tilter",init<optional<const LatticeTiltGeometry&> >());
  class_<EwaldTilter, bases<ReflectionConstModOPAlgorithm> >("EwaldTilter",init<Real>());
  class_<AnisoScaling>("AnisoScaling", init<>())
    .def("Add",&AnisoScaling::Add)
    .def("Apply",&AnisoScaling::Apply)
    .def("Estimate",&AnisoScaling::Estimate)
    .def("GetA",&AnisoScaling::GetA)
    .def("GetB1",&AnisoScaling::GetB1)
    .def("GetB2",&AnisoScaling::GetB2)
    .def("GetB3",&AnisoScaling::GetB3)
    ;

  def("CreateImageFromReflectionList",CreateImageFromReflectionList,CreateImageFromReflectionList_overloads());

  class_<LLPredict>("LLPredict",init<const ost::img::ConstImageHandle&>())
    .def("Get",&LLPredict::Get)
    ;

  def("RListWBin",RListWBin);
}
