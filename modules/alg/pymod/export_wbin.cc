//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/weighted_bin.hh>
#include <iplt/alg/weighted_flexible_bin.hh>

using namespace iplt;
using namespace iplt::alg;

void export_wbin()
{
  class_<WeightedBin>("WeightedBin", init<>())
    .def(init<int,Real,Real>())
    .def("Add",&WeightedBin::Add)
    .def("GetAverage",&WeightedBin::GetAverage)
    .def("GetStdDev",&WeightedBin::GetStdDev)
    .def("GetWeightAverage",&WeightedBin::GetWeightAverage)
    .def("GetSize",&WeightedBin::GetSize)
    .def("GetBinCount",&WeightedBin::GetBinCount)
    .def("GetLimit",&WeightedBin::GetLimit)
    .def("GetLowerLimit",&WeightedBin::GetLowerLimit)
    .def("GetUpperLimit",&WeightedBin::GetUpperLimit)
    .def("CalcBin",&WeightedBin::CalcBin)
    ;
  class_<WeightedFlexibleBin>("WeightedFlexibleBin", init<>())
    .def(init<int,Real,Real>())
    .def("Add",&WeightedFlexibleBin::Add)
    .def("GetAverage",&WeightedFlexibleBin::GetAverage)
    .def("GetStdDev",&WeightedFlexibleBin::GetStdDev)
    .def("GetWeightAverage",&WeightedFlexibleBin::GetWeightAverage)
    .def("GetSize",&WeightedFlexibleBin::GetSize)
    .def("GetBinCount",&WeightedFlexibleBin::GetBinCount)
    .def("GetLimit",&WeightedFlexibleBin::GetLimit)
    .def("GetLowerLimit",&WeightedFlexibleBin::GetLowerLimit)
    .def("GetUpperLimit",&WeightedFlexibleBin::GetUpperLimit)
    .def("CalcBin",&WeightedFlexibleBin::CalcBin)
    .def("SetGlobalLowerLimit",&WeightedFlexibleBin::SetGlobalLowerLimit)
    .def("SetGlobalUpperLimit",&WeightedFlexibleBin::SetGlobalUpperLimit)
    .def("SetBinCount",&WeightedFlexibleBin::SetBinCount)
    ;
}

