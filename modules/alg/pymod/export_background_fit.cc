//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/fit_background.hh>

using namespace iplt::alg;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(add_overloads,Add, 3,4)

void export_background_fit()
{

  class_<FitBackground>("FitBackground", init<optional<bool> >() )
    .def("GetS1",&FitBackground::GetS1)
    .def("GetS2",&FitBackground::GetS2)
    .def("GetB1",&FitBackground::GetB1)
    .def("GetB2",&FitBackground::GetB2)
    .def("GetC",&FitBackground::GetC)
    .def("GetOrigin",&FitBackground::GetOrigin)
    .def("GetChi",&FitBackground::GetChi)
    .def("Apply",&FitBackground::Apply)
    .def("Add",&FitBackground::Add,add_overloads())
    .def("AsImage",&FitBackground::AsImage)
    ;

}
