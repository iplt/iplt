//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <iostream>

#include <ost/log.hh>
#include <ost/base.hh>
#include <ost/img/value_util.hh>

#include "llfitamp_base.hh"

namespace iplt {  namespace alg {

LLFitAmpBase::LLFitAmpBase(unsigned int count, Real sampling):
  count_(count), sampling_(sampling),data_list_(),guess_(),
  max_iter_(1000),iter_count_(0),
  lim1_(1e-10),lim2_(1e-10),
  curve_(),dirty_curve_(true),curve_count_(1000),
  max_zstar_(1.0),max_intens_(1.0),
  relative_weighting_(1.0),
  fit_bfac_(false),
  bfac_(0.0)
{
}

LLFitAmpBase::~LLFitAmpBase()
{}

namespace {
bool check_zstar_range(Real zstar, Real sampling)
{
  if(zstar>(0.5/sampling)) {
    LOG_DEBUG( "ignored entry due to zstar out of bounds (" << zstar << " > " << 0.5/sampling << ")" );
    return false;
  } else if(zstar<(-0.5/sampling)) {
    LOG_DEBUG( "ignored entry due to zstar out of bounds (" << zstar << " < " << -0.5/sampling << ")" );
    return false;
  }
  return true;
}
} // anon ns

void LLFitAmpBase::Add(Real zstar, Real intens, Real sig_intens)
{
  if(check_zstar_range(zstar,sampling_) && sig_intens>0.0) {
    detail::DataEntry en={zstar,intens,sig_intens};
    data_list_.push_back(en);
    LOG_DEBUG( "adding entry with zstar=" << zstar << " int= " << intens << " sigint= " << sig_intens );
  }
}

void LLFitAmpBase::SetGuess(const std::vector<Real>& guess)
{
  if(guess.size()==count_) {
    guess_ = guess;
    for(int n=0;n<guess_.size();++n) {
      LOG_DEBUG( "guess " << n << ": " << guess_[n] );
    }
  } else {
    LOG_INFO( "LLFitAmp guess rejected guess due to missmatch in number of values" );
  }
}

void LLFitAmpBase::Clear()
{
  data_list_.clear();
  guess_.clear();
}

void LLFitAmpBase::SetMaxIter(unsigned int m)
{
  max_iter_=m;
}

unsigned int LLFitAmpBase::GetIterCount() const
{
  return iter_count_;
}

unsigned int LLFitAmpBase::GetCount() const
{
  return count_;
}

Real LLFitAmpBase::GetSampling() const
{
  return sampling_;
}

void LLFitAmpBase::SetIterationLimits(Real l1,Real l2)
{
  lim1_=l1;
  lim2_=l2;
}

void LLFitAmpBase::SetRelativeZWeight(Real w)
{
  relative_weighting_=w;
}

LLFitCurvePoint LLFitAmpBase::ClosestAt(Real zstar, Real intens)
{
  if(dirty_curve_) {
    curve_.clear();
    max_intens_ = 1e-100;
    Real curve_start = 0.0;
    Real curve_end = 0.5/sampling_;
    max_zstar_ = curve_end;
    Real f = (curve_end-curve_start)/static_cast<Real>(curve_count_);
    for(int n=0;n<curve_count_;++n) {
      Real zs = static_cast<Real>(n)*f+curve_start;
      std::pair<Real,Real> result = CalcAt(zs);
      LLFitCurvePoint cp = {zs,result.first,result.second};
      curve_.push_back(cp);
      max_intens_ = std::max(result.first,max_intens_);
    }
    dirty_curve_=false;
  }

  Real r_dist2 = 1e100;
  LLFitCurvePoint r_cp;
  for(std::vector<LLFitCurvePoint>::const_iterator it=curve_.begin();it!=curve_.end();++it) {
    Real d1 = relative_weighting_*(it->zfit-zstar)/max_zstar_;
    Real d2 = (it->ifit-intens)/max_intens_;
    Real dd = d1*d1+d2*d2;
    if(dd<r_dist2) {
      r_dist2 = dd;
      r_cp=(*it);
    }
  }
  return r_cp;
}
      
void LLFitAmpBase::Cleanup()
{
  data_list_.clear();
  guess_.clear();
  curve_.clear();
  dirty_curve_=true;
}

void LLFitAmpBase::SetBFactor(Real b)
{
  bfac_=b;
}

Real LLFitAmpBase::GetBFactor() const
{
  return bfac_;
}

void LLFitAmpBase::FitBFactor(bool f)
{
  fit_bfac_=f;
}


}} // ns
