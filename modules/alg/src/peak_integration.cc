//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#include <iostream>
#include <limits>

#include <gsl/gsl_cdf.h>

#include <ost/img/image.hh>
#include <ost/img/function.hh>
#include <ost/img/extent.hh>
#include <ost/log.hh>

#include "peak_integration.hh"
#include "peak_integration_impl.hh"

namespace iplt {  namespace alg {

//#define PEAKINT_G2_METHOD_TEST

PeakIntegration::PeakIntegration(int startsize, int endsize, int laxsize):
  NonModAlgorithm("PeakIntegration"),
  start_size_(startsize),
  end_size_(endsize),
  lax_size_(laxsize),
  bg_border_(1),
  volume_(0.0),
  sigma_(0.0),
  background_(0.0),
  background_sigma_(0.0),
  final_size_(1),
  method_(0)
{}

void PeakIntegration::Visit(const ost::img::Function& f)
{
  ost::img::ImageHandle tmp=GenerateImage(f);
  Visit(tmp);
}

void PeakIntegration::Visit(const ost::img::ConstImageHandle& data)
{
  //Real imean_bg_lim=0.0;

  ost::img::Point cen=data.GetExtent().GetCenter();
  LOG_DEBUG( "running peakint @" << cen << " (" << data.GetExtent() << ") ");
#ifdef PEAKINT_G2_METHOD_TEST
  LOG_DEBUG( "method = " << method_);
#endif
  LOG_DEBUG( std::endl);

  Real final_bg=0.0;
  Real final_bsig=0.0;
  Real final_vol=0.0;
  Real final_sigi=0.0;
  Real final_i_sigi=0.0;
  Real best_imean_bg=-std::numeric_limits<Real>::max();
  int final_size=start_size_;
  bool ignore_flag=false;

  // stepwise increase of inner (intensity) box
  for(int size=start_size_;size<=end_size_;++size) {

      ost::img::Extent outer_ext(ost::img::Size((size+bg_border_)*2+1,(size+bg_border_)*2+1),cen);
      ost::img::Extent inner_ext(ost::img::Size(size*2+1,size*2+1),cen);

    ost::img::ImageHandle tmp=data.Extract(outer_ext);

    boost::shared_ptr<detail::PeakIntegrationBase> pib;

    try {
#ifdef PEAKINT_G2_METHOD_TEST
      if(method_==0) {
	boost::shared_ptr<detail::SimplePeakIntegration> pi(new detail::SimplePeakIntegration(inner_ext));
	tmp.Apply(*pi);
	pib=pi;
      } else {
	boost::shared_ptr<detail::G2PeakIntegration> pi(new detail::G2PeakIntegration(inner_ext));
	tmp.Apply(*pi);
	pib=pi;
      }
#else
      boost::shared_ptr<detail::SimplePeakIntegration> pi(new detail::SimplePeakIntegration(inner_ext));
      tmp.Apply(*pi);
      pib=pi;
#endif
  } catch (ost::Error& e) {
      LOG_DEBUG( "error caught, skipping: " << e.what() );
      return;
    }

    Real inner_fcount = static_cast<Real>(pib->GetPeakCount());
    Real vol = pib->GetPeakVolume();
    Real bg = pib->GetBackgroundMean();
    Real bsig = pib->GetBackgroundSigma();
    Real sigi = pib->GetPeakSigma();
    
    Real inner_sum = pib->GetPeakSum();
    Real inner_mean = inner_sum/inner_fcount;
    Real i_sigi = vol/sigi;
    Real imean_bg = inner_mean/bg;

#if 0
    // catch some pathological cases and abort
    if(bg<1e-20) {
      ignore_flag=true;
    }
    if(imean_bg<imean_bg_lim && size>lax_size_) {
      ignore_flag=true;
    }
#endif

    // use maximal <int>/bg as quality indicator
    if(imean_bg>best_imean_bg) {
      best_imean_bg=imean_bg;
      final_vol=vol;
      final_bg=bg;
      final_bsig=bsig;
      final_sigi=sigi;
      final_i_sigi=i_sigi;
      final_size=size;
    }

    LOG_DEBUG( " " << size*2+1 << ": \tint: " << vol << " \tsigi: " << sigi << " \tbg: " << bg << " \tbsig: " << bsig << " \ti/sigi: " << i_sigi << " \tisum: " << pib->GetPeakCount() << " \tosum: " << pib->GetBackgroundCount() << " \timean: " << inner_mean << " \timean/bg: " << inner_mean/bg << " \tpsum:" << inner_sum );

    if (ignore_flag) break;

  }

  if(ignore_flag) {
    final_vol=0.0;
    final_sigi=1.0;
    final_i_sigi=0.0;
  }

  background_=final_bg;
  background_sigma_=final_bsig;
  volume_=final_vol;
  sigma_=final_sigi;
  final_size_=final_size;
  ave_ratio_=best_imean_bg;

  LOG_DEBUG( "int: " << final_vol << " bg: " << final_bg << " sigi: " << final_sigi << " bsig: " << final_bsig << " i/sigi: " << final_i_sigi << " ave_ratio: " << ave_ratio_ << " weight: " << GetWeight() << " size: " << final_size );

}

Real PeakIntegration::GetQuality() const 
{
  return volume_/sigma_;
}

Real PeakIntegration::GetWeight() const 
{
  return CalcPeakIntegrationWeight(volume_/sigma_);
}

Real CalcPeakIntegrationWeight(Real isigi, Real s, Real o)
{
  return gsl_cdf_ugaussian_P(isigi/s-o);
}

ost::img::Size PeakIntegration::GetMaximalSize() const
{
  int sz=end_size_+bg_border_;
  return ost::img::Size(sz*2+1,sz*2+1);
}

void PeakIntegration::SetMethod(int m)
{
  method_=m;
}

}} // namespaces
