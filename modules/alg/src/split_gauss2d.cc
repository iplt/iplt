//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include "split_gauss2d.hh"

namespace iplt {  namespace alg {

std::ostream& operator<<(std::ostream& o, const ParamsSplitGauss2D& p)
{
  o << "A=" << p.GetA();
  o << " B=" << p.GetBx() << "," << p.GetBy();
  o << " u=" << p.GetUx() << "," << p.GetUy();
  o << " d=" << p.GetDelta();
  o << " w=" << p.GetW();
  o << " P=" << p.GetPx() << "," << p.GetPy();
  o << " C=" << p.GetC();
  
  return o;
}

Real FuncSplitGauss2D::Func(const ost::img::Point &p) const
{
  geom::Vec3 d(GetPixelSampling());

  Real x0 = (d[0]*(Real)p[0])-ux_;
  Real x1 = x0-delta_;
  Real x2 = x0+delta_;
  Real y = (d[1]*(Real)p[1])-uy_;

  Real X1 = x1*cosw_-y*sinw_;
  Real X2 = x2*cosw_-y*sinw_;
  Real Y = x0*sinw_+y*cosw_;

  return A_ * ( exp( - X1*X1*iBx2_) + exp( - X2*X2*iBx2_) ) * exp( - Y*Y*iBy2_ ) +
    Px_*x0 + Py_*y + C_;
}

std::ostream& operator<<(std::ostream& o, const FuncSplitGauss2D& p)
{
  o << "A=" << p.GetA();
  o << " B=" << p.GetBx() << "," << p.GetBy();
  o << " u=" << p.GetUx() << "," << p.GetUy();
  o << " d=" << p.GetDelta();
  o << " w=" << p.GetW();
  o << " P=" << p.GetPx() << "," << p.GetPy();
  o << " C=" << p.GetC();
  
  return o;
}

}} // ns
