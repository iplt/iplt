//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Valerio Mariani
*/

#ifndef IPLT_EX_NUMERICAL_H
#define IPLT_EX_NUMERICAL_H

#include <vector> 

#include <ost/base.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg { namespace detail {

int max(int a, int b);
void swap (double * a, double * b);
double sign (double a, double b);
void shft3 (double * a, double * b, double * c, double d);
DLLEXPORT_IPLT_ALG void mnbrak ( double * ax, double * bx, double *cx, double *fa, double *fb, double * fc, double (*func) ( double , void * ), void * func_params );
DLLEXPORT_IPLT_ALG double brent ( double ax, double bx, double cx, double (*func) ( double,  void * ), double tol, double * xmin, void * func_params );
double zbrent ( double (*func) ( double, void *), double x1, double x2, double tol, void * func_params );
DLLEXPORT_IPLT_ALG void polint(const std::vector<double>& xa, const std::vector<double>& ya, double x, double& y, double& dy);

}}} //ns

#endif // NUMERICAL_H
