//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  Author: Ansgar Philippsen
*/

#include <iostream>

#include <ost/message.hh>

#include "gauss2d.hh"

namespace iplt {  namespace alg {

std::ostream& operator<<(std::ostream& o, const ParamsGauss2D& p)
{
  o << "A=" << p.GetA();
  o << " B=" << p.GetBx() << "," << p.GetBy();
  o << " u=" << p.GetUx() << "," << p.GetUy();
  o << " w=" << p.GetW();
  o << " P=" << p.GetPx() << "," << p.GetPy();
  o << " C=" << p.GetC();
  
  return o;
}


// FuncGauss2D

FuncGauss2D::FuncGauss2D():
  ost::img::RealFunction(ost::img::SPATIAL),
  ParamsGauss2D(1.0,1.0,1.0,0.0, 0.0,0.0,0.0, 0.0,0.0)
{
  calc_derived();
}

FuncGauss2D::FuncGauss2D(const ParamsGauss2D& p):
  ost::img::RealFunction(ost::img::SPATIAL),
  ParamsGauss2D(p)
{
  calc_derived();
}

FuncGauss2D::FuncGauss2D(Real A, Real Bx, Real By, Real C, Real ux, Real uy, Real w, Real Px, Real Py):
  ost::img::RealFunction(ost::img::SPATIAL),
  ParamsGauss2D(A,Bx,By,C,ux,uy,w,Px,Py)
{
  calc_derived();
}


void FuncGauss2D::SetA(Real A) {ParamsGauss2D::SetA(A); calc_derived();}
void FuncGauss2D::SetBx(Real Bx) {ParamsGauss2D::SetBx(Bx); calc_derived();}
void FuncGauss2D::SetBy(Real By) {ParamsGauss2D::SetBy(By); calc_derived();}
void FuncGauss2D::SetC(Real C) {ParamsGauss2D::SetC(C); calc_derived();}
void FuncGauss2D::SetUx(Real ux) {ParamsGauss2D::SetUx(ux); calc_derived();}
void FuncGauss2D::SetUy(Real uy) {ParamsGauss2D::SetUy(uy); calc_derived();}
void FuncGauss2D::SetW(Real w) {ParamsGauss2D::SetW(w); calc_derived();}
void FuncGauss2D::SetPx(Real Px) {ParamsGauss2D::SetPx(Px); calc_derived();}
void FuncGauss2D::SetPy(Real Py) {ParamsGauss2D::SetPy(Py); calc_derived();}

Real FuncGauss2D::Func(const geom::Vec3 &v) const
{
  geom::Vec3 d=CompMultiply(GetPixelSampling(),v);
  Real x=d[0]-ux_;
  Real y=d[1]-uy_;

  Real X = x*cosw_-y*sinw_;
  Real Y = x*sinw_+y*cosw_;

  return A_ * exp( - X*X*iBx2_ - Y*Y*iBy2_ ) + Px_*x + Py_*y + C_;
}


void FuncGauss2D::calc_derived()
{
  iBx2_=1.0/(Bx_*Bx_);
  iBy2_=1.0/(By_*By_);
  sinw_=sin(w_);
  cosw_=cos(w_);
}

}} // namespaces
