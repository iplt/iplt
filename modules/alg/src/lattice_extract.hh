//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  extract values from image based on 2D lattice

  Authors: Ansgar Philippsen, Andreas Schenk, Giani Signorell
*/

#ifndef IPLT_ALG_LATTICE_EXTRACT_H
#define IPLT_ALG_LATTICE_EXTRACT_H

#include <ost/img/image.hh>
#include <ost/img/algorithm.hh>

#include <ost/info/info_fw.hh>

#include <ost/img/alg/stat.hh>
#include <iplt/alg/peak_search.hh>

#include <iplt/lattice.hh>

#include "peak_integration.hh"
#include "lattice_point.hh"

namespace iplt {  namespace alg {

//! Lattice Extract
/*!
  Use a given lattice to extract values from an image
*/
class DLLEXPORT_IPLT_ALG LatticeExtract: public NonModAlgorithm {
public:
  //! initialize with lattice and fit radius
  LatticeExtract(const Lattice& lat, int size_start, int size_end, bool dynamic_fitting, bool pre_check);

  //! set new lattice to use
  void SetLattice(const Lattice& l) {lattice_=l;}
  //! retrieve current lattice
  const Lattice& GetLattice() const {return lattice_;}

  void SetFitSize(int start,int end) {size_start_=start; size_end_=std::max(start,end);}
  int GetStartSize() const {return size_start_;}
  int GetEndSize() const {return size_end_;}

  void SetDynamicMode(bool d) {dynamic_=d;}

  //! Get resulting lattice point list
  LatticePointList GetList() const;

  //! algorithm interface
  void Visit(const ost::img::ConstImageHandle& img);
  //! algorithm interface
  void Visit(const ost::img::Function& fnc);

  ost::img::ImageHandle GetCorrectedImage() const;

private:
  Lattice lattice_;
  int size_start_,size_end_;
  bool dynamic_;
  bool pre_check_;

  LatticePointList lpl_;

  ost::img::ImageHandle corr_img_;

  bool pi_check(const ost::img::ConstImageHandle& im, const ost::img::Point& p);
};

//! Set lattice extract parameters based on info group
/*!
  The parameters from the given LatticeExtract object are modified
  according to the given InfoGroup instance. The resulting 
  LatticeExtract object is returned.

  Note that only the parameters that are actually present in
  the info group are used, allowing cumulative use of this
  function.
*/
DLLEXPORT_IPLT_ALG void UpdateFromInfo(LatticeExtract& le, const ost::info::InfoGroup& g);

}} // ns

#endif
