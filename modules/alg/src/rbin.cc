//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <iostream>
#include <cmath>

#include <ost/log.hh>

#include "rbin.hh"

namespace iplt {  namespace alg {

  ResolutionBin::ResolutionBin():
  bincount_(1),
  lim_low_(1.0),
  lim_hi_(0.5),
  use_rsq_(false),
  binsize_(0.5),
  bin_(bincount_)
{}

ResolutionBin::ResolutionBin(int bincount, Real reslow, Real reshi, bool use_rsq):
  bincount_(bincount),
  lim_low_(use_rsq ? 1.0/(reslow*reslow) : 1.0/reslow),
  lim_hi_(use_rsq ? 1.0/(reshi*reshi) : 1.0/reshi),
  use_rsq_(use_rsq),
  binsize_((lim_hi_-lim_low_)/static_cast<Real>(bincount_)),
  bin_(bincount_)
{
  if(use_rsq_) {
    LOG_INFO( "setting up 1/r^2 resolution binning" );
  } else {
    LOG_INFO( "setting up 1/r resolution binning" );
  }
}

void ResolutionBin::Add(Real resol, Real val, Real we)
{
  Real r = use_rsq_ ? 1.0/(resol*resol) : 1.0/resol;
  if(r>=lim_low_ && r<lim_hi_ && val>0.0) {
    int indx=static_cast<int>(std::floor((r-lim_low_)/binsize_));
    BinEntry be = {val,we};
    bin_[indx].push_back(be);
    if(use_rsq_) {
      LOG_TRACE( "adding " << val << " (" << we << ") at 1/r^2 " << r << " to bin " << indx );
    } else {
      LOG_TRACE( "adding " << val << " (" << we << ") at 1/r " << r << " to bin " << indx );
    }
  } else {
    if(use_rsq_) {
      LOG_TRACE( "ignoring " << val << " at 1/r^2 " << r );
    } else {
      LOG_TRACE( "ignoring " << val << " at 1/r " << r );
    }
  }
}

Real ResolutionBin::GetBinAverage(int n) const 
{
  Real ave=0.0;
  Real wsum=0.0;
  if (n>=0 && n<bincount_) {
    if(bin_[n].size()>0) {
      LOG_TRACE( "calculating average for bin " << n << " from " << bin_[n].size() << " entries" );
      for(unsigned int i=0;i<bin_[n].size();++i) {
	Real w=bin_[n][i].weight;
	ave+=bin_[n][i].value*w;
	wsum+=w;
      }
      ave/=wsum;
    }
  }
  return ave;
}

Real ResolutionBin::GetBinStdDev(int n) const
{
  Real stddev=0.0;
  Real wsum=0.0;
  if (n>=0 && n<bincount_) {
    if(bin_[n].size()>1) {
      Real ave = this->GetBinAverage(n);
      for(unsigned int i=0;i<bin_[n].size();++i) {
	Real w = bin_[n][i].weight;
	Real d = bin_[n][i].value-ave;
	stddev+=d*d*w;
	wsum+=w;
      }
      stddev=std::sqrt(stddev/wsum);
    }
  }
  return stddev;
}

int ResolutionBin::GetBinCount() const
{
  return bincount_;
}

Real ResolutionBin::GetBinResolution(int n)
{
  return use_rsq_ ? 1.0/sqrt(static_cast<Real>(n)*binsize_+lim_low_) : 1.0/(static_cast<Real>(n)*binsize_+lim_low_);
}

}} // ns
