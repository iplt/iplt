//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  log plot fitting

  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_LOG_FIT_HH
#define IPLT_EX_LOG_FIT_HH

#include <iplt/reflection_algorithm.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

struct LogFitEntry {
  LogFitEntry(Real xx=0.0, Real yy=0.0, Real ww=0.0):
    x(xx),y(yy),w(ww)
  {}
  Real x,y,w;
};

typedef std::vector< LogFitEntry > LogFitList;

/*
  y = S Log[ Bx + O ] + C
*/
class DLLEXPORT_IPLT_ALG LogFit
{

public:
  LogFit(bool zero_offset=false);

  void Add(Real x, Real y);
  void Add(Real x, Real y, Real w);

  Real GetS() const;
  Real GetB() const;
  Real GetC() const;
  Real GetChi() const;

  void SetMaxIter(unsigned int m);
  void SetLimits(Real l1, Real l2);

  void Apply();

private:
  LogFitList list_;
  bool zero_offset_;
  Real S_,B_,C_,chi_;
  unsigned int maxiter_;
  Real lim1_,lim2_;
};

}} // ns

#endif

