//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_ALG_ASCALE_HH
#define IPLT_EX_ALG_ASCALE_HH

#include <iplt/reflection_algorithm.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

/*
  anisotropic scaling with four parameters against a reference
  dataset

  log(I(h,k)/I0) = A + B1 h^2 + B2 k^2 + B3 hk
*/

class DLLEXPORT_IPLT_ALG AnisoScaling {
public:
  struct FitEntry {
    Real x,y1,y2,w;
  };

  typedef std::vector<FitEntry> FitEntryList;

public:
  AnisoScaling();

  void Add(Real x, Real y1, Real y2, Real w);
  void Apply();

  Real Estimate(Real y1, Real y2) const;

  Real GetA() const;
  Real GetB1() const;
  Real GetB2() const;
  Real GetB3() const;

private:

  FitEntryList elist_;
  int max_iter_;
  Real ilim_;
  Real A_,B1_,B2_,B3_;

};


}} // ns

#endif
