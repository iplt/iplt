//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  general weighted binning binning

  Authors: Ansgar Philippsen, Andreas Schenk
*/

#ifndef IPLT_EX_WBIN_H
#define IPLT_EX_WBIN_H

#include <vector>

#include <ost/base.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

/*
  general binning, used for example in resolution binning
*/
class DLLEXPORT_IPLT_ALG WeightedBin {
public:
  //! default ctor with count=1, lower limit=1 and higher limit=2, not really useful
  WeightedBin();

  //! Initialize with number of bins and bounds
  /*!
    bincount: number of bins to use
    limlow: lower limit 
    limhi: higher limit
  */
  WeightedBin(unsigned int bincount, Real lim_low,Real lim_hi);

  //! Add value at a given point, possibly with a weight
  void Add(Real x, Real y, Real weight=1.0);

  // calculate bin for given x, -1 is returned for out of bounds
  int CalcBin(Real x) const;

  //! Retrieve (weighted) average value from specified bin
  /*!
    ave=sum[value_i*weight_i]/sum[weight_i]
  */
  Real GetAverage(unsigned int bin) const;

  //! Retrieve (weighted) std dev from specified bin
  /*!
    sdev^2 = sum[value_i*weight_i-<ave>]/sum[weight_i]
  */
  Real GetStdDev(unsigned int bin) const;

  //! Retrieve average of weights
  /*!
    <we>=1.0/sum[1.0/weight_i]
    
    this is simply the stdev additive rule, where a cumulative
    standard deviation of additions is given by sqrt[sum[stdev_i^2]]
  */
  Real GetWeightAverage(unsigned int bin) const;

  //! return number of entries in given bin
  int GetSize(unsigned int bin) const;

  //! Return number of bins
  int GetBinCount() const;

  //! Retrieve middle limit of this bin
  Real GetLimit(unsigned int bin) const;

  Real GetLowerLimit(unsigned int bin) const;

  Real GetUpperLimit(unsigned int bin) const;

private:
  unsigned int bincount_;
  Real lim_low_,lim_hi_;
  Real binsize_;

  struct BinEntry {
    BinEntry():num(0),sum(0.0),sum2(0.0),weightsum(0.0),iweightsum(0.0){}
    unsigned int num;
    Real sum;
    Real sum2;
    Real weightsum;
    Real iweightsum;
  };
  
  std::vector<BinEntry> bins_;
};

}} // ns

#endif
