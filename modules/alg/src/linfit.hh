//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Valerio Mariani
*/


#ifndef IPLT_EX_LINFIT_H
#define IPLT_EX_LINFIT_H

#include <iostream>
#include <vector>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include <ost/base.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

//! Helper class for linear least squares fit
/*
  Given a number of datapoints (x,y), calculates
  the parameters 'm' and 'b' that fit best (in the
  least squares sense) the following model:

  y=mx+b

  Datapoints may have weights assigned to them for both
  x and y axes. The weight is the reciprocal of the variance.
  The returned goodness of fit is the square root of chi^2.
*/
class DLLEXPORT_IPLT_ALG LinearFit {
public:
  //! ctor
  LinearFit();

  //! force linear fit to have an offset of zero
  void ForceOrigin(bool f);

  //! add datapoint, using default weights of 1.0
  void AddDatapoint(double x, double y);

  //! add datapoint and weight for y, using default x weight of 1.0
  void AddDatapoint(double x, double y, double wy);

  //! add datapoint and weight for both coordinates
  void AddDatapoint(double x, double wx, double y, double wy);

  /*!
    Apply fit with given datapoints, return
    result as pair, with 'm' as first and 'b' as second
  */
  std::pair<double,double> Apply();

  std::pair<double,double> Estimate(double x) const;

  //! return number points
  int PointCount() const;

  //! clear points
  void Clear();

  double GetScale() const;
  double GetOffset() const;
  double GetGOF() const;
  double GetWGOF() const;
  double GetQ() const;
  double GetRSQ() const;
  double GetScaleVariance() const;
  double GetOffsetVariance() const;

  
private:
  std::vector<double> x_;
  std::vector<double> y_;
  std::vector<double> wx_;
  std::vector<double> wy_;
  bool zo_flag_;
  bool wx_flag_;
  double m_;
  double b_;
  double chi_;
  double chi2_;
  double q_;
  double rsq_;

  double var_m_;
  double var_b_;
  double cov_;

  void apply_wy();
  void apply_wxy();
  double r_square();
};

// 2D linear fit
class DLLEXPORT_IPLT_ALG LinearFit2D {
public:
  //! ctor
  LinearFit2D();

  ~LinearFit2D();

  //! force linear fit to have an offset of zero
  void ForceOrigin(bool f);

  //! add datapoint, using default weights of 1.0
  void AddDatapoint(double x1, double x2, double y);

  //! add datapoint and weight for y
  void AddDatapoint(double x1, double x2, double y, double wy);

  void Apply();

  std::pair<double,double> Estimate(double x1, double x2) const;

  //! return number points
  int PointCount() const;

  //! clear points
  void Clear();

  double GetScale1() const;
  double GetScale2() const;
  double GetOffset() const;
  double GetGOF() const;
  
private:
  std::vector<double> x1_;
  std::vector<double> x2_;
  std::vector<double> y_;
  std::vector<double> wy_;
  bool zo_flag_;
  double m1_;
  double m2_;
  double b_;
  double chi_;
  double chi2_;

  gsl_vector* x12_;
  gsl_vector* m12b_;
  gsl_matrix* cov_;

  void apply_wy();
};

}} // ns

#endif
