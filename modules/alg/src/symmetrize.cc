//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include "symmetrize.hh"

namespace iplt {  namespace alg {

ReflectionList Symmetrize::Visit(const ReflectionList& r)
{
  String phaid="";
  unsigned columnnum=r.GetPropertyCount();
  for(unsigned i=0;i<columnnum;i++){
    switch(r.GetPropertyType(i)){
    case iplt::PT_PHASE:
      phaid=r.GetPropertyName(i);
      break;
    default:
      break;
    }
  }
  ReflectionList result(r,false);
  if(phaid==""){
    for(ReflectionProxyter it=r.Begin();!it.AtEnd();++it){
      result.AddReflection(symmetry_.Symmetrize(it.GetIndex()),it);
    }
  }else{
    for(ReflectionProxyter it=r.Begin();!it.AtEnd();++it){
      //ReflectionProxyter tmp=result.AddReflection(ReflectionIndex(symmetry_.Symmetrize(it.GetIndex().ToVec3())),it);
      ReflectionProxyter tmp=result.AddReflection(symmetry_.Symmetrize(it.GetIndex()),it);
      tmp.Set(phaid,symmetry_.Symmetrize(it.GetIndex(),tmp.Get(phaid)));
    }
  }
  result.SetSymmetry(symmetry_);
  return result;
}

}} //ns
