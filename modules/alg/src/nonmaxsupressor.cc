//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  author: Andreas Schenk
*/

#include <iostream>
#include <vector>

#include <ost/img/alg/stat.hh>

#include "nonmaxsupressor.hh"

namespace iplt {  namespace alg { 

using namespace std;

NonMaxSupressor::NonMaxSupressor():
  ost::img::ConstModOPAlgorithm("NonMaxSupressor")
{}

ost::img::ImageHandle NonMaxSupressor::Visit(const ost::img::ConstImageHandle& i) const
{
  ost::img::ImageHandle iout=i.Copy();
  static Real msk[][3][3]={
    {{0,1,0},{0,0,0},{0,1,0}}, // 0 deg
    {{0,0,1},{0,0,0},{1,0,0}}, // 45 deg
    {{0,0,0},{1,0,1},{0,0,0}}, // 90 deg
    {{1,0,0},{0,0,0},{0,0,1}}  // 135 deg
  };

  for(ost::img::ExtentIterator it(i.GetExtent()); !it.AtEnd(); ++it) {
    int mask_id = static_cast<int>(round(std::arg(i.GetComplex(it))*4.0/M_PI))%4;
    // C++ and python behavior different: in C++ -3 %4 = -3, in python  -3 %4 = 1
    mask_id= mask_id<0 ? mask_id+4: mask_id;
    Real maxval=i.GetReal(it);
    for(int ki=0;ki<=2;++ki) {
      for(int kj=0;kj<=2;++kj) {
          ost::img::Point kp=ost::img::Point(it)+ost::img::Point(ki-1,kj-1);
        maxval = std::max(std::abs(i.GetComplex(kp))*msk[mask_id][kj][ki],maxval);
      }
    }
    
    if(maxval>std::abs(i.GetComplex(it))) {
      iout.SetReal(it,0);
    }
    
  }
  return iout;
}

}}//ns
