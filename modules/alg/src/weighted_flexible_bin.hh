//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  weighted binning with flexible bin size

  Author: Andreas Schenk
*/

#ifndef IPLT_EX_WFBIN_H
#define IPLT_EX_WFBIN_H

#include <set>

#include <ost/base.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

class DLLEXPORT_IPLT_ALG WeightedFlexibleBin {
  struct BinEntry {
    BinEntry():x(0),y(0.0),w(0.0){}
    BinEntry(Real xx, Real yy, Real ww):x(xx),y(yy),w(ww){}
    bool operator<(const BinEntry& rhs) const{return x<rhs.x;}
    Real x;
    Real y;
    Real w;
  };
public:
  //! default ctor with count=1, lower limit=1 and higher limit=2, not really useful
  WeightedFlexibleBin();

  //! Initialize with number of bins and bounds
  /*!
    bincount: number of bins to use
    limlow: lower limit 
    limhi: higher limit
  */
  WeightedFlexibleBin(unsigned int bincount, Real lim_low,Real lim_hi);

  //! Add value at a given point, possibly with a weight
  void Add(Real x, Real y, Real weight=1.0);

  // calculate bin for given x, -1 is returned for out of bounds
  int CalcBin(Real x) const;

  //! Retrieve (weighted) average value from specified bin
  /*!
    ave=sum[value_i*weight_i]/sum[weight_i]
  */
  Real GetAverage(unsigned int bin) const;

  //! Retrieve (weighted) std dev from specified bin
  /*!
    sdev^2 = sum[value_i*weight_i-<ave>]/sum[weight_i]
  */
  Real GetStdDev(unsigned int bin) const;

  //! Retrieve average of weights
  /*!
    <we>=1.0/sum[1.0/weight_i]
    
    this is simply the stdev additive rule, where a cumulative
    standard deviation of additions is given by sqrt[sum[stdev_i^2]]
  */
  Real GetWeightAverage(unsigned int bin) const;

  //! return number of entries in given bin
  int GetSize(unsigned int bin) const;

  //! Return number of bins
  int GetBinCount() const;
  void SetBinCount(unsigned int count);

  //! Retrieve middle limit of this bin
  Real GetLimit(unsigned int bin) const;

  Real GetLowerLimit(unsigned int bin) const;

  Real GetUpperLimit(unsigned int bin) const;

  
  void SetGlobalLowerLimit(Real limit);
  void SetGlobalUpperLimit(Real limit);

private:
  unsigned int bincount_;
  Real lim_low_,lim_hi_;
  Real binsize_;
  std::multiset<BinEntry> data_;
};

}} // ns

#endif
