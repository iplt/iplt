//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <ost/log.hh>

#include <ost/img/alg/fft.hh>
#include <ost/img/alg/clear.hh>

#include "lattice_point.hh"
#include "fourier_mask.hh"

namespace iplt {  namespace alg {

FourierMask::FourierMask(const Lattice& lat, const ost::img::Extent&subext, Real hw):
  ModOPAlgorithm("FourierMask"),
  lattice_(lat),
  subext_(subext),
  B_(1.0/(2.0*hw*hw))
{}

ImageHandle FourierMask::Visit(const ost::img::ConstImageHandle& ih)
{
  ::ost::img::alg::FFT fft;
  ost::img::ImageHandle workimg;
  if(ih.IsSpatial()) {
    workimg = ih.Apply(fft);
  } else {
    workimg = ih.Copy();
  }

  ost::img::ImageHandle resimg = workimg.Copy(false);

#ifndef WIN32
  ::ost::img::alg::Clear clr;
  resimg.ApplyIP(clr);
#endif
  
  LatticePointList lpl = PredictLatticePoints(lattice_,workimg.GetExtent());

  for(LatticePointProxyter lpp = lpl.Begin();!lpp.AtEnd();++lpp) {
    LatticePoint pnt=lpp.LPoint();

    ost::img::Point plit(pnt.GetPosition());
    geom::Vec3 pos(pnt.GetPosition());
    geom::Vec3 diff(pos-plit.ToVec3());

    for(ost::img::ExtentIterator eit(subext_); !eit.AtEnd(); ++eit) {
      Real x=Length(Vec3(ost::img::Point(eit).ToVec3())+diff);
      ost::img::Point pp(plit+eit);
      Real tmp=exp(-x*x*B_);
      resimg.SetComplex(pp,workimg.GetComplex(pp)*tmp);
    }
  }    

  // return input image matching domain
  if(ih.IsSpatial()) {
    resimg.ApplyIP(fft);
  }

  return resimg;
}


}} // ns
