//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <iostream>
#include <cmath>

#include <ost/log.hh>

#include "weighted_flexible_bin.hh"

namespace iplt {  namespace alg {

WeightedFlexibleBin::WeightedFlexibleBin():
  bincount_(1),
  lim_low_(1.0),
  lim_hi_(0.5),
  binsize_(0.5),
  data_()
{}

WeightedFlexibleBin::WeightedFlexibleBin(unsigned int bincount, Real limlow, Real limhi):
  bincount_(bincount),
  lim_low_(limlow),
  lim_hi_(limhi),
  binsize_((lim_hi_-lim_low_)/static_cast<Real>(bincount_)),
  data_()
{}

void WeightedFlexibleBin::Add(Real x, Real y, Real weight)
{
  if(weight<=0.0) {
    LOG_DEBUG( "wbin: ignored weight<=0" );
    return;
  }
  data_.insert(BinEntry(x,y,weight));
}

int WeightedFlexibleBin::CalcBin(Real x) const
{
  if(x>=lim_low_ && x<lim_hi_) {
    return static_cast<int>(std::floor((x-lim_low_)/binsize_));
  } else {
    return -1;
  }
}


Real WeightedFlexibleBin::GetAverage(unsigned int n) const 
{
  if (n>=bincount_)return 0.0;
  Real limit_low=GetLowerLimit(n);
  Real limit_high=GetUpperLimit(n);
  std::multiset<BinEntry>::const_iterator it=data_.lower_bound(BinEntry(limit_low,0.0,0.0));
  std::multiset<BinEntry>::const_iterator end=data_.lower_bound(BinEntry(limit_high,0.0,0.0));
  Real sum=0.0,weightsum=0.0;
  for(;it!=end;++it){
    sum+=it->y*it->w;
    weightsum+=it->w;
  }
  if(weightsum>0.0){
    return sum/weightsum;
  }else{
    return 0.0;
  }
}

Real WeightedFlexibleBin::GetStdDev(unsigned int n) const
{
  if (n>=bincount_)return 0.0;
  Real limit_low=GetLowerLimit(n);
  Real limit_high=GetUpperLimit(n);
  std::multiset<BinEntry>::const_iterator it=data_.lower_bound(BinEntry(limit_low,0.0,0.0));
  std::multiset<BinEntry>::const_iterator end=data_.lower_bound(BinEntry(limit_high,0.0,0.0));
  Real sum=0.0,weightsum=0.0,sum2=0.0;
  for(;it!=end;++it){
    sum+=it->y*it->w;
    sum2+=it->y*it->w*it->y*it->w;
    weightsum+=it->w;
  }
  if(weightsum>0.0){
    Real average=sum/weightsum;
    Real var=sum2/weightsum-average*average;
    return sqrt(var);
  }else{
    return 0.0;
  }
}

Real WeightedFlexibleBin::GetWeightAverage(unsigned int n) const
{
  if (n>=bincount_)return 0.0;
  Real limit_low=GetLowerLimit(n);
  Real limit_high=GetUpperLimit(n);
  std::multiset<BinEntry>::const_iterator it=data_.lower_bound(BinEntry(limit_low,0.0,0.0));
  std::multiset<BinEntry>::const_iterator end=data_.lower_bound(BinEntry(limit_high,0.0,0.0));
  Real iweightsum=0.0;
  for(;it!=end;++it){
    iweightsum+=1.0/it->w;
  }
  return 1.0/iweightsum;
}

int WeightedFlexibleBin::GetSize(unsigned int n) const
{
  if (n>=bincount_)return 0;
  Real limit_low=GetLowerLimit(n);
  Real limit_high=GetUpperLimit(n);
  std::multiset<BinEntry>::const_iterator it=data_.lower_bound(BinEntry(limit_low,0.0,0.0));
  std::multiset<BinEntry>::const_iterator end=data_.lower_bound(BinEntry(limit_high,0.0,0.0));
  unsigned int count=0;
  for(;it!=end;++it){
    ++count;
  }
  return count;
}


int WeightedFlexibleBin::GetBinCount() const
{
  return bincount_;
}

Real WeightedFlexibleBin::GetLimit(unsigned int n) const
{
  return (static_cast<Real>(n)+0.5)*binsize_+lim_low_;
}

Real WeightedFlexibleBin::GetLowerLimit(unsigned int n) const
{
  return static_cast<Real>(n)*binsize_+lim_low_;
}

Real WeightedFlexibleBin::GetUpperLimit(unsigned int n) const
{
  return static_cast<Real>(n+1)*binsize_+lim_low_;
}
void WeightedFlexibleBin::SetBinCount(unsigned int count)
{
  bincount_=count;
  binsize_=(lim_hi_-lim_low_)/static_cast<Real>(bincount_);
 
}
void WeightedFlexibleBin::SetGlobalLowerLimit(Real limit)
{
  lim_low_=limit;
  binsize_=(lim_hi_-lim_low_)/static_cast<Real>(bincount_);
  
}
void WeightedFlexibleBin::SetGlobalUpperLimit(Real limit)
{
  lim_hi_=limit;
  binsize_=(lim_hi_-lim_low_)/static_cast<Real>(bincount_);
  
}

}} // ns
