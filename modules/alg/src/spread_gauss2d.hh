//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  Author: Ansgar Philippsen
*/

#include <ost/img/image.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

/*
  Spread a gaussian function onto an image
  at the specified (fractional) coordinates
  with the given amplitude and sigma.
  The integrated pixel values will sum to
  the given amplitude (well, almost)
*/
class DLLEXPORT_IPLT_ALG SpreadGauss2D: public ost::img::ModIPAlgorithm
{
public:
  SpreadGauss2D(const geom::Vec2& xy, Real amp, Real sigma);

  virtual void Visit(ost::img::ImageHandle& ih);
private:
  Real x_, y_, amp_, sigma_;
};

}} // ns
