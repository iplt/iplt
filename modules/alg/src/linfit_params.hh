//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Valerio Mariani
*/

#ifndef IPLT_EX_LINFIT_PARAMS_H
#define IPLT_EX_LINFIT_PARAMS_H

#include <gsl/gsl_vector.h>

namespace iplt {  namespace alg { namespace detail {

struct LinFitErrXYParams
{
  gsl_vector * xx_p;
  gsl_vector * yy_p;
  gsl_vector * sx_p; 
  gsl_vector * sy_p;
  gsl_vector * ww_p;
  double aa;
  double offs;
};

}}} //ns

#endif // IPLT_EX_LINFIT_PARAMS_H
