//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <iostream>

#include <ost/log.hh>

#include <iplt/reflection_list.hh>

#include "lattice_point.hh"

namespace iplt {  namespace alg {

LatticePoint::LatticePoint(const geom::Vec2& pos,
			   const ost::img::Extent&h,
			   const alg::Stat& stat,
			   const alg::FitGauss2D& fit,
			   const PeakIntegration& pi,
			   Real icbg,
			   Real ave_bg,Real ave_sigbg):
  pos_(pos), reg_(h), stat_(stat), fit_(fit), pi_(pi), ic_bg_(icbg),
  ave_bg_(ave_bg),ave_sigbg_(ave_sigbg)
{}

Vec2 LatticePoint::GetPosition() const
{
  return pos_;
}

void LatticePoint::SetPosition(const geom::Vec2& pos)
{
  pos_=pos;
}

Extent LatticePoint::GetRegion() const
{
  return reg_;
}

void LatticePoint::SetRegion(const ost::img::Extent&h)
{
  reg_=h;
}

alg::FitGauss2D LatticePoint::GetFit() const
{
  return fit_;
}

void LatticePoint::SetFit(const alg::FitGauss2D& fit)
{
  fit_=fit;
}

alg::Stat LatticePoint::GetStat() const
{
  return stat_;
}

void LatticePoint::SetStat(const alg::Stat& stat)
{
  stat_=stat;
}

PeakIntegration LatticePoint::GetPI() const
{
  return pi_;
}

void LatticePoint::SetPI(const PeakIntegration& pi)
{
  pi_=pi;
}

Real LatticePoint::GetICBG() const
{
  return ic_bg_;
}

void LatticePoint::SetICBG(Real icbg)
{
  ic_bg_=icbg;
}

Real LatticePoint::GetAveBG() const
{
  return ave_bg_;
}

void LatticePoint::SetAveBG(Real abg)
{
  ave_bg_=abg;
}

Real LatticePoint::GetAveSigBG() const
{
  return ave_sigbg_;
}

void LatticePoint::SetAveSigBG(Real asbg)
{
  ave_sigbg_=asbg;
}

void LatticePoint::CalcAveBG(bool smart)
{
  Real bglist[]={fit_.GetC(),pi_.GetBackground(),ic_bg_};
  Real bgmean=(bglist[0]+bglist[1]+bglist[2])/3.0;
  Real dsum=0.0;
  for(int n=0;n<3;++n) {
    Real d=(bglist[n]-bgmean);
    dsum+=d*d;
  }
  Real sigbg=sqrt(dsum)/3.0;
  ave_bg_=bgmean;
  ave_sigbg_=sigbg;
}

LatticePoint::operator ost::img::Point() const
{
  return ost::img::Point(pos_);
}

long LatticePoint::MemSize() const
{
  return sizeof(*this);
}

////////////////////////////////////
// LatticePointProxyter

LatticePointProxyter::LatticePointProxyter():
  lmap_(),
  lit_()
{}

LatticePointProxyter::LatticePointProxyter(const lattice_detail::LMapPtr& lmap, 
					   const lattice_detail::LMapIterator& it):
  lmap_(lmap),
  lit_(it) 
{}

Point LatticePointProxyter::GetIndex() const 
{
  return lit_->first;
}

LatticePoint LatticePointProxyter::LPoint() const 
{
  return lit_->second;
}

LatticePoint& LatticePointProxyter::LPoint() 
{
  return lit_->second;
}

bool LatticePointProxyter::AtEnd() const 
{
  return lit_==(*lmap_).end();
}

bool LatticePointProxyter::IsValid() const
{
  return lmap_;
}

LatticePointProxyter& LatticePointProxyter::operator++() 
{
  if(lit_!=(*lmap_).end()) ++lit_;
  return *this;
}

LatticePointProxyter LatticePointProxyter::operator++(int)
{
  LatticePointProxyter tmp(lmap_,lit_);
  if(lit_!=(*lmap_).end()) ++lit_;
  return tmp;
}



////////////////////////////////////
// LatticePointList

LatticePointList::LatticePointList(const Lattice& l):
  lat_(l),
  lmap_(new lattice_detail::LMap())
{}

LatticePointList::LatticePointList(const LatticePointList& l):
  lat_(l.lat_),
  lmap_(new lattice_detail::LMap(*l.lmap_)) // deep copy
{}

LatticePointList& LatticePointList::operator=(const LatticePointList& l)
{
  if(&l!=this) {
    lat_=l.lat_;
    lmap_=lattice_detail::LMapPtr(new lattice_detail::LMap(*l.lmap_));
  }
  return *this;
}

Lattice LatticePointList::GetLattice() const
{
  return lat_;
}

void LatticePointList::SetLattice(const Lattice& lat)
{
  lat_=lat;
  /*
    adjust positions of lattice point entries
  */
  for(lattice_detail::LMapIterator lit = lmap_->begin(); lit!=lmap_->end();++lit) {
    lit->second.SetPosition(lat_.CalcPosition(lit->first));
  }
}

LatticePointProxyter LatticePointList::Add(const ost::img::Point& index, const LatticePoint& p)
{
  return LatticePointProxyter(lmap_,
			      (*lmap_).insert(lattice_detail::LMap::value_type(index,p)).first);
}

LatticePointProxyter LatticePointList::Begin() const
{
  return LatticePointProxyter(lmap_,(*lmap_).begin());
}

LatticePointProxyter LatticePointList::Find(const ost::img::Point& index)
{
  lattice_detail::LMapIterator lit = (*lmap_).find(index);
  if(lit==(*lmap_).end()) {
    return LatticePointProxyter();
  } else {
    return LatticePointProxyter(lmap_,lit);
  }
}

void LatticePointList::Clear()
{
  (*lmap_).clear();
}

int LatticePointList::NumEntries() const
{
  return (*lmap_).size();
}

long LatticePointList::MemSize() const
{
  long msize=0;
  for(lattice_detail::LMapIterator it=(*lmap_).begin();it!=(*lmap_).end();++it) {
    msize+=sizeof(it->first)+sizeof(it->second);
  }
  return msize + sizeof(lat_) + sizeof(lmap_);
}

////////////////////////////////////

LatticePointList PredictLatticePoints(const Lattice& lattice, const ost::img::Extent&extent)
{
  ost::img::PointList plist = Predict(lattice,extent);

  LatticePointList result(lattice);
  
  for(PointList::const_iterator it=plist.begin();it!=plist.end();++it) {
    ost::img::Point indx=*it;
    geom::Vec2 pos=lattice.CalcPosition(indx);
    LatticePoint lp(pos);
    result.Add(indx,lp);
  }
  return result;
}

ReflectionList ConvertToReflectionList(const LatticePointList& l)
{
  ReflectionList rlist(l.GetLattice());
  rlist.AddProperty("ave_bg");
  rlist.AddProperty("ave_sigbg");
  rlist.AddProperty("pos_x");
  rlist.AddProperty("pos_y");
  rlist.AddProperty("pi_volume");
  rlist.AddProperty("pi_sigma");
  rlist.AddProperty("pi_background");
  rlist.AddProperty("pi_bgsigma");
  rlist.AddProperty("pi_averatio");
  rlist.AddProperty("ic_background");
  rlist.AddProperty("stat_mean");
  rlist.AddProperty("stat_stdev");
  rlist.AddProperty("stat_sum");
  rlist.AddProperty("gaussfit_chi");
  rlist.AddProperty("gaussfit_gof");
  rlist.AddProperty("gaussfit_sigamp");
  rlist.AddProperty("gaussfit_quality");
  rlist.AddProperty("gaussfit_A");
  rlist.AddProperty("gaussfit_Bx");
  rlist.AddProperty("gaussfit_By");
  rlist.AddProperty("gaussfit_C");
  rlist.AddProperty("gaussfit_Ux");
  rlist.AddProperty("gaussfit_Uy");
  rlist.AddProperty("gaussfit_W");
  rlist.AddProperty("gaussfit_Px");
  rlist.AddProperty("gaussfit_Py");

  for(LatticePointProxyter lpp=l.Begin();!lpp.AtEnd();++lpp) {
    ReflectionProxyter rp=rlist.AddReflection(lpp.GetIndex());
    rp.Set("ave_bg",lpp.LPoint().GetAveBG());
    rp.Set("ave_sigbg",lpp.LPoint().GetAveSigBG());
    rp.Set("pos_x",lpp.LPoint().GetPosition()[0]);
    rp.Set("pos_y",lpp.LPoint().GetPosition()[1]);
    rp.Set("pi_volume",lpp.LPoint().GetPI().GetVolume());
    rp.Set("pi_sigma",lpp.LPoint().GetPI().GetSigma());
    rp.Set("pi_background",lpp.LPoint().GetPI().GetBackground());
    rp.Set("pi_bgsigma",lpp.LPoint().GetPI().GetBackgroundSigma());
    rp.Set("pi_averatio",lpp.LPoint().GetPI().GetAverageRatio());
    rp.Set("ic_background",lpp.LPoint().GetICBG());
    rp.Set("stat_mean",lpp.LPoint().GetStat().GetMean());
    rp.Set("stat_stdev",lpp.LPoint().GetStat().GetStandardDeviation());
    rp.Set("stat_sum",lpp.LPoint().GetStat().GetSum());
    rp.Set("gaussfit_chi",lpp.LPoint().GetFit().GetChi());
    rp.Set("gaussfit_gof",lpp.LPoint().GetFit().GetGOF());
    rp.Set("gaussfit_sigamp",lpp.LPoint().GetFit().GetSigAmp());
    rp.Set("gaussfit_quality",lpp.LPoint().GetFit().GetQuality());
    rp.Set("gaussfit_A",lpp.LPoint().GetFit().GetA());
    rp.Set("gaussfit_Bx",lpp.LPoint().GetFit().GetBx());
    rp.Set("gaussfit_By",lpp.LPoint().GetFit().GetBy());
    rp.Set("gaussfit_C",lpp.LPoint().GetFit().GetC());
    rp.Set("gaussfit_Ux",lpp.LPoint().GetFit().GetUx());
    rp.Set("gaussfit_Uy",lpp.LPoint().GetFit().GetUy());
    rp.Set("gaussfit_W",lpp.LPoint().GetFit().GetW());
    rp.Set("gaussfit_Px",lpp.LPoint().GetFit().GetPx());
    rp.Set("gaussfit_Py",lpp.LPoint().GetFit().GetPy());
  }
  
  return rlist;
}

LatticePointList ConvertToLatticePointList(const ReflectionList& rlist)
{
  bool p_pos_x = rlist.HasProperty("pos_x");
  bool p_pos_y = rlist.HasProperty("pos_y");
  bool p_ave_bg = rlist.HasProperty("ave_bg");
  bool p_ave_sigbg = rlist.HasProperty("ave_sigbg");
  bool p_pi_volume = rlist.HasProperty("pi_volume");
  bool p_pi_sigma = rlist.HasProperty("pi_sigma");
  bool p_pi_background = rlist.HasProperty("pi_background");
  bool p_pi_bgsigma = rlist.HasProperty("pi_bgsigma");
  bool p_pi_averatio = rlist.HasProperty("pi_averatio");
  bool p_ic_background = rlist.HasProperty("ic_background");
  bool p_stat_mean = rlist.HasProperty("stat_mean");
  bool p_stat_stdev = rlist.HasProperty("stat_stdev");
  bool p_stat_sum = rlist.HasProperty("stat_sum");
  bool p_fit_chi = rlist.HasProperty("gaussfit_chi");
  bool p_fit_gof = rlist.HasProperty("gaussfit_gof");
  bool p_fit_sigamp = rlist.HasProperty("gaussfit_sigamp");
  bool p_fit_quality = rlist.HasProperty("gaussfit_quality");
  bool p_fit_A = rlist.HasProperty("gaussfit_A");
  bool p_fit_Bx = rlist.HasProperty("gaussfit_Bx");
  bool p_fit_By = rlist.HasProperty("gaussfit_By");
  bool p_fit_C = rlist.HasProperty("gaussfit_C");
  bool p_fit_Ux = rlist.HasProperty("gaussfit_Ux");
  bool p_fit_Uy = rlist.HasProperty("gaussfit_Uy");
  bool p_fit_W = rlist.HasProperty("gaussfit_W");
  bool p_fit_Px = rlist.HasProperty("gaussfit_Px");
  bool p_fit_Py = rlist.HasProperty("gaussfit_Py");

  if (!p_pos_x) LOG_DEBUG( "rlist2lpl: missing pos_x" );
  if (!p_pos_y) LOG_DEBUG( "rlist2lpl: missing pos_y" );
  if (!p_ave_bg) LOG_DEBUG( "rlist2lpl: missing ave_bg" );
  if (!p_ave_sigbg) LOG_DEBUG( "rlist2lpl: missing ave_sigbg" );
  if (!p_pi_volume) LOG_DEBUG( "rlist2lpl: missing pi_volume" );
  if (!p_pi_sigma) LOG_DEBUG( "rlist2lpl: missing pi_sigma" );
  if (!p_pi_bgsigma) LOG_DEBUG( "rlist2lpl: missing pi_bgsigma" );
  if (!p_pi_background) LOG_DEBUG( "rlist2lpl: missing pi_background" );
  if (!p_pi_averatio) LOG_DEBUG( "rlist2lpl: missing pi_averatio" );
  if (!p_ic_background) LOG_DEBUG( "rlist2lpl: missing ic_background" );
  if (!p_stat_mean) LOG_DEBUG( "rlist2lpl: missing stat_mean" );
  if (!p_stat_stdev) LOG_DEBUG( "rlist2lpl: missing stat_stdev" );
  if (!p_stat_sum) LOG_DEBUG( "rlist2lpl: missing stat_sum" );
  if (!p_fit_chi) LOG_DEBUG( "rlist2lpl: missing fit_chi" );
  if (!p_fit_gof) LOG_DEBUG( "rlist2lpl: missing fit_gof" );
  if (!p_fit_sigamp) LOG_DEBUG( "rlist2lpl: missing fit_sigamp" );
  if (!p_fit_quality) LOG_DEBUG( "rlist2lpl: missing fit_quality" );
  if (!p_fit_A) LOG_DEBUG( "rlist2lpl: missing fit_A" );
  if (!p_fit_Bx) LOG_DEBUG( "rlist2lpl: missing fit_Bx" );
  if (!p_fit_By) LOG_DEBUG( "rlist2lpl: missing fit_By" );
  if (!p_fit_C) LOG_DEBUG( "rlist2lpl: missing fit_C" );
  if (!p_fit_Ux) LOG_DEBUG( "rlist2lpl: missing fit_Ux" );
  if (!p_fit_Uy) LOG_DEBUG( "rlist2lpl: missing fit_Uy" );
  if (!p_fit_W) LOG_DEBUG( "rlist2lpl: missing fit_W" );
  if (!p_fit_Px) LOG_DEBUG( "rlist2lpl: missing fit_Px" );
  if (!p_fit_Py) LOG_DEBUG( "rlist2lpl: missing fit_Py" );

  LatticePointList lpl(rlist.GetLattice());

  for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp) {
    alg::Stat stat;
    stat.SetMean(p_stat_mean ? rp.Get("stat_mean") : 0.0);
    stat.SetSum(p_stat_sum ? rp.Get("stat_sum") : 0.0);
    stat.SetStandardDeviation(p_stat_stdev ? rp.Get("stat_stdev") : 0.0);

    alg::FitGauss2D fit(p_fit_A ? rp.Get("gaussfit_A") : 0.0,
			p_fit_Bx ? rp.Get("gaussfit_Bx") : 0.0,
			p_fit_By ? rp.Get("gaussfit_By") : 0.0,
			p_fit_C ? rp.Get("gaussfit_C") : 0.0,
			p_fit_Ux ? rp.Get("gaussfit_Ux") : 0.0,
			p_fit_Uy ? rp.Get("gaussfit_Uy") : 0.0,
			p_fit_W ? rp.Get("gaussfit_W") : 0.0,
			p_fit_Px ? rp.Get("gaussfit_Px") : 0.0,
			p_fit_Py ? rp.Get("gaussfit_Py") : 0.0);
    fit.SetChi(p_fit_chi ? rp.Get("gaussfit_chi") : 0.0);
    fit.SetGOF(p_fit_gof ? rp.Get("gaussfit_gof") : 0.0);
    fit.SetSigAmp(p_fit_sigamp ? rp.Get("gaussfit_sigamp") : 0.0);
    fit.SetQuality(p_fit_quality ? rp.Get("gaussfit_quality") : 0.0);

    PeakIntegration pi;
    pi.SetVolume(p_pi_volume ? rp.Get("pi_volume") : 0.0);
    pi.SetSigma(p_pi_sigma ? rp.Get("pi_sigma") : 0.0);
    pi.SetBackground(p_pi_background ? rp.Get("pi_background") : 0.0);
    pi.SetBackgroundSigma(p_pi_bgsigma ? rp.Get("pi_bgsigma") : 0.0);
    pi.SetAverageRatio(p_pi_averatio ? rp.Get("pi_averatio") : 0.0);

    Real icbg = p_ic_background ? rp.Get("ic_background") : 0.0;

    Real ave_bg = p_ave_bg ? rp.Get("ave_bg") : 0.0;
    Real ave_sigbg = p_ave_sigbg ? rp.Get("ave_sigbg") : 0.0;

    lpl.Add(rp.GetIndex().AsDuplet(),
	    LatticePoint(geom::Vec2(p_pos_x ? rp.Get("pos_x") : 0.0,
			      p_pos_y ? rp.Get("pos_y") : 0.0),
			 ost::img::Extent(),
			 stat,
			 fit,
			 pi,
			 icbg,
			 ave_bg,ave_sigbg));
  }
  return lpl;
}


}} // ns
