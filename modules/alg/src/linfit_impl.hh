//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Valerio Mariani
*/

#ifndef IPLT_EX_LINFIT_IMPL_H
#define IPLT_EX_LINFIT_IMPL_H

#include <gsl/gsl_vector.h>

namespace iplt {  namespace alg { namespace detail {

//! Least square fitting. Linear regression with errors in x and y
/*!
  This function computes the best-fit linear regression coefficients (c0,c1) of
  the model Y = c_0 + c_1 X for the weighted datasets (x, y), two vectors of 
  length n with strides xstride and ystride. The vector wxa, of length n and 
  stride wxstride, specifies the x weight of each datapoint. The vector wya, of
  length n and stride wystride, specifies the y weight of each datapoint. Weight
  is the reciprocal of the variance for each datapoint. The standard deviation
  for the parameters (c0, c1) is estimated from weighted data and returned via
  the parameters sigc0, sigc1. The weighted sum of squares of the residuals from
  the best-fit line, chi^2, is returned in chi2; 
*/
void fit_wxylinear (const double * xa, const size_t xstride, 
		    const double * w_xa, const size_t wxstride,
		    const double * y, const size_t ystride,
		    const double * w_ya, const size_t wystride, 
		    size_t n, double * c0, double * c1,
		    double * sigc0, double * sigc1, double * chi2, double *q);

//! Least square fitting. Linear regression with errors in x and y. Slope only
/*!
  This function computes the best-fit linear regression coefficient (c1) of the
  model Y = c_1 X for the weighted datasets (x, y), two vectors of length n with
  strides xstride and ystride. The vector wxa, of length n and stride wxstride, 
  specifies the x weight of each datapoint. The vector wya, of length n and stride
  wystride, specifies the y weight of each datapoint. Weight is the reciprocal of
  the variance for each datapoint. The standard deviation for the parameter c1 is
  estimated from weighted data and returned via the parameter sigc1. The weighted
  sum of squares of the residuals from the best-fit line, chi^2, is returned in
  chi2; 
*/
void fit_wxymul (const double * xa, const size_t xstride,
		 const double * w_xa, const size_t wxstride,
		 const double * ya, const size_t ystride,
		 const double * w_ya, const size_t wystride,
		 size_t n, double * c1, double * sigc1, double * chi2, double *q);


}}} // ns


#endif
