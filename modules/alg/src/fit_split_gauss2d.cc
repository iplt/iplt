//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  SplitGauss2D fitting

  Author: Ansgar Philippsen
*/

#include <iostream>

#include <boost/shared_ptr.hpp>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>

#include <ost/img/image.hh>
#include <ost/log.hh>
#include <ost/img/alg/stat.hh>

#include "fit_split_gauss2d.hh"
#include "split_gauss2d.hh"

namespace iplt {  namespace alg {

using namespace ost::img::alg;

FitSplitGauss2D::FitSplitGauss2D(Real A, Real Bx, Real By, Real C, Real ux, Real uy, Real w, Real Px, Real Py, Real delta):
  NonModAlgorithm("FitSplitGauss2D"),
  ParamsSplitGauss2D(A,Bx,By,C,ux,uy,w,Px,Py,delta),
  max_iter_(200),
  chi_(0.0), gof_(0.0), qual_(0.0), qual2_(0.0),sigamp_(0.0),
  sigma_(1.0),
  lim1_(1e-20),lim2_(1e-20),
  itcount_(0)
{
}

namespace {

struct fit_gauss2d_params { 
  const ost::img::Data& data;
  ost::img::Extent ext;
  ost::img::Point offset;
  Real sigma;
};


/*
  provide function value and derivative values at the same time
*/
template <bool b1, bool b2>
int gauss2d_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  fit_gauss2d_params* prm = reinterpret_cast<fit_gauss2d_params*>(params);
  
  // retrieve paramers
  Real A =  gsl_vector_get(x, FitSplitGauss2D::ID_A);
  Real Bx = gsl_vector_get(x, FitSplitGauss2D::ID_BX);
  Real By = gsl_vector_get(x, FitSplitGauss2D::ID_BY);
  Real ux = gsl_vector_get(x, FitSplitGauss2D::ID_UX);
  Real uy = gsl_vector_get(x, FitSplitGauss2D::ID_UY);
  Real C  = gsl_vector_get(x, FitSplitGauss2D::ID_C);
  Real w =  gsl_vector_get(x, FitSplitGauss2D::ID_W);
  Real Px = gsl_vector_get(x, FitSplitGauss2D::ID_PX);
  Real Py = gsl_vector_get(x, FitSplitGauss2D::ID_PY);
  Real delta = gsl_vector_get(x, FitSplitGauss2D::ID_D);

  // derived values
  Real sinw = sin(w);
  Real cosw = cos(w);
  Real Bx2 = Bx*Bx;
  Real iBx2 = 1.0/Bx2;
  Real Bx3 = Bx2*Bx;
  Real iBx3 = 1.0/Bx3;
  Real By2 = By*By;
  Real iBy2 = 1.0/By2;
  Real By3 = By2*By;
  Real iBy3 = 1.0/By3;

  Real inv_sigma = 1.0/prm->sigma;
  
  ost::img::Extent ext = prm->ext;
  ost::img::Point offset = prm->offset;

  //int nx = ext.GetSize().GetWidth();
  //int ny = ext.GetSize().GetHeight();
  
  for(int xi=ext.GetStart()[0];xi<=ext.GetEnd()[0];++xi) {
    // TODO: pixel scaling
    Real x0 = Real(xi)-ux;
    Real x1 = x0-delta;
    Real x2 = x0+delta;
    
    for(int yi=ext.GetStart()[1];yi<=ext.GetEnd()[1];++yi) {
      // TODO: pixel scaling
      Real y = Real(yi)-uy;

      // precalc values
      Real xx1 = cosw*x1-sinw*y;
      Real xx2 = cosw*x2-sinw*y;
      Real yy = sinw*x0+cosw*y;
      Real exx1 = exp(-xx1*xx1*iBx2);
      Real exx2 = exp(-xx2*xx2*iBx2);
      Real exy = exp(-yy*yy*iBy2);

      Real exyx12=exy*(exx1+exx2);

      int indx = ext.Point2Offset(ost::img::Point(xi,yi,0));

      if(b1) {
	Real f_xy = A*exyx12+Px*x0+Py*y+C;
	// retrieve actual value from Data*
        gsl_vector_set(f, indx, (f_xy - prm->data.GetReal(ost::img::Point(xi,yi,0)+offset))*inv_sigma );
      }

      if(b2) {
	Real der_A  = exyx12;
	Real der_Bx = A*exy*2.0*iBx3*(exx1*xx1*xx1+exx2*xx2*xx2);
	Real der_By = 2.0*A*iBy3*exy*(exx1+exx2)*yy*yy;
	Real der_Ux = 2.0*A*(exy*iBx2*cosw*(exx1*xx1+exx2*xx2)+iBy2*exyx12*sinw*yy)-Px;
	Real der_Uy = 2.0*A*(-exy*iBx2*sinw*(exx1*xx1+exx2*xx2)+iBy2*exyx12*cosw*yy)-Py;
	Real der_C  = 1.0;
	Real tmpw1 = -sinw*x1-cosw*y;
	Real tmpw2 = -sinw*x2-cosw*y;
	Real tmpw3 = cosw*x0-sinw*y;
	Real der_w  = -2.0*A*exy*iBx2*(exx1*tmpw1*xx1+exx2*tmpw2*xx2)-
	  2.0*A*iBy2*exyx12*yy*tmpw3;
	Real der_Px = x0;
	Real der_Py = y;
	Real der_delta = 2.0*A*iBx2*exy*cosw*(exx1*xx1-exx2*xx2);


	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_A,  der_A*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_BX, der_Bx*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_BY, der_By*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_UX, der_Ux*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_UY, der_Uy*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_C,  der_C*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_W,  der_w*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_PX, der_Px*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_PY, der_Py*inv_sigma);
	gsl_matrix_set(J, indx, FitSplitGauss2D::ID_D , der_delta*inv_sigma);
      }
    }
  }
  return GSL_SUCCESS;
}

static int gauss2d_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return gauss2d_tmpl<true,false>(x,params,f,0);
}

static int gauss2d_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return gauss2d_tmpl<false,true>(x,params,0,J);
}

static int gauss2d_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return gauss2d_tmpl<true,true>(x,params,f,J);
}

// helper classes for exception safety

struct gsl_vector_deleter {
  void operator()(gsl_vector* p) {
    gsl_vector_free(p);
  }
};

typedef boost::shared_ptr<gsl_vector> gsl_vector_ptr;

}

/*
  fit given input data to a split 2D gaussian

  monolithic code, very much so copied from fit gauss2d
*/
void FitSplitGauss2D::VisitData(const ost::img::Data& data)
{
  // number of parameters to fit
  int paramN=10;

  // number of values is the total number of image points in 2D plane
  int valueN=data.GetSize().GetSlab();

  ost::img::Extent cen_ext(data.GetSize());
  ost::img::Point offset(data.GetExtent().GetStart()-cen_ext.GetStart());

  // subtract offset from given center
  ux_ -= (Real)offset[0];
  uy_ -= (Real)offset[1];

  // initialize custom data to pass to fitting functions
  fit_gauss2d_params prm = {data, cen_ext, offset, sigma_};

  // vector holding parameter,
  gsl_vector_ptr X(gsl_vector_alloc(paramN),gsl_vector_deleter());

  // initialize with stored values
  gsl_vector_set(X.get(),ID_A, A_);
  gsl_vector_set(X.get(),ID_BX,Bx_);
  gsl_vector_set(X.get(),ID_BY,By_);
  gsl_vector_set(X.get(),ID_UX,ux_);
  gsl_vector_set(X.get(),ID_UY,uy_);
  gsl_vector_set(X.get(),ID_C,C_);
  gsl_vector_set(X.get(),ID_W,w_);
  gsl_vector_set(X.get(),ID_PX,Px_);
  gsl_vector_set(X.get(),ID_PY,Py_);
  gsl_vector_set(X.get(),ID_D ,delta_);

  // prepare the functions
  gsl_multifit_function_fdf func;
  func.f = &gauss2d_f;
  func.df = &gauss2d_df;
  func.fdf = &gauss2d_fdf;
  func.n = valueN;
  func.p = paramN;
  func.params = &prm;

  // levenberg-marquardt solver
  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;
  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, valueN, paramN);
  gsl_multifit_fdfsolver_set (solver, &func, X.get());

  LOG_DEBUG( "commencing iterations" );
  int iter=0;
  int status=GSL_CONTINUE;
  // actual iterations
  while (status == GSL_CONTINUE && iter < max_iter_) {
    ++iter;

    gsl_multifit_fdfsolver_iterate (solver);

    // ensure that Bx is >=0
    gsl_vector_set(solver->x, ID_BX, 
		   std::abs(gsl_vector_get(solver->x, ID_BX)));
    // ensure that By is >=0
    gsl_vector_set(solver->x, ID_BY, 
		   std::abs(gsl_vector_get(solver->x, ID_BY)));
    // ensure that omega is in 0..2pi range
    gsl_vector_set(solver->x, ID_W, 
		   std::fmod(gsl_vector_get(solver->x, ID_W),2.0*M_PI));

    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1_, lim2_);
  } 
  itcount_ = iter;
  LOG_DEBUG( "done with iterations (" << iter << "): " << gsl_strerror (status) );

  // write fitted values back
  A_  = gsl_vector_get(solver->x, ID_A);
  Bx_ = gsl_vector_get(solver->x, ID_BX);
  By_ = gsl_vector_get(solver->x, ID_BY);
  ux_ = gsl_vector_get(solver->x, ID_UX);
  uy_ = gsl_vector_get(solver->x, ID_UY);
  C_  = gsl_vector_get(solver->x, ID_C);
  w_  = gsl_vector_get(solver->x, ID_W);
  Px_ = gsl_vector_get(solver->x, ID_PX);
  Py_ = gsl_vector_get(solver->x, ID_PY);
  delta_ = gsl_vector_get(solver->x, ID_D);

  // add offset back to center
  ux_ += (Real)offset[0];
  uy_ += (Real)offset[1];

  /*
    \chi and the resulting goodness of fit
  */
  chi_ = gsl_blas_dnrm2(solver->f);
  gof_ = (chi_*chi_)/ (Real)(valueN - paramN);
  
  // clean up
  gsl_multifit_fdfsolver_free (solver);

  // handled by shared_ptr
  //gsl_vector_free(X);


  /*
    Calculate quality - alternative goodness of fit

    The ratio between (a) the average deviation (squared sum of squared differences)
    of the fit with the real data and (b) the standard deviation squared of the
    actual data.

    Rational: The variance within the fit should be better than the variance
    within the overall data.

    NOTE: this is just \chi, normalized by the number of points, using
    the standard deviation in the actual data as an error estimate.

    qual^2 = 1/N \sum{ (y_i - f(x_i;a))^2 / stdev^2 }

    NOTE2: it is not clear if this is useful at all!
  */
  FuncSplitGauss2D ff = this->AsFunction();

  Real total_diff_abs_sq=0.0;
  Real total_diff_rel_sq=0.0;
  for(ost::img::ExtentIterator it(data.GetExtent()); !it.AtEnd(); ++it) {
    Real data_val = data.GetReal(it);
    Real diff_abs = ff.GetReal(it)-data_val;
    Real diff_rel = diff_abs/(data_val-ff.GetBackground(geom::Vec2(((ost::img::Point)it).ToVec2())));
    total_diff_abs_sq += diff_abs*diff_abs;
    total_diff_rel_sq += diff_rel*diff_rel;
  }

  Stat stat;
  data.Apply(stat);
  Real sdev = stat.GetStandardDeviation();
  Real value_count = Real(data.GetSize().GetVol());
  qual_ = (total_diff_abs_sq==0.0) ? 1e10 : (sdev / sqrt(total_diff_abs_sq / value_count));
  qual2_ = (total_diff_rel_sq==0.0) ? 1e10 : (sdev / sqrt(total_diff_rel_sq / value_count));

  /*
    calculate standard deviation of function
  */
  sigamp_ = sqrt(total_diff_abs_sq/(value_count-1.0));
}

FuncSplitGauss2D FitSplitGauss2D::AsFunction() const
{
  return FuncSplitGauss2D(A_, Bx_, By_, C_, ux_, uy_, w_*180.0/M_PI, Px_, Py_,delta_);
}


std::ostream& operator<<(std::ostream& o, const alg::FitSplitGauss2D& f)
{
  o << (const alg::ParamsSplitGauss2D&)(f) << " chi=" << f.GetChi() << " gof=" << f.GetGOF() << " abs-qual=" << f.GetAbsQuality() << " rel-qual=" << f.GetRelQuality();
  return o;
}

}} // namespaces
