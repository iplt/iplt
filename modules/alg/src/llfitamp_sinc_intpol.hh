//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_LLFITAMP_SINC_INTPOL_HH
#define IPLT_EX_LLFITAMP_SINC_INTPOL_HH

#include "llfit_base.hh"

namespace iplt {  namespace alg {


/*
  A Fourier component at F(z*) can be seen as a sinc-interpolated
  composition from Fourier components at intervals of 1/(ND), where
  N is the number of sample points and D is the real-space pixel 
  sampling

  F(z*) = Sum[A_n Exp[i phi_n] sinc[z*-n/(ND),W],{n,-N/2,N/2}]

  The intensity is the squared amplitude, and is given by

  F(z*)^2 = Sum[A_n Cos[phi_n] sinc[z*-n/(ND),W]]^2 +
            Sum[A_n Sin[phi_n] sinc[z*-n/(ND),W]]^2

  for a given set of F(z*), this routine attempts to find a best guess
  for A_n (and phi_n).

  In the special case of hermitian symmetry, A_{n} == A_{-n} and
  phi_{n} == -phi_{-n}, the above expression simplifies to

  F(z*)^2 = [C0 + C1]^2 + [S0 + S1]^2
    
       C0 = A_0 Cos[phi_0] sinc[z*,W]
       C1 = Sum[A_n Cos[phi_n] (sinc[z*-n/(ND),W] + sinc[z*+n/(ND),W]),{n,1,N/2}]
       S0 = 0.0
       C1 = Sum[A_n Sin[phi_n] (sinc[z*-n/(ND),W] - sinc[z*+n/(ND),W]),{n,1,N/2}]

  and only N/2 complex values need to be determined

  As an extension, an exponential falloff can be added in the form of

  F(z*)'   = Exp[-B z*^2] F(z*)
  F(z*)'^2 = Exp[-2B z*^2] F(z*)^2

  In the implementations, the real and imaginary parts of the complex
  Fourier components are fitted, rather than the amplitude and phase. The
  conversion is simply given by R_n=A_n*cos(phi_n) and I_n = A_n*sin(phi_n)
*/

// full (non hermitian) sinc interpolated lattice line fitting
class DLLEXPORT_IPLT_ALG LLFitAmpSincIntpol: public LLFitBase {
 public:
  LLFitAmpSincIntpol(unsigned int count, Real sampling, guessmode gm=GUESSMODE_BINAVERAGE);

  virtual LLFitBasePtr Clone();
  virtual void Apply();

  // return icalc,sigicalc
  virtual std::pair<Real,Real> CalcIntensAt(Real zstar) const;

  virtual std::map<Real,Complex> GetComponents() const;

private:
  std::vector<Complex> sc_;
  std::vector<Complex> sv_; // sinc variance
};

// lattice line fitting with enforced hermitian symmetry
class DLLEXPORT_IPLT_ALG LLFitAmpSincIntpolHerm: public LLFitBase {
 public:
  LLFitAmpSincIntpolHerm(unsigned int count, Real sampling, guessmode gm=GUESSMODE_BINAVERAGE);

  virtual LLFitBasePtr Clone();
  virtual void Apply();

  virtual std::pair<Real,Real> CalcIntensAt(Real zstar) const;

  virtual std::map<Real,Complex> GetComponents() const;

private:
  std::vector<Complex> sc_; // sinc components
  std::vector<Complex> sv_; // sinc variance
};

}} // ns

#endif
