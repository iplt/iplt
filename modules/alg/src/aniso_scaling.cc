//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>

#include <ost/log.hh>

#include "aniso_scaling.hh"

namespace iplt {  namespace alg {

AnisoScaling::AnisoScaling():
  elist_(),
  max_iter_(200),
  ilim_(1e-8)
{}

void AnisoScaling::Add(Real x, Real y1, Real y2, Real w)
{
  FitEntry e={x,y1,y2,sqrt(w)}; // convert w to 1/sig
  elist_.push_back(e);
}

Real AnisoScaling::Estimate(Real y1, Real y2) const
{
  return A_ + B1_*y1*y1 + B2_*y2*y2 + B3_*y1*y2;
}

Real AnisoScaling::GetA() const {return A_;}
Real AnisoScaling::GetB1() const {return B1_;}
Real AnisoScaling::GetB2() const {return B2_;}
Real AnisoScaling::GetB3() const {return B3_;}


namespace {

template <bool b1, bool b2>
int ascale_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  AnisoScaling::FitEntryList& elist = *(reinterpret_cast<AnisoScaling::FitEntryList*>(params));

  Real A = gsl_vector_get(x,0);
  Real B1 = gsl_vector_get(x,1);
  Real B2 = gsl_vector_get(x,2);
  Real B3 = gsl_vector_get(x,3);

  int indx=0;
  for(AnisoScaling::FitEntryList::const_iterator it=elist.begin();it!=elist.end();++it) {
    if(b1) {
      Real fx = A+B1*it->y1*it->y1+B2*it->y2*it->y2+B3*it->y1*it->y2;
      gsl_vector_set(f,indx,(fx - it->x)*it->w);
    }
    if(b2) {
      Real der_A = 1.0;
      Real der_B1 = it->y1*it->y1;
      Real der_B2 = it->y2*it->y2;
      Real der_B3 = it->y1*it->y2;
      gsl_matrix_set(J, indx, 0, der_A*it->w);
      gsl_matrix_set(J, indx, 1, der_B1*it->w);
      gsl_matrix_set(J, indx, 2, der_B2*it->w);
      gsl_matrix_set(J, indx, 3, der_B3*it->w);
    }
    ++indx;
  }
  return GSL_SUCCESS;
}

static int ascale_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return ascale_tmpl<true,false>(x,params,f,0);
}
static int ascale_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return ascale_tmpl<false,true>(x,params,0,J);
}
  static int ascale_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return ascale_tmpl<true,true>(x,params,f,J);
}

}

void AnisoScaling::Apply()
{
  int paramN=4;
  int valueN=elist_.size();

  gsl_vector* X = gsl_vector_alloc(paramN);
  gsl_vector_set(X,0,0.0); // A
  gsl_vector_set(X,1,1.0); // B1
  gsl_vector_set(X,2,1.0); // B2
  gsl_vector_set(X,3,0.5); // B3

  gsl_multifit_function_fdf func;
  func.f = &ascale_f;
  func.df = &ascale_df;
  func.fdf = &ascale_fdf;
  func.n = valueN;
  func.p = paramN;
  func.params = &elist_;

  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;
  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, valueN, paramN);
  gsl_multifit_fdfsolver_set (solver, &func, X);

  int iter=0;
  int status=GSL_CONTINUE;
  // actual iterations
  while (status == GSL_CONTINUE && iter < max_iter_) {
    ++iter;

    gsl_multifit_fdfsolver_iterate (solver);
    status = gsl_multifit_test_delta (solver->dx, solver->x, ilim_, ilim_);
  } 

  if(iter==max_iter_) {
    LOG_DEBUG( "not converged after " << max_iter_ << " iterations" );
  } else {
    LOG_DEBUG( "converged after " << iter << " iterations" );
  }

  A_ = gsl_vector_get(solver->x,0);
  B1_ = gsl_vector_get(solver->x,1);
  B2_ = gsl_vector_get(solver->x,2);
  B3_ = gsl_vector_get(solver->x,3);

  LOG_DEBUG( "found A=" << A_ << "  B1=" << B1_ << "  B2=" << B2_ << "  B3=" << B3_ );

  gsl_multifit_fdfsolver_free (solver);

  gsl_vector_free(X);
}

}} // ns
