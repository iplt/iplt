//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  Authors: Ansgar Philippsen, Andreas Schenk
*/

#ifndef IPLT_EX_REFINE_LATTICE_H
#define IPLT_EX_REFINE_LATTICE_H

#include <iplt/lattice.hh>

#include "lattice_point.hh"

namespace iplt {  namespace alg {

//! Refine lattice based on existing lattice point list

/*!
  Uses the coordinate offsets of each lattice point
  to refine the lattice using a least square minimization:

  For each lattice point, the integer indices u and v are known, as
  well as the fractional position on the image. The sought lattice,
  defined by the vectors P,Q,O, a barrel distortion constant kb, and 
  a spiral distortion ks gives the solution to each point as follows:


  L_ix = (u_i * Px + v_i * Qx)
         * ( 1 + kb * | (u_i * P + v_i * Q) | ^2)
	 + (u_i * Py + v_i * Qy)
	 * | (u_i * P + v_i * Q) |^2 * ks
	 + O

  L_iy = (u_i * Py + v_i * Qy)
         * ( 1 + kb * | (u_i * P + v_i * Q) | ^2)
	 - (u_i * Px + v_i * Qx) 
	 * | (u_i * P + v_i * Q) |^2 * ks 
	 + O

  If the distortion can be ignored, ie by forcing kb and ks to zero, then
  this equation simplifies to one with only linear terms, and a  linear 
  least squares procdure is utilized. Otherwise, a non-linear least squares
  procedure must be employed.
*/
DLLEXPORT_IPLT_ALG Lattice RefineLattice(const LatticePointList& lpl, bool fit_distortion=true, bool fit_origin=true);

}} // ns

#endif
