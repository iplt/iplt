//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/* 
   Authors: Ansgar Philippsen, Andreas Schenk
*/

#ifndef IPLT_EX_FOURIER_MASK_HH
#define IPLT_EX_FOURIER_MASK_HH

/*
  lattice search in fourier space

  masking of certain area around the latticepoints

  (a) remaining frequencies set to zero
  (b) assembly of new, condensed image
*/

#include <ost/img/algorithm.hh>
#include <iplt/lattice.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

//! Mask out lattice in fourier space
class DLLEXPORT_IPLT_ALG FourierMask: public ModOPAlgorithm {
public:
  FourierMask(const Lattice& lat, const ost::img::Extent&subext, Real hw);

  virtual ost::img::ImageHandle Visit(const ost::img::ConstImageHandle& ih);
private:
  Lattice lattice_;
  ost::img::Extent subext_;
  Real B_;
};


}} // ns


#endif
