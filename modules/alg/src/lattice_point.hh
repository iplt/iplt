//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_LATTICE_POINT_H
#define IPLT_EX_LATTICE_POINT_H

/*
  lattice points and organization of them into an associative list

  Author: Ansgar Philippsen
*/

#include <map>

#include <boost/shared_ptr.hpp>

#include <ost/img/image_handle.hh>
#include <ost/img/point.hh>
#include <ost/geom/geom.hh>
#include <ost/img/alg/stat.hh>
#include <iplt/alg/fit_gauss2d.hh>

#include <iplt/lattice.hh>

#include "peak_integration.hh"

namespace iplt {  

// fw decl
class ReflectionList;

namespace alg {

using ::ost::img::alg::Stat;
using ::iplt::alg::FitGauss2D;

//! Encapsulate various aspects of a lattice point
/*!
  This class allows storage of the various parameters that
  are connected to a particular lattice point:

  - position as geom::Vec2, signifies exact location on image
  - gaussfit object (set e.g. by lattice extract)
  - the region of the original image (as ost::img::Extent)
  - stat object about the region
  - peak integration object

  Note that the index is not stored here, as this is only 
  applicable together with a Lattice, such as the case for
  LatticePointProxyter in connection with a LatticePointList;

*/
class DLLEXPORT_IPLT_ALG LatticePoint {
public:
  LatticePoint(const geom::Vec2& pos=Vec2(),
	       const ost::img::Extent&region=Extent(),
	       const Stat& stat=Stat(),
	       const FitGauss2D& fit=FitGauss2D(),
	       const PeakIntegration& pi=PeakIntegration(),
	       Real ic_bg=0.0,
	       Real ave_bg=0.0,Real ave_sigbg=0.0);

  geom::Vec2 GetPosition() const;
  void SetPosition(const geom::Vec2& p);

  FitGauss2D GetFit() const;
  void SetFit(const FitGauss2D& fit);

  ost::img::Extent GetRegion() const;
  void SetRegion(const ost::img::Extent&h);

  Stat GetStat() const;
  void SetStat(const Stat& stat);

  PeakIntegration GetPI() const;
  void SetPI(const PeakIntegration& pi);

  Real GetICBG() const;
  void SetICBG(Real icbg);

  Real GetAveBG() const;
  void SetAveBG(Real abg);

  Real GetAveSigBG() const;
  void SetAveSigBG(Real asbg);

  void CalcAveBG(bool smart);

  // allow implicit conversion to point
  operator ost::img::Point() const;

  long MemSize() const;

private:
  geom::Vec2 pos_;
  ost::img::Extent reg_;
  Stat stat_;
  FitGauss2D fit_;
  PeakIntegration pi_;
  Real ic_bg_;
  Real ave_bg_;
  Real ave_sigbg_;
};


/*
  Detail namespace for lattice related things. Considered
  part of the private interface.
*/
namespace lattice_detail {
  typedef std::map<Point, LatticePoint> LMap;
  typedef LMap::iterator LMapIterator;
  typedef boost::shared_ptr<LMap> LMapPtr;
}

// fw decl
class LatticePointList;

//! Provides iteration over lattice points as well as access to them
class DLLEXPORT_IPLT_ALG LatticePointProxyter {
  // for access to ctor
  friend class LatticePointList;
public:
  // copy ctor and assignement op defaults should work

  // index for this point
  ost::img::Point GetIndex() const;

  // actual lattice point
  LatticePoint LPoint() const;

  LatticePoint& LPoint();

  bool AtEnd() const;

  bool IsValid() const;

  // prefix increment
  LatticePointProxyter& operator++();

  // postfix increment
  LatticePointProxyter operator++(int);

protected:
  //! Initialization of an empty, ie invalid proxyter
  LatticePointProxyter();

  //! Initialization 
  /*!
    Requires lattice map shared ptr and lattice map iterator
  */
  LatticePointProxyter(const lattice_detail::LMapPtr& lmap, 
		       const lattice_detail::LMapIterator& it);

private:
  lattice_detail::LMapPtr lmap_;
  lattice_detail::LMapIterator lit_;
};

/*
  The LatticePointList associates indices with lattice points.
  It encapsulates the overall results accumulated through
  conserted LatticeSearch, LatticeRefine, LatticeExtract, etc actions
*/
class DLLEXPORT_IPLT_ALG LatticePointList {
public:
  //! Initialization requires a lattice
  LatticePointList(const Lattice& l);

  LatticePointList(const LatticePointList& l);

  LatticePointList& operator=(const LatticePointList& l);

  //! Return lattice of this list
  Lattice GetLattice() const;

  void SetLattice(const Lattice& lat);

  //! Add new lattice point at specified index
  LatticePointProxyter Add(const ost::img::Point& index, const LatticePoint& p=LatticePoint());

  //! Return first lattice point
  LatticePointProxyter Begin() const;

  //! Find proxyter
  /*!
    If the lattice point with the given index does
    not exist, an invalid proxyter is returned (see
    LatticePointProxyter::IsValid)
  */
  LatticePointProxyter Find(const ost::img::Point& index);

  //! Remove all entries
  void Clear();

  //! Return number of entries
  int NumEntries() const;
  
  // re-index
  // ??

  long MemSize() const;

private:
  Lattice lat_;
  lattice_detail::LMapPtr lmap_;
};

//! Generate lattice point list based on given lattice and extent
DLLEXPORT_IPLT_ALG LatticePointList PredictLatticePoints(const Lattice& l, const ost::img::Extent&e);

//! Convert lattice point list to reflection list
DLLEXPORT_IPLT_ALG ReflectionList ConvertToReflectionList(const LatticePointList& l);

//! Convert reflection list to lattice point list
DLLEXPORT_IPLT_ALG LatticePointList ConvertToLatticePointList(const ReflectionList& l);

}} // ns

#endif

