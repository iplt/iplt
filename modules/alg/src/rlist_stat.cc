
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#include <vector>
#include <algorithm>
#include <limits>
#include <ost/img/point_list.hh>
#include "rlist_stat.hh"

namespace iplt {  namespace alg{

RListStat::RListStat():
  ReflectionNonModAlgorithm(),
  min_h_(std::numeric_limits<int>::max()),
  max_h_(std::numeric_limits<int>::min()),
  min_k_(std::numeric_limits<int>::max()),
  max_k_(std::numeric_limits<int>::min()),
  mean_h_(0.0),
  mean_k_(0.0),
  min_zstar_(std::numeric_limits<int>::max()),
  max_zstar_(-std::numeric_limits<int>::max()),
  mean_zstar_(0.0),
  indexes_()
{
}

RListStat::~RListStat()
{
}

void RListStat::Visit(const ReflectionList& in)
{
	if(in.NumReflections()==0)
		throw(ReflectionException("no reflections found"));
	std::vector<String> proplist=in.GetPropertyList();
	colmin_.clear();
	colmax_.clear();
	colmean_.clear();
	indexes_.clear();
  min_h_=std::numeric_limits<int>::max();
  max_h_=std::numeric_limits<int>::min();
  min_k_=std::numeric_limits<int>::max();
  max_k_=std::numeric_limits<int>::min();
  mean_h_=0.0;
  mean_k_=0.0;
  min_zstar_=std::numeric_limits<int>::max();
  max_zstar_=-std::numeric_limits<int>::max();
  mean_zstar_=0.0;
	for(std::vector<String>::const_iterator it=proplist.begin(); it!=proplist.end();++it){
		colmin_[*it]=std::numeric_limits<int>::max();
		colmax_[*it]=-std::numeric_limits<int>::max();
		colmean_[*it]=0.0;
	}
	for(ReflectionProxyter rp=in.Begin();!rp.AtEnd();++rp) {
		for(std::vector<String>::const_iterator it=proplist.begin(); it!=proplist.end();++it){
			colmin_[*it]=std::min(colmin_[*it],rp.Get(*it));
			colmax_[*it]=std::max(colmax_[*it],rp.Get(*it));
			colmean_[*it]+=rp.Get(*it);
		}
		ReflectionIndex idx=rp.GetIndex();
		min_h_=std::min<int>(min_h_,idx.GetH());
		max_h_=std::max<int>(max_h_,idx.GetH());
		mean_h_+=idx.GetH();
		min_k_=std::min<int>(min_k_,idx.GetK());
		max_k_=std::max<int>(max_k_,idx.GetK());
		mean_k_+=idx.GetK();
		min_zstar_=std::min<Real>(min_zstar_,idx.GetZStar());
		max_zstar_=std::max<Real>(max_zstar_,idx.GetZStar());
		mean_zstar_+=idx.GetZStar();
		indexes_.insert(idx.AsDuplet());
	}
	for(std::vector<String>::const_iterator it=proplist.begin(); it!=proplist.end();++it){
		colmean_[*it]/=in.NumReflections();
	}
	mean_h_/=in.NumReflections();
	mean_k_/=in.NumReflections();
	mean_zstar_/=in.NumReflections();
}
int RListStat::GetMinimumH()
{
	return min_h_;
}
int RListStat::GetMinimumK()
{
	return min_k_;
}
int RListStat::GetMaximumH()
{
	return max_h_;
}
int RListStat::GetMaximumK()
{
	return max_k_;
}
Real RListStat::GetMinimumZStar()
{
	return min_zstar_;
}
Real RListStat::GetMaximumZStar()
{
	return max_zstar_;
}
Real RListStat::GetMeanH()
{
	return mean_h_;
}
Real RListStat::GetMeanK()
{
	return mean_k_;
}
Real RListStat::GetMeanZStar()
{
	return mean_zstar_;
}
Real RListStat::GetMinimum(String name)
{
	if(colmin_.find(name)==colmin_.end())
		throw(ReflectionException("column type "+ name + " not found."));
	return colmin_[name];
}
Real RListStat::GetMaximum(String name)
{
	if(colmax_.find(name)==colmax_.end())
		throw(ReflectionException("column type "+ name + " not found."));
	return colmax_[name];
}
Real RListStat::GetMean(String name)
{
	if(colmean_.find(name)==colmean_.end())
		throw(ReflectionException("column type "+ name + " not found."));
	return colmean_[name];
}

PointList RListStat::GetIndexList()
{
  // convert to ost::img::PointList to avoid Python wrapper for PointSet
  ost::img::PointList result;
  for(PointSet::const_iterator it=indexes_.begin();it!=indexes_.end();++it){
    result.push_back(*it);
  }
  return result;
}


}} //ns
