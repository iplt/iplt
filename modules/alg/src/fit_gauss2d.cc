//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Gauss2D fitting

  Author: Ansgar Philippsen
*/

#include <iostream>

#include <boost/shared_ptr.hpp>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>

#include <ost/img/image.hh>
#include <ost/message.hh>
#include <ost/img/alg/stat.hh>

#include "fit_gauss2d.hh"
#include "gauss2d.hh"


namespace iplt {  namespace alg {

FitGauss2D::FitGauss2D(double A, double Bx, double By, double C, double ux, double uy, double w, double Px, double Py):
  NonModAlgorithm("FitGauss2D"),
  ParamsGauss2D(A,Bx,By,C,ux,uy,w,Px,Py),
  max_iter_(200),
  chi_(0.0), gof_(0.0), qual_(0.0), qual2_(0.0),sigamp_(0.0),
  sigma_(1.0),
  lim1_(1e-20),lim2_(1e-20),
  itcount_(0)
{
  //if(A<=0.0) throw FitGauss2DError("Parameter A must be larger than zero");
  //if(Bx<=0.0) throw FitGauss2DError("Parameter Bx must be larger than zero");
  //if(By<=0.0) throw FitGauss2DError("Parameter By must be larger than zero");
}

namespace {

struct fit_gauss2d_params { 
  const ost::img::Data& data;
  ost::img::Extent ext;
  ost::img::Point offset;
  double sigma;
};


/*
  provide function value and derivative values at the same time
*/
template <bool b1, bool b2>
int gauss2d_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  fit_gauss2d_params* prm = reinterpret_cast<fit_gauss2d_params*>(params);
  
  // retrieve paramers
  double A =  gsl_vector_get(x, FitGauss2D::ID_A);
  double Bx = gsl_vector_get(x, FitGauss2D::ID_BX);
  double By = gsl_vector_get(x, FitGauss2D::ID_BY);
  double ux = gsl_vector_get(x, FitGauss2D::ID_UX);
  double uy = gsl_vector_get(x, FitGauss2D::ID_UY);
  double C  = gsl_vector_get(x, FitGauss2D::ID_C);
  double w =  gsl_vector_get(x, FitGauss2D::ID_W);
  double Px = gsl_vector_get(x, FitGauss2D::ID_PX);
  double Py = gsl_vector_get(x, FitGauss2D::ID_PY);

  // derived values
  double sinw = sin(w);
  double cosw = cos(w);
  double Bx2 = Bx*Bx;
  double iBx2 = 1.0/Bx2;
  double Bx3 = Bx2*Bx;
  double iBx3 = 1.0/Bx3;
  double By2 = By*By;
  double iBy2 = 1.0/By2;
  double By3 = By2*By;
  double iBy3 = 1.0/By3;

  double inv_sigma = 1.0/prm->sigma;
  
  ost::img::Extent ext = prm->ext;
  ost::img::Point offset = prm->offset;

  //int nx = ext.GetSize().GetWidth();
  //int ny = ext.GetSize().GetHeight();
  
  for(int xi=ext.GetStart()[0];xi<=ext.GetEnd()[0];++xi) {
    // TODO: pixel scaling
    double x = double(xi)-ux;
    
    for(int yi=ext.GetStart()[1];yi<=ext.GetEnd()[1];++yi) {
      // TODO: pixel scaling
      double y = double(yi)-uy;

      double X = x*cosw-y*sinw;
      double X2 = X*X;
      double Y = x*sinw+y*cosw;
      double Y2 = Y*Y;

      // actual model and derivatives
      double vex  = -X2*iBx2-Y2*iBy2;
      double evex = exp(vex);
      double f_xy = A*evex+C+Px*x+Py*y;

      int indx = ext.Point2Offset(ost::img::Point(xi,yi,0));

      if(b1) {
	// retrieve actual value from Data*
	gsl_vector_set(f, indx, (f_xy - prm->data.GetReal(ost::img::Point(xi,yi,0)+offset))*inv_sigma );
      }

      if(b2) {
	double der_A  = evex;
	double der_C  = 1.0;
	double tae    = 2.0*A*evex;
	double der_Bx = tae * X2 * iBx3;
	double der_By = tae * Y2 * iBy3;
	double der_Ux = tae * ( sinw*Y*iBy2 + cosw*X*iBx2 ) - Px;
	double der_Uy = tae * ( cosw*Y*iBy2 - sinw*X*iBx2 ) - Py;
	double tmpw1  = -Y * X * iBx2;
	double tmpw2  =  Y * X * iBy2;
	double der_w  = tae * ( - tmpw1 - tmpw2 );
	double der_Px = x;
	double der_Py = y;

	gsl_matrix_set(J, indx, FitGauss2D::ID_A,  der_A*inv_sigma);
	gsl_matrix_set(J, indx, FitGauss2D::ID_BX, der_Bx*inv_sigma);
	gsl_matrix_set(J, indx, FitGauss2D::ID_BY, der_By*inv_sigma);
	gsl_matrix_set(J, indx, FitGauss2D::ID_UX, der_Ux*inv_sigma);
	gsl_matrix_set(J, indx, FitGauss2D::ID_UY, der_Uy*inv_sigma);
	gsl_matrix_set(J, indx, FitGauss2D::ID_C,  der_C*inv_sigma);
	gsl_matrix_set(J, indx, FitGauss2D::ID_W,  der_w*inv_sigma);
	gsl_matrix_set(J, indx, FitGauss2D::ID_PX, der_Px*inv_sigma);
	gsl_matrix_set(J, indx, FitGauss2D::ID_PY, der_Py*inv_sigma);
      }
    }
  }
  return GSL_SUCCESS;
}

static int gauss2d_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return gauss2d_tmpl<true,false>(x,params,f,0);
}

static int gauss2d_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return gauss2d_tmpl<false,true>(x,params,0,J);
}

static int gauss2d_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return gauss2d_tmpl<true,true>(x,params,f,J);
}

// helper classes for exception safety

struct gsl_vector_deleter {
  void operator()(gsl_vector* p) {
    gsl_vector_free(p);
  }
};

typedef boost::shared_ptr<gsl_vector> gsl_vector_ptr;

}

/*
  fit given input data to a 2D gaussian

  monolithic code, aweful... but works
*/
void FitGauss2D::VisitData(const ost::img::Data& data)
{
  // number of parameters to fit
  int paramN=9;

  // number of values is the total number of image points in 2D plane
  int valueN=data.GetSize().GetSlab();

  ost::img::Extent cen_ext(data.GetSize());
  ost::img::Point offset(data.GetExtent().GetStart()-cen_ext.GetStart());

  // subtract offset from given center
  ux_ -= (double)offset[0];
  uy_ -= (double)offset[1];

  // initialize custom data to pass to fitting functions
  fit_gauss2d_params prm = {data, cen_ext, offset, sigma_};

  // vector holding parameter,
  //gsl_vector* X = gsl_vector_alloc(paramN);
  gsl_vector_ptr X(gsl_vector_alloc(paramN),gsl_vector_deleter());

  // initialize with stored values
  gsl_vector_set(X.get(),ID_A, A_);
  gsl_vector_set(X.get(),ID_BX,Bx_);
  gsl_vector_set(X.get(),ID_BY,By_);
  gsl_vector_set(X.get(),ID_UX,ux_);
  gsl_vector_set(X.get(),ID_UY,uy_);
  gsl_vector_set(X.get(),ID_C,C_);
  gsl_vector_set(X.get(),ID_W,w_);
  gsl_vector_set(X.get(),ID_PX,Px_);
  gsl_vector_set(X.get(),ID_PY,Py_);

  // prepare the functions
  gsl_multifit_function_fdf func;
  func.f = &gauss2d_f;
  func.df = &gauss2d_df;
  func.fdf = &gauss2d_fdf;
  func.n = valueN;
  func.p = paramN;
  func.params = &prm;

  // levenberg-marquardt solver
  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;
  //cerr << "creating the solver for " << valueN << " data points and " << paramN << " parameters" << endl;
  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, valueN, paramN);
  gsl_multifit_fdfsolver_set (solver, &func, X.get());

  //cerr << "commencing iterations" << endl;
  int iter=0;
  int status=GSL_CONTINUE;
  // actual iterations
  while (status == GSL_CONTINUE && iter < max_iter_) {
    ++iter;

    gsl_multifit_fdfsolver_iterate (solver);

    // ensure that Bx is >=0
    gsl_vector_set(solver->x, ID_BX, 
		   std::abs(gsl_vector_get(solver->x, ID_BX)));
    // ensure that By is >=0
    gsl_vector_set(solver->x, ID_BY, 
		   std::abs(gsl_vector_get(solver->x, ID_BY)));
    // ensure that omega is in 0..2pi range
    gsl_vector_set(solver->x, ID_W, 
		   std::fmod(gsl_vector_get(solver->x, ID_W),2.0*M_PI));

    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1_, lim2_);
  } 
  itcount_ = iter;
  //cerr << "done with iterations (" << iter << "): " << gsl_strerror (status) << endl;

     
  // write fitted values back
  A_  = gsl_vector_get(solver->x, ID_A);
  Bx_ = gsl_vector_get(solver->x, ID_BX);
  By_ = gsl_vector_get(solver->x, ID_BY);
  ux_ = gsl_vector_get(solver->x, ID_UX);
  uy_ = gsl_vector_get(solver->x, ID_UY);
  C_  = gsl_vector_get(solver->x, ID_C);
  w_  = gsl_vector_get(solver->x, ID_W);
  Px_ = gsl_vector_get(solver->x, ID_PX);
  Py_ = gsl_vector_get(solver->x, ID_PY);

  // add offset back to center
  ux_ += (double)offset[0];
  uy_ += (double)offset[1];

  /*
    \chi and the resulting goodness of fit
  */
  chi_ = gsl_blas_dnrm2(solver->f);
  gof_ = (chi_*chi_)/ (double)(valueN - paramN);
  
  // clean up
  gsl_multifit_fdfsolver_free (solver);

  // handled by shared_ptr
  //gsl_vector_free(X);


  /*
    Calculate quality - alternative goodness of fit

    The ratio between (a) the average deviation (squared sum of squared differences)
    of the fit with the real data and (b) the standard deviation squared of the
    actual data.

    Rational: The variance within the fit should be better than the variance
    within the overall data.

    NOTE: this is just \chi, normalized by the number of points, using
    the standard deviation in the actual data as an error estimate.

    qual^2 = 1/N \sum{ (y_i - f(x_i;a))^2 / stdev^2 }

    NOTE2: it is not clear if this is useful at all!
  */
  FuncGauss2D ff = this->AsFunction();

  double total_diff_abs_sq=0.0;
  double total_diff_rel_sq=0.0;
  for(ost::img::ExtentIterator it(data.GetExtent()); !it.AtEnd(); ++it) {
    double data_val = data.GetReal(it);
    double diff_abs = ff.GetReal(it)-data_val;
    double diff_rel = diff_abs/(data_val-ff.GetBackground(geom::Vec2(((ost::img::Point)it).ToVec2())));
    total_diff_abs_sq += diff_abs*diff_abs;
    total_diff_rel_sq += diff_rel*diff_rel;
  }

  ost::img::alg::Stat stat;
  data.Apply(stat);
  double sdev = stat.GetStandardDeviation();
  double value_count = double(data.GetSize().GetVol());
  qual_ = (total_diff_abs_sq==0.0) ? 1e10 : (sdev / sqrt(total_diff_abs_sq / value_count));
  qual2_ = (total_diff_rel_sq==0.0) ? 1e10 : (sdev / sqrt(total_diff_rel_sq / value_count));

  /*
    calculate standard deviation of function
  */
  sigamp_ = sqrt(total_diff_abs_sq/(value_count-1.0));
}

FuncGauss2D FitGauss2D::AsFunction() const
{
  return FuncGauss2D(A_, Bx_, By_, C_, ux_, uy_, w_*180.0/M_PI, Px_, Py_);
}



std::ostream& operator<<(std::ostream& o, const FitGauss2D& f)
{
  o << (const alg::ParamsGauss2D&)(f) << " chi=" << f.GetChi() << " gof=" << f.GetGOF() 
    << " abs-qual=" << f.GetAbsQuality() << " rel-qual=" << f.GetRelQuality()
    << " iterations="<<f.GetIterationCount()<<" max_iterations="<<f.GetMaxIter();
  return o;
}

}} // namespaces
