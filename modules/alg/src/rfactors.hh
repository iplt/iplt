//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_ALG_RFACTORS_HH
#define IPLT_EX_ALG_RFACTORS_HH

/*
  various rfactor calculator classes for convenience and speed

  Author: Ansgar Philippsen

  These are the standard x-ray crystallography definitions

  Normal Rfactor:

    Sum_h[||Fobs(h)| - k |Fcalc(h)||] / Sum_h[|Fobs(h)|]

  Rsym:

    Sum_h Sum_i [| I_i(h)-<I_i(h)> |] / Sum_h Sum_i [| I_i(h) |]

    where <I_i(h)> is the average intensity of the same observations
    of a given reflection, i.e. the inner sum_i loops over all symmetry
    related intensities, and the outer sum_h loops over all independent
    reflections

  Rmerge:

    Sum_h Sum_j [| I(h) - I(h,j) |] / Sum_h[J * I(h)]

    where I(h) is the final value of the intensity, and I(h,j) the
    intensity at h from each dataset j (of which there are J)


  For the case of a merged dataset from different crystals, the above
  I(h) for the Rmerge only makes sense when having a value from a 
  lattice-line fit. Prior to that, in order to assess the deviation of
  a single image from the lattice-line 'point-cloud', a local average
  withing a small z* window of the same duplet (h,k) is formed.
  This results in the following definition for each image i:

    Sum_h [| I_i(h) - <Iz>(h) |] / Sum_h[| I_i(h) |]

  where <Iz> is the average of all intensities in the merge at the
  given (h,k) duplet _and_ whose z* fall into a range of +-zlim of
  the target reflection's z*.


  The weighted version of the Rfactors contains a weight in front
  of each abs, e.g.

    Sum_h [ w(h) | I_i(h) - <wIz>(h) |] / Sum_h[ w(h) | I_i(h) |]

    w(h) := 1.0/sigI(h)^2
    and <wIz> is meant to indicate a weighted average as well

  
*/

#include <vector>
#include <map>

#include <ost/base.hh>
#include <iplt/reflection_list.hh>
#include <iplt/symmetry.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

namespace detail {
  struct RMergeEntry {Real zstar,iobs,sigiobs,we;};
  typedef std::vector<RMergeEntry> RMergeEntryList;
  typedef std::map<Point,RMergeEntryList > RMergeEntryMap;

  struct RFitEntry {Real zmin,zmax,zfactor; std::vector<std::pair<Real,Real> > ilist;};
  typedef std::map<Point,RFitEntry> RFitEntryMap;
}


// pure virtual base class
class DLLEXPORT_IPLT_ALG RCalculatorBase 
{
 public:
  // initialize with limit on z and resolution binning values
  RCalculatorBase(Real zlim,Real rlow, Real rhigh, int bcount);

  virtual ~RCalculatorBase();

  virtual ReflectionList Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs);

  Real GetRF() const;
  Real GetWRF() const;
  int GetRCount() const;

  Real GetRFBin(int n) const;
  Real GetWRFBin(int n) const;
  int GetRCountBin(int n) const;

  Real GetBinLowerResolutionLimit(int n) const;

protected:
  Real zlim_;
  int bcount_;
  Real rlow_,rhigh_;
  Real ir2_low_, ir2_high_, ir2_delta_;
  Real rf_;
  Real wrf_;
  int rcount_;
  std::vector<Real> rfbin_;
  std::vector<Real> wrfbin_;
  std::vector<int> rcount_bin_;
};

// Rmerge calculation based on local lattice line binning of a reference
class DLLEXPORT_IPLT_ALG RMergeCalculator: public RCalculatorBase 
{
public:
  // initialize with merge rlist and limit on z
  RMergeCalculator(const ReflectionList& merge, const String& miobs, const String& msigiobs,Real zlim,Real rlow, Real rhigh, int bcount);

  virtual ReflectionList Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs);

  void SetSigmaRejectionLevel(Real r);

private:
  ReflectionList merge_;
  String miobs_,msigiobs_;
  detail::RMergeEntryMap entry_map_;
  Real rej_level_;

  template <bool BIN>
  ReflectionList do_apply(const ReflectionList& data, const String& diobs, const String&dsigiobs);
};

// Rsym calculation
class DLLEXPORT_IPLT_ALG RSymCalculator: public RCalculatorBase
{
 public:
  RSymCalculator(const Symmetry& sym, Real zlim,Real rlow, Real rhigh, int bcount);

  virtual ReflectionList Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs);
 private:
  Symmetry sym_;
};

// Rfactor calculation with a lattice line fit as a reference
class DLLEXPORT_IPLT_ALG RFitCalculator: public RCalculatorBase 
{
public:
  // initialize with llfit curve rlist
  /*
    This assumes per lattice line hk a number of finely sampled points at equidistant z*.
    min, max and stepsize can be different for each lattice line.
  */
  RFitCalculator(const ReflectionList& curve, const String& miobs, const String& msigiobs, Real rlow, Real rhigh, int bcount);

  virtual ReflectionList Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs);

private:
  detail::RFitEntryMap entry_map_;
};

// Rfactor calculation with icalc values per reflection
class DLLEXPORT_IPLT_ALG RCalcCalculator: public RCalculatorBase 
{
public:
  RCalcCalculator(const String& micalc, const String& msigicalc, Real rlow, Real rhigh, int bcount);

  virtual ReflectionList Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs);

private:
  String micalc_,msigicalc_;
};



}} // ns


#endif
