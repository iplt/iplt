//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <ost/units.hh>
#include "expand.hh"

namespace iplt {  namespace alg {

ReflectionList Expand::Visit(const ReflectionList& r) const
{
  Symmetry symmetry=r.GetSymmetry();
  if(explicit_symmetry_){
    symmetry=symmetry_;
  }
 	ReflectionList result;
 	result.CopyProperties(r);
  result.SetSymmetry(Symmetry("P 1"));
	if(r.HasPropertyType(iplt::PT_PHASE)){
	  String phaid=r.GetPropertyNameByType(iplt::PT_PHASE);
    for(ReflectionProxyter it=r.Begin();!it.AtEnd();++it){
        SymmetryReflectionList srlist=symmetry.Expand(SymmetryReflection(it.GetIndex(),1.0,it.Get(phaid)),hermitian_);
        for(SymmetryReflectionList::const_iterator srit=srlist.begin();srit!=srlist.end();++srit){
          ReflectionProxyter rptmp=result.AddReflection(srit->GetReflectionIndex(),it);
          rptmp.Set(phaid,srit->GetPhase());
        }
    }
	}else{
    for(ReflectionProxyter it=r.Begin();!it.AtEnd();++it){
          SymmetryReflectionList srlist=symmetry.Expand(SymmetryReflection(it.GetIndex(),1.0,0.0),hermitian_);
          for(SymmetryReflectionList::const_iterator srit=srlist.begin();srit!=srlist.end();++srit){
            result.AddReflection(srit->GetReflectionIndex(),it);
          }
    }
	}
 	return result;
}

}} // ns
