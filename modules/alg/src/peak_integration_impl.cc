//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include "peak_integration_impl.hh"

#ifndef NO_EXPL_INST
template class ost::img::ImageStateNonModAlgorithm<iplt::alg::detail::SimplePeakIntegrationBase>;
template class ost::img::ImageStateNonModAlgorithm<iplt::alg::detail::G2PeakIntegrationBase>;
#endif

namespace iplt {  namespace alg {namespace detail {

// peak integration base

PeakIntegrationBase::PeakIntegrationBase(const ost::img::Extent& peak_region):
  peak_region_(peak_region)
{
  reset();
}

void PeakIntegrationBase::reset()
{
  peak_sum_=0.0;
  peak_volume_=0.0;
  peak_sigma_=0.0;
  peak_count_=0;
  bg_mean_=0.0;
  bg_sigma_=0.0;
  bg_count_=0;
}

// simple peak integration

template <typename T, class D>
void SimplePeakIntegrationBase::VisitState(const ost::img::ImageStateImpl<T,D>& isi)
{
  reset();

  // assemble inner and outer statistics
  Real inner_sum=0.0;
  Real inner_sum2=0.0;
  int inner_count=0;
  Real outer_sum=0.0;
  Real outer_sum2=0.0;
  int outer_count=0;
  
  for(ost::img::ExtentIterator it(isi.GetExtent());!it.AtEnd();++it) {
    Real val=ost::img::Val2Val<T,Real>(isi.Value(it));
    if(peak_region_.Contains(it)) {
      inner_sum+=val;
      inner_sum2+=val*val;
      ++inner_count;
    } else {
      outer_sum+=val;
      outer_sum2+=val*val;
      ++outer_count;
    }
  }
  Real inner_fcount=static_cast<Real>(inner_count);
  
  Real outer_fcount=static_cast<Real>(outer_count);
  Real outer_mean=outer_sum/outer_fcount;
  Real outer_var=outer_sum2/outer_fcount-outer_mean*outer_mean;
  Real outer_sig=sqrt(outer_var);
  
  // now convert the simple statistics to relevant values
  bg_mean_=outer_mean;
  peak_volume_=inner_sum-bg_mean_*inner_fcount;
  peak_count_=inner_count;
  bg_sigma_=outer_sig;
  bg_count_=outer_count;
  /*
    error propagation comes from the conceptual formula
      \sum[Pi-Bg]
    where each Pi and Bg have SigBg as their error, so
    the inner difference has sqrt(2)*SigBg as its error,
    and the overall sum has sqrt(2N)*SigBg
  */
  peak_sigma_=bg_sigma_*sqrt(inner_fcount*2.0);
  peak_sum_=inner_sum;
}

// G2 noise model

template <typename T, class D>
void G2PeakIntegrationBase::VisitState(const ost::img::ImageStateImpl<T,D>& isi)
{
  reset();

  Real inner_sum=0.0;
  Real inner_sum2=0.0;
  int inner_count=0;
  Real outer_sum=0.0;
  Real outer_sum2=0.0;
  int outer_count=0;
  
  for(ost::img::ExtentIterator it(isi.GetExtent());!it.AtEnd();++it) {
    Real val=ost::img::Val2Val<T,Real>(isi.Value(it));
    if(peak_region_.Contains(it)) {
      inner_sum+=val;
      inner_sum2+=val*val;
      ++inner_count;
    } else {
      outer_sum+=val;
      outer_sum2+=val*val;
      ++outer_count;
    }
  }

  /*
    calculate the ( f(mu,sigma) )^2 statistics for the background
  */
  Real outer_fcount=static_cast<Real>(outer_count);
  Real outer_mean=outer_sum/outer_fcount;
  Real outer_var=outer_sum2/outer_fcount-outer_mean*outer_mean;

  Real tmp1=(outer_mean*outer_mean-0.5*outer_var);
  if(tmp1<0.0) throw ost::Error("internal error #1 in G2PeakIntegration");
  tmp1=sqrt(tmp1);
  Real outer_mu = sqrt(tmp1);
  Real outer_sig2=outer_mean - tmp1;

  if(outer_sig2<0.0) throw ost::Error("internal error #3 in G2PeakIntegration");

  /*
    and now the same thing for the peak region
  */
  Real inner_fcount=static_cast<Real>(inner_count);
  Real inner_mean=inner_sum/inner_fcount;
  Real inner_var=inner_sum2/inner_fcount-inner_mean*inner_mean;

  Real tmp2=(inner_mean*inner_mean-0.5*inner_var);
  if(tmp2<0.0) throw ost::Error("internal error #2 in G2PeakIntegration");
  tmp2=sqrt(tmp2);
  Real inner_mu = sqrt(tmp2);
  //Real inner_sig2 = inner_mean - tmp2;

  // output intensity, hence squared versions of above derived values

  bg_mean_=outer_mu*outer_mu;
  bg_sigma_=outer_sig2;
  bg_count_=outer_count;

  // the 'true' signal above the background
  Real amp_mean = inner_mu-outer_mu;
  // (Sum[amp])^2
  peak_volume_=amp_mean*amp_mean*inner_fcount;

  LOG_DEBUG( "im: " << inner_mu << " om: " << outer_mu << " d: " << amp_mean << " v: " << peak_volume_ );

  /*
    the inner_sig depends on the actual peak shape, and is
    therefore not suited as a signal sigma estimation
  */
  peak_sigma_=outer_sig2*sqrt(inner_fcount);

  peak_count_=inner_count;
  peak_sum_=inner_sum;

}

}}}
