//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  extract gaussian fit values from image based on 2D lattice

  Author: Ansgar Philippsen
*/

#include <cstdlib>
#include <iostream>
#include <sstream>

#include <ost/log.hh>

#include <ost/info/info.hh>

#include "lattice_gaussian_extract.hh"

namespace iplt {  namespace alg {

using ::iplt::alg::FitGauss2DError;

LatticeGaussianExtract::LatticeGaussianExtract(const Lattice& lat,unsigned  int box_size,unsigned int maxiter,Real abslim,Real rellim):
  NonModAlgorithm("LatticeGaussianExtract"),
  lattice_(lat),
  box_size_(box_size),
  maxiter_(maxiter),
  abslim_(abslim),
  rellim_(rellim),
  lpl_tmpl_(lattice_),
  lpl_(lattice_),
  corr_img_(CreateImage(ost::img::Size(1)))
{}

LatticeGaussianExtract::LatticeGaussianExtract(const LatticePointList& lpl,unsigned  int box_size,unsigned int maxiter,Real abslim,Real rellim):
  NonModAlgorithm("LatticeGaussianExtract"),
  lattice_(lpl.GetLattice()),
  box_size_(box_size),
  maxiter_(maxiter),
  abslim_(abslim),
  rellim_(rellim),
  lpl_tmpl_(lattice_),
  lpl_(lattice_),
  corr_img_(CreateImage(ost::img::Size(1)))
{
  for(LatticePointProxyter lpp=lpl.Begin();!lpp.AtEnd();++lpp) {
    lpl_tmpl_.Add(lpp.GetIndex(),lpp.LPoint());
  }
}

void LatticeGaussianExtract::Visit(const ost::img::Function& fnc)
{
  ost::img::ImageHandle tmp = GenerateImage(fnc);
  Visit(tmp);
}

void LatticeGaussianExtract::Visit(const ost::img::ConstImageHandle& im)
{
  if(lpl_tmpl_.NumEntries()==0) {
    LOG_DEBUG( "creating predicted lattice point list" );
    lpl_ = PredictLatticePoints(lattice_, im.GetExtent());
  } else {
    LOG_DEBUG( "using template lattice point list" );
    lpl_ = lpl_tmpl_;
  }

  LOG_DEBUG( "reseting corrected image" );
  corr_img_.Reset(im.GetExtent(),ost::img::REAL,ost::img::SPATIAL);

  /* 
     iterate over all predicted points, fit them, and assign
     resulting params
  */
  LOG_DEBUG( "commencing lpl iteration" );
  ost::img::ImageHandle subimg;
  for(LatticePointProxyter lpp=lpl_.Begin(); !lpp.AtEnd(); ++lpp) {
    try {
      ost::img::Point index(lpp.GetIndex());
      LatticePoint& lp=lpp.LPoint();

      geom::Vec2 pos(lp.GetPosition());
      ost::img::Point ipos(pos);
      LOG_DEBUG( "fitting " << ipos << " i=(" << index[0]<< "," << index[1]<< ") " );

      ost::img::Point sub_offset(box_size_,box_size_);
      ost::img::Extent sub_ext(ipos-sub_offset, ipos+sub_offset);
      lp.SetRegion(sub_ext);
      subimg = im.Extract(sub_ext);
      // store origin for later offset calculation
      ost::img::Point origin = subimg.GetSpatialOrigin();
      // move spatial origin to center for proper background plane calculation
      subimg.CenterSpatialOrigin();

      alg::Stat stat;
      subimg.Apply(stat);
      lp.SetStat(stat);
      LOG_DEBUG( "stat: " << stat );
      
      Real b0 = Real(box_size_)/sqrt(log(2.0));
      Real A = stat.GetMaximum()-stat.GetMinimum();
      if(A<=0.0) {
	LOG_DEBUG( "ignored due to: gf A <= 0.0" );
	continue;
      }

      // run gaussian fit on reflection
      alg::FitGauss2D gauss2d_fit(A, // A
				  b0, // Bx
				  b0, // By
				  stat.GetMinimum(), // C
				  stat.GetMaximumPosition()[0], // Ux
				  stat.GetMaximumPosition()[1], // Uy
				  0.0 // w
				  );
      gauss2d_fit.SetMaxIter(maxiter_);  
      gauss2d_fit.SetLim(abslim_,rellim_);
      gauss2d_fit.SetSigma(stat.GetStandardDeviation() );
      LOG_DEBUG( "pre: " << gauss2d_fit );
      subimg.Apply(gauss2d_fit);
      LOG_DEBUG( "fit: " << gauss2d_fit );
      // correct for spatial origin
      gauss2d_fit.SetU(gauss2d_fit.GetU()+Point(origin-subimg.GetSpatialOrigin()).ToVec2());
      LOG_DEBUG( "corrected offset to " << gauss2d_fit.GetU() );

      lp.SetFit(gauss2d_fit);
    } catch (FitGauss2DError& e) {
      LOG_DEBUG( "ignored due to: " << e.what() );
    }
  }
}

void LatticeGaussianExtract::SetLattice(const Lattice& l)
{
  lattice_=l;
}

const Lattice& LatticeGaussianExtract::GetLattice() const
{
  return lattice_;
}
  
void LatticeGaussianExtract::SetBoxSize(unsigned int sz)
{
  box_size_=sz;
}

unsigned int LatticeGaussianExtract::GetBoxSize() const
{
  return box_size_;
}

void LatticeGaussianExtract::SetMaxIter(unsigned int val)
{
 maxiter_=val;  
}

unsigned int LatticeGaussianExtract::GetMaxIter() const
{
  return maxiter_;
}

void LatticeGaussianExtract::SetLim(Real lim1, Real lim2)
{
 abslim_=lim1;
 rellim_=lim2;
}

LatticePointList LatticeGaussianExtract::GetList() const
{
  return lpl_;
}

ImageHandle LatticeGaussianExtract::GetCorrectedImage() const
{
  return corr_img_;
}

void UpdateFromInfo(LatticeGaussianExtract& le, const ost::info::InfoGroup& g)
{
  int box_size=le.GetBoxSize();
  if(g.HasItem("BoxSize")) {
    box_size=g.GetItem("BoxSize").AsInt();
  }
  if(g.HasItem("BoxGrowAmount")) {
    box_size=box_size+g.GetItem("BoxGrowAmount").AsInt();
  }
  if(g.HasItem("BackgroundRimSize")) {
    box_size=box_size+g.GetItem("BackgroundRimSize").AsInt();
  } else {
    box_size+=1;
  }

  LOG_VERBOSE( "setting LatticeGaussianExtract box size to " << box_size );
  le.SetBoxSize(box_size);
    
}


}} // ns

