//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <iostream>
#include <cmath>

#include <ost/log.hh>

#include "weighted_bin.hh"

namespace iplt {  namespace alg {

WeightedBin::WeightedBin():
  bincount_(1),
  lim_low_(1.0),
  lim_hi_(0.5),
  binsize_(0.5),
  bins_(bincount_)
{}

WeightedBin::WeightedBin(unsigned int bincount, Real limlow, Real limhi):
  bincount_(bincount),
  lim_low_(limlow),
  lim_hi_(limhi),
  binsize_((lim_hi_-lim_low_)/static_cast<Real>(bincount_)),
  bins_(bincount_)
{}

void WeightedBin::Add(Real x, Real y, Real weight)
{
  if(weight<=0.0) {
    LOG_DEBUG( "wbin: ignored weight<0" );
    return;
  }
  if(x>=lim_low_ && x<lim_hi_) {
    int indx=static_cast<int>(std::floor((x-lim_low_)/binsize_));
    bins_[indx].num+=1;
    bins_[indx].sum+=y*weight;
    bins_[indx].sum2+=y*y*weight;
    bins_[indx].weightsum+=weight;
    bins_[indx].iweightsum+=1.0/weight;
    //LOG_TRACE( "adding " << y << " (" << weight << ") at " << x << " to bin " << indx );
  } else {
    //LOG_TRACE( "ignoring " << y << " at " << x );
  }
}

int WeightedBin::CalcBin(Real x) const
{
  if(x>=lim_low_ && x<lim_hi_) {
    return static_cast<int>(std::floor((x-lim_low_)/binsize_));
  } else {
    return -1;
  }
}


Real WeightedBin::GetAverage(unsigned int n) const 
{
  if (n>=bincount_ || bins_[n].weightsum==0.0)return 0.0;
  return bins_[n].sum/bins_[n].weightsum;
}

Real WeightedBin::GetStdDev(unsigned int n) const
{
  if (n>=bincount_ || bins_[n].weightsum==0.0)return 0.0;
  Real average=GetAverage(n);
  Real var=bins_[n].sum2/bins_[n].weightsum-average*average;
  return sqrt(var);
}

Real WeightedBin::GetWeightAverage(unsigned int n) const
{
  if (n>=bincount_ || bins_[n].weightsum==0.0)return 0.0;
  return 1.0/bins_[n].iweightsum;
}

int WeightedBin::GetSize(unsigned int n) const
{
  if (n>=bincount_)return 0;
  return bins_[n].num;
}


int WeightedBin::GetBinCount() const
{
  return bincount_;
}

Real WeightedBin::GetLimit(unsigned int n) const
{
  return (static_cast<Real>(n)+0.5)*binsize_+lim_low_;
}

Real WeightedBin::GetLowerLimit(unsigned int n) const
{
  return static_cast<Real>(n)*binsize_+lim_low_;
}

Real WeightedBin::GetUpperLimit(unsigned int n) const
{
  return static_cast<Real>(n+1)*binsize_+lim_low_;
}

}} // ns
