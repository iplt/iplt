//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <ost/log.hh>

#include "tilter.hh"

namespace iplt {  namespace alg {

Tilter::Tilter():
  ReflectionConstModOPAlgorithm(),
  explicit_tilt_(false),
  tilt_geometry_()
{}

Tilter::Tilter(const LatticeTiltGeometry& tg):
  ReflectionConstModOPAlgorithm(),
  explicit_tilt_(true),
  tilt_geometry_(tg)
{}

ReflectionList Tilter::Visit(const ReflectionList& rlist) const
{
  // this is the assumed error in the lattice components
  static Real delta_lat_step=0.01;
  typedef std::vector<std::pair<LatticeTiltGeometry,Lattice> > TGList;
  
  // the actual tilt geometry may differ from the one derived by
  // the unit cell and the lattice
  ReciprocalUnitCell ruc(rlist.GetUnitCell());
  Lattice lat=rlist.GetLattice();
  LatticeTiltGeometry ltg;
  if(explicit_tilt_){
    ltg=tilt_geometry_;
  }else{
    ltg=CalcTiltFromReciprocalLattice(lat,ruc);
  }
  
  // some pre-calculations for the sigma zstar estimation later on
  TGList tg_list;
  for(int dax=-1;dax<=1;dax++) {
    for(int day=-1;day<=1;day++) {
      geom::Vec2 tmp_lat_a = lat.GetFirst()+delta_lat_step*Vec2(static_cast<Real>(dax),static_cast<Real>(day));
      for(int dbx=-1;dbx<=1;dbx++) {
        for(int dby=-1;dby<=1;dby++) {
          geom::Vec2 tmp_lat_b = lat.GetSecond()+delta_lat_step*Vec2(static_cast<Real>(dbx),static_cast<Real>(dby));
          Lattice tmp_lat(tmp_lat_a,tmp_lat_b);
          tg_list.push_back(std::make_pair(CalcTiltFromReciprocalLattice(tmp_lat,ruc),tmp_lat));
        }
      }
    }
  }


  ReflectionList result(rlist,false);
  result.AddProperty("sigmazstar");

  for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp) {
    ReflectionIndex idx = rp.GetIndex();
    Real zstar = ltg.GetZStar(ost::img::Point(idx.GetH(),idx.GetK()));
    idx = ReflectionIndex(idx.GetH(),idx.GetK(),zstar);
    ReflectionProxyter rp2 = result.AddReflection(idx,rp);

    // sigma zstar estimation
    Real sum1=0.0;
    Real sum2=0.0;
    for(TGList::const_iterator it=tg_list.begin();it!=tg_list.end();++it) {
      Real tmp_zstar = it->first.GetZStar(ost::img::Point(idx.GetH(),idx.GetK()));
      sum1+=tmp_zstar;
      sum2+=tmp_zstar*tmp_zstar;
    }
    Real iN = 1.0/static_cast<Real>(tg_list.size());
    Real sigmaz2 = sum2*iN-sum1*iN*sum1*iN;
    if(sigmaz2>0.0) {
      rp2.Set("sigmazstar",sqrt(sigmaz2));
    } else {
      rp2.Set("sigmazstar",0.0);
    }
  }
  return result;
}

}} // ns
