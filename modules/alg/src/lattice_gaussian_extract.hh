//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  extract gaussian fit values from image based on 2D lattice

  Authors Ansgar Philippsen
*/

#ifndef IPLT_ALG_LATTICE_GAUSSIAN_EXTRACT_HH
#define IPLT_ALG_LATTICE_GAUSSIAN_EXTRACT_HH

#include <ost/img/image.hh>
#include <ost/img/algorithm.hh>

#include <ost/info/info_fw.hh>

#include <ost/img/alg/stat.hh>
#include <iplt/alg/peak_search.hh>

#include <iplt/lattice.hh>

#include "lattice_point.hh"

namespace iplt {  namespace alg {

//! Lattice Gaussian Fit Extract
/*!
  Use a given lattice to perform 2D gaussian fit on each
  predicted lattice position
*/
class DLLEXPORT_IPLT_ALG LatticeGaussianExtract: public NonModAlgorithm {
public:
  //! initialize with lattice and fit radius
  LatticeGaussianExtract(const Lattice& lat,unsigned  int box_size,unsigned int maxiter=100,Real abslim=1.0e-12,Real rellim=1.0e-12);

  //! initialize with lattice point list and fit radius
  /*
    the given lattice point list serves as a template
   */
  LatticeGaussianExtract(const LatticePointList& lpl,unsigned  int box_size,unsigned int maxiter=100,Real abslim=1.0e-12,Real rellim=1.0e-12);

  //! set new lattice to use
  void SetLattice(const Lattice& l);
  //! retrieve current lattice
  const Lattice& GetLattice() const;
  
  //! set box size to use
  void SetBoxSize(unsigned int sz);
  //! retrieve current box size
  unsigned int GetBoxSize() const;

  //! set maximum iteration count
  void SetMaxIter(unsigned int val);
  //! retrieve maximum iteration count
  unsigned int GetMaxIter() const;

  void SetLim(Real lim1, Real lim2);

  //! Get resulting lattice point list
  LatticePointList GetList() const;

  //! algorithm interface
  void Visit(const ost::img::ConstImageHandle& img);
  //! algorithm interface
  void Visit(const ost::img::Function& fnc);

  //! debug interface
  ost::img::ImageHandle GetCorrectedImage() const;

private:
  Lattice lattice_;
  unsigned int box_size_;
  unsigned int maxiter_;
  Real abslim_;
  Real rellim_;
  LatticePointList lpl_tmpl_;

  LatticePointList lpl_;

  ost::img::ImageHandle corr_img_;
};

//! Set lattice extract parameters based on info group
/*!
  The parameters from the given LatticeGaussianExtract object are
  modified according to the given InfoGroup instance. The resulting 
  LatticeGaussianExtract object is returned.

  Note that only the parameters that are actually present in
  the info group are used, allowing cumulative use of this
  function.
*/
DLLEXPORT_IPLT_ALG void UpdateFromInfo(LatticeGaussianExtract& le, const ost::info::InfoGroup& g);

}} // ns

#endif
