//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <iostream>

#include <fftw3.h>
#include <boost/shared_ptr.hpp>
#include <ost/img/alg/fftw_helper.hh>

#include "llpredict.hh"
#include "llpredict_impl.hh"

namespace iplt {  namespace alg { namespace llpredict {

//declaration
template<typename V, class D>
ost::img::ImageStateBasePtr make_ref(const ost::img::ImageStateImpl<V,D>& isi);

//specialization
template<>
ost::img::ImageStateBasePtr make_ref(const ost::img::RealSpatialImageState& isi)
{
  // create empty result
  ost::img::Extent res_ext(isi.GetSize());
  boost::shared_ptr<ost::img::ComplexSpatialImageState> result (new ost::img::ComplexSpatialImageState(res_ext, ost::img::PixelSampling()));

  // make copy of input for in-place transform
  for(unsigned int i=0;i<isi.GetSize().GetVolume();++i) {
    result->Value(i)=isi.Value(i);
  }

  // setup fft in x and y
  int rank = 2; // 2D transformation 
  int tsize[] = {isi.GetSize().GetWidth(),isi.GetSize().GetHeight()};
  int howmany = isi.GetSize().GetDepth();
  int stride = isi.GetSize().GetDepth();
  int dist = 1; // 

  OST_FFTW_fftw_complex* data = reinterpret_cast<OST_FFTW_fftw_complex*>(result->Data().GetData());

  OST_FFTW_fftw_plan plan = OST_FFTW_fftw_plan_many_dft(rank, tsize, howmany,
				      data, NULL,stride,dist, // input
				      data, NULL,stride,dist, // output
				      FFTW_FORWARD,FFTW_ESTIMATE);
  OST_FFTW_fftw_execute(plan);
  OST_FFTW_fftw_destroy_plan(plan);

  ost::img::Point spatial_origin=isi.GetSpatialOrigin();
  result->AdjustPhaseOrigin(ost::img::Point(spatial_origin[0],spatial_origin[1],0));
  return result;
}

// generic case: throw exception
template<typename V, class D>
ost::img::ImageStateBasePtr make_ref(const ost::img::ImageStateImpl<V,D>& isi)
{
  throw LLPredictException("reference not real and/or not in spatial domain");
}

template<typename V, class D>
ost::img::ImageStateBasePtr MakeRefFnc::VisitState(const ost::img::ImageStateImpl<V,D>& isi)
{
  // call specialized code
  return make_ref(isi);
}

} // ns



}} // ns

template class ost::img::ImageStateModOPAlgorithm<iplt::alg::llpredict::MakeRefFnc>;
