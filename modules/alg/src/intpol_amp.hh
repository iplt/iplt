//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

// DEPRECATED

#ifndef IPLT_EX_INTPOL_AMP_H
#define IPLT_EX_INTPOL_AMP_H

#include <map>
#include <list>

#include <ost/img/point.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  

// fw decl
class ReflectionList;

namespace alg {

class DLLEXPORT_IPLT_ALG IntpolAmp {
private:
  struct HKEntry {
    HKEntry():
      h(0), k(0)
    {}
    HKEntry(const ost::img::Point& p):
      h(p[0]),k(p[1])
    {}
    bool operator<(const HKEntry& e) const {
      return h==e.h ? k<e.k : h<e.h;
    }
    int h,k;
  };
  struct LEntry {
    LEntry():
      l(0.0), sigma_l(1.0), a(0.0), sigma_a(1.0)
    {}
    LEntry(Real l_, Real sl_, Real a_, Real sa_):
      l(l_), sigma_l(sl_), a(a_), sigma_a(sa_)
    {}
    bool operator<(const LEntry& e) const {
      return l<e.l;
    }
    Real l, sigma_l;
    Real a, sigma_a;
  };
  typedef std::list<LEntry> LList;
  typedef std::map<HKEntry,LList> HKList;

public:
  /*
    Initialize with a reflection list, the name of four columns:
     - fractional l values 
     - fractional l values error estimation
     - value to be interpolated
     - value error estimation
    and the fractional window width to use for fitting
  */
  IntpolAmp(const ReflectionList& l, 
	    const String& frac_l, 
	    const String& frac_l_ee,
	    const String& val,
	    const String& val_ee,
	    Real window);

  // Set width of window to be used for the interpolation
  void SetWindow(Real w) {window_=w;}

  // Retrieve an interpolated value at a given h,k and fractional l
  Real GetInterpolatedValue(const ost::img::Point& hk, Real frac_l);

  void Dump() const;

private:
  HKList hklist_;
  Real window_;
};

}} // ns

#endif
