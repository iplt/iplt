//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <iostream>

#include <ost/log.hh>

#include <gsl/gsl_multifit.h>

#include "llfitamp_sinc_intpol_sq.hh"

namespace iplt {  namespace alg {

LLFitAmpSincIntpolSQ::LLFitAmpSincIntpolSQ(unsigned int count, Real sampling, guessmode gm):
  LLFitBase(count,sampling,gm),
  sc_(),
  fit_X_(NULL),fit_V_(NULL)
{}

LLFitAmpSincIntpolSQ::~LLFitAmpSincIntpolSQ()
{
  if(fit_X_) gsl_vector_free(fit_X_);
  if(fit_V_) gsl_matrix_free(fit_V_);
}
LLFitBasePtr LLFitAmpSincIntpolSQ::Clone()
{
    return LLFitBasePtr(new LLFitAmpSincIntpolSQ(*this));
}

void LLFitAmpSincIntpolSQ::Apply()
{
  // observations
  int N = intens_list_.size();
  // parameters in the model
  int P = 2*count_;

  // sinc matrix
  gsl_matrix* M = gsl_matrix_alloc(N,P);
  // observations and sigma vectors
  gsl_vector* B = gsl_vector_alloc(N);
  gsl_vector* S = gsl_vector_alloc(N);
  // solution vector
  gsl_vector* X = gsl_vector_alloc(P);
  // covariance matrix
  gsl_matrix* V = gsl_matrix_alloc(P,P);

  int poff = count_ - 1;
  Real W=static_cast<Real>(P)*sampling_;
  Real iND = 1.0/W;
  Real bfac2 = bfac_*bfac_;

  for(int n=0;n<N;++n) {
    gsl_vector_set(B,n,intens_list_[n].intens);
    gsl_vector_set(S,n,intens_list_[n].sig_intens);
    Real zstar=intens_list_[n].zstar;
    for(int p=0;p<P;++p) {
      Real pp = static_cast<Real>(p-poff)*iND;
      gsl_matrix_set(M,n,p,Sinc(zstar-pp,W,bfac2));
    }
  }
  
  gsl_multifit_linear_workspace* WORK= gsl_multifit_linear_alloc(N,P);

  double tol=1.0e-5;
  size_t rank;
  double chisq;

  gsl_multifit_wlinear_svd(M, S, B, tol, &rank, X, V, &chisq, WORK);
  LOG_VERBOSE( "svd fit returned rank="<< rank <<" and chisq=" << chisq );

  sc_.resize(P);
  for(int p=0;p<P;++p) {
    sc_[p]=gsl_vector_get(X,p);
  }

  gsl_matrix_free(M);
  gsl_vector_free(B);
  gsl_vector_free(S);
  if(fit_X_) gsl_vector_free(fit_X_);
  fit_X_=X;
  if(fit_V_) gsl_matrix_free(fit_V_);
  fit_V_=V;

  gsl_multifit_linear_free(WORK);
}

std::pair<Real,Real> LLFitAmpSincIntpolSQ::CalcIntensAt(Real zstar) const
{
  int P = 2*count_;
  int poff = count_ - 1;
  Real W=static_cast<Real>(P)*sampling_;
  Real iND = 1.0/W;
  Real bfac2=bfac_*bfac_;

  gsl_vector* S = gsl_vector_alloc(P);
  for(int p=0;p<P;++p) {
    Real pp = static_cast<Real>(p-poff)*iND;
    gsl_vector_set(S,p,Sinc(zstar-pp,W,bfac2));
  }

  double y,sigy;
  gsl_multifit_linear_est (S, fit_X_, fit_V_, &y,&sigy);

  gsl_vector_free(S);

  return std::make_pair(y,sigy);
}

std::map<Real,Complex> LLFitAmpSincIntpolSQ::GetComponents() const
{
  Real stepsize=1.0/(sampling_*count_)*0.5;
  int offset=-static_cast<int>(sc_.size())/2;
  std::map<Real,Complex> result;
  for(unsigned int i=0;i<sc_.size();++i){
    result[stepsize*(i+offset)]=sc_[i];
  }
  return result;
}


}} // ns
