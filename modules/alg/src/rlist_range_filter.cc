
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#include "rlist_range_filter.hh"

namespace iplt {  namespace alg{

RListRangeFilter::RListRangeFilter():
  ReflectionConstModOPAlgorithm(),
  ranges_()
{
}

RListRangeFilter::~RListRangeFilter()
{
}

RListRangeFilter::RListRangeFilter(const ReflectionIndex& start,const ReflectionIndex& end):
  ReflectionConstModOPAlgorithm(),
  ranges_()
{
  AddRange(start,end);
}

void RListRangeFilter::AddRange(const ReflectionIndex& start,const ReflectionIndex& end)
{
  ranges_.push_back(ReflectionIndexRange(start,end));
}

void RListRangeFilter::ClearRanges()
{
  ranges_.clear();
}

ReflectionList RListRangeFilter::Visit(const ReflectionList& in) const
{
  ReflectionList result(in,false);
  for(ReflectionProxyter rp=in.Begin();!rp.AtEnd();++rp) {
    ReflectionIndex index=rp.GetIndex();
    for(ReflectionIndexRangeList::const_iterator it=ranges_.begin();it!=ranges_.end();++it){
      if(index.GetH()>=it->first.GetH()         && index.GetH()<=it->second.GetH() && 
         index.GetK()>=it->first.GetK()         && index.GetK()<=it->second.GetK() &&
         index.GetZStar()>=it->first.GetZStar() && index.GetZStar()<=it->second.GetZStar()){
        result.AddReflection(index,rp);
      }
    }
  }
  return result;
}


}} //ns
