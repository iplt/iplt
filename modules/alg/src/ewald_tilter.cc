//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <ost/geom/geom.hh>
#include <iplt/tilt.hh>
#include <ost/log.hh>

#include "ewald_tilter.hh"

namespace iplt {  namespace alg {

EwaldTilter::EwaldTilter(Real lambda):
  ReflectionConstModOPAlgorithm(),
  lambda_(lambda)
{}

ReflectionList EwaldTilter::Visit(const ReflectionList& rlist) const
{
  Lattice lat = rlist.GetLattice();
  LOG_DEBUG( lat );
  ReciprocalUnitCell ruc = rlist.GetUnitCell();
  LOG_DEBUG( ruc );
  LatticeTiltGeometry tg=CalcTiltFromReciprocalLattice(lat,ruc);
  LOG_DEBUG( tg );
  geom::Vec2 avec=Rotate(geom::Vec2(ruc.GetA(),0.0),tg.GetAStarVectorAngle()-tg.GetXAxisAngle());
  LOG_DEBUG( avec );
  geom::Vec2 bvec=Rotate(geom::Vec2(ruc.GetB()*cos(ruc.GetGamma()),ruc.GetB()*sin(ruc.GetGamma())),tg.GetAStarVectorAngle()-tg.GetXAxisAngle());
  LOG_DEBUG( bvec );
  Real alpha=tg.GetTiltAngle();

  ReflectionList result(rlist,false);

  for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp) {
    ReflectionIndex idx = rp.GetIndex();

    geom::Vec2 hk=idx.GetH()*avec+idx.GetK()*bvec;
    LOG_DEBUG( "("<< idx.GetH() << ","<< idx.GetK()<< "):"<< "hkvec: " << hk );
    Real ang=SignedAngle(hk,Vec2(cos(tg.GetXAxisAngle()),sin(tg.GetXAxisAngle())));
    LOG_DEBUG( "("<< idx.GetH() << ","<< idx.GetK()<< "):"<< "angle: " << ang );
    Real hkxz=sin(ang)*Length(hk);
    LOG_DEBUG( "("<< idx.GetH() << ","<< idx.GetK()<< "):"<< "hkxz: " << hkxz );
    Real hky=cos(ang)*Length(hk);
    LOG_DEBUG( "("<< idx.GetH() << ","<< idx.GetK()<< "):"<< "hky: " << hky );
    Real lamstarinv=sqrt((1.0/lambda_)*(1.0/lambda_)+hky*hky);
    LOG_DEBUG( "("<< idx.GetH() << ","<< idx.GetK()<< "):"<< "lamstarinv: " << lamstarinv );
    Real theta=asin((1.0/lambda_*sin(alpha)+hkxz)/lamstarinv)-alpha;
    LOG_DEBUG( "("<< idx.GetH() << ","<< idx.GetK()<< "):"<< "theta: " << theta );
    geom::Vec2 vzstar=lamstarinv*Vec2(cos(theta),sin(theta))-Vec2(1.0/lambda_,0.0)-Vec2(sin(alpha),cos(alpha))*hkxz;
    LOG_DEBUG( "("<< idx.GetH() << ","<< idx.GetK()<< "):"<< "vzstar: " << vzstar );
    Real zstar=Length(vzstar)*(vzstar[0]>=0?1.0:-1.0);
    LOG_DEBUG( "("<< idx.GetH() << ","<< idx.GetK()<< "):"<< "zstar: " << zstar );

    idx = ReflectionIndex(idx.GetH(),idx.GetK(),zstar);
    ReflectionProxyter rp2 = result.AddReflection(idx,rp);

  }
  return result;
}

}} // ns
