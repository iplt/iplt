//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_PEAK_INT_H
#define IPLT_EX_PEAK_INT_H

/*
  integration of lattice peaks
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#include <iplt/alg/module_config.hh>
#include <ost/img/algorithm.hh>

namespace iplt {  namespace alg {

        class DLLEXPORT_IPLT_ALG PeakIntegration: public ost::img::NonModAlgorithm
{
public:
  PeakIntegration(int startsize=5,int endsize=6,int laxsize=2);

  // sum of peak integration
  Real GetVolume() const {return volume_;}
  void SetVolume(Real v) {volume_=v;}

  // sigma of background multiplied with number of integration pixels
  Real GetSigma() const {return sigma_;}
  void SetSigma(Real s) {sigma_=s;}

  // average of background pixels
  Real GetBackground() const {return background_;}
  void SetBackground(Real b) {background_=b;}

  Real GetBackgroundSigma() const {return background_sigma_;}
  void SetBackgroundSigma(Real b) {background_sigma_=b;}

  // <volume>/bg
  Real GetAverageRatio() const {return ave_ratio_;}
  void SetAverageRatio(Real r) {ave_ratio_=r;}

  Real GetQuality() const;

  //! returns GaussianP(i/sigi)
  Real GetWeight() const;

  int GetFinalSize() const {return final_size_;}

  int GetStartSize() const {return start_size_;}
  int GetEndSize() const {return end_size_;}

  void SetBackgroundRimSize(int bgs) {bg_border_=bgs;}

  ost::img::Size GetMaximalSize() const;

  void SetMethod(int m);

  virtual void Visit(const ost::img::ConstImageHandle& i);
  virtual void Visit(const ost::img::Function& f);

protected:

  int start_size_, end_size_, lax_size_;
  int bg_border_;
  Real volume_;
  Real sigma_;
  Real background_;
  Real background_sigma_;
  Real ave_ratio_;
  int final_size_;
  int method_;
};

DLLEXPORT_IPLT_ALG Real CalcPeakIntegrationWeight(Real isigi, Real s=1.0, Real o=1.0);

}} // ns

#endif
