//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>

#include <ost/log.hh>
#include <ost/message.hh>

#include "llfitamp_sinc_intpol.hh"

namespace iplt {  namespace alg {

LLFitAmpSincIntpol::LLFitAmpSincIntpol(unsigned int count, Real sampling, guessmode gm):
  LLFitBase(count,sampling,gm),
  sc_(),
  sv_()
{}

LLFitBasePtr LLFitAmpSincIntpol::Clone()
{
    return LLFitBasePtr(new LLFitAmpSincIntpol(*this));
}

namespace {

template <bool func, bool derivatives,bool even>
int T_funcSI(const gsl_vector* X, void* vparams, gsl_vector* f, gsl_matrix* J)
{
  detail::LLFitParams* params = reinterpret_cast<detail::LLFitParams*>(vparams);

  int noff = params->count/2;
  Real W=static_cast<Real>(params->count)*params->sampling;
  Real iND = 1.0/W;
  Real bfac2=params->bfac*params->bfac;

  std::vector<Real> sinc_term(params->count+1);

  int index=0;
  for(detail::IntensList::const_iterator it=params->intens.begin(); it!=params->intens.end();++it) {
    Real sum_re=0.0;
    Real sum_im=0.0;
    Real i_sigma = 1.0/it->sig_intens;
    for(int n=0;n<params->count;++n) {
      Real pn = static_cast<Real>(n-noff)*iND;

      Real Rn = gsl_vector_get(X,n*2+0);
      Real In = gsl_vector_get(X,n*2+1);

      sinc_term[n]=Sinc(it->zstar-pn,W,bfac2);
	
      sum_re+=Rn*sinc_term[n];
      sum_im+=In*sinc_term[n];
    }
    if(even){
      Real pn = static_cast<Real>(params->count-noff)*iND;

      Real Rn = gsl_vector_get(X,0*2+0);
      Real In = gsl_vector_get(X,0*2+1);

      sinc_term[params->count]=Sinc(it->zstar-pn,W,bfac2);

      sum_re+=Rn*sinc_term[params->count];
      sum_im+=In*sinc_term[params->count];
    }

    if(func) {
      Real fx = sum_re*sum_re+sum_im*sum_im;
      gsl_vector_set(f,index,(fx-it->intens)*i_sigma);
    }
    if(derivatives) {
      for(int c=1;c<params->count;++c) {
        Real der_Rn = 2.0*sinc_term[c]*sum_re;
        Real der_In = 2.0*sinc_term[c]*sum_im;

        gsl_matrix_set(J,index,c*2+0,der_Rn*i_sigma);
        gsl_matrix_set(J,index,c*2+1,der_In*i_sigma);
      }
      if(even){
        Real der_Rn = 2.0*(sinc_term[0]+sinc_term[params->count])*sum_re;
        Real der_In = 2.0*(sinc_term[0]+sinc_term[params->count])*sum_im;
        gsl_matrix_set(J,index,0*2+0,der_Rn*i_sigma);
        gsl_matrix_set(J,index,0*2+1,der_In*i_sigma);
      }else{
        Real der_Rn = 2.0*(sinc_term[0])*sum_re;
        Real der_In = 2.0*(sinc_term[0])*sum_im;
        gsl_matrix_set(J,index,0*2+0,der_Rn*i_sigma);
        gsl_matrix_set(J,index,0*2+1,der_In*i_sigma);
      }
    }

    ++index;
  }

  return GSL_SUCCESS;
}

static int SI_func_f_even(const gsl_vector* x, void* params, gsl_vector* f)
{
  return T_funcSI<true,false,true>(x,params,f,0);
}

static int SI_func_df_even(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return T_funcSI<false,true,true>(x,params, 0, J);
}

static int SI_func_fdf_even(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return T_funcSI<true,true,true>(x,params, f, J);
}

static int SI_func_f_odd(const gsl_vector* x, void* params, gsl_vector* f)
{
  return T_funcSI<true,false,false>(x,params,f,0);
}

static int SI_func_df_odd(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return T_funcSI<false,true,false>(x,params, 0, J);
}

static int SI_func_fdf_odd(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return T_funcSI<true,true,false>(x,params, f, J);
}

} // anon ns

void LLFitAmpSincIntpol::Apply()
{
  detail::LLFitParams params = {intens_list_,phase_list_,sampling_,count_};

  int paramN = count_*2;

  int valueN = intens_list_.size();

  if(valueN < paramN) {
      throw ost::Error("Error in LLFitAmpSincIntpol: dataN < paramN  ");
  }

  gsl_vector* X = gsl_vector_alloc(paramN);

  gsl_vector_set_all(X,1.0);

  gsl_multifit_function_fdf func;
  if(count_%2==0){
    func.f = &SI_func_f_even;
    func.df = &SI_func_df_even;
    func.fdf = &SI_func_fdf_even;
  }else{
    func.f = &SI_func_f_odd;
    func.df = &SI_func_df_odd;
    func.fdf = &SI_func_fdf_odd;
  }
  func.n = valueN;
  func.p = paramN;
  func.params = &params;

  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;

  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, valueN, paramN);
  gsl_multifit_fdfsolver_set (solver, &func, X);

  LOG_VERBOSE( "commencing iterations" );
  
  unsigned int iter=0;
  int status=GSL_CONTINUE;
  // actual iterations
  
  while (status == GSL_CONTINUE && iter < max_iter_) {
    ++iter;
    
    gsl_multifit_fdfsolver_iterate (solver);
    
    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1_, lim2_);
  } 
  
  LOG_VERBOSE( "done with iterations (" << iter << "): " << gsl_strerror (status) );
  
  iter_count_=iter;
  
  Real chi=gsl_blas_dnrm2(solver->f);
  LOG_VERBOSE( "chi = " << chi );
  LOG_VERBOSE( "chisq/dog = " << (chi*chi)/static_cast<Real>(valueN-paramN) );

  gsl_matrix* covar = gsl_matrix_alloc(paramN,paramN);
  gsl_multifit_covar(solver->J, 0.0, covar);

  sc_.clear();
  sv_.clear();

  for(unsigned int c=0;c<count_;++c) {
    Real Rn=gsl_vector_get(solver->x,c*2+0);
    Real In=gsl_vector_get(solver->x,c*2+1);
    Real sRn2=(gsl_matrix_get(covar,c*2+0,c*2+0));
    Real sIn2=(gsl_matrix_get(covar,c*2+1,c*2+1));
    sc_.push_back(Complex(Rn,In));
    sv_.push_back(Complex(sRn2,sIn2));
  } 

  gsl_matrix_free(covar);
  gsl_multifit_fdfsolver_free(solver);
  gsl_vector_free(X);
}

std::pair<Real,Real> LLFitAmpSincIntpol::CalcIntensAt(Real zstar) const
{
  Real w = static_cast<Real>(count_)*sampling_;
  Real iw = 1.0/w;
  Real sum_re=0.0;
  Real sum_im=0.0;
  Real sum_re_sig2=0.0;
  Real sum_im_sig2=0.0;
  int noff = static_cast<int>(sc_.size())/2;
  Real bfac2=bfac_*bfac_;
  for(int n=0;n<sc_.size();++n) {
    Real zsc = iw*static_cast<Real>(n-noff);
    Real snc = Sinc(zstar-zsc,w,bfac2);
    sum_re+=sc_[n].real()*snc;
    sum_im+=sc_[n].imag()*snc;
    sum_re_sig2+=sv_[n].real()*snc*snc;
    sum_im_sig2+=sv_[n].imag()*snc*snc;
  }
  if(count_%2==0){
    Real zsc = iw*static_cast<Real>(sc_.size()-noff);
    Real snc = Sinc(zstar-zsc,w,bfac2);
    sum_re+=sc_[0].real()*snc;
    sum_im+=sc_[0].imag()*snc;
    sum_re_sig2+=sv_[0].real()*snc*snc;
    sum_im_sig2+=sv_[0].imag()*snc*snc;
  }

  Real icalc = sum_re*sum_re+sum_im*sum_im;

  /*
    error propagation:
     the relative error is given by:
      
      sigicalc / icalc = sqrt [ (2 sig_sum_re/sum_re)^2 + (2 sig_sum_im/sum_im)^2 ]

     and for each sum, the error is the sqrt of the sum of the squared errors, hence
     the squared sigmas are stored, and this means

      sigicalc / icalc = 2 sqrt [ sig_sum_re2/(sum_re^2) + sig_sum_im2/(sum_im^2) ]

  */
  Real t_re = 0.0;
  Real t_im = 0.0;
  Real sum_re2 = sum_re*sum_re;
  Real sum_im2 = sum_im*sum_im;
  if(sum_re_sig2>0.0 && sum_re2>0.0) {
    t_re = sum_re_sig2/sum_re2;
  }
  if(sum_im_sig2>0.0 && sum_im2>0.0) {
    t_im = sum_im_sig2/sum_im2;
  }

  Real sigicalc = 2.0*icalc*sqrt(t_re+t_im);
  return std::make_pair(icalc,sigicalc);
}

std::map<Real,Complex> LLFitAmpSincIntpol::GetComponents() const
{
  Real stepsize=1.0/(sampling_*count_);
  int offset=-static_cast<int>(sc_.size())/2;
  std::map<Real,Complex> result;
  for(int i=0;i<sc_.size();++i){
    result[stepsize*(i+offset)]=sc_[i];
  }
  return result;
}

}} // ns
