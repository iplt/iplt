//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Valerio Mariani
*/

#include <gsl/gsl_fit.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_sf_gamma.h>
#include <ost/log.hh>
#include <ost/message.hh>

#include "linfit.hh"
#include "linfit_impl.hh"

#ifdef IPLT_GSL_COMPAT
#include <gsl/gsl_blas.h>
/*
  copied from GSL source code, required for some old linux distros
*/
extern "C" {
int gsl_multifit_linear_est (const gsl_vector * x,
			     const gsl_vector * c,
			     const gsl_matrix * cov, double *y, double *y_err)
{
  if (x->size != c->size) {
    GSL_ERROR ("number of parameters c does not match number of observations x",GSL_EBADLEN);
  } else if (cov->size1 != cov->size2) {
    GSL_ERROR ("covariance matrix is not square", GSL_ENOTSQR);
  } else if (c->size != cov->size1) {
    GSL_ERROR ("number of parameters c does not match size of covariance matrix cov",GSL_EBADLEN);
  } else {
    size_t i, j;
    double var = 0;
      
    gsl_blas_ddot(x, c, y);       /* y = x.c */
    
    /* var = x' cov x */
    
    for (i = 0; i < x->size; i++) {
      const double xi = gsl_vector_get (x, i);
      var += xi * xi * gsl_matrix_get (cov, i, i);

      for (j = 0; j < i; j++) {
	const double xj = gsl_vector_get (x, j);
	var += 2 * xi * xj * gsl_matrix_get (cov, i, j);
      }
    }
    
    *y_err = sqrt (var);
    
    return GSL_SUCCESS;
  }
}
} // extern "C"
#endif

namespace iplt {  namespace alg {

namespace {

void linfit_error_handler(const char* reason, const char* file, int line, int gsl_errno)
{
  std::ostringstream str;
  str << "A GSL error occured during linfit: " << reason << "; invoked from " << file << " line " << line;
  throw ost::Error(str.str());
}

}

/*
  From CPAN perl module Statistics::LineFit 
  by Richard Anderson
*/
double LinearFit::r_square () 
{
  int n=x_.size();
  double sx=0.0;
  double sy=0.0;
  double sx2=0.0;
  double sy2=0.0;
  double sxy=0.0;

  for (int index=0; index < n; ++index) {
    if (wx_flag_ == true) {
      sx += wx_[index] * x_[index];
      sx2 += wx_[index] * wx_[index] * x_[index] * x_[index];
      sxy += wx_[index] * x_[index] * wy_[index] * y_[index];
    } else {
      sx +=  x_[index];
      sx2 += x_[index] * x_[index];
      sxy += x_[index] * wy_[index] * y_[index];
    }
    sy += wy_[index] * y_[index];
    sy2 += wy_[index] * wy_[index] * y_[index] * y_[index];
  }

  double r_sq_num = (sxy - (sx * sy)/n) * (sxy - (sx * sy)/n);
  double r_sq_den = (sx2 - (sx*sx/n)) * (sy2 - (sy*sy/n));
  
  if (r_sq_den != 0) {
    return r_sq_num/r_sq_den;
  } else {
    return 1.0;
  }
}

LinearFit::LinearFit(): 
  x_(), y_(), wx_(), wy_(), 
  zo_flag_(false), wx_flag_(false),
  m_(1.0), b_(0.0), chi_(0.0), chi2_(0.0), q_(0.0),
  var_m_(1.0), var_b_(1.0), cov_(0.0)
{
}

void LinearFit::ForceOrigin(bool f)
{
  zo_flag_=f;
}

void LinearFit::AddDatapoint(double x, double y) 
{
  LOG_TRACE( "adding " << x << "," << y );
  x_.push_back(x);
  y_.push_back(y);
  wx_.push_back(1.0);
  wy_.push_back(1.0);
}

void LinearFit::AddDatapoint(double x, double y, double wy)
{
  if(wy>0.0) {
    LOG_TRACE( "adding " << x << "," << y << " (" << wy << ")" );
    x_.push_back(x);
    y_.push_back(y);
    wx_.push_back(1.0);
    wy_.push_back(wy);
  }
}

void LinearFit::AddDatapoint(double x, double wx, double y, double wy)
{
  if(wy>0.0 && wx>0.0) {
    LOG_TRACE( "adding " << x << " (" << wx << ")" << "," << y << " (" << wy << ")" );
    x_.push_back(x);
    y_.push_back(y);
    wx_.push_back(wx);
    wy_.push_back(wy);
    wx_flag_=true;
  }
}

std::pair<double,double> LinearFit::Apply()
{
  // install gsl error handler
  gsl_error_handler_t* old_handler = gsl_set_error_handler(linfit_error_handler);
  try {
    // both apply_* routines will set b_, m_, and chi2_
    if(wx_flag_) {
      apply_wxy();
    } else {
      apply_wy();
    }

    LOG_DEBUG( "fit result: m="<<m_ << " b="<<b_ );
    
    // derived values
    chi_ = std::sqrt(chi2_);
    q_= gsl_sf_gamma_inc_Q(0.5*static_cast<double>(x_.size()-2),chi2_*0.5);
    rsq_ = r_square();
    
    
    LOG_DEBUG( "  dof: " << 0.5*static_cast<double>(x_.size()-2) << " chi2:" << chi2_*0.5 << " Q:" << q_ ); 
  } catch (...) {
    gsl_set_error_handler(old_handler);
    throw;
  }

  gsl_set_error_handler(old_handler);
  return std::make_pair(m_,b_);
}

int LinearFit::PointCount() const 
{
  return x_.size();
}

void LinearFit::Clear()
{
  x_.clear();
  y_.clear();
  wx_.clear();
  wy_.clear();
}

double LinearFit::GetScale() const
{
  return m_;
}

double LinearFit::GetOffset() const 
{
  return b_;
}

double LinearFit::GetGOF() const 
{
  return chi_;
}

double LinearFit::GetWGOF() const
{
  return chi_/static_cast<double>(x_.size());
}

double LinearFit::GetQ() const
{
  return q_;
}

double LinearFit::GetRSQ() const
{
  return rsq_;
}

double LinearFit::GetScaleVariance() const
{
  return var_m_;
}

double LinearFit::GetOffsetVariance() const
{
  return var_b_;
}

std::pair<double,double> LinearFit::Estimate(double x) const
{
  double y=0.0;
  double yerr=0.0;
  
  if(zo_flag_) {
    gsl_fit_mul_est(x,m_,var_m_,&y,&yerr);
  } else {
    gsl_fit_linear_est(x, b_, m_, var_b_, var_m_, cov_,&y, &yerr);
  }
  
  return std::make_pair(y,yerr);
}

// private methods

void LinearFit::apply_wy()
{
  double c[6];
  if(x_.size()==0) {
    LOG_ERROR( "no points to fit" );
  } else {
    LOG_DEBUG( "wy fitting with " << x_.size() << " datapoints" );
    if(zo_flag_) {
      gsl_fit_wmul(&x_[0],1,
		   &wy_[0],1,
		   &y_[0],1,
		   x_.size(),
		   &c[0],&c[1],&c[2]);
      m_ = c[0];
      b_ = 0.0;
      chi2_ = c[2];
      var_m_ = c[1];
      var_b_ = 0.0;
      cov_ = 0.0;

    } else {
      gsl_fit_wlinear(&x_[0],1,
		      &wy_[0],1,
		      &y_[0],1,
		      x_.size(),
		      &c[0],&c[1],&c[2],&c[3],&c[4],&c[5]);
      m_ = c[1];
      b_ = c[0];
      chi2_ = c[5];
      var_b_ = c[2];
      cov_ = c[3];
      var_m_ = c[4];
    }
  }
}

void LinearFit::apply_wxy()
{
  double c[6];
  if(x_.size()==0) {
    LOG_ERROR( "no points to fit" );
  } else {
    LOG_DEBUG( "wxy fitting with " << x_.size() << " datapoints" );
    if(zo_flag_) {
      detail::fit_wxymul(&x_[0],1,
			 &wx_[0],1,
			 &y_[0],1,
			 &wy_[0],1,
			 x_.size(),
			 &c[0],&c[1],&c[2],&c[3]);
      m_ = c[0];
      b_ = 0.0;
      chi2_ = c[2];
      var_b_ = 0.0;
      cov_ = 0.0;
      var_m_ = c[1]*c[1];
    } else {
      detail::fit_wxylinear(&x_[0],1,
			    &wx_[0],1,
			    &y_[0],1,
			    &wy_[0],1,
			    x_.size(),
			    &c[0],&c[1],&c[2],&c[3],&c[4],&c[5]);

      b_ = c[0];
      m_=c[1];
      chi2_ = c[4];
      var_b_ = c[2]*c[2];
      cov_ = 0.0;
      var_m_ = c[3]*c[3];
    }
  }
}

// linfit 2d

LinearFit2D::LinearFit2D(): 
  x1_(), x2_(), y_(), wy_(), 
  zo_flag_(false),
  m1_(1.0), m2_(1.0), b_(0.0), chi_(0.0), chi2_(0.0),
  x12_(0),m12b_(0),cov_(0)
{
  x12_ = gsl_vector_alloc(3);
  m12b_ = gsl_vector_alloc(3);
  cov_ = gsl_matrix_alloc(3,3);
  ForceOrigin(false);
}

LinearFit2D::~LinearFit2D()
{
  gsl_vector_free(x12_);
  gsl_vector_free(m12b_);
  gsl_matrix_free(cov_);
}


void LinearFit2D::ForceOrigin(bool f)
{
  zo_flag_=f;
  int P = zo_flag_ ? 2 : 3;
  gsl_matrix_free(cov_);
  cov_ = gsl_matrix_alloc(P,P);
  gsl_vector_free(x12_);
  x12_ = gsl_vector_alloc(P);
  gsl_vector_free(m12b_);
  m12b_ = gsl_vector_alloc(P);
}

void LinearFit2D::AddDatapoint(double x1, double x2, double y) 
{
  x1_.push_back(x1);
  x2_.push_back(x2);
  y_.push_back(y);
  wy_.push_back(1.0);
}

void LinearFit2D::AddDatapoint(double x1, double x2, double y, double wy) 
{
  x1_.push_back(x1);
  x2_.push_back(x2);
  y_.push_back(y);
  wy_.push_back(wy);
}

void LinearFit2D::apply_wy()
{
  if(x1_.size()<=(zo_flag_ ? 0 : 1)) {
    LOG_ERROR( "not enough points to fit" );
  } else {
    LOG_DEBUG( "wy fitting with " << x1_.size() << " datapoints" );

    int P = zo_flag_ ? 2 : 3;

    gsl_multifit_linear_workspace *wsp = gsl_multifit_linear_alloc(x1_.size(),P);

    gsl_matrix* X = gsl_matrix_alloc(x1_.size(),P);
    gsl_vector* Y = gsl_vector_alloc(y_.size());
    gsl_vector* W = gsl_vector_alloc(wy_.size());
    gsl_vector* C = gsl_vector_alloc(P);
    for(unsigned int n=0;n<x1_.size();++n) {
      gsl_matrix_set(X,n,0,x1_[n]);
      gsl_matrix_set(X,n,1,x2_[n]);
      if(!zo_flag_) gsl_matrix_set(X,n,2,1.0);
      gsl_vector_set(Y,n,y_[n]);
      gsl_vector_set(W,n,wy_[n]);
    }

    gsl_multifit_wlinear(X,W,Y,C,cov_,&chi2_,wsp);
    chi_ = sqrt(chi2_);
    
    m1_=gsl_vector_get(C,0);
    m2_=gsl_vector_get(C,1);
    b_=zo_flag_ ? 0.0 : gsl_vector_get(C,2);

    gsl_vector_free(C);
    gsl_vector_free(W);
    gsl_vector_free(Y);
    gsl_matrix_free(X);

    gsl_multifit_linear_free(wsp);
  }
}

void LinearFit2D::Apply()
{
  // install gsl error handler
  //gsl_error_handler_t* old_handler = gsl_set_error_handler(linfit_error_handler);
  try {
    apply_wy();

  } catch (...) {
    //gsl_set_error_handler(old_handler);
    throw;
  }

  //gsl_set_error_handler(old_handler);
}
      
std::pair<double,double> LinearFit2D::Estimate(double x1, double x2) const
{
  gsl_vector_set(x12_,0,x1);
  gsl_vector_set(x12_,1,x2);
  if(!zo_flag_) gsl_vector_set(x12_,2,1.0);
  gsl_vector_set(m12b_,0,m1_);
  gsl_vector_set(m12b_,1,m2_);
  if(!zo_flag_) gsl_vector_set(m12b_,2,b_);

  double y,yerr;

  gsl_multifit_linear_est(x12_,m12b_,cov_,&y,&yerr);
  return std::make_pair(y,yerr);
}

int LinearFit2D::PointCount() const 
{
  return x1_.size();
}

void LinearFit2D::Clear()
{
  x1_.clear();
  x2_.clear();
  y_.clear();
  wy_.clear();
}

double LinearFit2D::GetScale1() const
{
  return m1_;
}

double LinearFit2D::GetScale2() const
{
  return m2_;
}

double LinearFit2D::GetOffset() const 
{
  return b_;
}

double LinearFit2D::GetGOF() const 
{
  return chi_;
}

}} // ns

