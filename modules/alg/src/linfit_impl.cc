//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Valerio Mariani
*/

#include <cmath>
#include <iostream>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_sf_gamma.h>

#include "linfit_params.hh"
#include "linfit_impl.hh"
#include "numerical.hh"

namespace iplt {  namespace alg { namespace detail {

double chirxy (double bang, void * chirxyparams)
{
  const double BIG =1.0e30;
  unsigned int nn;
  double b;
  double avex=0.0; 
  double avey=0.0;
  double sumw=0.0;
  double sx_j,sy_j,ww_j,xx_j,yy_j;
  unsigned int j;
  double ans;
  LinFitErrXYParams * params;  

  params = reinterpret_cast<LinFitErrXYParams*>(chirxyparams);
  nn=params->xx_p->size;
  b=tan(bang);
  for(j=0;j<nn;++j)
  {
    sx_j = gsl_vector_get(params->sx_p,j);
    sy_j = gsl_vector_get(params->sy_p,j);
    gsl_vector_set(params->ww_p,j,(b*sx_j*b*sx_j + sy_j*sy_j));
    ww_j = gsl_vector_get(params->ww_p,j);
    ww_j = (ww_j < (1.0 / BIG) ? BIG : (1.0/ww_j));
    gsl_vector_set(params->ww_p,j,ww_j);
    sumw += ww_j;
    xx_j=gsl_vector_get(params->xx_p,j);
    yy_j=gsl_vector_get(params->yy_p,j);
    avex += ww_j * xx_j;
    avey += ww_j * yy_j;
  }
  avex /= sumw;
  avey /= sumw;
  params->aa=avey-b*avex;
  for ( ans = -params->offs,j=0;j<nn;++j)
  {
    ww_j = gsl_vector_get(params->ww_p,j);
    xx_j = gsl_vector_get(params->xx_p,j);
    yy_j = gsl_vector_get(params->yy_p,j);
    ans += ww_j * (yy_j - (params->aa) - b*xx_j) * (yy_j - (params->aa) - b*xx_j);       
  }
  return ans;
}

double chirxymul (double bang, void * chirxyparams)
{
  const double BIG =1.0e30;
  unsigned int nn;
  double b;
  double avex=0.0; 
  double avey=0.0;
  double sumw=0.0;
  double sx_j,sy_j,ww_j,xx_j,yy_j;
  unsigned int j;
  double ans;
  LinFitErrXYParams * params;  

  params = reinterpret_cast<LinFitErrXYParams*>(chirxyparams);
  nn=params->xx_p->size;
  b=tan(bang);
  for(j=0;j<nn;++j)
  {
    sx_j = gsl_vector_get(params->sx_p,j);
    sy_j = gsl_vector_get(params->sy_p,j);
    gsl_vector_set(params->ww_p,j,(b*sx_j*b*sx_j + sy_j*sy_j));
    ww_j = gsl_vector_get(params->ww_p,j);
    ww_j = (ww_j < (1.0 / BIG) ? BIG : (1.0/ww_j));
    gsl_vector_set(params->ww_p,j,ww_j);
    xx_j=gsl_vector_get(params->xx_p,j);
    yy_j=gsl_vector_get(params->yy_p,j);
    avex += ww_j * xx_j;
    avey += ww_j * yy_j;
  }
  avex /= sumw;
  avey /= sumw;
  for ( ans = -params->offs,j=0;j<nn;++j)
  {
    ww_j = gsl_vector_get(params->ww_p,j);
    xx_j = gsl_vector_get(params->xx_p,j);
    yy_j = gsl_vector_get(params->yy_p,j);
    ans += ww_j * (yy_j - b*xx_j) * (yy_j - b*xx_j);       
  }
  return ans;
}


void fit_wxymul (const double * xa, const size_t xstride, const double * w_xa, const size_t wxstride, const double * ya, const size_t ystride, const double * w_ya, const size_t wystride, size_t n, double * c1, double * sigc1, double * chi2, double *q)
{
  const double ACC=1.0e-3;
  const double BIG=1.0e30;
  double scale;
  double sx_j,sy_j;
  unsigned int j;
  double trash1,trash2;
  double d1,d2;
  double ang[7],ch[7];
  double r2;
  double bmx, bmn; 
  double var_x,var_y;
  LinFitErrXYParams params;
  gsl_vector * x;
  gsl_vector * sig_x;
  gsl_vector * y;
  gsl_vector * sig_y;
  gsl_vector * we_p;

  x=gsl_vector_alloc(n);
  sig_x=gsl_vector_alloc(n);
  y=gsl_vector_alloc(n);
  sig_y=gsl_vector_alloc(n);
  for (j=0; j<n; j=j+xstride)
  {
    gsl_vector_set(x,j,xa[j]);  
  }
  for (j=0; j<n; j=j+wxstride)
  {
    gsl_vector_set(sig_x,j,sqrt(1.0/w_xa[j]));  
  }
  for (j=0; j<n; j=j+ystride)
  {
    gsl_vector_set(y,j,ya[j]);  
  }
  for (j=0; j<n; j=j+wystride)
  {
    gsl_vector_set(sig_y,j,sqrt(1.0/w_ya[j]));  
  }
  params.xx_p = gsl_vector_alloc(n); 
  params.yy_p = gsl_vector_alloc(n);
  params.sx_p = gsl_vector_alloc(n);
  params.sy_p = gsl_vector_alloc(n);
  params.ww_p = gsl_vector_alloc(n);
  we_p = gsl_vector_alloc(n);
  var_x = gsl_stats_variance(x->data,1,n);
  var_y = gsl_stats_variance(y->data,1,n);
  scale = sqrt(var_x/var_y);  
  for (j = 0;j<n;++j)
  {
    gsl_vector_set(params.xx_p,j,gsl_vector_get(x,j));
    gsl_vector_set(params.yy_p,j,gsl_vector_get(y,j)*scale);
    gsl_vector_set(params.sx_p,j,gsl_vector_get(sig_x,j));
    gsl_vector_set(params.sy_p,j,gsl_vector_get(sig_y,j)*scale);
    sx_j=gsl_vector_get(params.sx_p,j);
    sy_j=gsl_vector_get(params.sy_p,j);
    gsl_vector_set(params.ww_p,j,sqrt(sx_j*sx_j + sy_j*sy_j));
    gsl_vector_set(we_p,j,1.0/(sx_j*sx_j + sy_j*sy_j));
  }

  gsl_fit_wmul (params.xx_p->data, 1, we_p->data, 1, params.yy_p->data, 1, n, c1, &trash1, &trash2);
  params.offs = 0.0;
  ang[0] = 0.0;
  ang[1] = atan(*c1);
  ang[3] = 0.0;
  ang[4] = ang[1]; 
  ang[5] = 1.571000;
  for (j=3; j<6;++j)
  {
    ch[j]=chirxymul(ang[j], &params);
  }
  mnbrak (&ang[0],&ang[1],&ang[2],&ch[0],&ch[1],&ch[2],&chirxymul,&params);
  *chi2=brent(ang[0],ang[1],ang[2],&chirxymul,ACC,c1,&params);
  *chi2=chirxymul(*c1,&params);
  *q=gsl_sf_gamma_inc_Q(0.5*(n-2),*chi2*0.5);
  r2=0.0;
  for (j=0;j<n;++j) 
  {
    r2 += gsl_vector_get(params.ww_p,j);
  }
  r2=1.0/r2;
  bmx=BIG;
  bmn=BIG;
  params.offs=*chi2+1.0;
  for (j=0;j<6;++j)
  {
    if (ch[j] > params.offs)
    {
      d1=std::fabs(ang[j]-*c1);
      while (d1 >= M_PI) 
      {
        d1 -= M_PI;
      }
      d2=M_PI-d1;
      if (ang[j] < *c1)
      {
        swap(&d1,&d2);
      }
      if (d1 < bmx) 
      {
        bmx=d1;
      }
      if (d2 < bmn)
      {
        bmn=d2;
      }
    }
  }
  if (bmx < BIG)
  {
    bmx=zbrent(&chirxymul,*c1,*c1+bmx,ACC,&params)-*c1;
    bmn=zbrent(&chirxymul,*c1,*c1-bmn,ACC,&params)-*c1;
    *sigc1=sqrt(0.5*(bmx*bmx+bmn*bmn))/(scale*cos(*c1)*cos(*c1));
  }
  else
  {
    *sigc1=BIG;
  }
  *c1=tan(*c1)/scale;
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_vector_free(sig_x);
  gsl_vector_free(sig_y);
  gsl_vector_free(params.xx_p);
  gsl_vector_free(params.yy_p);
  gsl_vector_free(params.sx_p);
  gsl_vector_free(params.sy_p);
  gsl_vector_free(params.ww_p);
  gsl_vector_free(we_p);
}


void fit_wxylinear (const double * xa, const size_t xstride, const double * w_xa, const size_t wxstride, const double * ya, const size_t ystride, const double * w_ya, const size_t wystride, size_t n, double * c0, double * c1, double * sigc0, double * sigc1, double * chi2, double *q)
{
  const double ACC=1.0e-3;
  const double BIG=1.0e30;
  double scale;
  double sx_j,sy_j;
  unsigned int j;
  double trash1,trash2,trash3,trash4,trash5;
  double amn,amx,d1,d2;
  double ang[7],ch[7];
  double r2;
  double bmx, bmn; 
  double var_x,var_y;
  LinFitErrXYParams params;
  gsl_vector * x;
  gsl_vector * sig_x;
  gsl_vector * y;
  gsl_vector * sig_y;
  gsl_vector * we_p;

  x=gsl_vector_alloc(n);
  sig_x=gsl_vector_alloc(n);
  y=gsl_vector_alloc(n);
  sig_y=gsl_vector_alloc(n);
  for (j=0; j<n; j=j+xstride)
  {
    gsl_vector_set(x,j,xa[j]);  
  }
  for (j=0; j<n; j=j+wxstride)
  {
    gsl_vector_set(sig_x,j,sqrt(1.0/w_xa[j]));  
  }
  for (j=0; j<n; j=j+ystride)
  {
    gsl_vector_set(y,j,ya[j]);  
  }
  for (j=0; j<n; j=j+wystride)
  {
    gsl_vector_set(sig_y,j,sqrt(1.0/w_ya[j]));  
  }
  params.xx_p = gsl_vector_alloc(n); 
  params.yy_p = gsl_vector_alloc(n);
  params.sx_p = gsl_vector_alloc(n);
  params.sy_p = gsl_vector_alloc(n);
  params.ww_p = gsl_vector_alloc(n);
  we_p = gsl_vector_alloc(n);
  var_x = gsl_stats_variance(x->data,1,n);
  var_y = gsl_stats_variance(y->data,1,n);
  scale = sqrt(var_x/var_y);  
  for (j = 0;j<n;++j)
  {
    gsl_vector_set(params.xx_p,j,gsl_vector_get(x,j));
    gsl_vector_set(params.yy_p,j,gsl_vector_get(y,j)*scale);
    gsl_vector_set(params.sx_p,j,gsl_vector_get(sig_x,j));
    gsl_vector_set(params.sy_p,j,gsl_vector_get(sig_y,j)*scale);
    sx_j=gsl_vector_get(params.sx_p,j);
    sy_j=gsl_vector_get(params.sy_p,j);
    gsl_vector_set(params.ww_p,j,sqrt(sx_j*sx_j + sy_j*sy_j));
    gsl_vector_set(we_p,j,1.0/(sx_j*sx_j + sy_j*sy_j));
  }
  gsl_fit_wlinear (params.xx_p->data, 1, we_p->data, 1, params.yy_p->data, 1, n, &trash1, c1, &trash2, &trash3, &trash4, &trash5);
  params.offs = 0.0;
  ang[0] = 0.0;
  ang[1] = atan(*c1);
  ang[3] = 0.0;
  ang[4] = ang[1]; 
  ang[5] = 1.571000;
  for (j=3; j<6;++j)
  {
    ch[j]=chirxy(ang[j], &params);
  }
  mnbrak (&ang[0],&ang[1],&ang[2],&ch[0],&ch[1],&ch[2],&chirxy,&params);
  *chi2=brent(ang[0],ang[1],ang[2],&chirxy,ACC,c1,&params);
  *chi2=chirxy(*c1,&params);
  *c0=params.aa;
  *q=gsl_sf_gamma_inc_Q(0.5*(n-2),*chi2*0.5);
  r2=0.0;
  for (j=0;j<n;++j) 
  {
    r2 += gsl_vector_get(params.ww_p,j);
  }
  r2=1.0/r2;
  bmx=BIG;
  bmn=BIG;
  params.offs=*chi2+1.0;
  for (j=0;j<6;++j)
  {
    if (ch[j] > params.offs)
    {
      d1=std::fabs(ang[j]-*c1);
      while (d1 >= M_PI) 
      {
        d1 -= M_PI;
      }
      d2=M_PI-d1;
      if (ang[j] < *c1)
      {
        swap(&d1,&d2);
      }
      if (d1 < bmx) 
      {
        bmx=d1;
      }
      if (d2 < bmn)
      {
        bmn=d2;
      }
    }
  }
  if (bmx < BIG)
  {
    bmx=zbrent(&chirxy,*c1,*c1+bmx,ACC,&params)-*c1;
    amx=params.aa-*c0;
    bmn=zbrent(&chirxy,*c1,*c1-bmn,ACC,&params)-*c1;
    amn=params.aa-*c0;
    *sigc1=sqrt(0.5*(bmx*bmx+bmn*bmn))/(scale*cos(*c1)*cos(*c1));
    *sigc0=sqrt(0.5*(amx*amx+amn*amn)+r2)/scale;
  }
  else
  {
    *sigc1=BIG;
    *sigc0=BIG;
  }
  *c0 /= scale;
  *c1=tan(*c1)/scale;
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_vector_free(sig_x);
  gsl_vector_free(sig_y);
  gsl_vector_free(params.xx_p);
  gsl_vector_free(params.yy_p);
  gsl_vector_free(params.sx_p);
  gsl_vector_free(params.sy_p);
  gsl_vector_free(params.ww_p);
  gsl_vector_free(we_p);
}


}}} // ns
