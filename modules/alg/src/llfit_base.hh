//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_LLFIT_BASE_HH
#define IPLT_EX_LLFIT_BASE_HH

#include <vector>
#include <map>

#include <ost/base.hh>
#include <ost/geom/geom.hh>

#include <ost/img/data_types.hh>
#include <iplt/alg/module_config.hh>

#include "llfit_fw.hh"

namespace iplt {  namespace alg {

Real Sinc(Real x, Real w, Real b2);

namespace detail {

struct IntensEntry {
  IntensEntry(Real z, Real i, Real s):zstar(z),intens(i),sig_intens(s){}
  Real zstar;
  Real intens;
  Real sig_intens;
};

struct PhaseEntry {
  Real zstar;
  Real phase;
  Real sig_phase;
};

typedef std::vector<IntensEntry> IntensList;
typedef std::vector<PhaseEntry> PhaseList;

struct LLFitParams {
  IntensList& intens;
  PhaseList& phase;
  Real sampling;
  int count;
  Real bfac;
};

} // ns detail

struct LLFitAmpCurvePoint {
  Real zfit;
  Real ifit;
  Real sigifit;
};

// base class for all lattice line fits
class DLLEXPORT_IPLT_ALG LLFitBase {
public:
  enum guessmode {
    GUESSMODE_BINAVERAGE,
    GUESSMODE_CONST,
    GUESSMODE_RANDOM,
  };
  //! initialize with pixel-count and pixel-sampling
  LLFitBase(unsigned int count, Real sampling, guessmode gm=GUESSMODE_RANDOM);

  virtual ~LLFitBase();


  virtual LLFitBasePtr Clone()=0;

  //! do actual fitting
  virtual void Apply() = 0;

  // calculate intens and error from fit at given zstar
  virtual std::pair<Real,Real> CalcIntensAt(Real zstar) const = 0;

  // calculate phase and error from fit at given zstar
  virtual std::pair<Real,Real> CalcPhaseAt(Real zstar) const;

  // return closest point (as intens, zstar) on curve to given pair
  // using an explicit weighting
  LLFitAmpCurvePoint ClosestIntensAt(Real zstar, Real zstar_w, Real intens, Real intens_w);

  // return closest point (as intens, zstar) on curve to given pair
  // using a relative weighting
  LLFitAmpCurvePoint RelativeClosestIntensAt(Real zstar, Real intens);

   // return actual number of iterations until convergence
  virtual unsigned int GetIterCount() const;

  virtual std::map<Real,Complex> GetComponents() const = 0;

  virtual void Cleanup();

  // return pixel count
  unsigned int GetCount() const;
  // set pixel count
  void SetCount(unsigned int count);

  // return pixel sampling
  Real GetSampling() const;
  // return pixel sampling
  void SetSampling(Real sampling);

  //! add intensity and its sigma at reciprocal z
  void AddIntens(Real zstar, Real intens, Real sig_intens);

  //! add phase and its sigma at reciprocal z
  void AddPhase(Real zstar, Real phi, Real sig_phi);
  
  //! clear data list
  void Clear();

  // set maximal iterations of fitting routine
  void SetMaxIter(unsigned int m);

  unsigned int GetMaxIter();

  void SetIterationLimits(Real,Real);

  void SetRelativeZWeight(Real w);

  void SetBFactor(Real b);
  Real GetBFactor() const;

protected:
  unsigned int count_;
  Real sampling_;
  detail::IntensList intens_list_;
  detail::PhaseList phase_list_;
  unsigned int max_iter_;
  unsigned int iter_count_;
  Real lim1_,lim2_;
  std::vector<LLFitAmpCurvePoint> curve_;
  bool dirty_curve_;
  int curve_count_;
  Real max_zstar_;
  Real max_intens_;
  Real relative_weighting_;
  Real bfac_;
  guessmode guessmode_;

 private:
  LLFitAmpCurvePoint do_closest_at(Real zstar, Real zstar_w, Real intens, Real intens_w, bool relative);
};

}} // ns

#endif

