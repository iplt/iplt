//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_LLPREDICT
#define IPLT_EX_LLPREDICT

#include <stdexcept>

#include <ost/img/image.hh>
#include <iplt/reflection_index.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

class DLLEXPORT_IPLT_ALG LLPredictException: public std::runtime_error
{
public:
  LLPredictException(const String& m):
    std::runtime_error(m) {}
};

//! Predict lattice line values
/*!
  Given a real-space volume (ie 3D density), predict values
  for for a particular lattice line {h,k}, at reciprocal
  height z*.

  The internal calculation uses an explicit fourier summation,
  whose exponential contains the multiplication of 'zstar' with 'z'
  (the reciprocal distance with the spatial one), where zstar is
  given directly, and 'z' is generated from the index and the
  spatial sampling.

  If zstar is given in reciprocal angstrom, for example, the
  spatial sampling should be given in angstrom.
*/

class DLLEXPORT_IPLT_ALG LLPredict 
{
public:
  //! initialize with 3D volume
  /*!
    The pixel sampling must be properly set to ensure
    correct conversion from frequency to fractional index
  */
  LLPredict(const ost::img::ConstImageHandle& ih);

  //! get prediction for lattice line hk and zstar
  Complex Get(const ReflectionIndex& ri) const;

  //! debug interface, do not use
  ost::img::ImageHandle GetRef() const {return ref_;}

private:
  ost::img::ImageHandle ref_;
  Real delta_;
};

}} // ns


#endif
