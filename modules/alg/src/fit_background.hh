//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#ifndef FIT_BACKGROUND_HH_
#define FIT_BACKGROUND_HH_


#include <vector>
#include <ost/base.hh>
#include <ost/img/image.hh>
#include <ost/geom/vec2.hh>
#include <iplt/alg/module_config.hh>

struct BackgroundFitEntry {
  BackgroundFitEntry(Real xx=0.0, Real yy=0.0,Real v=0, Real ww=0.0):
    x(xx),y(yy),val(v),w(ww)
  {}
  Real x,y,val,w;
};

typedef std::vector< BackgroundFitEntry > BackgroundFitList;

namespace iplt { namespace alg{

class DLLEXPORT_IPLT_ALG FitBackground
{
public:
  FitBackground(bool zo=false);
  virtual ~FitBackground();
  
  void Add(Real x, Real y, Real val, Real w=1.0);

  Real GetS1() const;
  Real GetB1() const;
  Real GetS2() const;
  Real GetB2() const;
  geom::Vec2 GetOrigin() const;
  Real GetC() const;
  Real GetChi() const;

  void Apply();

  ost::img::ImageHandle AsImage(const ost::img::Extent& e);

private:
  BackgroundFitList list_;
  bool zero_offset_;
  Real S1_;
  Real B1_;
  Real S2_;
  Real B2_;
  geom::Vec2 ori_;
  Real C_,chi_;
  Real maxval_;
  Real maxx_;
  Real maxy_;
  Real minx_;
  Real miny_;
};

}}
#endif /*FIT_BACKGROUND_HH_*/
