//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  Gauss2D stuff

  Author: Ansgar Philippsen
*/

#ifndef IPLT_ALG_GAUSS2D_H
#define IPLT_ALG_GAUSS2D_H

#include <cmath>
#include <iosfwd>

#include <ost/img/function.hh>
#include <ost/img/point.hh>
#include <iplt/alg/module_config.hh>
namespace iplt {  namespace alg {

//! store parameters that describe a 2D pseudo-gaussian
/*!
  The parameters are meant for the following 2D pseudo-gaussian function:

  x' = x-ux;

  y' = y-uy;

  x" = x' cos[w] - y' sin[w]

  y" = x' sin[w] + y' cos[w]

  f(x,y) = A exp[ (x"/Bx)^2 + (y"/By)^2 ] + Px * x' + Py * y' + C
*/
class DLLEXPORT_IPLT_ALG ParamsGauss2D {
public:
  ParamsGauss2D(Real A, Real Bx, Real By, Real C, Real ux, Real uy, Real w, Real Px, Real Py):
    A_(A), Bx_(Bx),  By_(By), C_(C), ux_(ux), uy_(uy), w_(w), Px_(Px), Py_(Py) {}

  ParamsGauss2D():
    A_(1.0), Bx_(1.0), By_(1.0), C_(0.0), ux_(0.0), uy_(0.0), w_(0.0), Px_(0.0), Py_(0.0) {}

  // getter/setter methods
  Real GetA() const {return A_;}
  void SetA(Real A) {A_ = A;}
  Real GetBx() const {return Bx_;}
  void SetBx(Real Bx) {Bx_ = Bx; }
  Real GetBy() const {return By_;}
  void SetBy(Real By) {By_ = By; }
  Real GetC() const {return C_;}
  void SetC(Real C) {C_ = C;}
  Real GetUx() const {return ux_;}
  void SetUx(Real ux) {ux_ = ux;}
  Real GetUy() const {return uy_;}
  void SetUy(Real uy) {uy_ = uy;}
  Real GetW() const {return w_;}
  void SetW(Real w) {w_ = w;}
  Real GetPx() const {return Px_;}
  void SetPx(Real Px) {Px_ = Px;}
  Real GetPy() const {return Py_;}
  void SetPy(Real Py) {Py_ = Py;}

  geom::Vec2 GetB() const {return geom::Vec2(GetBx(),GetBy());}
  void SetB(const geom::Vec2& b) {SetBx(b[0]); SetBy(b[1]);}
  geom::Vec2 GetU() const {return geom::Vec2(GetUx(),GetUy());}
  void SetU(const geom::Vec2& u) {SetUx(u[0]); SetUy(u[1]);}
  geom::Vec2 GetP() const {return geom::Vec2(GetPx(),GetPy());}
  void SetP(const geom::Vec2& p) {SetPx(p[0]); SetPy(p[1]);}

  //! Return volume of fitted gaussian
  /*
    Calculates the volume of the gaussian function,
    not including the background (C, Px, Py terms), using:

    vol = pi * A * Bx * By
  */
  Real GetVolume() const {
    return M_PI * A_ * std::fabs(Bx_) * std::fabs(By_);
  }

  Real GetBackground(const geom::Vec2& p) {
    return Px_*(p[0]-ux_)+Py_*(p[1]-uy_)+C_;
  }

protected:
  Real A_, Bx_, By_, C_, ux_, uy_, w_, Px_, Py_;
};

DLLEXPORT_IPLT_ALG std::ostream& operator<<(std::ostream&, const ParamsGauss2D&);

//! 2D Gaussian function
class DLLEXPORT_IPLT_ALG FuncGauss2D: public ost::img::RealFunction, public ParamsGauss2D
{
public:
  FuncGauss2D();
  FuncGauss2D(const ParamsGauss2D& p);
  FuncGauss2D(Real A, Real Bx=1.0, Real By=1.0, Real C=0.0, Real ux=0.0, Real uy=0.0, Real w=0.0, Real Px=0.0, Real Py=0.0);

  void SetA(Real A);
  void SetBx(Real Bx);
  void SetBy(Real By);
  void SetC(Real C);
  void SetUx(Real ux);
  void SetUy(Real uy);
  void SetW(Real w);
  void SetPx(Real Px);
  void SetPy(Real Py);

  // ost::img::RealFunction interface implementation
  virtual Real Func(const geom::Vec3 &v) const;

protected:
  // derived
  Real iBx2_, iBy2_ , sinw_, cosw_;

  void calc_derived();
};


class DLLEXPORT_IPLT_ALG Gauss2DPeak: public ost::img::Point, public ParamsGauss2D {
public:
  Gauss2DPeak():
    ost::img::Point(),
    ParamsGauss2D(0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0)
  {}
  Gauss2DPeak(const ost::img::Point &pn, const ParamsGauss2D& pr):
    ost::img::Point(pn),
    ParamsGauss2D(pr)
  {}
};

typedef std::vector<Gauss2DPeak> Gauss2DPeakList;

}} // namespaces

#endif
