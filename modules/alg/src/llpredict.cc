//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <iostream>
#include <cmath>

#include <ost/log.hh>

#include "llpredict.hh"
#include "llpredict_impl.hh"

namespace iplt {  namespace alg {

namespace {

ost::img::ImageHandle prepare_ref(const ost::img::ConstImageHandle& ih)
{
  llpredict::MakeRef make_ref;
  return ih.StateApply(make_ref);
}

} // anon ns


LLPredict::LLPredict(const ost::img::ConstImageHandle& ih):
  ref_(prepare_ref(ih)),
  delta_(ih.GetSpatialSampling()[2])
{
}

/*
  the explicit fourier summation has 
  
   spatial distance * reciprocal distance

  in its exponential. The reciprocal distance is
  given directly with zstar, the spatial distance
  is calculated from the depth index, multiplied
  with the sampling.

*/
Complex LLPredict::Get(const ReflectionIndex& ri) const
{
  int h=ri.GetH();
  int k=ri.GetK();
  Real zstar=ri.GetZStar();
  ost::img::Point hk(h<0 ? ref_.GetSize()[0]+h : h, k<0 ? ref_.GetSize()[1]+k : k,0);

  // explicit fourier sum
  Real pre = -2.0*M_PI*zstar*delta_;
  int cstart = ref_.GetExtent().GetStart()[2];
  int cend = ref_.GetExtent().GetEnd()[2];
  Complex sum;
  for(int c=cstart;c<=cend;++c) {
    Real ex = pre*static_cast<Real>(c);
    sum+=ref_.GetComplex(ost::img::Point(hk[0],hk[1],c)) * Complex(cos(ex),sin(ex));
  }
  return sum;
}


}} // ns
