//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Gauss2D fitting

  Author: Ansgar Philippsen
*/

#ifndef IPLT_ALG_FIT_GAUSS2D_H
#define IPLT_ALG_FIT_GAUSS2D_H

#include "gauss2d.hh"

#include <ost/img/algorithm.hh>
#include <ost/img/image.hh>
#include <ost/message.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

class DLLEXPORT_IPLT_ALG FitGauss2DError: public ost::Error {
public:
  FitGauss2DError(const String& s):
    Error(String("FitGauss2DError: ") + s) {}
};

//! 2D Gaussian fitting algorithm
class DLLEXPORT_IPLT_ALG FitGauss2D: public ost::img::NonModAlgorithm, public ParamsGauss2D
{
public:
  enum {
    ID_A=0,
    ID_BX, ID_BY,
    ID_UX, ID_UY,
    ID_C,
    ID_W,
    ID_PX, ID_PY
  };

  FitGauss2D(double A=1.0, double Bx=1.0, double By=1.0, double C=0.0, double ux=0.0, double uy=0.0, double w=0.0, double Px=0.0, double Py=0.0);

  void SetSigma(double s) {sigma_ = s;}
  void SetMaxIter(int n) {max_iter_ = n;}
  unsigned int GetMaxIter() const{return max_iter_;}
  void SetLim(double l1, double l2) {lim1_=l1; lim2_=l2;}

  double GetChi() const {return chi_;}
  void SetChi(double c) {chi_=c;}
  double GetGOF() const {return gof_;}
  void SetGOF(double g) {gof_=g;}
  double GetQuality() const {return qual_;}
  void SetQuality(double q) {qual_=q;}
  double GetAbsQuality() const {return qual_;}
  void SetAbsQuality(double q) {qual_=q;}
  double GetRelQuality() const {return qual2_;}
  void SetRelQuality(double q) {qual2_=q;}
  int GetIterationCount() const {return itcount_;}
  double GetSigAmp() const {return sigamp_;}
  void SetSigAmp(double sa) {sigamp_=sa;}

  // return as function
  FuncGauss2D AsFunction() const;

  // alg interface
  virtual void Visit(const ost::img::ConstImageHandle& i){VisitData(i);}
  virtual void Visit(const ost::img::Function& f) {VisitData(f);}

protected:
  void VisitData(const ost::img::Data& data);

private:
  int max_iter_;
  double chi_, gof_, qual_, qual2_, sigamp_;
  double sigma_;
  double lim1_, lim2_;
  int itcount_;
};



DLLEXPORT_IPLT_ALG std::ostream& operator<<(std::ostream& o, const FitGauss2D& f);

}} // namespaces

#endif
