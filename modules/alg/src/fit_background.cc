//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <limits>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_blas.h>

#include <ost/log.hh>

#include "fit_background.hh"

namespace iplt { namespace alg{
using namespace geom;




namespace {


struct BgFitParams 
{
  BackgroundFitList* value_list;
  bool zero_offset;
};

template<bool b1,bool b2>
int bg_fit_func_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  BgFitParams* prm = reinterpret_cast<BgFitParams*>(params);

  Real S1=gsl_vector_get(x,0);
  Real B1=gsl_vector_get(x,1);
  Real S2=gsl_vector_get(x,2);
  Real B2=gsl_vector_get(x,3);
  Real X0=gsl_vector_get(x,4);
  Real Y0=gsl_vector_get(x,5);
  Real C=0.0;
  if(!prm->zero_offset) {
    C = gsl_vector_get(x,6);
  }
  unsigned int idx=0;
  for(BackgroundFitList::const_iterator it=prm->value_list->begin();it!=prm->value_list->end();++it) {
    Real r=sqrt((it->x-X0)*(it->x-X0)+(it->y-Y0)*(it->y-Y0));
    Real dFdS1=exp(B1*r);                                 //  dF/dS1
    Real dFdS2=exp(B2*r);                                 //  dF/dS2
    if(b1) {
      gsl_vector_set(f,idx,(C+S1*dFdS1+S2*dFdS2-it->val)*it->w);
    }

    if(b2) { 
      gsl_matrix_set(J,idx,0,dFdS1*it->w);                        //  dF/dS1
      gsl_matrix_set(J,idx,1,S1*dFdS1*r*it->w);             //  dF/dB1
      gsl_matrix_set(J,idx,2,dFdS2*it->w);                  //  dF/dS2
      gsl_matrix_set(J,idx,3,S2*dFdS2*r*it->w);             //  dF/dB2
      Real q=-(B1*S1*dFdS1+B2*S2*dFdS2)/r;                //  intermediate value for dF/dX0 and dF/dY0
      gsl_matrix_set(J,idx,4,q*(it->x-X0)*it->w);           //  dF/dX0
      gsl_matrix_set(J,idx,5,q*(it->y-Y0)*it->w);           //  dF/dY0
      if(!prm->zero_offset) {
         gsl_matrix_set(J,idx,6,it->w*1.0);         // dF/dC
      }
    }
    ++idx;
  }
  return GSL_SUCCESS;
}

static int bg_fit_func_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return bg_fit_func_tmpl<true,false>(x,params,f,0);
}

static int bg_fit_func_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return bg_fit_func_tmpl<false,true>(x,params,0,J);
}

static int bg_fit_func_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return bg_fit_func_tmpl<true,true>(x,params,f,J);
}

} // anon ns



FitBackground::FitBackground(bool zo):
  list_(),
  zero_offset_(zo),
  S1_(0.0),
  B1_(0.0),
  S2_(0.0),
  B2_(0.0),
  ori_(),
  C_(0.0),
  chi_(0.0),
  maxval_(-std::numeric_limits<Real>::max()),
  maxx_(-std::numeric_limits<Real>::max()),
  maxy_(-std::numeric_limits<Real>::max()),
  minx_(std::numeric_limits<Real>::max()),
  miny_(std::numeric_limits<Real>::max())
{
}

FitBackground::~FitBackground()
{
}


void FitBackground::Add(Real x, Real y, Real val, Real w)
{
  list_.push_back(BackgroundFitEntry(x,y,val,w));
  maxval_=std::max<Real>(maxval_,val);
  maxx_=std::max<Real>(maxx_,x);
  maxy_=std::max<Real>(maxy_,y);
  minx_=std::min<Real>(minx_,x);
  miny_=std::min<Real>(miny_,y);
}


void FitBackground::Apply()
{
  if(zero_offset_) {
    LOG_VERBOSE( "background fit (with zero offset) using " << list_.size() << " points" );
  } else {
    LOG_VERBOSE( "background fit using " << list_.size() << " points" );
  }

  if(list_.size()<6+(zero_offset_ ? 0 : 1)) {
    LOG_INFO( "not enough points ("<< list_.size() << ") for background fit, returning unity" );
    chi_ = 0.0;
    return;
  }

  int num_p = zero_offset_ ? 6 : 7;

  BgFitParams params = {&list_,zero_offset_};
  
  // set initial guesses
  gsl_vector* X = gsl_vector_alloc(num_p);
  gsl_vector_set(X,0,maxval_*0.75);          //  S1
  gsl_vector_set(X,1,-2.0);                  //  B1
  gsl_vector_set(X,2,maxval_*0.25);          //  S2
  gsl_vector_set(X,3,-1.0);                  //  B2
  gsl_vector_set(X,4,(maxx_+minx_)/2.0);     //  X0
  gsl_vector_set(X,5,(maxy_+miny_)/2.0);     //  Y0
  if(!zero_offset_) {
    gsl_vector_set(X,6,C_);                  //  C
  }
  
  gsl_multifit_function_fdf func;
  func.f = &bg_fit_func_f;
  func.df = &bg_fit_func_df;
  func.fdf = &bg_fit_func_fdf;
  func.n = list_.size();
  func.p = num_p;
  func.params = &params;

  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;
  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, list_.size(),num_p);
  gsl_multifit_fdfsolver_set (solver, &func, X);

  Real lim1=1e-5;
  Real lim2=1e-5;
  int iter=0;
  int max_iter=1000;
  int status=GSL_CONTINUE;
  LOG_DEBUG( "running gsl_multifit solver" );

  while (status == GSL_CONTINUE && iter < max_iter) {
    ++iter;

    gsl_multifit_fdfsolver_iterate (solver);
    //enforce negative B factors
    gsl_vector_set(solver->x, 1,-std::abs(gsl_vector_get(solver->x, 1)));
    gsl_vector_set(solver->x, 3,-std::abs(gsl_vector_get(solver->x, 3)));
    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1, lim2);
  } 
  S1_=gsl_vector_get(solver->x,0);
  B1_=gsl_vector_get(solver->x,1);
  S2_=gsl_vector_get(solver->x,2);
  B2_=gsl_vector_get(solver->x,3);
  ori_=Vec2(gsl_vector_get(solver->x,4),gsl_vector_get(solver->x,5));
  if(!zero_offset_) {
    C_ = gsl_vector_get(solver->x,6);
  }
  chi_ = gsl_blas_dnrm2(solver->f);

  gsl_multifit_fdfsolver_free (solver);

  gsl_vector_free(X);

}
Real FitBackground::GetS1() const
{
  return S1_;
}
Real FitBackground::GetB1() const
{
  return B1_;
}
Real FitBackground::GetS2() const
{
  return S2_;
}
Real FitBackground::GetB2() const
{
  return B2_;
}

Vec2 FitBackground::GetOrigin() const
{
  return ori_;
}

Real FitBackground::GetC() const
{
  return C_;
}

Real FitBackground::GetChi() const
{
  return chi_;
}

namespace {

}

ost::img::ImageHandle FitBackground::AsImage(const ost::img::Extent& e)
{
  ost::img::ImageHandle ih=CreateImage(e);
  for(ost::img::ExtentIterator it(e);!it.AtEnd();++it) {
    Real r=Length(ost::img::Point(it).ToVec2()-ori_);
    ih.SetReal(it,S1_*exp(B1_*r)+S2_*exp(B2_*r)+C_);
  }
  return ih;
}



}}//ns
