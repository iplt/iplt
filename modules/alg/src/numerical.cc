//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Valerio Mariani
*/

#include <gsl/gsl_min.h>
#include <gsl/gsl_roots.h>
#include <boost/shared_ptr.hpp>
#include <ost/log.hh>
#include <ost/message.hh>

#include <limits>
#include "numerical.hh"

namespace iplt {  namespace alg { namespace detail {

double max(double a, double b)
{
  if (a >= b) {
    return a;
  }
  return b;
}
  
void swap (double * a, double * b)
{
  double swap;
  
  swap=*a;
  *a=*b;
  *b=swap;
}

double sign (double a, double b)
{
  return b>=0 ? (a >=0 ? a : -a ) : (a >=0 ? -a : a) ; 
}

void shft3 (double * a, double * b, double * c, double d)
{
  *a=*b;
  *b=*c;
  *c=d;
}  

void mnbrak ( double * ax, double * bx, double *cx, double *fa, double *fb, double * fc, double (*func) ( double, void * ), void * func_params )
{ 
  const double GOLD=1.618034;
  const double GLIMIT=100.0;
  const double TINY=1.0e-20;
  double r,q,u,ulim,fu;

  *fa=func(*ax, func_params); 
  *fb=func(*bx, func_params); 
  if (*fb > *fa)
  {
    swap(ax,bx);
    swap(fb,fa);
  }
  *cx=*bx+GOLD*(*bx-*ax);
  *fc=func(*cx,func_params);
  while (*fb>*fc) 
  {
    r=(*bx-*ax)*(*fb-*fc);
    q=(*bx-*cx)*(*fb-*fa);
    u=(*bx)-((*bx-*cx)*q-(*bx-*ax)*r)/(2.0*sign(max(std::fabs(q-r),TINY),q-r));
    ulim=*bx+GLIMIT*(*cx-*bx);
    if ((*bx-u)*(u-*cx) > 0.0)
    {
      fu=func(u,func_params);
      if (fu < *fc)
      {
        *ax=*bx;
        *bx=u;    
        *fa=*fb;
        *fb=fu;
        return;
      } 
      else if (fu > *fb) 
      {
        *cx=u;
        *fc=fu;
        return;
      }
      u=*cx+GOLD*(*cx-*bx);
      fu=func(u,func_params);
    }
    else if ((*cx-u)*(u-ulim) > 0.0)
    {
      fu=func(u,func_params);
      if (fu < *fc)
      {
        shft3(bx,cx,&u,u+GOLD*(u-*cx));        
        shft3(fb,fc,&fu,func(u,func_params));
      }        
    }
    else if ((u-ulim)*(ulim-*cx) > 0.0)
    {
      u=ulim;
      fu=func(u,func_params);
    }
    else
    {
      u=*cx+GOLD*(*cx-*bx);
      fu=func(u,func_params);
    }
    shft3(ax,bx,cx,u);        
    shft3(fa,fb,fc,fu);
  }
}

double brent ( double ax, double bx, double cx, double (*func) ( double, void * ), double tol, double * xmin, void * func_params )
{
  const unsigned int ITMAX = 100;
  const double ZEPS = std::numeric_limits<double>::epsilon()*1.0e-3;
  gsl_function myfunc;
  gsl_min_fminimizer * minimizer;
  unsigned int iterations=0;
  int status=0;
  double low, high;
  double minimum=0.0; 
  double tol1,tol2,xm,funcval;
  double a,c;

  a=(ax<cx ? ax : cx );
  c=(ax>cx ? ax : cx );
  myfunc.function=func;
  myfunc.params = func_params;
  minimizer = gsl_min_fminimizer_alloc (gsl_min_fminimizer_brent);
  gsl_min_fminimizer_set (minimizer, &myfunc, bx, a, c);
  while (status!=1 && iterations < ITMAX)
  {
    gsl_min_fminimizer_iterate(minimizer);
    minimum = gsl_min_fminimizer_x_minimum(minimizer);
    low = gsl_min_fminimizer_x_lower(minimizer);
    high = gsl_min_fminimizer_x_upper(minimizer);
    xm=0.5*(low+high);
    tol2=2.0*(tol1=tol*std::fabs(minimum)+ZEPS);
    if (std::fabs(minimum-xm) <= (tol2-0.5*(high-low)))
    {
      status=1;    
    }
    ++iterations;
  }
  *xmin=minimum;
  funcval=gsl_min_fminimizer_f_minimum(minimizer); 
  gsl_min_fminimizer_free(minimizer);
  return funcval;
}

double zbrent ( double (*func) ( double , void * ), double x1, double x2, double tol, void * func_params )
{
  const unsigned int ITMAX = 100;
  const double EPS = std::numeric_limits<double>::epsilon();
  gsl_function myfunc;
  gsl_root_fsolver * rootfinder;
  unsigned int iterations=0;
  double root, high, xm;
  double tol1,fb;
  double a,b;

  a=(x1<x2 ? x1 : x2 );
  b=(x1>x2 ? x1 : x2 );
  myfunc.function=func;
  myfunc.params = func_params;
  rootfinder = gsl_root_fsolver_alloc (gsl_root_fsolver_brent);
  gsl_root_fsolver_set (rootfinder, &myfunc, a, b);
  while (iterations < ITMAX)
  {
    gsl_root_fsolver_iterate (rootfinder);
    root = gsl_root_fsolver_root (rootfinder);
    high = gsl_root_fsolver_x_upper (rootfinder);
    fb=func(root,func_params);
    tol1=2.0*EPS*std::fabs(root)+0.5*tol;
    xm=0.5*(high-root);
    if (std::fabs(xm) <= tol1 || fb==0) 
    {
      gsl_root_fsolver_free(rootfinder);
      return root;
    }
    ++iterations;
  }
  gsl_root_fsolver_free(rootfinder);
  return 0.0;
}

void polint(const std::vector<double>& xa, const std::vector<double>& ya, double x, double& y, double& dy)
{ 
  unsigned int i,m,ns=0;
  double den,dif,dift,ho,hp,w;

  unsigned int n = xa.size();
  std::vector<double> c(n);
  std::vector<double> d(n);

  dif=std::fabs(x-xa[0]);
  for (i=0;i<n;i++) { 
    if ((dift=std::fabs(x-xa[i])) <  dif) {
        ns=i;
        dif=dift;
    }
    c[i]=ya[i]; 
    d[i]=ya[i];
  }
  y=ya[ns--]; 
  for (m=1;m<n;m++) { 
    for (i=0;i<=n-m;i++) { 
      ho=xa[i]-x;
      hp=xa[i+m]-x;
      w=c[i+1]-d[i];
      if ((den=ho-hp) == 0.0) 
      {
          throw(ost::Error("Duplicate entries in xa input"));
      }
      den=w/den;
      d[i]=hp*den; 
      c[i]=ho*den;
    }
    y += (dy=(2*(ns+1) < (n-m) ? c[ns+1] : d[ns--]));
  }
}

}}} // ns 
