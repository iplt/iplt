//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#ifndef NEXP_FIT_HH_
#define NEXP_FIT_HH_

#include <vector>
#include <iplt/alg/module_config.hh>

namespace iplt { namespace alg{
  
struct NExpFitEntry {
  NExpFitEntry(Real xx=0.0, Real yy=0.0, Real ww=0.0):
    x(xx),y(yy),w(ww)
  {}
  Real x,y,w;
};

typedef std::vector< NExpFitEntry > NExpFitList;


class DLLEXPORT_IPLT_ALG NExpFit
{
public:
	NExpFit(unsigned int num=1,bool zero_offset=false);
	virtual ~NExpFit();

  void Add(Real x, Real y);
  void Add(Real x, Real y, Real w);

  std::vector<Real> GetSVec() const;
  std::vector<Real> GetBVec() const;
  Real GetC() const;
  Real GetChi() const;

  void Apply();

private:
  unsigned int num_;
  NExpFitList list_;
  bool zero_offset_;
  std::vector<Real> svec_;
  std::vector<Real> bvec_;
  Real C_,chi_;
};

}}//ns

#endif /*NEXP_FIT_HH_*/
