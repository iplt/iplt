//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Giani Signorell, Ansgar Philippsen, Valerio Mariani, Andreas Schenk
*/

#ifndef IPLT_ALG_EX_ALG_PEAK_SEARCH_H
#define IPLT_ALG_EX_ALG_PEAK_SEARCH_H

#include <vector>

#include <ost/img/algorithm.hh>
#include <ost/img/image_state.hh>

#include <ost/img/peak.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

//! Peak Search
/*!
  Applies a peak search on the supplied image
*/

class DLLEXPORT_IPLT_ALG PeakSearchBase
{
public:
  enum modes{
    MODES_START,
    ABSOLUTE,
    RELATIVE_TO_PEAK,
    RELATIVE_TO_STDEV,
    MODES_END
  };
  //! Initialization
  /*!
    - outer window: overall search window around each point
    - outer sensitivity: how much bigger must a value be than the
      surrounding values, scaled with stdev
    - inner window: TODO
    - inner sensitivity: TODO
    - inner count lim
    - inner count min
  */
  PeakSearchBase(unsigned int outer_win=10,
	         Real outer_sens=1.0,
	         unsigned int inner_win=0,
	         Real inner_sens=0.0,
	         int inner_count_lim=0,
	         Real inner_count_sens=0.0,
           unsigned int mode_=RELATIVE_TO_STDEV);


  //! image state algorithm interface
  template <typename T, class D>
          void VisitState(const ost::img::ImageStateImpl<T,D>& isi);

  void VisitFunction(const ost::img::Function& f);

  static String GetAlgorithmName() {return "PeakSearch";}

  //! Main "return" function: returns the PeakList
  ost::img::PeakList GetPeakList() const;

  //! Clears the peak list removing all peaks in the list
  void ClearPeakList();

  //! Get/Set the size of the outer window and the sensitivity of the search
  void SetOuterWindowSize(unsigned int outer_win);
  unsigned int GetOuterWindowSize();
  void SetOuterSensitivity(Real outer_sens);
  Real GetOuterSensitivity();

  //! Get/Set the size of the inner window and the sensitivity of the search
  void SetInnerWindowSize(unsigned int outer_win);
  unsigned int GetInnerWindowSize();
  void SetInnerSensitivity(Real outer_sens);
  Real GetInnerSensitivity();

  //! Get/Set  the inner count lim and sens
  void SetInnerCountLimit(unsigned int limit);
  unsigned int GetInnerCountLimit();
  void SetInnerCountSensitivity(Real inner_count_sens);
  Real GetInnerCountSensitivity();

  //! Set the minimal value a ost::img::Point has to have to be a peak
  void SetThreshold( Real minimum );

  void AddExclusion(const ost::img::Extent& e);

  //! Set/get the peak search mode
  void SetMode( unsigned int mode );
  unsigned int GetMode();

private:
  typedef std::vector<ost::img::Extent> ExtList;

  unsigned int outer_win_;         //size of outer window
  unsigned int inner_win_;         //size of inner window
  Real outer_sens_;              //sensitivity of outer window region
  Real inner_sens_;              //sensitivity of inner window region
  unsigned int inner_count_lim_;
  Real inner_count_sens_;
  Real thd_value_; //minimal value accepted as peak
  bool thd_set_; //true if thd is set by SetLevel
  unsigned int mode_;
  ost::img::PeakList peaks_;
  ExtList ext_list_;

  bool is_excluded(const ost::img::Point& p);
  void check_values();
  template <typename T, class D>
  void visit_const_sens(const ost::img::ImageStateImpl<T,D>& isi);
  template <typename T, class D>
  void visit_variable_sens(const ost::img::ImageStateImpl<T,D>& isi);
};

// injection of base into algorithmic class
typedef ost::img::ImageStateNonModAlgorithm<PeakSearchBase> PeakSearch;

}} // ns

namespace ost{
namespace img{
IPLT_ALG_EXPLICIT_INST_DECL(class,ImageStateNonModAlgorithm< ::iplt::alg::PeakSearchBase>)
}} // ns

#endif
