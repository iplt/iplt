//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>

#include <ost/log.hh>

#include "llfitamp_gauss_sum.hh"

namespace iplt {  namespace alg {

////////////////////////////////////////////////////////
// Gauss Sum Fitting
////////////////////////////////////////////////////////

LLFitAmpGaussianSum::LLFitAmpGaussianSum(unsigned int count, Real sampling, guessmode gm):
  LLFitBase(count,sampling,gm)
{}

LLFitBasePtr LLFitAmpGaussianSum::Clone()
{
    return LLFitBasePtr(new LLFitAmpGaussianSum(*this));
}

namespace {

template <bool b1, bool b2>
int T_funcGS(const gsl_vector* X, void* vparams, gsl_vector* f, gsl_matrix* J)
{
  detail::LLFitParams* params = reinterpret_cast<detail::LLFitParams*>(vparams);

  /* 
     parameter vector X contains the gaussian amplitudes to be fitted,
     their positions are given by the corresponding positions in real-space,
     and the sigma is given by the spatial sampling.
  */

  // for each sample point to be fitted
  // iterate over all data points (i.e. (zstar;amp,phi) pairs)
  int index=0;
  std::vector<Real> cos_term(X->size);
  std::vector<Real> sin_term(X->size);

  for(detail::IntensList::const_iterator it=params->intens.begin();
      it!=params->intens.end();++it) {

    Real pre=exp(-M_PI*M_PI*it->zstar*it->zstar*params->sampling*params->sampling);
    Real cos_sum=0.0;
    Real sin_sum=0.0;
    for(unsigned int ix=0;ix<X->size;++ix) {
      Real ex=-2.0*M_PI*it->zstar*static_cast<Real>(ix)*params->sampling;
      cos_term[ix]=cos(ex);
      sin_term[ix]=sin(ex);
      Real v=gsl_vector_get(X,ix);
      cos_sum+=v*cos_term[ix];
      sin_sum+=v*sin_term[ix];
    }

    Real pre_cos_sum = cos_sum*pre;
    Real pre_sin_sum = sin_sum*pre;

    if(b1) {
      Real fx = pre_cos_sum*pre_cos_sum+pre_sin_sum*pre_sin_sum;
      gsl_vector_set(f,index,(fx - it->intens)/it->sig_intens);
    }
    
    if(b2) {
      for(unsigned int ix=0;ix<X->size;++ix) {
	Real der = 2.0*pre*(cos_term[ix]*pre_cos_sum+sin_term[ix]*pre_sin_sum);
	gsl_matrix_set(J,index,ix,der/it->sig_intens);
      }
    }

    ++index;
  }
  return GSL_SUCCESS;
}

static int GS_func_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return T_funcGS<true,false>(x,params,f,0);
}

static int GS_func_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return T_funcGS<false,true>(x,params, 0, J);
}

static int GS_func_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return T_funcGS<true,true>(x,params, f, J);
}

} // anon ns

void LLFitAmpGaussianSum::Apply()
{
  detail::LLFitParams params = {intens_list_,phase_list_,sampling_,count_,bfac_};

  int paramN = count_;

  int valueN = intens_list_.size();

  if(paramN>=valueN) {
    LOG_DEBUG( "not enough data points, returning all zero" );
    return;
  }

  gsl_vector* X = gsl_vector_alloc(paramN);

  gsl_vector_set_all(X,1.0);

  gsl_multifit_function_fdf func;
  func.f = &GS_func_f;
  func.df = &GS_func_df;
  func.fdf = &GS_func_fdf;
  func.n = valueN;
  func.p = paramN;
  func.params = &params;

  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;

  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, valueN, paramN);
  gsl_multifit_fdfsolver_set (solver, &func, X);

  LOG_DEBUG( "commencing iterations" );
  
  unsigned int iter=0;
  int status=GSL_CONTINUE;
  // actual iterations
  Real lim1 = 1e-30;
  Real lim2 = 1e-30;
  
  while (status == GSL_CONTINUE && iter < max_iter_) {
    ++iter;
    
    gsl_multifit_fdfsolver_iterate (solver);
    
    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1, lim2);
  } 
  
  LOG_DEBUG( "done with iterations (" << iter << "): " << gsl_strerror (status) );
  
  Real chi=gsl_blas_dnrm2(solver->f);
  LOG_DEBUG( "chi = " << chi );
  LOG_DEBUG( "chisq/dog = " << (chi*chi)/static_cast<Real>(valueN-paramN) );

  std::vector<Real> result;
  
  // all discrete frequencies
  for(unsigned int p=0;p<count_;++p) {
    Real freq = 0.5*static_cast<Real>(p)/(sampling_*static_cast<Real>(count_));

    Real pre=exp(-M_PI*M_PI*freq*freq*sampling_*sampling_);
    Real cos_sum=0.0;
    Real sin_sum=0.0;

    for(unsigned int ix=0;ix<count_;++ix) {
      Real ex=-2.0*M_PI*freq*static_cast<Real>(ix)*sampling_;
      Real v=gsl_vector_get(solver->x,ix);
      cos_sum+=v*cos(ex);
      sin_sum+=v*sin(ex);
    }
    
    // final result is amp^2, must be converted to amp
    result.push_back(pre*std::sqrt(cos_sum*cos_sum+sin_sum*sin_sum));
  }
}


std::pair<Real,Real> LLFitAmpGaussianSum::CalcIntensAt(Real zstar) const
{
  return std::make_pair(0.0,0.0);
}

std::map<Real,Complex> LLFitAmpGaussianSum::GetComponents() const
{
  // TODO
  return std::map<Real,Complex>();
}

////////////////////////////////////////////////////////
// Complex Gauss Sum Fitting
////////////////////////////////////////////////////////

// not tested

LLFitAmpComplexGaussianSum::LLFitAmpComplexGaussianSum(unsigned int count, Real sampling, guessmode gm):
  LLFitBase(count,sampling,gm),
  gc_(),gv_()
{}

LLFitBasePtr LLFitAmpComplexGaussianSum::Clone()
{
    return LLFitBasePtr(new LLFitAmpComplexGaussianSum(*this));
}

namespace {

template <bool b1, bool b2>
int T_funcCGS(const gsl_vector* X, void* vparams, gsl_vector* f, gsl_matrix* J)
{
  detail::LLFitParams* params = reinterpret_cast<detail::LLFitParams*>(vparams);

  /* 
     parameter vector X contains the gaussian amplitudes and phases 
     to be fitted, otherwise the same as above gaussian fit code
  */

  // for each sample point to be fitted
  // iterate over all data points (i.e. (zstar;amp,phi) pairs)
  unsigned int NN = X->size/2;
  int index=0;
  std::vector<Real> cos_term(NN);
  std::vector<Real> sin_term(NN);

  for(detail::IntensList::const_iterator it=params->intens.begin();
      it!=params->intens.end();++it) {
    Real i_sigi = 1.0/it->sig_intens;

    //Real pre=exp(-8.0*M_PI*M_PI*it->zstar*it->zstar*params->sampling*params->sampling);
    Real pre=exp(params->bfac*it->zstar*it->zstar);
    Real cos_sum=0.0;
    Real sin_sum=0.0;
    // -N/2 to +N/2 ??
    for(unsigned int ix=0;ix<NN;++ix) {
      Real ex=-2.0*M_PI*it->zstar*static_cast<Real>(ix)*params->sampling;
      Real amp=gsl_vector_get(X,ix*2+0);
      Real phi=gsl_vector_get(X,ix*2+1);
      cos_term[ix]=cos(phi+ex);
      sin_term[ix]=sin(phi+ex);
      cos_sum+=amp*cos_term[ix];
      sin_sum+=amp*sin_term[ix];
    }

    if(b1) {
      Real fx = pre*(cos_sum*cos_sum+sin_sum*sin_sum);
      gsl_vector_set(f,index,(fx - it->intens)*i_sigi);
    }
    
    if(b2) {
      // -N/2 to +N/2 ??
      for(unsigned int ix=0;ix<NN;++ix) {
	Real derA = 2.0*pre*(cos_term[ix]*cos_sum+sin_term[ix]*sin_sum);
	gsl_matrix_set(J,index,ix*2+0,derA*i_sigi);
	Real amp=gsl_vector_get(X,ix*2+0);
	Real derF = 2.0*pre*amp*(-sin_term[ix]*cos_sum+cos_term[ix]*sin_sum);
	gsl_matrix_set(J,index,ix*2+1,derF*i_sigi);
      }
    }

    ++index;
  }
  return GSL_SUCCESS;
}

static int CGS_func_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return T_funcCGS<true,false>(x,params,f,0);
}

static int CGS_func_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return T_funcCGS<false,true>(x,params, 0, J);
}

static int CGS_func_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return T_funcCGS<true,true>(x,params, f, J);}

} // anon ns

void LLFitAmpComplexGaussianSum::Apply()
{
  detail::LLFitParams params = {intens_list_,phase_list_,sampling_,count_,bfac_};

  // each parameter consists of the amplitude and the phase
  int paramN = count_*2;

  int valueN = intens_list_.size();

  if(paramN>=valueN) {
    LOG_DEBUG( "not enough data points" );
    return;
  }

  /*
    the parameter vector contains intercalated amp/phase pairs, ie
    X[i*2+0]=amp_{i}
    X[i*2+1]=phi_{i}
  */
  gsl_vector* X = gsl_vector_alloc(paramN);

  for(unsigned int c=0;c<count_;++c) {
    gsl_vector_set(X,c*2+0,1.0);
    gsl_vector_set(X,c*2+1,0.0);
  }

  gsl_multifit_function_fdf func;
  func.f = &CGS_func_f;
  func.df = &CGS_func_df;
  func.fdf = &CGS_func_fdf;
  func.n = valueN;
  func.p = paramN;
  func.params = &params;

  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;

  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, valueN, paramN);
  gsl_multifit_fdfsolver_set (solver, &func, X);

  LOG_DEBUG( "commencing iterations" );
  
  unsigned int iter=0;
  int status=GSL_CONTINUE;
  // actual iterations
  Real lim1 = lim1_;
  Real lim2 = lim2_;
  
  while (status == GSL_CONTINUE && iter < max_iter_) {
    ++iter;
    
    gsl_multifit_fdfsolver_iterate (solver);
    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1, lim2);
  } 
  
  LOG_DEBUG( "done with iterations (" << iter << "): " << gsl_strerror (status) );
  iter_count_=iter;
  
  Real chi=gsl_blas_dnrm2(solver->f);
  LOG_DEBUG( "chi = " << chi );
  LOG_DEBUG( "chisq/dog = " << (chi*chi)/static_cast<Real>(valueN-paramN) );

  gsl_matrix* covar = gsl_matrix_alloc(paramN,paramN);
  gsl_multifit_covar(solver->J, 0.0, covar);

  gc_.clear();
  gv_.clear();

  for(unsigned int c=0;c<count_;++c) {
    Real An=gsl_vector_get(solver->x,c*2+0);
    Real Fn=gsl_vector_get(solver->x,c*2+1);
    Real sAn2=(gsl_matrix_get(covar,c*2+0,c*2+0));
    Real sFn2=(gsl_matrix_get(covar,c*2+1,c*2+1));
    gc_.push_back(Complex(An,Fn));
    gv_.push_back(Complex(sAn2,sFn2));
  }
}

std::pair<Real,Real> LLFitAmpComplexGaussianSum::CalcIntensAt(Real zstar) const
{
  //Real pre=exp(-8.0*M_PI*M_PI*zstar*zstar*sampling_*sampling_);
  Real pre=exp(bfac_*zstar*zstar);
  Real cos_sum=0.0;
  Real sin_sum=0.0;
  for(unsigned int n=0;n<gc_.size();++n) {
    Real ex=-2.0*M_PI*zstar*static_cast<Real>(n)*sampling_;
    Real An=gc_[n].real();
    Real Fn=gc_[n].imag();

    cos_sum+=An*cos(ex+Fn);
    sin_sum+=An*sin(ex+Fn);
  }

  Real icalc = pre*(cos_sum*cos_sum+sin_sum*sin_sum);
  Real scalc = icalc*0.1; // bad hack
  return std::make_pair(icalc,scalc);
}

std::map<Real,Complex> LLFitAmpComplexGaussianSum::GetComponents() const
{
  Real stepsize=1.0/(sampling_*count_);
  int offset=-static_cast<int>(gc_.size())/2;
  std::map<Real,Complex> result;
  for(unsigned int i=0;i<gc_.size();++i){
    result[stepsize*(i+offset)]=gc_[i];
  }
  return result;
}

}} // ns
