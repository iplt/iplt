//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  author: Andreas Schenk
*/

#include <ost/img/progress.hh>
#include "canny.hh"
#include "sobel.hh"
#include "hysteresis.hh"
#include "nonmaxsupressor.hh"

namespace iplt {  namespace alg { 

//! initialize with upper and lower threshold for extension by hysteresis
Canny::Canny(Real upperthreshold, Real lowerthreshold):
  ost::img::ConstModOPAlgorithm("Canny"), upperthreshold_(upperthreshold), lowerthreshold_(lowerthreshold)
{}

ost::img::ImageHandle Canny::Visit(const ost::img::ConstImageHandle& i) const
{
  Sobel sobel;
  NonMaxSupressor nms;
  Hysteresis hys(upperthreshold_,lowerthreshold_);
  ost::img::Progress::Instance().Register(this,3);
  ost::img::ImageHandle sobelout=i.Apply(sobel);
  ost::img::Progress::Instance().AdvanceProgress(this);
  ost::img::ImageHandle nmsout=sobelout.Apply(nms);
  ost::img::Progress::Instance().AdvanceProgress(this);
  ost::img::ImageHandle hysout= nmsout.Apply(hys);
  ost::img::Progress::Instance().AdvanceProgress(this);
  ost::img::Progress::Instance().DeRegister(this);
  return hysout;
}

}}//ns
