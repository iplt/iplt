//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_LLFITAMP_SINC_INTPOL_SQ_HH
#define IPLT_EX_LLFITAMP_SINC_INTPOL_SQ_HH

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include "llfit_base.hh"

namespace iplt {  namespace alg {

class DLLEXPORT_IPLT_ALG LLFitAmpSincIntpolSQ: public LLFitBase {
 public:
  LLFitAmpSincIntpolSQ(unsigned int count, Real sampling, guessmode gm=GUESSMODE_RANDOM);
  ~LLFitAmpSincIntpolSQ();

  virtual LLFitBasePtr Clone();
  virtual void Apply();

  // return icalc,sigicalc
  virtual std::pair<Real,Real> CalcIntensAt(Real zstar) const;

  virtual std::map<Real,Complex> GetComponents() const;

private:
  std::vector<Complex> sc_;
  gsl_vector* fit_X_;
  gsl_matrix* fit_V_;
  
};

}} // ns

#endif
