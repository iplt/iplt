//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Split Gauss2D fitting

  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_FIT_SPLIT_GAUSS2D_H
#define IPLT_EX_FIT_SPLIT_GAUSS2D_H

#include "split_gauss2d.hh"

#include <ost/img/algorithm.hh>
#include <ost/img/image.hh>
#include <ost/log.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

        class DLLEXPORT_IPLT_ALG FitSplitGauss2DError: public ost::Error {
public:
  FitSplitGauss2DError(const String& s):
    Error(String("FitSplitGauss2DError: ") + s) {}
};

//! 2D Gaussian fitting algorithm
/*
  unelegant partial copy/paste from FitGauss2D
*/
        class DLLEXPORT_IPLT_ALG FitSplitGauss2D: public ost::img::NonModAlgorithm, public ParamsSplitGauss2D
{
public:
  enum {
    ID_A=0,
    ID_BX, ID_BY,
    ID_UX, ID_UY,
    ID_C,
    ID_W,
    ID_PX, ID_PY,
    ID_D
  };

  FitSplitGauss2D(Real A=1.0, Real Bx=1.0, Real By=1.0, Real C=0.0, Real ux=0.0, Real uy=0.0, Real w=0.0, Real Px=0.0, Real Py=0.0,Real delta=0);

  void SetSigma(Real s) {sigma_ = s;}
  void SetMaxIter(int n) {max_iter_ = n;}
  void SetLim(Real l1, Real l2) {lim1_=l1; lim2_=l2;}

  Real GetChi() const {return chi_;}
  void SetChi(Real c) {chi_=c;}
  Real GetGOF() const {return gof_;}
  void SetGOF(Real g) {gof_=g;}
  Real GetQuality() const {return qual_;}
  void SetQuality(Real q) {qual_=q;}
  Real GetAbsQuality() const {return qual_;}
  void SetAbsQuality(Real q) {qual_=q;}
  Real GetRelQuality() const {return qual2_;}
  void SetRelQuality(Real q) {qual2_=q;}
  int GetIterationCount() const {return itcount_;}
  Real GetSigAmp() const {return sigamp_;}
  void SetSigAmp(Real sa) {sigamp_=sa;}

  // return as function
  FuncSplitGauss2D AsFunction() const;

  // alg interface
  virtual void Visit(const ost::img::ConstImageHandle& i){VisitData(i);}
  virtual void Visit(const ost::img::Function& f) {VisitData(f);}

protected:
  void VisitData(const ost::img::Data& data);

private:
  int max_iter_;
  Real chi_, gof_, qual_, qual2_, sigamp_;
  Real sigma_;
  Real lim1_, lim2_;
  int itcount_;
};


DLLEXPORT_IPLT_ALG std::ostream& operator<<(std::ostream& o, const FitSplitGauss2D& f);

}} // namespaces

#endif
