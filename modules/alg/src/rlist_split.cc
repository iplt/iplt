
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include "rlist_split.hh"

namespace iplt {  namespace alg{

RListSplitByProperty::RListSplitByProperty(const String& prop):
  ReflectionNonModAlgorithm(),
  property_(prop),
  lists_()
{
}

RListSplitByProperty::~RListSplitByProperty()
{
}

void RListSplitByProperty::Visit(const ReflectionList& rlist)
{
  lists_.clear();
  if(not rlist.HasProperty(property_)) {
    return;
  }
	for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp) {
    Real val = rp.Get(property_);
    PropertyReflectionListMap::iterator it=lists_.find(val);
    if(it==lists_.end()) {
      lists_[val]=ReflectionList(rlist,false);
      lists_[val].CopyProxyter(rp);
    } else {
      it->second.CopyProxyter(rp);
    }
	}
}

RListSplitByProperty::PropertyReflectionListMap RListSplitByProperty::GetRListMap() const
{
  return lists_;
}



RListSplitByIndex::RListSplitByIndex():
  ReflectionNonModAlgorithm(),
  lists_()
{
}

RListSplitByIndex::~RListSplitByIndex()
{
}

void RListSplitByIndex::Visit(const ReflectionList& rlist)
{
  lists_.clear();
	for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp) {
    ost::img::Point idx = rp.GetIndex().AsDuplet();
    IndexReflectionListMap::iterator it=lists_.find(idx);
    if(it==lists_.end()) {
      lists_[idx]=ReflectionList(rlist,false);
      lists_[idx].CopyProxyter(rp);
    } else {
      it->second.CopyProxyter(rp);
    }
	}
}

RListSplitByIndex::IndexReflectionListMap RListSplitByIndex::GetRListMap() const
{
  return lists_;
}

}} //ns
