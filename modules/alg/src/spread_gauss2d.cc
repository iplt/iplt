//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  Author: Ansgar Philippsen
*/

#include <cmath>
#include <boost/math/special_functions/erf.hpp>

#include <ost/img/image_state.hh>
#include <ost/message.hh>

#include "spread_gauss2d.hh"

namespace iplt {  namespace alg {

namespace {

struct SpreadGauss2DFnc {

  // required default ctor
  SpreadGauss2DFnc() {}

  SpreadGauss2DFnc(Real x, Real y, Real amp, Real sigma):
    x_(x), y_(y), amp_(amp), sigma_(sigma)
  {}

  template <typename T, class D>
  void VisitState(ost::img::ImageStateImpl<T,D>& isi) const {
    static Real lim=0.0001;

    Real xyrange=M_SQRT2*sigma_*sqrt( log(1.0/(lim*std::sqrt(2.0*M_PI)*sigma_)) );
    int xysize=2*static_cast<int>(round(xyrange))+1;
    ost::img::Extent fullext(ost::img::Size(xysize,xysize),
		   ost::img::Point(static_cast<int>(round(x_)),
			 static_cast<int>(round(y_))));

    // make sure extent is not out-of-bounds
    //if (!fullext.Contains(isi.GetExtent())) {
    //  return;
    //}
    ost::img::Extent ext=Overlap(fullext,isi.GetExtent());

    // loop over extent and add integration value to pixels
    for(ost::img::ExtentIterator it(ext);!it.AtEnd();++it) {
      ost::img::Point pnt(it);
      Real x1=static_cast<Real>(pnt[0])-x_;
      Real y1=static_cast<Real>(pnt[1])-y_;
      Real val = amp_*Integrate(x1-0.5,x1+0.5,y1-0.5,y1+0.5,sigma_);
      isi.Value(pnt)+=ost::img::Val2Val<Real,T>(val);
    }
  }

  Real Integrate(Real x1, Real x2, Real y1, Real y2, Real sigma) const
  {
    Real fac=1.0/(M_SQRT2*sigma);
    Real erf_x1 = boost::math::erf(x1*fac);
    Real erf_x2 = boost::math::erf(x2*fac);
    Real erf_y1 = boost::math::erf(y1*fac);
    Real erf_y2 = boost::math::erf(y2*fac);
    return 0.25*(erf_x1-erf_x2)*(erf_y1-erf_y2);
  }

  static String GetAlgorithmName() {return "SpreadGauss2D";}

  Real x_, y_, amp_, sigma_;
};


} // anon ns

SpreadGauss2D::SpreadGauss2D(const geom::Vec2& xy, Real amp, Real sigma):
  ost::img::ModIPAlgorithm("SpreadGauss2D"),
  x_(xy[0]), y_(xy[1]), amp_(amp), sigma_(sigma)
{}

void SpreadGauss2D::Visit(ost::img::ImageHandle& ih)
{
  ost::img::image_state::ImageStateConstModIPAlgorithm<SpreadGauss2DFnc> sg2d(x_,y_,amp_,sigma_);
  ih.ApplyIP(sg2d);
}

}} // ns
