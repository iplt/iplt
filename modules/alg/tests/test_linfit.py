#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from random import *
from ex import *

try:
    from pyx import *
except ImportError:
    print "pyx module could not be imported"
    has_pyx=False
else:
    has_pyx=True


def RanGauss(x):
  return gauss(0,x)

def plot_fit (m, b, err_x, err_y, x0, n):
  
  # Instantiation of the linear fit algorithms
  linfit=LinearFit()
  linfiterrxy=LinearFit()

  # The datalist is generated
  datalist=[]
  for number in range (0,n):
    line=[]
    x0_err=RanGauss(err_x)
    x0_corr=x0[number]+x0_err
    line.append(x0_corr)
    if err_x != 0.0:
      weight_x=1.0/(err_x*err_x)
    else:
      weight_x=1.0
    y0_num=b+x0[number]*m
    y0_err=RanGauss(err_y)
    y0_corr=y0_num+y0_err
    line.append(y0_corr)
    if err_y != 0.0:
      weight_y=1.0/(err_y*err_y);
    else:
      weight_y=1.0
    datalist.append(line) 
    linfit.AddDatapoint(x0_corr,y0_corr,weight_y) 
    linfiterrxy.AddDatapoint(x0_corr,weight_x, y0_corr,weight_y) 
    #print "Added data point: ",x0_corr,y0_corr,weight_x,weight_y

  # Linear fitting is performed
  print "Fitting with errors on y only...."
  linfit.Apply();
  m1=linfit.GetScale();
  b1=linfit.GetOffset();
  g1=linfit.GetGOF();
  q1=linfit.GetQ()
  rsq1=linfit.GetRSQ();
  print "Fitting with errors on x and y...."
  linfiterrxy.Apply();
  m2=linfiterrxy.GetScale();
  b2=linfiterrxy.GetOffset();
  g2=linfiterrxy.GetGOF();
  q2=linfiterrxy.GetQ()
  rsq2=linfiterrxy.GetRSQ();

  print "Number of points: ", n
  print "Fitting parameters:"
  print "Real: m:",m,"b: ",b
  print "Errors: err_x:",err_x,"err_y: ",err_y
  print "Error in y: m:",m1,"b:",b1,"GOF:",g1,"Q:",q1,"R squared:",rsq1
  print "Error in x and y: m:",m2,"b:",b2," GOF:",g2,"Q:",q2,"R squared:",rsq2

  if not has_pyx:
    return
  
  #The graph is drawn

  g=graph.graphxy(width=10)
  g.plot(graph.data.list(datalist,x=1,y=2),[graph.style.symbol(graph.style.symbol.cross,size=0.075)])
  g.plot(graph.data.function('y(x)=m*x+b',context=locals()),[graph.style.line([color.rgb.red,style.linewidth(0.02)])])
  g.text(0.5,5.75,'Real:    m: ' + str(m) + 'b: ' + str(b),[text.size.tiny]) 
  g.plot(graph.data.function('y(x)=m1*x+b1',context=locals()),[graph.style.line([color.rgb.green,style.linewidth(0.02)])])
  g.text(0.5,5.55,'Only Y:  m: ' + str(m1) + 'b: ' + str(b1) + 'GOF: ' + str(g1),[text.size.tiny])
  g.plot(graph.data.function('y(x)=m2*x+b2',context=locals()),[graph.style.line([color.rgb.blue,style.linewidth(0.02)])])
  g.text(0.5,5.35,'X and Y: m: ' + str(m2) + 'b: ' + str(b2) + 'GOF: ' + str(g2),[text.size.tiny])
  g.text(0.5,5.15,'Err x: ' +str(err_x) + ' Err y: '+str(err_y),[text.size.tiny]) 
  g.writeEPSfile('graph_y_'+str(err_y)+'_x_'+str(err_x)+'.eps',paperformat=document.paperformat.A4,fittosize=1)
 

def plot_fit_varx (m, b, err_x, err_y, x0,n):
  
  # Instantiation of the linear fit algorithms
  linfit=LinearFit()
  linfiterrxy=LinearFit()

  # The datalist is generated
  datalist=[]
  for number in range (0,n):
    line=[]
    x0_err=RanGauss((number/30.0)*err_x)
    x0_corr=x0[number]+x0_err
    line.append(x0_corr)
    if (number/30.0)*err_x != 0.0:
      weight_x=1.0/(((number/30.0)*err_x)*((number/30.0)*err_x))
    else:
      weight_x=1.0
    y0_num=b+x0[number]*m
    y0_err=RanGauss(err_y)
    y0_corr=y0_num+y0_err
    line.append(y0_corr)
    if err_y != 0.0:
      weight_y=1.0/(err_y*err_y);
    else:
      weight_y=1.0
    datalist.append(line) 
    linfit.AddDatapoint(x0_corr,y0_corr,weight_y) 
    linfiterrxy.AddDatapoint(x0_corr,weight_x, y0_corr,weight_y) 
    #print "Added data point: ",x0_corr,y0_corr,weight_x,weight_y

  # Linear fitting is performed
  print "Fitting with errors on y only...."
  linfit.Apply()
  m1=linfit.GetScale()
  b1=linfit.GetOffset()
  g1=linfit.GetGOF()
  q1=linfit.GetQ()
  rsq1=linfit.GetRSQ();
  print "Fitting with errors on x and y...."
  linfiterrxy.Apply()
  m2=linfiterrxy.GetScale()
  b2=linfiterrxy.GetOffset()
  g2=linfiterrxy.GetGOF()
  q2=linfiterrxy.GetQ()
  rsq2=linfit.GetRSQ();

  print "Number of points: ", n
  print "Fitting parameters:"
  print "Real: m:",m,"b: ",b
  print "Errors: err_x:",err_x,"err_y: ",err_y
  print "Error in y: m:",m1,"b:",b1,"GOF:",g1,"Q:",q1,"R squared:",rsq1
  print "Error in x and y: m:",m2,"b:",b2," GOF:",g2,"Q:",q2,"R squared:",rsq2

  if not has_pyx:
    return

  #The graph is drawn
  g=graph.graphxy(width=10)
  g.plot(graph.data.list(datalist,x=1,y=2),[graph.style.symbol(graph.style.symbol.cross,size=0.075)])
  g.plot(graph.data.function('y(x)=m*x+b',context=locals()),[graph.style.line([color.rgb.red,style.linewidth(0.02)])])
  g.text(0.5,5.75,'Real:    m: ' + str(m) + 'b: ' + str(b),[text.size.tiny]) 
  g.plot(graph.data.function('y(x)=m1*x+b1',context=locals()),[graph.style.line([color.rgb.green,style.linewidth(0.02)])])
  g.text(0.5,5.55,'Only Y:  m: ' + str(m1) + 'b: ' + str(b1) + 'GOF: ' + str(g1),[text.size.tiny])
  g.plot(graph.data.function('y(x)=m2*x+b2',context=locals()),[graph.style.line([color.rgb.blue,style.linewidth(0.02)])])
  g.text(0.5,5.35,'X and Y: m: ' + str(m2) + 'b: ' + str(b2) + 'GOF: ' + str(g2),[text.size.tiny])
  g.text(0.5,5.15,'Err x: 0->' +str(err_x) + ' Err y: '+str(err_y),[text.size.tiny]) 
  g.writeEPSfile('graph_y_'+str(err_y)+'_x_'+str(err_x)+'.eps',paperformat=document.paperformat.A4,fittosize=1)


# Main body of the program

# Sequence of values for the x variable
x0 = [0.32104781504463165, 0.41463909601008764, 0.43265159623088745, 1.972415716902568, 2.0718186695317775, 2.2857119245589432, 2.6262881866639596, 2.6748772836910275, 2.7509305880165491, 3.5124254227676266, 3.5978275193716636, 3.7685285104279931, 4.0080062088534998, 4.2618583273270341, 4.5460725093919327, 4.8055427525434888, 5.2505989201048155, 5.8178057234110794, 5.866695085928761, 5.8910751030244839, 6.2389969694743383, 6.3432717999546373, 6.4162280421007392, 7.0745575717962641, 7.357821525629876, 7.8738153636869468, 8.3070687934054241, 8.6321247395581846, 8.6540638798593754, 8.8063786215816631, 8.857672446671339, 9.0169224419791174, 9.3071726382082769, 9.733951229178599, 9.8390167986350079, 9.8722720482851543, 10.465482551782916, 10.490065911737258, 10.662987295876258, 11.840209188842106, 12.19462659571205, 12.41409192566749, 12.657818045094675, 12.914161156202686, 13.324682389601691, 13.438157921531621, 13.547109438115227, 13.846328804107612, 14.837989727846876, 15.297095823469656, 15.526860803502213, 15.718894081330745, 16.675892615143791, 17.662385048896059, 17.793143704977965, 18.807888356124579, 19.064660065544665, 19.084226704049687, 19.320495778079724, 19.377083580147712, 20.616441571343469, 20.674327796795836, 20.732033020737781, 20.892133182738778, 21.151310137538076, 21.39978436897848, 21.817904783534232, 21.94549765841699, 22.063390199872952, 22.391124012021439, 22.787431892297374, 22.946006808960341, 23.028021222855163, 23.299562577211372, 23.357777834345136, 23.547468581222475, 23.791416320198593, 23.818390651000321, 24.372757644518021, 25.038802369599054, 25.220963875025014, 25.31816316363982, 25.846315197577681, 26.181453523035799, 26.814244086820924, 26.820320139388507, 27.199956027028716, 27.409000330217818, 27.493139969030338, 27.876455889675853, 27.916030180340254, 28.105617878363166, 28.295911107095442, 28.38638183015134, 28.539786888420281, 28.597769259290384, 28.860286266405314, 29.357439633890397, 29.533943817758697, 29.89176652920424]

# number of points that should be used

n=40

# Slope and intercept

m=2.0
b=4.0

# Test Cycle

for err_y in [0.0,1.0,2.0,3.0,4.0,5.0]:
  for err_x in [0.0,1.0,2.0,3.0,4.0,5.0]:
    plot_fit_varx(m, b, err_x, err_y, x0,n)
    print '\n\n'
