//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <cmath>
#include <iplt/alg/weighted_bin.hh>

using namespace iplt;
using namespace iplt::alg;

extern const Real tolerance;

BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(wbin)
{
  WeightedBin wb(2,0.0,1.0);

  wb.Add(0.2,10.0,1.0);
  wb.Add(0.3,20.0,0.5);

  Real ave0 = (10.0*1.0+20.0*0.5)/(1.0+0.5);

  BOOST_CHECK_CLOSE(wb.GetAverage(0),ave0,tolerance);
}


BOOST_AUTO_TEST_SUITE_END()

