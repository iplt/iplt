#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2011 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import random
import unittest
import iplt

class TestAnisoScaling(unittest.TestCase):
  def test_aniso_scaling(self):
    A=0.5
    B1=1.7
    B2=-0.8
    B3=0.3
    ascale = iplt.alg.AnisoScaling()
    for y1 in range(1,20):
        for y2 in range(1,20):
            x=A+B1*y1*y1+B2*y2*y2+B3*y1*y2
            xerr = x*(random.random()-0.5)*0.2 # +- 10%
            ascale.Add(x,y1,y2,1.0/(xerr*xerr))

    ascale.Apply()
    self.assertAlmostEqual(A, ascale.GetA(),5)
    self.assertAlmostEqual(B1, ascale.GetB1(),5)
    self.assertAlmostEqual(B2, ascale.GetB2(),5)
    self.assertAlmostEqual(B3, ascale.GetB3(),5)

if __name__ == "__main__":
  try:
    unittest.main()
  except Exception, e:
    print e