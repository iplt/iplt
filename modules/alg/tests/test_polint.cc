//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Valerio Mariani
*/

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
# include <vector>
# include <iplt/alg/numerical.hh>

BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(polyint)
{
  using namespace iplt::alg::detail;

  std::vector<double> xa;
  std::vector<double> ya;
  xa.push_back(0.0); ya.push_back(-168000.0);
  xa.push_back(2.0); ya.push_back(-110808.0);
  xa.push_back(4.0); ya.push_back(-69632.0);
  xa.push_back(6.0); ya.push_back(-41160.0);
  xa.push_back(8.0); ya.push_back(-22464.0);
  xa.push_back(10.0); ya.push_back(-11000.0);
  xa.push_back(12.0);  ya.push_back(-4608.0);
  xa.push_back(14.0);  ya.push_back(-1512.0);
  xa.push_back(16.0);  ya.push_back(-320.0);
  xa.push_back(18.0);  ya.push_back(-24.0);
  xa.push_back(20.0);   ya.push_back(-0.0);
  xa.push_back(22.0);   ya.push_back(-8.0);
  xa.push_back(24.0);   ya.push_back(-192.0);
  xa.push_back(26.0);   ya.push_back(-1080.0);
  xa.push_back(28.0);   ya.push_back(-3584.0);
  xa.push_back(30.0);  ya.push_back(-9000.0);
  xa.push_back(32.0);  ya.push_back(-19008.0);
  xa.push_back(34.0);  ya.push_back(-35672.0);
  xa.push_back(36.0);  ya.push_back(-61440.0);
  xa.push_back(38.0);  ya.push_back(-99144.0);
  xa.push_back(40.0);  ya.push_back(-152000.0); 

  double x = 35 ;
  double y, dy;

  polint(xa, ya, x, y, dy);
  
  BOOST_CHECK(y == -47250.0);
}


BOOST_AUTO_TEST_SUITE_END()

