//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <iplt/alg/linfit.hh>

using namespace iplt;
using namespace iplt::alg;

extern const Real tolerance;

BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(linfit)
{
  LinearFit2D linfit2d1;
  linfit2d1.ForceOrigin(true);

  LinearFit2D linfit2d2;
  linfit2d2.ForceOrigin(false);

  Real A = 4.5;
  Real B = -3.1;
  Real C = 0.6;

  for(Real a=1.0;a<5.0;a+=1.0) {
    for(Real b=1.0;b<5.0;b+=1.0) {
      Real y = A*a+B*b;
      linfit2d1.AddDatapoint(a,b,y);
      linfit2d2.AddDatapoint(a,b,y+C);
    }
  }

  linfit2d1.Apply();
  linfit2d2.Apply();

  BOOST_CHECK_CLOSE(linfit2d1.GetScale1(),A,tolerance);
  BOOST_CHECK_CLOSE(linfit2d1.GetScale2(),B,tolerance);
  BOOST_CHECK_CLOSE(linfit2d1.GetOffset(),0.0,tolerance);

  BOOST_CHECK_CLOSE(linfit2d2.GetScale1(),A,tolerance);
  BOOST_CHECK_CLOSE(linfit2d2.GetScale2(),B,tolerance);
  BOOST_CHECK_CLOSE(linfit2d2.GetOffset(),C,tolerance);

  std::pair<Real,Real> est = linfit2d1.Estimate(2.0,3.0);
  BOOST_CHECK_CLOSE(est.first,(2.0*A+3.0*B),tolerance);
  est = linfit2d2.Estimate(2.0,3.0);
  BOOST_CHECK_CLOSE(est.first,(2.0*A+3.0*B+C),tolerance);
}


BOOST_AUTO_TEST_SUITE_END()

