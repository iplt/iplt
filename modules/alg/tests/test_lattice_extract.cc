//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <ost/img/image.hh>
#include <ost/img/alg/randomize.hh>
#include <iplt/lattice.hh>
#include <iplt/alg/lattice_extract.hh>

using namespace iplt;
using namespace iplt::alg;


BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(lattice_extract)
{
  try {
    ::ost::img::alg::Randomize rnd;
    ost::img::ImageHandle im=CreateImage(ost::img::Size(32,32));
    im.ApplyIP(rnd);
    im.SetReal(ost::img::Point(0,0),10.0);
    Lattice lat(geom::Vec2(24.0,0.0),Vec2(0.0,24.0),Vec2(0.0,0.0));
    LatticeExtract le(lat,1,4,true,true);
    im.Apply(le);
  } catch(...) {
    BOOST_FAIL("Exception encountered while applying LatticeExtract");
  }
}


BOOST_AUTO_TEST_SUITE_END()
