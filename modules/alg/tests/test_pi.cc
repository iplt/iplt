//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <ost/img/image.hh>
#include <iplt/alg/peak_integration.hh>

using namespace ost::img;
using namespace iplt::alg;

extern const Real tolerance;

BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(peak_integration)
{
  ost::img::ImageHandle im=CreateImage(ost::img::Extent(ost::img::Point(-2,-2),ost::img::Point(2,2)));

  for(ost::img::ExtentIterator it(im.GetExtent());!it.AtEnd(); ++it) {
    ost::img::Point p(it);
    im.SetReal(it, Real(9.0-(p[0]*p[0]+p[1]*p[1])));
  }
  Real inner_sum = Real(4*7.0+4*8.0+9.0);
  Real ring_sum = Real(4*1.0+8*4.0+4*5.0);
  Real bg=ring_sum/16.0;
  Real vol =inner_sum-9.0*bg;

  PeakIntegration pi(1,1);
  im.Apply(pi);
  BOOST_CHECK_CLOSE(vol,pi.GetVolume(),tolerance);
  BOOST_CHECK_CLOSE(bg,pi.GetBackground(),tolerance);
}

BOOST_AUTO_TEST_SUITE_END()
