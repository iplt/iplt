//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <iplt/lattice.hh>
#include <iplt/alg/lattice_point.hh>

using namespace iplt;
using namespace iplt::alg;

BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(lattice_point_predict)
{
  geom::Vec2 a(2.0,5.0);
  geom::Vec2 b(7.0,3.0);
  Lattice lat(a,b,Vec2(0.0,0.0));
  ost::img::Extent ext(ost::img::Point(-10,-10),ost::img::Point(10,10));
  LatticePointList lpl = PredictLatticePoints(lat,ext);

  for(LatticePointProxyter lpp = lpl.Begin();!lpp.AtEnd();++lpp) {
    ost::img::Point indx = lpp.GetIndex();
    geom::Vec2 x = static_cast<Real>(indx[0])*a+static_cast<Real>(indx[1])*b;
    BOOST_REQUIRE(ext.Contains(ost::img::Point(x)));
  }
}

BOOST_AUTO_TEST_CASE(lattice_point_proxiter)
{
  LatticePointList lpl((Lattice(geom::Vec2(1.0,0.0),Vec2(0.0,1.0),Vec2(0.0,0.0))));

  lpl.Add(ost::img::Point(1,2),LatticePoint());
  lpl.Add(ost::img::Point(3,4),LatticePoint());

  LatticePointProxyter lpp=lpl.Begin();

  BOOST_CHECK(lpp.GetIndex()==Point(1,2));
  ++lpp;
  BOOST_CHECK(lpp.GetIndex()==Point(3,4));
  ++lpp;
  BOOST_CHECK(lpp.AtEnd());

  lpp = lpl.Find(ost::img::Point(1,2));
  BOOST_CHECK(lpp.IsValid());
  lpp = lpl.Find(ost::img::Point(5,6));
  BOOST_CHECK(!lpp.IsValid());
}


BOOST_AUTO_TEST_SUITE_END()


