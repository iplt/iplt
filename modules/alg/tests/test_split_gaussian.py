#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


SetVerbosityLevel(5)

import ex.alg

fg=ex.alg.FuncSplitGauss2D(10.0)
fg.SetDelta(8.0)
fg.SetBx(5.0)
fg.SetBy(4.0)
fg.SetUx(3.9)
fg.SetUy(-2.5)
fg.SetW(33.0)
fg.SetExtent(ost::img::Extent(ost::img::Size(50,50),ost::img::Point(0,0)))

ifg=CreateImage(ost::img::Extent(ost::img::Size(50,50),ost::img::Point(0,0)))
ifg.Paste(fg)
itmp=CreateImage(ost::img::Extent(ost::img::Size(50,50),ost::img::Point(0,0)))
itmp.ApplyIP(alg.Randomize())
ifg+=2*itmp

fit=ex.alg.FitSplitGauss2D()
fit.SetDelta(10.0)

fit.SetMaxIter(1000)
fit.SetSigma(1.0)
ifg.Apply(fit)
fg2=fit.AsFunction()

print fg
print fg2

fg2.SetExtent(fg.GetExtent())

#Viewer(ifg)
#Viewer(fg,"original")
#Viewer(fg2,"fit")


