//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <ost/img/image.hh>
#include <iplt/alg/lattice_search.hh>


using namespace iplt;
using namespace ost::img;
using namespace iplt::alg;

extern boost::test_tools::predicate_result compare_Vec2(const Vec2&,const Vec2&);

BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(lattice_search)
{
  ost::img::ImageHandle img=CreateImage(Extent(Point(0,0),Point(100,100)));
  for(unsigned int i=0;i<100;i+=30){
    for(unsigned int j=0;j<100;j+=20){
      img.SetReal(Point(i,j),255);
    }
  }
  LatticeSearch ls;
  img.Apply(ls);
  Lattice lat=ls.GetLattice();
  BOOST_CHECK(compare_Vec2(lat.GetFirst(),Vec2(0,-20)));
  BOOST_CHECK(compare_Vec2(lat.GetSecond(),Vec2(30,0)));
  BOOST_CHECK(compare_Vec2(lat.GetOffset(),Vec2(30,20)));
}


BOOST_AUTO_TEST_SUITE_END()

