import sys, os, platform

if platform.machine()=='x86_64':
  sys.path.insert(0, os.getenv('DNG_ROOT')+'/lib64/iplt')
else:
  sys.path.insert(0,os.getenv('DNG_ROOT')+'/lib/iplt')
     
from ost import *
import ost

ost.SetPrefixPath(os.getenv('DNG_ROOT'))
InGUIMode=False
#!/usr/bin/env iplt

import sys
import iplt.proc.diff.diff_merge as dm

m=dm.DiffMerge(sys.argv[1:])
m.Run()
