import sip
from iplt.proc.diff.diff_manager.diff_manager import *
from ost import  gui,PushVerbosityLevel


def _InitPanels(app, panels):
  panels.AddWidgetToPool('ost.gui.PythonShell', 1)
  if not panels.Restore("iplt/ui/perspective/panels"):
    panels.AddWidget(gui.PanelPosition.BOTTOM_PANEL, app.py_shell)

def _InitIPLTNextGen():
  app=gui.GostyApp.Instance()
  app.SetAppTitle("IPLT")
  main_area=app.perspective.main_area
  _InitPanels(app, app.perspective.panels)
  app.perspective.Restore()

_InitIPLTNextGen()

PushVerbosityLevel(1)
dm = DiffManager()
dm.showMaximized()
app=gui.GostyApp.Instance()
app.perspective.main_area.AddWidget("Diffraction Manager", dm)
dm.setWindowState(dm.windowState() | Qt.WindowMaximized)
