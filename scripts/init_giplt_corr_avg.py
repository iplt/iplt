from optparse import OptionParser
from math import *
import sip
from PyQt4.QtCore import *
from PyQt4.QtGui import *
 
from ost.geom import *
import ost.gui
from ost.gui import PointlistOverlay
from ost.img import CreateImage,PointList,PeakList,Size,Extent,Point
from ost.img.alg import FFT,Conj,Stat,DFT,Fill
from ost.img.alg import HighPassFilter, LowPassFilter,GaussianHighPassFilter, GaussianLowPassFilter
from ost.img.alg import FermiHighPassFilter, FermiLowPassFilter,ButterworthHighPassFilter, ButterworthLowPassFilter
from ost.img.alg import FractionalShift,Transform,Rotate2D
from ost.io import LoadImage,SaveImage

from iplt import Lattice,TCIFData, ReciprocalUnitCell, SpatialUnitCell
from iplt.gui.inputdialog import *
from iplt.gui import LatticeOverlay,UnitCellOverlay,CTFOverlay
from iplt.alg import LatticeFilter, GaussianLatticeFilter, CTFCorrection,PeakSearch,FitGauss2D

EPSILON=1e-10

#---------------- Dialogs ------------------------------------------------
loadrawdialog=InputDialog("Raw image")
loadrawdialog.AddPath("Filename:","open")
loadrawdialog.AddFloat("Sampling (A): ",1)

micdialog=InputDialog("Microscope parameters")
micdialog.AddFloat("Cs (mm): ",0)
micdialog.AddFloat("Cc (mm): ",0)
micdialog.AddInt("Acceleration voltage (kV): ",0,1000,0)
micdialog.AddCheckBox("Overfocus: ",False)

filterdialog=InputDialog("Filter parameters")
filterdialog.AddInt("Mask radius (px): ",0,1000,1)
filterdialog.AddList("Filter type: ",['Sharp edge','Gaussian'])
filterdialog.AddFloat("Highpass threshold (A): ",0.0,1000.0,0.0)
filterdialog.AddFloat("Lowpass threshold (A): ",0.0,1000.0,0.0)
filterdialog.AddList("Filter type: ",['Sharp edge','Gaussian','Fermi','Butterworth'])

savefiltereddialog=InputDialog("Save")
savefiltereddialog.AddPath("Filename: ","save")

peaksearchdialog=InputDialog("Peak search parameters")
peaksearchdialog.AddInt("Outer window radius (px): ",0,10000)
peaksearchdialog.AddInt("Inner window radius (px): ",0,10000)
peaksearchdialog.AddFloat("Outer Window Sensitivity: ")

peakrefinedialog=InputDialog("Peak Refinement Parameter")
peakrefinedialog.AddInt("Refinement radius (px): ",0,10000)

correlation_average_dialog=InputDialog("Correlation averaging parameters")
correlation_average_dialog.AddInt("Image Size (px): ",0,10000)

savecadialog=InputDialog("Save")
savecadialog.AddPath("Filename: ","save")

symmetrizedialog=InputDialog("Symmetrize")
symmetrizedialog.AddInt("Symmetry: ",1,100,1)


#--------------------- globals --------------------------------------
image=0
corrected_image=0
fft=0
corrected_fft=0
filtered_image=0
cross_correlation=0
cross_correlation_reference=0
shifted_correlation_average=0
correlation_average=0
symmetrized_image=0

v_image=0
v_corrected_image=0
v_fft=0
v_corrected_fft=0
v_filtered_image=0
v_cross_correlation=0
v_correlation_average=0
v_shifted_correlation_average=0
v_symmetrized_image=0

ov_lattice=0
ov_ctf=0
ov_peaks=PointlistOverlay("Peaks")

spatial_unitcell=0
spatial_lattice=0
peaklist=0

#---------------------------- helper functions ----------------------------------
def Viewer(image,title=""):
  app=ost.gui.GostyApp.Instance()
  viewer=app.CreateDataViewer(image)
  app.perspective.main_area.AddWidget(title, viewer)
  return viewer

def CorrelationAverage(image,peakpostions,width,height):
  result=CreateImage(Size(width,height))
  result.SetSpatialSampling(image.GetSpatialSampling())
  imext=image.GetExtent()
  count=0
  for v in peakpostions:
    sext=Extent(Size(width,height),Point(v))
    if imext.Contains(sext.GetStart()) and imext.Contains(sext.GetEnd()):
      itmp=image.Extract(sext)
      itmp.SetSpatialOrigin(Point(0,0))
      if abs(v[0]-round(v[0]))>EPSILON or abs(v[1]-round(v[1]))>EPSILON:
        itmp.ApplyIP(FractionalShift(v[0]-round(v[0]),v[1]-round(v[1])))
      result+=itmp
      count+=1
  if count>0:
    return result/float(count)
  else:
    return result


def CreateLatticeFromUnitCell(uc,sampling):
  return Lattice(uc.GetVecA()/sampling[0],uc.GetVecB()/sampling[1])

def FilterPeaklist(peaklist,real_lattice,rel_dis):
  result=PeakList()
  for p in peaklist:
    comp=real_lattice.CalcComponents(p)
    dx=abs(comp[0]-round(comp[0]))
    dy=abs(comp[1]-round(comp[1]))
    if rel_dis*rel_dis>dx*dx+dy*dy:
      result.append(p)
  return result

def CrossCorrelate(im,ref):
  imfft=im.Apply(DFT())
  reffft=ref.Apply(DFT())
  reffft.ApplyIP(Conj())
  result=imfft*reffft
  result.ApplyIP(DFT())
  return result

def Symmetrize(im,sym):
    result=im.Copy()
    for i in range(1,sym):
      result+=im.Apply(Transform(Rotate2D(2*pi*float(i)/float(sym))))
    result/=sym
    return result

#------------------------- Step callbacks -------------------------------------
def LoadRawImage():
  global image,fft,v_image,v_fft,ov_lattice
  if loadrawdialog.exec_():
    data=loadrawdialog.GetData()
    image=LoadImage(data[0])
    image.CenterSpatialOrigin()
    image.SetPixelSampling(data[1])
    v_image=Viewer(image)
    v_image.SetName(data[0])
    fft=image.Apply(FFT())
    v_fft=Viewer(fft)
    v_fft.SetName('Uncorrected Fourier transform')
    lattice=Lattice(Vec2(100,0),Vec2(0,100)) 
    ov_lattice=LatticeOverlay(lattice)
    ov_lattice.SetSymbolSize(8)
    v_fft.AddOverlay(ov_lattice)
    return True
  else:
    return False


def CTFFit():
  global ov_ctf
  if micdialog.exec_():
    data=micdialog.GetData()
    td=TCIFData()
    td.SetAccelerationVoltage(data[2]*Units.kV)
    td.SetSphericalAberration(data[0]*Units.mm)
    td.SetChromaticAberration(data[1]*Units.mm)
    if data[3]==True:
      td.SetDefocusX(-5000.0*Units.A)
      td.SetDefocusY(-5000.0*Units.A)
    ov_ctf=CTFOverlay(td)
    v_fft.AddOverlay(ov_ctf)
    return True
  else:
    return False

def CTFCorrect():
  global corrected_image, v_corrected_image, corrected_fft,v_corrected_fft,fft,image
  t = ov_ctf.GetTCIFData()
  corr = CTFCorrection(t)
  corrected_fft=fft.Apply(corr)
  corrected_image = corrected_fft.Apply(FFT())        
  v_corrected_image=Viewer(corrected_image,'CTF corrected image')
  v_corrected_fft=Viewer(corrected_fft,'CTF corrected FFT')
  if options.lowmem_flag:
    image=0
    fft=0
  return True

def FourierFilter():
  global filtered_image,v_filtered_image,spatial_unitcell,image,fft,corrected_fft
  
  if filterdialog.exec_():
    data=filterdialog.GetData()
    if data[1]=='Sharp edge':
     lattice_filter=LatticeFilter(ov_lattice.GetLattice() ,data[0])
    else:
     lattice_filter=GaussianLatticeFilter(ov_lattice.GetLattice() ,data[0])
    if corrected_fft!=0:
      filtered_fft=corrected_fft.Apply(lattice_filter)
    else:
      filtered_fft=fft.Apply(lattice_filter)
    filtered_image=filtered_fft.Apply(FFT()) 
    v_filtered_image=Viewer(filtered_image)
    v_filtered_image.SetName('Fourier peak filtered image')
    if data[4]=='Sharp edge':
      highpassfilter=HighPassFilter(data[2])
      lowpassfilter=LowPassFilter(data[3])
    elif data[4]=='Gaussian':
      highpassfilter=GaussianHighPassFilter(data[2])
      lowpassfilter=GaussianLowPassFilter(data[3])
    elif data[4]=='Fermi':
      idialog=InputDialog("Fermi")
      idialog.AddFloat("Temp Factor: ",0)
      if idialog.exec_():
        data2=idialog.GetData()
	print data[2],data2[0]
        highpassfilter=FermiHighPassFilter(data[2],data2[0])
        lowpassfilter=FermiLowPassFilter(data[3],data2[0])
      else:
        return False
    else:
      idialog=InputDialog("Butterworth")
      idialog.AddFloat("Width: ",0)
      if idialog.exec_():
        data2=idialog.GetData()
        highpassfilter=ButterworthHighPassFilter(data[2]-data2[0],data[2])
        lowpassfilter=ButterworthLowPassFilter(data[3],data[3]+data2[0])
      else:
        return False
    filtered_image.ApplyIP(highpassfilter)
    filtered_image.ApplyIP(lowpassfilter)
    if fft==0:
      reciprocal_unitcell=ReciprocalUnitCell(ov_lattice.GetLattice() ,corrected_fft.GetPixelSampling())
    else:
      reciprocal_unitcell=ReciprocalUnitCell(ov_lattice.GetLattice() ,fft.GetPixelSampling())
    spatial_unitcell=SpatialUnitCell(reciprocal_unitcell)
    print "Unit Cell:"+str(spatial_unitcell)
    uc_overlay=UnitCellOverlay(spatial_unitcell)
    v_filtered_image.AddOverlay(uc_overlay)
    if options.lowmem_flag:
      image=0
      fft=0
      corrected_fft=0
    return True
  else:
    return False


def SaveFiltered():
  if savefiltereddialog.exec_():
    data=savefiltereddialog.GetData()
    SaveImage(filtered_image,data[0])
    return True
  else:
    return False

def CreateReference():
  global cross_correlation_reference,spatial_lattice
  selection_small=filtered_image.Extract(v_filtered_image.GetSelection())
  origin=selection_small.GetSpatialOrigin()
  selection_small.CenterSpatialOrigin()
  shift=origin-selection_small.GetSpatialOrigin()
  spatial_lattice=CreateLatticeFromUnitCell(spatial_unitcell,filtered_image.GetSpatialSampling())
  spatial_lattice.SetOffset(shift.ToVec2())
  print "Lattice: "+str(spatial_lattice)
  s=Stat()
  selection_small.Apply(s)
  cross_correlation_reference=CreateImage(filtered_image.GetExtent())
  cross_correlation_reference.ApplyIP(Fill(s.GetMean()))
  cross_correlation_reference.CenterSpatialOrigin()
  cross_correlation_reference.Paste(selection_small)
  return True

def CrossCorrelateAndDisp():
  global cross_correlation,v_cross_correlation,cross_correlation_reference
  if corrected_image!=0:
    cross_correlation=CrossCorrelate(corrected_image,cross_correlation_reference)
  else:
    cross_correlation=CrossCorrelate(image,cross_correlation_reference)
  v_cross_correlation=Viewer(cross_correlation,'Cross correlation')
  ov_peaks.Clear()
  v_cross_correlation.AddOverlay(ov_peaks)
  if options.lowmem_flag:
    cross_correlation_reference=0
  return True

def DoPeakSearch():
  global peaklist,vcc2,ov_ps
  if peaksearchdialog.exec_():
    data=peaksearchdialog.GetData()
    peaksearch=PeakSearch(data[0],data[2],data[1])
    cross_correlation.Apply(peaksearch)
    peaklist=peaksearch.GetPeakList()
    ov_peaks.Clear()
    ov_peaks.Add(PointList(peaklist))
    v_cross_correlation.GetOverlayManager().ActivateOverlay("Peaks")
    print "Number of peaks found: "+str(len(peaklist))
    return True
  else:
    return False
    
    
def PeakRefine():
  global peaklist
  if peakrefinedialog.exec_():
    radius=peakrefinedialog.GetData()[0]
    rpl = []
    for p in peaklist:
      gauss = FitGauss2D()
      gauss.SetUx(p.ToVec2()[0])
      gauss.SetUy(p.ToVec2()[1])
      gauss.SetA(cross_correlation.GetReal(p))
      img=cross_correlation.Extract(Extent(Point(p[0]-radius,p[1]-radius),Point(p[0]+radius,p[1]+radius)))
      img.Apply(gauss)
      if( (abs(gauss.GetU()[0]-p[0]) <= radius) & (abs(gauss.GetU()[1]-p[1]) <= radius ) ):
        rpl.append(gauss.GetU())
      else:
        rpl.append(p)
    peaklist = rpl
    return True
  else:
    return False
      

def CorrelationAverageAndDisp():
  global correlation_average,v_correlation_average,uc_overlay2
  if correlation_average_dialog.exec_():
    imsize=correlation_average_dialog.GetData()[0]
    if corrected_image!=0:
      correlation_average=CorrelationAverage(corrected_image,peaklist,imsize,imsize)
    else:
      correlation_average=CorrelationAverage(image,peaklist,imsize,imsize)
    correlation_average.CenterSpatialOrigin()
    v_correlation_average=Viewer(correlation_average)
    v_correlation_average.SetName('Correlation average')
    uc_overlay2=UnitCellOverlay(spatial_unitcell)
    v_correlation_average.AddOverlay(uc_overlay2)
    return True
  else:
    return False

def ShiftOrigin():
  global shifted_correlation_average,v_shifted_correlation_average
  avec=Vec3(spatial_unitcell.GetVecA())
  bvec=Vec3(spatial_unitcell.GetVecB())
  phase_shift=Vec3(uc_overlay2.GetPhaseShift())
  pixel_shift=CompDivide((phase_shift[0]/(2.0*pi)*avec+phase_shift[1]/(2.0*pi)*bvec),correlation_average.GetSpatialSampling())
  shifted_correlation_average=correlation_average.Apply(FractionalShift(-pixel_shift))
  v_shifted_correlation_average=Viewer(shifted_correlation_average,'Shifted correlation average')
  return True

def DoSymmetrize():
  global symmetrized_image,v_symmetrized_image
  if symmetrizedialog.exec_():
    sym=symmetrizedialog.GetData()[0]
    symmetrized_image=Symmetrize(shifted_correlation_average,sym)
    v_symmetrized_image=Viewer(symmetrized_image)
    return True
  else:
    return False

def SaveCorrelationAverage():
  if savecadialog.exec_():
    SaveImage(symmetrized_image,savecadialog.GetData()[0])
    return True
  else:
    return False
    
def SetReference():
  global cross_correlation_reference
  s=Stat()
  correlation_average.Apply(s)
  cross_correlation_reference=CreateImage(image.GetExtent())
  cross_correlation_reference.ApplyIP(Fill(s.GetMean()))
  cross_correlation_reference.CenterSpatialOrigin()
  cross_correlation_reference.Paste(correlation_average)
  return True




def _InitPanels(app, panels):
 global dock, dockwidget
 window=app.perspective.GetMenu("Window")
 window.addMenu(app.perspective.panels.menu)
 panels.AddWidgetToPool('ost.gui.PythonShell', 1)
 dock = QWidget()
 vb=QVBoxLayout()
 pb=QPushButton('Load raw image')
 QObject.connect(pb,SIGNAL("clicked()"),LoadRawImage)
 vb.addWidget(pb)
 pb=QPushButton('Fit CTF')
 QObject.connect(pb,SIGNAL("clicked()"),CTFFit)
 vb.addWidget(pb)
 pb=QPushButton('Correct CTF')
 QObject.connect(pb,SIGNAL("clicked()"),CTFCorrect)
 vb.addWidget(pb)
 pb=QPushButton('Fourier filter')
 QObject.connect(pb,SIGNAL("clicked()"),FourierFilter)
 vb.addWidget(pb)
 pb=QPushButton('Save filtered image')
 QObject.connect(pb,SIGNAL("clicked()"),SaveFiltered)
 vb.addWidget(pb)
 pb=QPushButton('Create reference')
 QObject.connect(pb,SIGNAL("clicked()"),CreateReference)
 vb.addWidget(pb)
 pb=QPushButton('Cross correlate')
 QObject.connect(pb,SIGNAL("clicked()"),CrossCorrelateAndDisp)
 vb.addWidget(pb)
 pb=QPushButton('Peak search')
 QObject.connect(pb,SIGNAL("clicked()"),DoPeakSearch)
 vb.addWidget(pb)
 pb=QPushButton('Refine peaks')
 QObject.connect(pb,SIGNAL("clicked()"),PeakRefine)
 vb.addWidget(pb)
 pb=QPushButton('Correlation average')
 QObject.connect(pb,SIGNAL("clicked()"),CorrelationAverageAndDisp)
 vb.addWidget(pb)
 pb=QPushButton('Shift origin')
 QObject.connect(pb,SIGNAL("clicked()"),ShiftOrigin)
 vb.addWidget(pb)
 pb=QPushButton('Symmetrize')
 QObject.connect(pb,SIGNAL("clicked()"),DoSymmetrize)
 vb.addWidget(pb)
 pb=QPushButton('Save symmetrized average')
 QObject.connect(pb,SIGNAL("clicked()"),SaveCorrelationAverage)
 vb.addWidget(pb)
 pb=QPushButton('Set average as reference')
 QObject.connect(pb,SIGNAL("clicked()"),SetReference)
 vb.addWidget(pb)
 dock.setLayout(vb)
 dockwidget=ost.gui.Widget(dock)
 panels.AddWidgetToPool("Steps",dockwidget)
 if not panels.Restore("giplt_corr_avg/ui/perspective/panels"):
   panels.AddWidget(ost.gui.PanelPosition.LEFT_PANEL, dockwidget)
   dock.show()
 


def _InitCorrAvg():
  global options,args
  app=ost.gui.GostyApp.Instance()
  app.SetAppTitle("IPLT - Iplt Next Generation: Correlation Averaging")
  main_area=app.perspective.main_area
  _InitPanels(app, app.perspective.panels)
  app.perspective.Restore()
  parser=OptionParser()
  parser.add_option("-l", "--lowmem",action="store_true",default=False,dest="lowmem_flag",
                    help='delete unneeded images to preserve memory')
  (options,args) = parser.parse_args()         


#------------------------------ main ---------------------------------------

_InitCorrAvg()


