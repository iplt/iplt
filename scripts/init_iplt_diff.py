import sys, os, platform

if platform.machine()=='x86_64':
  sys.path.insert(0, os.getenv('DNG_ROOT')+'/lib64/iplt')
else:
  sys.path.insert(0,os.getenv('DNG_ROOT')+'/lib/iplt')
     
from ost import *
import ost

ost.SetPrefixPath(os.getenv('DNG_ROOT'))
InGUIMode=False


from iplt.proc.diff.diff_command import *

dc=DiffCommand()
dc.Run()
